<?
return array (
	'default' => array('controller' => 'adm_index', 'action' => 'page404'),
	'/admin/' => array('controller' => 'adm_index', 'action' => 'index', 'rules' => array(
		'login/' => array('controller' => 'adm_index', 'action' => 'login'),
		'logout/' => array('controller' => 'adm_index', 'action' => 'logout'),
		'setts/' => array('controller' => 'adm_setts', 'action' => 'index'),
		'admins/' => array('controller' => 'adm_admins', 'action' => 'index', 'rules' => array(
			'add/' => array('controller' => 'adm_admins', 'action' => 'add'),
			'edit/(\d+)/' => array('controller' => 'adm_admins', 'action' => 'edit', 'params' => array('id'=>1)),
			'delete/(\d+)/' => array('controller' => 'adm_admins', 'action' => 'delete', 'params' => array('id'=>1))
		)),
		'pages/' => array('controller' => 'adm_pages', 'action' => 'index', 'rules' => array(
			'add/' => array('controller' => 'adm_pages', 'action' => 'add'),
			'edit/(\d+)/' => array('controller' => 'adm_pages', 'action' => 'edit', 'params' => array('id'=>1)),
			'delete/(\d+)/' => array('controller' => 'adm_pages', 'action' => 'delete', 'params' => array('id'=>1)),
			'contentlist/' => array('controller' => 'adm_pages', 'action' => 'contentlist')
		)),
		'cats/' => array('controller' => 'adm_cats', 'action' => 'index', 'rules' => array(
			'add/' => array('controller' => 'adm_cats', 'action' => 'add'),
			'edit/(\d+)/' => array('controller' => 'adm_cats', 'action' => 'edit', 'params' => array('id' => 1)),
			'delete/(\d+)/' => array('controller' => 'adm_cats', 'action' => 'delete', 'params' => array('id' => 1)),
			'switch/(\d+)/' => array('controller' => 'adm_cats', 'action' => 'switchstatus', 'params' => array('id' => 1))
		)),
		'items/' => array('controller' => 'adm_items', 'action' => 'index', 'rules' => array(
			'(\d+)/' => array('controller' => 'adm_items', 'action' => 'index', 'params' => array('cat_id' => 1), 'rules' => array(
				'add/' => array('controller' => 'adm_items', 'action' => 'add'),
				'copy/(\d+)/' => array('controller' => 'adm_items', 'action' => 'add', 'params' => array('id' => 1)),
				'edit/(\d+)/' => array('controller' => 'adm_items', 'action' => 'edit', 'params' => array('id' => 1)),
				'delete/(\d+)/' => array('controller' => 'adm_items', 'action' => 'delete', 'params' => array('id' => 1)),
				'switch/(\d+)/' => array('controller' => 'adm_items', 'action' => 'switchstatus', 'params' => array('id'=>1))
			)),
			'articulautocomplete/' => array('controller' => 'adm_items', 'action' => 'articulautocomplete')
		)),
		'kititems/' => array('controller' => 'adm_kititems', 'action' => 'index', 'rules' => array(
			'add/' => array('controller' => 'adm_kititems', 'action' => 'add'),
			'edit/(\d+)/' => array('controller' => 'adm_kititems', 'action' => 'edit', 'params' => array('id' => 1)),
			'delete/(\d+)/' => array('controller' => 'adm_kititems', 'action' => 'delete', 'params' => array('id' => 1)),
			'switch/(\d+)/' => array('controller' => 'adm_kititems', 'action' => 'switchstatus', 'params' => array('id'=>1))
		)),
		'constructoritems/' => array('controller' => 'adm_constructoritems', 'action' => 'index', 'rules' => array(
			'(\d+)/' => array('controller' => 'adm_constructoritems', 'action' => 'index', 'params' => array('type' => 1), 'rules' => array(
				'add/' => array('controller' => 'adm_constructoritems', 'action' => 'add'),
				'edit/(\d+)/' => array('controller' => 'adm_constructoritems', 'action' => 'edit', 'params' => array('id' => 1)),
				'delete/(\d+)/' => array('controller' => 'adm_constructoritems', 'action' => 'delete', 'params' => array('id' => 1)),
				'switch/(\d+)/' => array('controller' => 'adm_constructoritems', 'action' => 'switchstatus', 'params' => array('id'=>1))
			))
		)),
		'hardwareitems/' => array('controller' => 'adm_hardwareitems', 'action' => 'index', 'rules' => array(
			'(\d+)/' => array('controller' => 'adm_hardwareitems', 'action' => 'index', 'params' => array('type' => 1), 'rules' => array(
				'add/' => array('controller' => 'adm_hardwareitems', 'action' => 'add'),
				'edit/(\d+)/' => array('controller' => 'adm_hardwareitems', 'action' => 'edit', 'params' => array('id' => 1)),
				'delete/(\d+)/' => array('controller' => 'adm_hardwareitems', 'action' => 'delete', 'params' => array('id' => 1)),
				'switch/(\d+)/' => array('controller' => 'adm_hardwareitems', 'action' => 'switchstatus', 'params' => array('id'=>1))
			))
		)),
		'gallery/' => array('controller' => 'adm_gallery', 'action' => 'index', 'rules' => array(
			'add/' => array('controller' => 'adm_gallery', 'action' => 'add'),
			'edit/(\d+)/' => array('controller' => 'adm_gallery', 'action' => 'edit', 'params' => array('id' => 1)),
			'delete/(\d+)/' => array('controller' => 'adm_gallery', 'action' => 'delete', 'params' => array('id' => 1)),
			'switch/(\d+)/' => array('controller' => 'adm_gallery', 'action' => 'switchstatus', 'params' => array('id'=>1))
		)),
		'itemgallery/(\d+)/' => array('controller' => 'adm_itemgallery', 'action' => 'index', 'params' => array('item_id' => 1), 'rules' => array(
			'add/' => array('controller' => 'adm_itemgallery', 'action' => 'add'),
			'edit/(\d+)/' => array('controller' => 'adm_itemgallery', 'action' => 'edit', 'params' => array('id' => 1)),
			'delete/(\d+)/' => array('controller' => 'adm_itemgallery', 'action' => 'delete', 'params' => array('id' => 1)),
			'switch/(\d+)/' => array('controller' => 'adm_itemgallery', 'action' => 'switchstatus', 'params' => array('id' => 1)),
			'main/(\d+)/' => array('controller' => 'adm_itemgallery', 'action' => 'switchmain', 'params' => array('id' => 1))
		)),
		'itemcolors/(\d+)/' => array('controller' => 'adm_itemcolors', 'action' => 'index', 'params' => array('item_id' => 1), 'rules' => array(
			'add/' => array('controller' => 'adm_itemcolors', 'action' => 'add'),
			'edit/(\d+)/' => array('controller' => 'adm_itemcolors', 'action' => 'edit', 'params' => array('id' => 1)),
			'delete/(\d+)/' => array('controller' => 'adm_itemcolors', 'action' => 'delete', 'params' => array('id' => 1)),
			'switch/(\d+)/' => array('controller' => 'adm_itemcolors', 'action' => 'switchstatus', 'params' => array('id' => 1)),
			'main/(\d+)/' => array('controller' => 'adm_itemcolors', 'action' => 'switchmain', 'params' => array('id' => 1))
		)),
		'slider/' => array('controller' => 'adm_slider', 'action' => 'index', 'rules' => array(
			'add/' => array('controller' => 'adm_slider', 'action' => 'add'),
			'edit/(\d+)/' => array('controller' => 'adm_slider', 'action' => 'edit', 'params' => array('id' => 1)),
			'delete/(\d+)/' => array('controller' => 'adm_slider', 'action' => 'delete', 'params' => array('id' => 1)),
			'switch/(\d+)/' => array('controller' => 'adm_slider', 'action' => 'switchstatus', 'params' => array('id'=>1))
		)),
		'topmenu/' => array('controller' => 'adm_topmenu', 'action' => 'index', 'rules' => array(
			'add/' => array('controller' => 'adm_topmenu', 'action' => 'add'),
			'edit/(\d+)/' => array('controller' => 'adm_topmenu', 'action' => 'edit', 'params' => array('id' => 1)),
			'delete/(\d+)/' => array('controller' => 'adm_topmenu', 'action' => 'delete', 'params' => array('id' => 1)),
			'switch/(\d+)/' => array('controller' => 'adm_topmenu', 'action' => 'switchstatus', 'params' => array('id'=>1))
		)),
		'users/' => array('controller' => 'adm_users', 'action' => 'index', 'rules' => array(
			'add/' => array('controller' => 'adm_users', 'action' => 'add'),
			'edit/(\d+)/' => array('controller' => 'adm_users', 'action' => 'edit', 'params' => array('id' => 1)),
			'delete/(\d+)/' => array('controller' => 'adm_users', 'action' => 'delete', 'params' => array('id' => 1))
		))
	))
);

?>
