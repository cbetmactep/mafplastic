<?

require_once($_SERVER['DOCUMENT_ROOT'].'/_includes/start.inc.php');

CDebug::AddLog('Starting application');

try {
	$cDispatcher = new CDispatcher($_SERVER['REQUEST_URI']);
	$cDispatcher->SetTemplatePath('../_templates/backend/');
	if (!CAdmin::getInstance()->isLogged() && $_SERVER['REQUEST_URI'] != '/admin/login/') $cDispatcher->Redirect('/admin/login/');
	$cDispatcher->Render();
} catch (Exception $e) {
	echo CDebug::PrintLog();
}

CDebug::AddLog('Finishing application');

?>