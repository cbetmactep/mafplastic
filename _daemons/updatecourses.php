<?

initLog();
writeLog('start courses update');

$sLocalDir = dirname(__FILE__);
require_once($sLocalDir.'/../_includes/start.inc.php');

if (date('d.m.Y') !== date('d.m.Y', strtotime(CSetts::getInstance()->currency_updated))) {
	$aCurrencies = CCurrencies::getInstance()->GetCourses();

	$aSetts = array(
		'currency_updated' => date('d.m.Y H:i:s'),
		'currency_usd' => $aCurrencies['usd'],
		'currency_eur' => $aCurrencies['eur']
	);
	CSetts::getInstance()->Save($aSetts);

	writeLog('finish');
} else {
	writeLog('already updated');
}

function writeLog($message)
{
	$message = date('d.m.Y H:i:s') . ' ' . $message;
	file_put_contents(realpath(dirname(__FILE__)) . '/log1.txt', $message . "\r\n", FILE_APPEND);
}

function initLog()
{
	file_put_contents(realpath(dirname(__FILE__)) . '/log1.txt', '');
}

?>