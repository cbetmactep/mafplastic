<?

initLog();
writeLog('start net params');

$sLocalDir = dirname(__FILE__);
require_once($sLocalDir.'/../_includes/start.inc.php');

$cFtp = @ftp_connect('81.24.125.70', 3763);
if (empty($cFtp)) {
	writeLog('ftp open error');
	die();
}
if (!@ftp_login($cFtp, 'zaglushkaFTP', 'CLEb}w!')) {
	writeLog('ftp auth error');
	ftp_close($cFtp);
	die();
}
if (!@ftp_pasv($cFtp, true)) {
	writeLog('ftp set passive mode error');
	ftp_close($cFtp);
	die();
}
if (!@ftp_get($cFtp, $sLocalDir.'/maf.xml', './maf.xml', FTP_BINARY)) {
	writeLog('ftp download error');
	ftp_close($cFtp);
	die();
}

$cXml = simplexml_load_file($sLocalDir.'/maf.xml');

if (!count($cXml->Nomenklatura)) {
	writeLog('nothing found in xml file');
	ftp_close($cFtp);
	die();
}

foreach ($cXml->Nomenklatura as $cItem) {
	$sArticul = (string)$cItem->Attributes()->Art;
	$fPrice = (float)str_replace(',', '.', preg_replace('/[^\d,]/is', '', (string)$cItem->Attributes()->price));
	$fCoeff = (float)str_replace(',', '.', preg_replace('/[^\d,]/is', '', (string)$cItem->Attributes()->k));
	$sCurrency = (string)$cItem->Attributes()->currency;
	if (empty($sArticul)) {
		continue;
	}

	switch ($sCurrency) {
		default:
			$iCurrency = CCurrencies::RUR;
			break;
		case 'EUR':
			$iCurrency = CCurrencies::EUR;
			break;
		case 'USD':
			$iCurrency = CCurrencies::USD;
			break;
	}

	$aItem = CKitItems::getInstance()->GetItemByArticul($sArticul);
	if (empty($aItem['id'])) {
		$aItem = CHardwareItems::getInstance()->GetItemByArticul($sArticul);
		if (empty($aItem['id'])) {
			$aItem = CItems::getInstance()->GetItemByArticul($sArticul);
			if (empty($aItem['id'])) {
				$aItem = CItemColors::getInstance()->GetItemByArticul($sArticul);
				if (empty($aItem['item_id'])) {
					writeLog('not found, articul '.$sArticul);
				} else {
					if (!CItems::getInstance()->UpdateNetPrice($aItem['item_id'], ceil($fPrice * $fCoeff * 100) / 100, $iCurrency)) {
						writeLog('update error, articul '.$sArticul);
					}
				}
			} else {
				if (!CItems::getInstance()->UpdateNetPrice($aItem['id'], ceil($fPrice * $fCoeff * 100) / 100, $iCurrency)) {
					writeLog('update error, articul '.$sArticul);
				}
			}
		} else {
			if (!CHardwareItems::getInstance()->UpdatePriceByArticul($sArticul, $fPrice)) {
				writeLog('update error, articul '.$sArticul);
			}			
		}
	} else {
		if (!CKitItems::getInstance()->UpdatePrice($aItem['id'], $fPrice)) {
			writeLog('update error, articul '.$sArticul);
		}
	}
}

$aSetts = array();
if ($cXml->DopRekvizit) {
	$aSetts['net_price_margin'] = (float)str_replace(',', '.', preg_replace('/[^\d,]/is', '', $cXml->DopRekvizit->Attributes()->ProcentRozn));
	$aSetts['net_price_margin'] += 1;
	$aSetts['net_price_work'] = (float)str_replace(',', '.', preg_replace('/[^\d,]/is', '', $cXml->DopRekvizit->Attributes()->Costwork));
}
if ($cXml->DopRekvizit->DopRashod) {
	foreach ($cXml->DopRekvizit->DopRashod as $cItem) {
		$sName = (string)$cItem->Attributes()->Statya;
		$fPrice = (float)str_replace(',', '.', preg_replace('/[^\d,]/is', '', (string)$cItem->Attributes()->Sum));
		switch ($sName) {
			case 'Амортизация':
				$aSetts['net_price_wearout'] = $fPrice;
				break;
			case 'Аренда':
				$aSetts['net_price_rent'] = $fPrice;
				break;
			case 'Коммунальные':
				$aSetts['net_price_utilities'] = $fPrice;
				break;
		}
	}
}
CSetts::getInstance()->Save($aSetts);

/*if (!@ftp_put($cFtp, './log_mafplastic.txt', $sLocalDir.'/log.txt', FTP_BINARY)) {
	writeLog('ftp upload error');
	ftp_close($cFtp);
	die();
}
ftp_close($cFtp);*/

writeLog('finish');

function writeLog($message)
{
	$message = date('d.m.Y H:i:s') . ' ' . $message;
	file_put_contents(realpath(dirname(__FILE__)) . '/log.txt', $message . "\r\n", FILE_APPEND);
}

function initLog()
{
	file_put_contents(realpath(dirname(__FILE__)) . '/log.txt', '');
}

?>
