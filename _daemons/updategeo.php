<?

initLog();
writeLog('start', true);

$sLocalDir = dirname(__FILE__);
require_once($sLocalDir.'/../_includes/start.inc.php');

exec('wget -qN "http://ipgeobase.ru/files/db/Main/geo_files.tar.gz" > /dev/null', $aReturn, $iReturn);
if ($iReturn) {
	writeLog('Can\'t get geo files');
	die();
}
exec('tar -xzf "geo_files.tar.gz" > /dev/null', $aReturn, $iReturn);
if ($iReturn) {
	writeLog('Can\'t extract geo files');
	die();
}
exec('iconv -f cp1251 -t utf-8 "cities.txt" > "cities_utf8.txt"', $aReturn, $iReturn);
if ($iReturn) {
	writeLog('Can\'t convert geo files');
	die();
}

if (!CGeo::getInstance()->CreateTmpTables()) {
	writeLog('Can\'t create tmp tables');
	die();
}

$oFP = fopen('cidr_optim.txt', 'r');
if (!$oFP) {
	writeLog('Can\'t read ips file');
	die();
}

writeLog('Start processing ips file');
while (($sLine = fgets($oFP)) !== false) {
	$aLine = array_map('trim', explode("\t", $sLine));
	if ($aLine[3] != 'RU') {
		continue;
	}
	//$aLine[0] = sprintf('%u', (int)$aLine[0]);
	//$aLine[1] = sprintf('%u', (int)$aLine[1]);
	$aLine[4] = intval($aLine[4]);
	if (!$aLine[0] || !$aLine[1] || !$aLine[4]) {
		writeLog('Error in data '.$aLine[0].' '.$aLine[1].' '.$aLine[4]);
		continue;
	}
	if (!CGeo::getInstance()->AddBlock($aLine[0], $aLine[1], $aLine[4])) {
		writeLog('Can\'t insert ips '.$aLine[0].' '.$aLine[1].' '.$aLine[4]);
		die();
	}
}
writeLog('Finish processing ips file');

if (!CGeo::getInstance()->CommitTables()) {
	writeLog('Can\'t commit changes');
	die();
}

$oFP = fopen('cities_utf8.txt', 'r');
if (!$oFP) {
	writeLog('Can\'t read cities file');
	die();
}

writeLog('Start processing cities file');
while (($sLine = fgets($oFP)) !== false) {
	$aLine = array_map('trim', explode("\t", $sLine));
	$aLine[0] = intval($aLine[0]);
	if (!$aLine[0]) {
		writeLog('Error in data '.$aLine[0].' '.$aLine[1].' '.$aLine[2].' '.$aLine[3]);
		continue;
	}
	if (!CGeo::getInstance()->AddCity($aLine[0], $aLine[1], $aLine[2], $aLine[3])) {
		writeLog('Can\'t insert city '.$aLine[0].' '.$aLine[1].' '.$aLine[2].' '.$aLine[3]);
		die();
	}
}
writeLog('Finish processing cities file');

writeLog('Finish');

//@unlink('geo_files.tar.gz');
@unlink('cidr_optim.txt');
@unlink('cities.txt');
@unlink('cities_utf8.txt');

function writeLog($message)
{
	$message = date('d.m.Y H:i:s') . ' ' . $message;
	file_put_contents(realpath(dirname(__FILE__)) . '/log_geo.txt', $message . "\r\n", FILE_APPEND);
}

function initLog()
{
	file_put_contents(realpath(dirname(__FILE__)) . '/log_geo.txt', '');
}

?>