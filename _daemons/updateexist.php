<?

initLog();
writeLog('start update exist');

$sLocalDir = dirname(__FILE__);
require_once($sLocalDir.'/../_includes/start.inc.php');

$cFtp = @ftp_connect('81.24.125.70', 3763);
if (empty($cFtp)) {
	writeLog('ftp open error');
	die();
}
if (!@ftp_login($cFtp, 'zaglushkaFTP', 'CLEb}w!')) {
	writeLog('ftp auth error');
	ftp_close($cFtp);
	die();
}
if (!@ftp_pasv($cFtp, true)) {
	writeLog('ftp set passive mode error');
	ftp_close($cFtp);
	die();
}
if (!@ftp_get($cFtp, $sLocalDir.'/Ostatki.xml', './Ostatki.xml', FTP_BINARY)) {
	writeLog('ftp download error');
	ftp_close($cFtp);
	die();
}
if (!@ftp_get($cFtp, $sLocalDir.'/OstatkiMAF.xml', './OstatkiMAF.xml', FTP_BINARY)) {
	writeLog('ftp download error');
	ftp_close($cFtp);
	die();
}

$cXml = simplexml_load_file($sLocalDir.'/Ostatki.xml');

if (!count($cXml->Nomenklatura)) {
	writeLog('nothing found in xml file');
	ftp_close($cFtp);
	die();
}

foreach ($cXml->Nomenklatura as $cItem) {
	$sArticul = (string)$cItem->Attributes()->Art;
	$iQuantity = floor((float)str_replace(',', '.', preg_replace('/[^\d,]/is', '', (string)$cItem->Attributes()->Kolvo)));
	if (empty($sArticul)) {
		continue;
	}

	$aItem = CItemColors::getInstance()->GetItemByArticul($sArticul);
	if (empty($aItem['articul'])) {
		writeLog('not found, articul '.$sArticul);
	} else {
		if (!CItemColors::getInstance()->UpdateRemains($aItem['articul'], $iQuantity)) {
			writeLog('update error, articul '.$sArticul);
		}
	}
}
if (!CItemColors::getInstance()->ClearRemains()) {
	writeLog('cant clear remains');
}

$cXml = simplexml_load_file($sLocalDir.'/OstatkiMAF.xml');

if (!count($cXml->Nomenklatura)) {
	writeLog('arsenal nothing found in xml file');
	ftp_close($cFtp);
	die();
}

foreach ($cXml->Nomenklatura as $cItem) {
	$sArticul = (string)$cItem->Attributes()->Art;
	$iQuantity = floor((float)str_replace(',', '.', preg_replace('/[^\d,]/is', '', (string)$cItem->Attributes()->Kolvo)));
	if (empty($sArticul)) {
		continue;
	}

	$aItem = CItemColors::getInstance()->GetItemByArticul($sArticul);
	if (empty($aItem['articul'])) {
		writeLog('arsenal not found, articul '.$sArticul);
	} else {
		if (!CItemColors::getInstance()->UpdateArsenalRemains($aItem['articul'], $iQuantity)) {
			writeLog('arsenal update error, articul '.$sArticul);
		}
	}
}
if (!CItemColors::getInstance()->ClearArsenalRemains()) {
	writeLog('arsenal cant clear remains');
}

if (!@ftp_put($cFtp, './log_mafplastic.txt', $sLocalDir.'/log.txt', FTP_BINARY)) {
	writeLog('ftp upload error');
	ftp_close($cFtp);
	die();
}
ftp_close($cFtp);

writeLog('finish');

function writeLog($message)
{
	$message = date('d.m.Y H:i:s') . ' ' . $message;
	file_put_contents(realpath(dirname(__FILE__)) . '/log.txt', $message . "\r\n", FILE_APPEND);
}

function initLog()
{
	file_put_contents(realpath(dirname(__FILE__)) . '/log.txt', '');
}

?>