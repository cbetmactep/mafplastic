<?

class CImage
{
	private $sSouceFName;
	private $aSourceInfo;
	private $sDestFName;
	private $bUseIMagick = true;
	private $sIMagickPath = '/usr/bin/';
	private $sWatermarkImage = '/images/watermark.png';
	private $iWatermarkMinWidth = 300;
	private $iWatermarkMinHeight = 300;

	function __construct()
	{
		$this->sWatermarkImage = $_SERVER['DOCUMENT_ROOT'].$this->sWatermarkImage;
	}

	function SetWatermark($sWatermarkImage)
	{
		$this->sWatermarkImage = $sWatermarkImage;
	}

	function SetSource($sFilename)
	{
		$this->aSourceInfo = @getimagesize($sFilename);
		$this->sSouceFName = $sFilename;
	}

	function SetDestionation($sFilename)
	{
		$this->sDestFName = $sFilename;
	}

	function Resize($iWidth,$iHeight,&$paParams = null)
	{
		if (!$this->aSourceInfo[0] || !$this->aSourceInfo[1]) return false;
		if ($iWidth == $this->aSourceInfo[0] && $iHeight == $this->aSourceInfo[1] && !$paParams['gray'] && !$paParams['watermark']) {
			if ($this->sSouceFName == $this->sDestFName) return true;
			if (!@copy($this->sSouceFName,$this->sDestFName)) return false;
			return true;
		}

		$iNewWidth=$iWidth;
		$iNewHeight=floor(($this->aSourceInfo[1]*$iNewWidth)/$this->aSourceInfo[0]);

		if ($paParams['crop']) {
			if ($iNewHeight<$iHeight) {
				$iNewHeight=$iHeight;
			}
		} else {
			if ($iNewHeight>$iHeight) {
				$iNewHeight=$iHeight;
			}
		}

		$iNewWidth=floor(($this->aSourceInfo[0]*$iNewHeight)/$this->aSourceInfo[1]);

		if (!$this->bUseIMagick) {
			if ($this->aSourceInfo[2]==1) $oImage=@imagecreatefromgif($this->sSouceFName);
			elseif ($this->aSourceInfo[2]==2) $oImage=@imagecreatefromjpeg($this->sSouceFName);
			elseif ($this->aSourceInfo[2]==3) $oImage=@imagecreatefrompng($this->sSouceFName);

			if (!$oImage) return false;

			if ($paParams['gray']) imagefilter($oImage,IMG_FILTER_GRAYSCALE);

			if ($paParams['strict']) {
				$oNewImage=@imagecreatetruecolor($iWidth,$iHeight);
				$iX=floor(($iWidth-$iNewWidth)/2);
				$iY=floor(($iHeight-$iNewHeight)/2);
			} else {
				$oNewImage=@imagecreatetruecolor($iNewWidth,$iNewHeight);
				$iX = 0;
				$iY = 0;
			}
			if (!$oNewImage) return false;

			$oBg=@imagecolorallocate($oNewImage,255,255,255);
			@imagefill($oNewImage,2,2,$oBg);

			@imagecopyresampled($oNewImage,$oImage,$iX,$iY,0,0,$iNewWidth,$iNewHeight,$this->aSourceInfo[0],$this->aSourceInfo[1]);
			if (function_exists("imageantialias")) imageantialias($oNewImage,true);

			if ($paParams['to_jpeg'] || $this->aSourceInfo[2] === IMAGETYPE_JPEG) {
				$bRes=imagejpeg($oNewImage,$this->sDestFName,$paParams['quality']);
			} elseif ($this->aSourceInfo[2] === IMAGETYPE_GIF) {
				$bRes=imagegif($oNewImage,$this->sDestFName);
			} elseif ($this->aSourceInfo[2] === IMAGETYPE_PNG) {
				$bRes=imagepng($oNewImage,$this->sDestFName);
			}
			if (!$bRes) {
				return false;
			}
			imagedestroy($oImage);
			imagedestroy($oNewImage);
		} else {
			$sCmd = $this->sIMagickPath.'convert "'.$this->sSouceFName.'"';
			$sCmd .= ' -quality '.$paParams['quality'].' -strip';
			if ($paParams['crop']) {
				$sCmd .= ' -resize "'.$iWidth.'x'.$iHeight.'^" -gravity center -crop "'.$iWidth.'x'.$iHeight.'+0+0"';
			} else {
				$sCmd .= ' -resize "'.$iWidth.'x'.$iHeight.'>"';
			}
			if ($paParams['strict']) {
				$sCmd .= ' -background '.($paParams['background']?$paParams['background']:'white').' -gravity center -extent "'.$iWidth.'x'.$iHeight.'"';
			}
			if ($paParams['gray']) {
				$sCmd .= ' -type grayscale';
			}
			if ($paParams['watermark'] && ($iNewWidth >= $this->iWatermarkMinWidth || $iNewHeight >= $this->iWatermarkMinHeight)) {
				$iWatermarkSize = $iNewHeight>$iNewWidth?$iNewWidth:$iNewHeight;
				$sCmd .= ' '.$this->sWatermarkImage.' -geometry "'.$iWatermarkSize.'x'.$iWatermarkSize.'+0+0" -gravity center -compose dissolve -set "option:compose:args" 75 -composite';
			}
			if (pathinfo($this->sDestFName, PATHINFO_EXTENSION) === 'jpg') {
				$sCmd .= ' -colorspace sRGB';
			}
			$sCmd .= ' "'.$this->sDestFName.'"';

			$aOutput = array();
			$iErr = null;
			exec($sCmd, $aOutput, $iErr);
			if ($iErr!=0) {
				return false;
			}
		}

		return true;
	}

	function PDFtoJPG($iWidth,$iHeight,&$paParams = null)
	{
		if ($this->bUseIMagick) {
			$sCmd = 'PATH=$PATH:'.$this->sIMagickPath.';export PATH;'.$this->sIMagickPath.'convert "'.$this->sSouceFName.'[0]"';
			//$sCmd = $this->sIMagickPath.'convert "'.$this->sSouceFName.'[0]"';
			$sCmd .= ' -quality '.$paParams['quality'].' -strip';
			if ($paParams['crop']) {
				$sCmd .= ' -resize "'.$iWidth.'x>"';
				$sCmd .= ' -gravity center -crop '.$iWidth.'x'.$iHeight.'+0+0';
			} else {
				$sCmd .= ' -resize "'.$iWidth.'x'.$iHeight.'>"';
			}
			if (pathinfo($this->sDestFName,PATHINFO_EXTENSION) == 'jpg') {
				$sCmd .= ' -colorspace RGB';
			}
			$sCmd .= ' "'.$this->sDestFName.'"';

			$aOutput = array();
			$iErr = null;
			exec($sCmd, $aOutput, $iErr);
			if ($iErr!=0) {
				return false;
			}
		} else {
			return false;
		}
		return true;
	}

}

?>
