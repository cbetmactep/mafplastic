<?

class CConstructorKitItems
{
	private $sTable = 'constructor_kit_items';
	private $iConstructorItemID;

	protected $aCheckRules = array(
		'constructor_item_id' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
		'kit_item_id' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
		'ord' => array('type' => CChecker::TYPE_INT),
		'quant' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
	);

	function __construct($iConstructorItemID = null) {
		$this->iConstructorItemID = $iConstructorItemID;
	}

	function Add($aData)
	{
		if ($this->iConstructorItemID) $aData['constructor_item_id'] = $this->iConstructorItemID;
		$aData = array_intersect_key($aData,$this->aCheckRules);
		if (!CChecker::CheckArray($aData,$this->aCheckRules)) {
			return false;
		}
		return CDb::getInstance()->insert($this->sTable,$aData);
	}

	function Clear()
	{
		return CDb::getInstance()->delete($this->sTable, array('constructor_item_id' => $this->iConstructorItemID));
	}

	function GetList()
	{
		$q = 'SELECT kit_item_id as id, quant FROM '.$this->sTable.' WHERE constructor_item_id = '.$this->iConstructorItemID.' ORDER BY ord ASC';
		$oRes = CDb::getInstance()->query($q);
		if (!$oRes) return false;
		$aReturn = array();
		while ($aRow = CDb::getInstance()->fetch_array($oRes)) {
			$aReturn[$aRow['id']] = $aRow;
		}
		return $aReturn;
	}

	function GetFullList()
	{
		$q = 'SELECT ki.*, _t.quant
				FROM '.$this->sTable.' AS _t
				LEFT JOIN kit_items AS ki ON (ki.id = _t.kit_item_id)
				WHERE _t.constructor_item_id = '.$this->iConstructorItemID.'
				ORDER BY _t.ord ASC';
		$oRes = CDb::getInstance()->query($q);
		if (!$oRes) return false;
		$aReturn = array();
		while ($aRow = CDb::getInstance()->fetch_array($oRes)) {
			$aRow['prices'] = json_decode($aRow['prices'], true);
			$aReturn[$aRow['id']] = $aRow;
		}
		return $aReturn;
	}

	function GetFullListByIDs($aConstructorItemsIDs)
	{
		$q = 'SELECT ki.*, _t.constructor_item_id, _t.quant
				FROM '.$this->sTable.' AS _t
				LEFT JOIN kit_items AS ki ON (ki.id = _t.kit_item_id)
				WHERE _t.constructor_item_id IN ('.join(',', $aConstructorItemsIDs).')
				ORDER BY _t.ord ASC';
		$oRes = CDb::getInstance()->query($q);
		if (!$oRes) return false;
		$aReturn = array();
		while ($aRow = CDb::getInstance()->fetch_array($oRes)) {
			$aRow['prices'] = json_decode($aRow['prices'], true);
			$aReturn[$aRow['constructor_item_id']][$aRow['id']] = $aRow;
		}
		return $aReturn;
	}
}

?>
