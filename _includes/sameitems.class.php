<?

class CSameItems
{
	private $sTable = 'same_items';
	private $iItemID;

	protected $aCheckRules = array(
		'item_id' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
		'sameitem_id' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
		'ord' => array('type' => CChecker::TYPE_INT)
	);

	function __construct($iItemID = null) {
		$this->iItemID = $iItemID;
	}

	function Add($aData)
	{
		if ($this->iItemID) $aData['item_id'] = $this->iItemID;
		$aData = array_intersect_key($aData,$this->aCheckRules);
		if (!CChecker::CheckArray($aData,$this->aCheckRules)) {
			return false;
		}

		$db = CDb::getInstance();
		return CDb::getInstance()->insert($this->sTable,$aData);
	}

	function Clear()
	{
		return CDb::getInstance()->delete($this->sTable, array('item_id' => $this->iItemID));
	}

	function GetList()
	{
		$q = 'SELECT * FROM '.$this->sTable.' WHERE item_id = '.$this->iItemID.' ORDER BY ord ASC';
		$oRes = CDb::getInstance()->query($q);
		if (!$oRes) return false;
		$aReturn = array();
		while ($aRow = CDb::getInstance()->fetch_array($oRes)) {
			$aReturn[] = $aRow['sameitem_id'];
		}
		return $aReturn;
	}

}

?>
