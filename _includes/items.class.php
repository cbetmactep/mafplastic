<?

class CItems extends CContent
{
	protected $sTable = 'items';
	protected $sOrderField = 'ord';
	protected $sOrderDir = 'ASC';

	protected $aCheckRules = array(
		'cat_id' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
		'title' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 300)),
		'articul' => array('type' => CChecker::TYPE_STRING, 'flags' => 1028, 'params' => array('max' => 200)),
		'enabled' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'in_stock' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'page_title' => array('type' => CChecker::TYPE_STRING, 'flags' => 1028, 'params' => array('max' => 255)),
		'page_description' => array('type' => CChecker::TYPE_STRING, 'flags' => 1028, 'params' => array('max' => 255)),
		'page_keywords' => array('type' => CChecker::TYPE_STRING, 'flags' => 1028, 'params' => array('max' => 255)),
		'prices' => array('type' => CChecker::TYPE_STRING),
		'descr' => array('type' => CChecker::TYPE_STRING,'flags' => 5, 'params' => array('max' => 1024000)),
		'on_main' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'type' => array('type' => CChecker::TYPE_INT,'flags' => 8198,'params' => array('min' => 0,'max' => 5)),
		'net_price' => array('type' => CChecker::TYPE_FLOAT),
		'net_price_currency' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 2))
	);

	private $aPriceCheckRules = array(
		'title' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 50)),
		'from' => array('type' => CChecker::TYPE_INT,'flags' => 8192),
		'to' => array('type' => CChecker::TYPE_INT,'flags' => 8192),
		'price' => array('type' => CChecker::TYPE_FLOAT)
	);

	static $aTypes = array(
		1 => 'канат',
		2 => 'крестообразное соединение',
		3 => 'крепление сетки вверху',
		4 => 'крепление сетки внизу',
		5 => 'крепление сетки справа и слева'
	);

	private static $_instance;

	/**
	 * Items class
	 *
	 * @return CItems
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	/**
	 * Same items factory
	 *
	 * @param int $iItemID
	 * @return CSameItems
	 */
	private function GetSameItemsClass($iItemID)
	{
		return new CSameItems($iItemID);
	}

	/**
	 * Component items factory
	 *
	 * @param int $iItemID
	 * @return CComponentItems
	 */
	private function GetComponentItemsClass($iItemID)
	{
		return new CComponentItems($iItemID);
	}

	public function Add($aData, $sFilename, $sUsageFilename, $sConfiguratorSchemeFilename)
	{
		$db = CDb::getInstance();
		$db->begin();

		$this->_processData($aData);

		$aSameItems = $aData['sameitems'];
		$aComponentItems = $aData['componentitems'];

		$iNewID = parent::Add($aData);
		if (empty($iNewID)) {
			$db->rollback();
			return false;
		}

		if ($sFilename && !$this->_uploadImage($iNewID, $sFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image' => 'ERROR_FILE_ERROR'));
			return false;
		}

		if ($sUsageFilename && !$this->_uploadUsageImage($iNewID, $sUsageFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('usage_image' => 'ERROR_FILE_ERROR'));
			return false;
		}

		if ($sConfiguratorSchemeFilename && !$this->_uploadConfiguratorSchemeImage($iNewID, $sConfiguratorSchemeFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('configurator_scheme_image' => 'ERROR_FILE_ERROR'));
			return false;
		}

		if (!$this->_updateSameItems($iNewID, $aSameItems)) {
			$db->rollback();
			return false;
		}

		if (!$this->_updateComponentItems($iNewID, $aComponentItems)) {
			$db->rollback();
			return false;
		}

		$db->commit();
		return $iNewID;
	}

	public function Update($iID, $aData, $sFilename, $sUsageFilename, $sConfiguratorSchemeFilename)
	{
		$db = CDb::getInstance();
		$db->begin();

		$this->_processData($aData);

		$aSameItems = $aData['sameitems'];
		$aComponentItems = $aData['componentitems'];

		if (!parent::Update($iID, $aData)) {
			$db->rollback();
			return false;
		}

		if ($sFilename && !$this->_uploadImage($iID, $sFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image' => 'ERROR_FILE_ERROR'));
			return false;
		}

		if ($sUsageFilename && !$this->_uploadUsageImage($iID, $sUsageFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image_usage' => 'ERROR_FILE_ERROR'));
			return false;
		}

		if ($sConfiguratorSchemeFilename && !$this->_uploadConfiguratorSchemeImage($iID, $sConfiguratorSchemeFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image_configurator_scheme' => 'ERROR_FILE_ERROR'));
			return false;
		}

		if (!$this->_updateSameItems($iID, $aSameItems)) {
			$db->rollback();
			return false;
		}

		if (!$this->_updateComponentItems($iID, $aComponentItems)) {
			$db->rollback();
			return false;
		}

		$db->commit();
		return true;
	}

	private function _processData(&$paData)
	{
		$paData['prices'] = (array)$paData['prices'];
		$aPrices = array();
		foreach ($paData['prices'] as $aPrice) {
			if (CChecker::CheckArray($aPrice, $this->aPriceCheckRules)) {
				$aPrices[] = $aPrice;
			}
		}
		$paData['prices'] = json_encode($aPrices);
	}

	function SwitchStatus($iID)
	{
		$q = 'UPDATE '.$this->sTable.' SET enabled = 1 - enabled WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	function SetOrder($iID, $iOrd)
	{
		$q = 'UPDATE '.$this->sTable.' SET ord = '.$iOrd.' WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	public function UpdateNetPrice($iID, $fPrice, $iCurrency)
	{
		$q = 'UPDATE '.$this->sTable.' SET net_price = '.$fPrice.', net_price_currency = '.$iCurrency.' WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}


	public function Delete($iID)
	{
		if (!parent::Delete($iID)) {
			return false;
		}

		$this->RemoveFiles($iID);

		return true;
	}

	public function RemoveFiles($iID)
	{
		@unlink(self::GetLink($iID, 1, true));
		@unlink(self::GetLink($iID, 2, true));
		@unlink(self::GetLink($iID, 3, true));
		@unlink(self::GetLink($iID, 4, true));
	}

	public function CopyFiles($iID, $iNewID)
	{
		$sPath = dirname(self::GetLink($iNewID, 1, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}
		@copy(self::GetLink($iID, 1, true), self::GetLink($iNewID, 1, true));
		@copy(self::GetLink($iID, 2, true), self::GetLink($iNewID, 2, true));
		@copy(self::GetLink($iID, 3, true), self::GetLink($iNewID, 3, true));
		@copy(self::GetLink($iID, 4, true), self::GetLink($iNewID, 4, true));
	}

	private function _uploadImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, 1, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		if (!@copy($sFilename, self::GetLink($iID, 1, true))) {
			$this->RemoveFiles($iID);
			return false;
		}

		$cImage = new CImage();
		$cImage->SetSource($sFilename);
		$aParams = array(
			'quality' => 100,
			'crop' => false
		);
		$cImage->SetDestionation(self::GetLink($iID, 2, true));
		if (!$cImage->Resize(394, 1000, $aParams)) {
			$this->RemoveFiles($iID);
			return false;
		}

		return true;
	}

	private function _uploadUsageImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, 3, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		$cImage = new CImage();
		$cImage->SetSource($sFilename);
		$aParams = array(
			'to_jpeg' => true,
			'quality' => 100,
			'crop' => true,
			'strict' => true
		);
		$cImage->SetDestionation(self::GetLink($iID, 3, true));
		if (!$cImage->Resize(200, 200, $aParams)) {
			return false;
		}

		return true;
	}

	private function _uploadConfiguratorSchemeImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, 4, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		if (!@copy($sFilename, self::GetLink($iID, 4, true))) {
			return false;
		}

		return true;
	}

	private function _updateSameItems($iID, $aSameItems)
	{
		$cSameItems = $this->GetSameItemsClass($iID);

		$cSameItems->Clear();
		$c = 1;
		if (!empty($aSameItems)) foreach ($aSameItems as $iID) {
			$aData = array(
				'sameitem_id' => (int)$iID,
				'ord' => $c++
			);
			if (!$cSameItems->Add($aData)) {
				CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('sameitems' => 'ERROR_WRONG_VALUE'));
				return false;
			}
		}
		return true;
	}

	private function _updateComponentItems($iID, $aComponentItems)
	{
		$cComponentItems = $this->GetComponentItemsClass($iID);

		$cComponentItems->Clear();
		$c = 1;
		if (!empty($aComponentItems)) foreach ($aComponentItems as $aItem) {
			$aData = array(
				'component_id' => (int)$aItem['id'],
				'ord' => $c++,
				'quant' => (int)$aItem['quant']
			);
			if (!$cComponentItems->Add($aData)) {
				CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('componentitems' => 'ERROR_WRONG_VALUE'));
				return false;
			}
		}
		return true;
	}

	public function GetList(CSInfo $cSInfo = null)
	{
		$sWhere = '';
		if (!$cSInfo) $cSInfo = new CSInfo();
		else {
			$aWhere = array();
			if ($cSInfo->cat_id) $aWhere[] = '_t.cat_id = '.$cSInfo->cat_id;
			if ($cSInfo->ids) $aWhere[] = '_t.id IN ('.join(',',$cSInfo->ids).')';
			if ($cSInfo->except_id) $aWhere[] = '_t.id != '.$cSInfo->except_id;
			if ($cSInfo->except_ids) $aWhere[] = '_t.id NOT IN ('.join(',',$cSInfo->except_ids).')';
			if ($cSInfo->enabled_only) $aWhere[] = '_t.enabled = 1';
			if ($cSInfo->on_main_only) $aWhere[] = '_t.on_main = 1';
			if ($cSInfo->type) $aWhere[] = '_t.type = '.$cSInfo->type;
			if ($cSInfo->articul) {
				$aWhere[] = '(_t.articul LIKE \'%'.CDb::getInstance()->escape($cSInfo->articul).'%\'
					OR _t.articul LIKE \'%'.CDb::getInstance()->escape(str_replace(',', '.', $cSInfo->articul)).'%\'
					OR _t.articul LIKE \'%'.CDb::getInstance()->escape(str_replace('.', ',', $cSInfo->articul)).'%\')';
                }
			if ($aWhere) $sWhere = join(' AND ',$aWhere);
		}

		$q = 'SELECT _t.*, ig.id AS image_id FROM '.$this->sTable.' AS _t'
			.' LEFT JOIN item_gallery AS ig ON (_t.id = ig.item_id AND ig.main = 1 AND ig.enabled = 1)'
			.($sWhere?' WHERE '.$sWhere:'').' ORDER BY _t.'.$this->sOrderField.' '.$this->sOrderDir;
		if (!$oRes = CDb::getInstance()->query($q)) return false;
		$aReturn = array();
		$aIDs = array();
		while ($aRow = CDb::getInstance()->fetch_array($oRes)) {
			$aRow['prices'] = json_decode($aRow['prices'], true);
			if ($cSInfo->set_id_keys) $aReturn[$aRow['id']] = $aRow;
			else $aReturn[] = $aRow;
			$aIDs[] = $aRow['id'];
		}

		if ($aReturn && $cSInfo->with_colors) {
			$cSInfo = new CSInfo(array(
				'item_ids' => $aIDs,
				'group_by_item' => true,
				'enabled_only' => $cSInfo->colors_enabled_only,
				'set_id_keys' => $cSInfo->colors_set_id_keys,
				'set_color_keys' => $cSInfo->colors_set_color_keys
			));
			$aColors = CItemColors::getInstance()->GetList($cSInfo);
			foreach ($aReturn as &$aItem) {
				if (isset($aColors[$aItem['id']])) {
					$aItem['colors'] = $aColors[$aItem['id']];
				} else {
					$aItem['colors'] = array();
				}
			}
			unset($aItem);
		}

		return $aReturn;
	}

	public function GetCatsItems($aCatIDs)
	{
		$q = 'SELECT * FROM '.$this->sTable.' WHERE cat_id IN ('.join(',', $aCatIDs).')';
		if (!$oRes = CDb::getInstance()->query($q)) return false;
		$aReturn = array();
		while ($aRow = CDb::getInstance()->fetch_array($oRes)) {
			$aReturn[$aRow['cat_id']][] = $aRow;
		}
		return $aReturn;
	}

	public function GetItem($iID)
	{
		$mReturn = parent::GetItem($iID);
		if (!$mReturn) return $mReturn;
		$mReturn['prices'] = json_decode($mReturn['prices'], true);
		$cSameItems = $this->GetSameItemsClass($iID);
		$mReturn['sameitems'] = $cSameItems->GetList();
		$cComponentItems = $this->GetComponentItemsClass($iID);
		$mReturn['componentitems'] = $cComponentItems->GetList();
		return $mReturn;
	}

	public function GetItemByArticul($sArticul)
	{
		if (CChecker::CheckString($sArticul, $this->aCheckRules['articul']['flags'], $this->aCheckRules['articul']['params'])) {
			return false;
		}
		$q = 'SELECT * FROM '.$this->sTable.' WHERE articul = \''.CDb::getInstance()->escape($sArticul).'\'';
		return CDb::getInstance()->query_first($q);
	}

	public function GetInComponents($iItemID)
	{
		$cComponentItems = $this->GetComponentItemsClass($iItemID);
		return $cComponentItems->GetRelList();
	}

	/**
	* types
	* 1 - original
	* 2 - thumb
	* 3 - usage
	* 4 - configurator scheme
	*/
	public static function GetLink($iID, $iType, $bFull = false)
	{
		$sUrl = ($bFull ? $_SERVER['DOCUMENT_ROOT'] : '').'/static/items/'.($iID%100).'/'.$iID;
		if ($iType == 2) {
			$sUrl .= '_thumb.jpg';
		} elseif ($iType == 3) {
			$sUrl .= '_usage.jpg';
		} elseif ($iType == 4) {
			$sUrl .= '_configurator_scheme.svg';
		} else {
			$sUrl .= '.jpg';
		}
		return $sUrl;
	}

}

?>
