<?
putenv('TMPDIR=/home/zagweb/mafplastic/tmp');

if (php_sapi_name() !== "cli") {
	//session_start();
}

require_once(dirname(__FILE__).'/config.php');
require_once(dirname(__FILE__).'/common.funcs.php');

//session_start();

$db=CDb::getInstance();
$db->connect($aDBConfig);
$db->query('SET NAMES utf8');

setlocale(LC_ALL, "en_EN.UTF-8");
mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');

$cSetts = CSetts::getInstance();
$cSetts->lang = 'ru';
$cSetts->date_format = 'd.m.Y';

$aMonths = array('', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа',
						'сентября', 'октября', 'ноября', 'декабря');

?>
