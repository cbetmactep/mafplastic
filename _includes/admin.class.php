<?

class CAdmin
{
	const ROLE_LOGIN = 1;
	const ROLE_MANAGEADMINS = 2;
	const ROLE_MANAGESETTS = 4;
	const ROLE_MANAGEPAGES = 8;
	const ROLE_MANAGECATS = 16;
	const ROLE_MANAGEITEMS = 32;
	const ROLE_MANAGEGALLERY = 64;
	const ROLE_MANAGESLIDER = 128;
	const ROLE_MANAGETOPMENU = 256;
	const ROLE_MANAGEUSERS = 512;

	public static $aRoles = array(
		1 => 'Вход в систему',
		2 => 'Управление администраторами',
		4 => 'Управление настройками',
		8 => 'Редактирование страниц',
		16 => 'Редактирование категорий',
		32 => 'Редактирование товаров',
		64 => 'Редактирование галереи',
		128 => 'Редактирование слайдера',
		256 => 'Редактирование верхнего меню',
		512 => 'Управление пользователями'
	);

	private $aUserInfo;

	private $aCheckRules = array(
		'username' => array('type'=>CChecker::TYPE_STRING,'flags'=>4113,'params'=>array('ereg'=>CChecker::EREG_USERNAME)),
		'password' => array('type'=>CChecker::TYPE_STRING,'flags'=>6,'params'=>array('min'=>4,'max'=>16)),
	);

	private static $_instance;

	/**
	 * Admin class
	 *
	 * @return CAdmin
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	function __construct()
	{
		$this->aUserInfo = $this->GetItem($_SESSION['admininfo']['id']);
	}

	function Add(&$paData)
	{
		$iRoles = $paData['role'];

		$paData = array_intersect_key($paData,$this->aCheckRules);
		if (!CChecker::CheckArray($paData,$this->aCheckRules)) {
			return false;
		}

		$paData['roles'] = 0;
		foreach ($iRoles as $iRole => $sOn) {
			$paData['roles'] = $paData['roles'] | $iRole;
		}
		unset($iRoles);

		$paData['password'] = md5($paData['password']);

		if (!CDb::getInstance()->insert('admins',$paData)) {
			if ($db->last_errno == 1062) {
				CChecker::SetLastErrors('ERROR_SOME_ERRORS', array('username' => 'ERROR_USERNAME_EXISTS'));
			}
			return false;
		}

		return true;
	}

	function Update($iAdminID,&$paData)
	{
		$aAdmin = $this->GetItem($iAdminID);
		if (!$aAdmin['id']) {
			CChecker::SetLastErrors('NO_SUCH_ADMIN');
			return false;
		}

		if (!$this->CheckRole(self::ROLE_MANAGEADMINS) && $this->aUserInfo['id']!=$iAdminID) {
			CChecker::SetLastErrors('NO_RIGHTS');
			return false;
		}

		if (!$paData['password']) {
			unset($paData['password']);
			unset($this->aCheckRules['password']);
		}
		unset($this->aCheckRules['username']);

		$iRoles = $paData['role'];

		$paData = array_intersect_key($paData,$this->aCheckRules);
		if (!CChecker::CheckArray($paData,$this->aCheckRules)) {
			return false;
		}

		unset($paData['roles']);
		if ($this->CheckRole(self::ROLE_MANAGEADMINS) && $iAdminID!=$this->aUserInfo['id']) {
			$paData['roles'] = 0;
			foreach ($iRoles as $iRole => $sOn) {
				$paData['roles'] = $paData['roles'] | $iRole;
			}
		}
		unset($iRoles);

		if ($paData['password']) $paData['password'] = md5($paData['password']);

		if (!CDb::getInstance()->update('admins',$paData,array('id'=>$iAdminID))) return false;

		return true;
	}

	function Delete($iAdminID)
	{
		$aAdmin = $this->GetItem($iAdminID);
		if (!$aAdmin['id']) {
			CChecker::SetLastErrors('NO_SUCH_ADMIN');
			return false;
		}

		if (!CDb::getInstance()->delete('admins',array('id'=>$iAdminID))) return false;

		return true;
	}

	function GetList()
	{
		$db = CDb::getInstance();

		$q = 'SELECT * FROM admins';
		if (!$oRes = $db->query($q)) return false;
		while ($aRow = $db->fetch_array($oRes)) {
			$aReturn[$aRow['id']] = $aRow;
		}
		return $aReturn;
	}

	function GetItem($iAdminID)
	{
		$q = 'SELECT * FROM admins WHERE id='.$iAdminID;
		return CDb::getInstance()->query_first($q);
	}

	function Login(&$paData)
	{
		if (CChecker::CheckString($paData['username'],$this->aUserRegCheckRules['username']['flags'],$this->aUserRegCheckRules['username']['params'])) {
			CChecker::SetLastErrors('ERROR_NO_USER');
			return false;
		}

		$q = "SELECT * FROM admins WHERE username='".$paData['username']."'";
		$aReturn = CDb::getInstance()->query_first($q);

		if (!$aReturn['id']) {
			CChecker::SetLastErrors('ERROR_NO_USER');
			return false;
		}
		if ($aReturn['password']!=md5($paData['password'])) {
			CChecker::SetLastErrors('ERROR_WRONG_PASSWORD');
			return false;
		}
		if (!($aReturn['roles'] & self::ROLE_LOGIN)) {
			CChecker::SetLastErrors('NO_RIGHTS');
			return false;
		}

		$_SESSION['admininfo'] = $aReturn;
		$this->aUserInfo = $aReturn;

		return true;

	}

	function CheckRole($iRole,$aUserInfo = array())
	{
		if ($aUserInfo) $iUserRole = $aUserInfo['roles'];
		else $iUserRole = $this->aUserInfo['roles'];
		if ($iUserRole & $iRole || $iUserRole === -1) return true;
		return false;
	}

	function isLogged()
	{
		return ($_SESSION['admininfo']['id']?true:false);
	}

	function Logout()
	{
		$_SESSION['admininfo'] = null;
	}

	function ID()
	{
		return $this->aUserInfo['id'];
	}

}

?>
