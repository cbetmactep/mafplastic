<?

class CSInfo
{
	public $perpage;
	public $page;
	public $pages;
	public $total_rows;

	private $aVariables = array();

	public function __construct($aSearchParams = null)
	{
		if ($aSearchParams) foreach ($aSearchParams as $sVar => $mValue) {
			$this->$sVar = $mValue;
		}
	}

	function __set($sVar,$sValue)
	{
		$this->aVariables[$sVar] = $sValue;
	}

	function &__get($sVar)
	{
		if (array_key_exists($sVar,$this->aVariables)) return $this->aVariables[$sVar];
		// can invoke error here
		return null;
	}

	function IsEmptySearch()
	{
		if ($this->aVariables) foreach($this->aVariables as $sVar => $sValue) {
			if ($sValue) return false;
		}
		return true;
	}

	function CalculatePages()
	{
		$this->pages = ceil($this->total_rows/$this->perpage);
		if ($this->page >= $this->pages) $this->page = $this->pages - 1;
	}

	function GetLimit()
	{
		return ' LIMIT '.($this->page*$this->perpage).','.$this->perpage;
	}

	function GetTotalRows()
	{
		return $this->total_rows;
	}

	function MakeShortPagination($sAddParams = '')
	{

		if ($this->pages <= 1) return;
		$sHTML = '';

		/*if ($this->page > 2 && $this->pages > 5) {
			$sHTML .= '<a href="?'.$sAddParams.'">1</a> ... ';
		}

		$iFrom = $this->page - 2;
		$iTo = $this->page + 3;
		if ($this->pages <= 5) {
			$iFrom = 0;
			$iTo = $this->pages;
		} elseif ($iFrom < 0) {
			$iFrom = 0;
			$iTo = 5;
		} elseif ($iTo >= $this->pages) {
			$iFrom = $this->pages - 5;
			$iTo = $this->pages;
		}

		for ($c = $iFrom; $c < $iTo; $c++) {
			if ($c != $this->page) $sHTML .= '<a href="?'.$sAddParams.($c?'page='.$c:'').'">'.($c+1).'</a> ';
			else $sHTML .= '<span>'.($c + 1).'</span> ';
		}

		if ($this->page < $this->pages - 3 && $this->pages > 5) {
			$sHTML .= '... <a href="?'.$sAddParams.'page='.($this->pages - 1).'">'.$this->pages.'</a>';
		}*/

		for ($c = 0; $c < $this->pages; $c++) {
			if ($c != $this->page) $sHTML .= '<a href="?'.$sAddParams.($c?'page='.$c:'').'">&nbsp;</a> ';
			else $sHTML .= '<a class="active" href="?'.$sAddParams.($c?'page='.$c:'').'">&nbsp;</a> ';
		}

		return $sHTML;

	}

	function MakePagination($sAddParams = '')
	{
		$sPath = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

		if ($this->pages <= 1) return;
		$sHTML = '';

		if ($this->page > 2 && $this->pages > 5) {
			$sHTML .= '<a href="'.$sPath.($sAddParams?'?'.$sAddParams:'').'">1</a> ... ';
		}

		$iFrom = $this->page - 2;
		$iTo = $this->page + 3;
		if ($this->pages <= 5) {
			$iFrom = 0;
			$iTo = $this->pages;
		} elseif ($iFrom < 0) {
			$iFrom = 0;
			$iTo = 5;
		} elseif ($iTo >= $this->pages) {
			$iFrom = $this->pages - 5;
			$iTo = $this->pages;
		}

		for ($c = $iFrom; $c < $iTo; $c++) {
			$sLink = $sPath;
			if ($sAddParams || $c) {
				$sLink = '?';
				if (!empty($sAddParams)) $sLink .= $sAddParams;
				if (!empty($c)) $sLink .= 'page='.$c;
			}
			if ($c != $this->page) $sHTML .= '<a href="'.$sLink.'">'.($c+1).'</a> ';
			else $sHTML .= '<span>'.($c + 1).'</span> ';
		}

		if ($this->page < $this->pages - 3 && $this->pages > 5) {
			$sHTML .= '... <a href="'.$sPath.'?'.$sAddParams.'page='.($this->pages - 1).'">'.$this->pages.'</a>';
		}

		return $sHTML;

	}

}

?>