<?

class CHardwareItems extends CContent
{
	protected $sTable = 'hardware_items';
	protected $sOrderField = 'thread ASC, size';
	protected $sOrderDir = 'ASC';

	protected $aCheckRules = array(
		'type' => array('type' => CChecker::TYPE_INT,'flags' => 6, 'params' => array('min' => 1, 'max' => 3)),
		'articul' => array('type' => CChecker::TYPE_STRING, 'flags' => 1028, 'params' => array('max' => 200)),
		'size' => array('type' => CChecker::TYPE_INT, 'flags' => 3, 'params' => array('min' => 1)),
		'thread' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
		'price' => array('type' => CChecker::TYPE_FLOAT, 'flags' => 1),
		'enabled' => array('type' => CChecker::TYPE_INT,'flags' => 6, 'params' => array('min' => 0, 'max' => 1))
	);

	static $aTypes = array(
		1 => 'шпильки',
		2 => 'болты',
		3 => 'метрические кольца'
	);

	static $aTypesSingle = array(
		1 => 'Шпилька',
		2 => 'Болт',
		3 => 'Метрическое кольцо'
	);

	const TYPE_STUD = 1;
	const TYPE_SCREW = 2;
	const TYPE_EYEBOLT = 3;

	private static $_instance;

	/**
	 * Hardware prices class
	 *
	 * @return CHardwareItems
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	function SwitchStatus($iID)
	{
		$q = 'UPDATE '.$this->sTable.' SET enabled = 1 - enabled WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	public function UpdatePrice($iID, $fPrice)
	{
		$q = 'UPDATE '.$this->sTable.' SET price = '.$fPrice.' WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	public function UpdatePriceByArticul($sArticul, $fPrice)
	{
		$q = 'UPDATE '.$this->sTable.' SET price = '.$fPrice.' WHERE articul = \''.CDb::getInstance()->escape($sArticul).'\'';
		return CDb::getInstance()->query($q);
	}

	public function GetList(CSInfo $cSInfo)
	{
		$aWhere = array();
		if ($cSInfo->enabled_only) {
			$aWhere[] = 'enabled = 1';
		}
		if ($cSInfo->type) {
			$aWhere[] = 'type = '.$cSInfo->type;
		}
		if ($cSInfo->ids && is_array($cSInfo->ids)) {
			$aWhere[] = 'id IN ('.join(',', $cSInfo->ids).')';
		}

		if ($aWhere) {
			$sWhere = join(' AND ', $aWhere);
		} else {
			$sWhere = '';
		}

		return parent::GetList($cSInfo, $sWhere);
	}

	public function GetItemByArticul($sArticul)
	{
		if (CChecker::CheckString($sArticul, $this->aCheckRules['articul']['flags'], $this->aCheckRules['articul']['params'])) {
			return false;
		}
		$q = 'SELECT * FROM '.$this->sTable.' WHERE articul = \''.CDb::getInstance()->escape($sArticul).'\'';
		return CDb::getInstance()->query_first($q);
	}
}

?>
