<?

class CConstructorItems extends CContent
{
	protected $sTable = 'constructor_items';
	protected $sOrderField = 'ord';
	protected $sOrderDir = 'ASC';

	public static $aColorsRGB = array(
		1 => array(
			1 => '#262626',
			2 => '#FFFFFF',
			3 => '#9B9B9B',
			6 => '#C9C9C9',
			7 => '#F50029',
			8 => '#E1BB0F',
			9 => '#478C3C',
			10 => '#265AD4',
			11 => '#FFFFFF',
			12 => '#F5A623',
			13 => '#8B572A',
			14 => '#44322D',
			15 => '#7ED321',
			16 => '#D1BC8A',
			17 => '#AF8A54',
			18 => '#EADEBD',
			19 => '#42AAFF',
			29 => '#D0021B',
			30 => '#4A4A4A',
			31 => '#CBD0CC',
			33 => '#FC89AC',
			34 => '#C8A2C8',
			35 => '#30D5C8',
			37 => '#3B99C9',
			54 => '#2B2C7C',
			75 => '#808000',
			76 => '#9013FE',
			82 => '#987654',
			101 => '#9CEE90',
			117 => '#ADD8E6',
			139 => '#FFD800',
			140 => '#FFFFFF'

		),
		2 => array(
			1 => '#2E2E2E',
			2 => '#FFFFFF',
			3 => '#9B9B9B',
			6 => '#C9C9C9',
			7 => '#C73D28',
			8 => '#CDA434',
			9 => '#417D49',
			10 => '#4169E1',
			11 => '#FFFFFF',
			12 => '#F5A623',
			13 => '#8B572A',
			14 => '#44322D',
			15 => '#7ED321',
			16 => '#D1BC8A',
			17 => '#AF8A54',
			18 => '#EADEBD',
			19 => '#42AAFF',
			29 => '#D0021B',
			30 => '#4A4A4A',
			31 => '#CBD0CC',
			33 => '#FC89AC',
			34 => '#C8A2C8',
			35 => '#30D5C8',
			37 => '#3B99C9',
			54 => '#2B2C7C',
			75 => '#808000',
			76 => '#9013FE',
			82 => '#987654',
			101 => '#9CEE90',
			117 => '#ADD8E6',
			139 => '#FFD800',
			140 => '#FFFFFF'
		),
		3 => array(
			1 => '#383838',
			2 => '#FFFFFF',
			3 => '#9B9B9B',
			6 => '#C9C9C9',
			7 => '#E10000',
			8 => '#C9A43D',
			9 => '#007D34',
			10 => '#2A52BE',
			11 => '#FFFFFF',
			12 => '#F5A623',
			13 => '#8B572A',
			14 => '#44322D',
			15 => '#7ED321',
			16 => '#D1BC8A',
			17 => '#AF8A54',
			18 => '#EADEBD',
			19 => '#42AAFF',
			29 => '#D0021B',
			30 => '#4A4A4A',
			31 => '#CBD0CC',
			33 => '#FC89AC',
			34 => '#C8A2C8',
			35 => '#30D5C8',
			37 => '#3B99C9',
			54 => '#2B2C7C',
			75 => '#808000',
			76 => '#9013FE',
			82 => '#987654',
			101 => '#9CEE90',
			117 => '#ADD8E6',
			139 => '#FFD800',
			140 => '#FFFFFF'
		)
	);

	public static $aRopeCapColors = array(
		1 => 1,
		2 => 2,
		3 => 3,
		4 => 4,
		5 => 5,
		6 => 6,
		7 => 7,
		8 => 6,
		9 => 3
	);

	protected $aCheckRules = array(
		'type' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 1,'max' => 3)),
		'subtype' => array('type' => CChecker::TYPE_INT,'flags' => 8198,'params' => array('min' => 0,'max' => 5)),
		'block_type' => array('type' => CChecker::TYPE_INT,'flags' => 8198,'params' => array('min' => 0,'max' => 8)),
		'item_1_id' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
		'item_2_id' => array('type' => CChecker::TYPE_INT, 'flags' => 8192),
		'item_3_id' => array('type' => CChecker::TYPE_INT, 'flags' => 8192),
		'item_4_id' => array('type' => CChecker::TYPE_INT, 'flags' => 8192),
		'item_5_id' => array('type' => CChecker::TYPE_INT, 'flags' => 8192),
		'item_6_id' => array('type' => CChecker::TYPE_INT, 'flags' => 8192),
		'item_7_id' => array('type' => CChecker::TYPE_INT, 'flags' => 8192),
		'item_8_id' => array('type' => CChecker::TYPE_INT, 'flags' => 8192),
		'item_9_id' => array('type' => CChecker::TYPE_INT, 'flags' => 8192),
		'enabled' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'descr' => array('type' => CChecker::TYPE_STRING,'flags' => 5, 'params' => array('max' => 1024000)),
		'item_1_deletable' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'item_2_deletable' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'item_3_deletable' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'item_4_deletable' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'item_5_deletable' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'item_6_deletable' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'item_7_deletable' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'item_8_deletable' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'item_9_deletable' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'size' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1000)),
		'rope_increment' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => -1000,'max' => 1000)),
		'time_coeff' => array('type' => CChecker::TYPE_FLOAT),
		'crimp_size' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1000))
	);

	static $aTypes = array(
		1 => 'канат',
		2 => 'крестообразное соединение',
		3 => 'крепление сетки'
	);

	static $aSubTypes = array(
		1 => 'шпильки и болты',
		2 => 'тэшки для рамы',
		3 => 'коуши',
		4 => 'тэшки для каната',
		5 => 'коуши к раме',
	);

	static $aBlockTypes = array(
		1 => 'Цвет',
		2 => 'Размер (шпилька)',
		3 => 'Размер (болт)',
		4 => 'Размер (метрическое кольцо)',
		5 => 'Цвет + размер (шпилька)',
		6 => 'Цвет + размер (болт)',
		7 => 'Цвет + размер (метрическое кольцо)',
		8 => 'Без всего'
	);

	const TYPE_ROPE = 1;
	const TYPE_JUNCTION = 2;
	const TYPE_CONNECTION = 3;

	const SUBTYPE_STUD_SCREW = 1;
	const SUBTYPE_T_FRAME = 2;
	const SUBTYPE_THIMBLE = 3;
	const SUBTYPE_T_ROPE = 4;
	const SUBTYPE_THIMBLE_FRAME = 5;

	const BLOCK_COLOR = 1;
	const BLOCK_SIZE_STUD = 2;
	const BLOCK_SIZE_SCREW = 3;
	const BLOCK_SIZE_EYEBOLT = 4;
	const BLOCK_SIZE_COLOR_STUD = 5;
	const BLOCK_SIZE_COLOR_SCREW = 6;
	const BLOCK_SIZE_COLOR_EYEBOLT = 7;
	const BLOCK_EMPTY = 8;

	private static $_instance;

	/**
	 * Constructor items class
	 *
	 * @return CConstructorItems
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	/**
	 * Kit items factory
	 *
	 * @param int $iItemID
	 * @return CConstructorKitItems
	 */
	private function GetConstructorKitItemsClass($iItemID)
	{
		return new CConstructorKitItems($iItemID);
	}

	public function Add($aData, $sImageFilename, $sSchemeFilename, $sSizesFilename)
	{
		$db = CDb::getInstance();
		$db->begin();

		$aKitItems = $aData['kititems'];

		$iNewID = parent::Add($aData);
		if (empty($iNewID)) {
			$db->rollback();
			return false;
		}

		if ($sImageFilename && !$this->_uploadImage($iNewID, $sImageFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image' => 'ERROR_FILE_ERROR'));
			return false;
		}

		if ($sSchemeFilename && !$this->_uploadSchemeImage($iNewID, $sSchemeFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image_scheme' => 'ERROR_FILE_ERROR'));
			return false;
		}

		if ($sSizesFilename && !$this->_uploadSizesImage($iNewID, $sSizesFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image_sizes' => 'ERROR_FILE_ERROR'));
			return false;
		}

		if (!$this->_updateKitItems($iNewID, $aKitItems)) {
			$db->rollback();
			return false;
		}

		$db->commit();
		return $iNewID;
	}

	public function Update($iID, $aData, $sImageFilename, $sSchemeFilename, $sSizesFilename)
	{
		$db = CDb::getInstance();
		$db->begin();

		unset($this->aCheckRules['type']);
		$aKitItems = $aData['kititems'];

		if (!parent::Update($iID, $aData)) {
			$db->rollback();
			return false;
		}

		if ($sImageFilename && !$this->_uploadImage($iID, $sImageFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image' => 'ERROR_FILE_ERROR'));
			return false;
		}

		if ($sSchemeFilename && !$this->_uploadSchemeImage($iID, $sSchemeFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image_scheme' => 'ERROR_FILE_ERROR'));
			return false;
		}

		if ($sSizesFilename && !$this->_uploadSizesImage($iID, $sSizesFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image_sizes' => 'ERROR_FILE_ERROR'));
			return false;
		}

		if (!$this->_updateKitItems($iID, $aKitItems)) {
			$db->rollback();
			return false;
		}

		$db->commit();
		return true;
	}

	function SwitchStatus($iID)
	{
		$q = 'UPDATE '.$this->sTable.' SET enabled = 1 - enabled WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	function SetOrder($iID, $iOrd)
	{
		$q = 'UPDATE '.$this->sTable.' SET ord = '.$iOrd.' WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	public function Delete($iID)
	{
		if (!parent::Delete($iID)) return false;

		$this->RemoveFiles($iID);

		return true;
	}

	public function RemoveFiles($iID)
	{
		@unlink(self::GetLink($iID, 1, true));
		@unlink(self::GetLink($iID, 2, true));
		@unlink(self::GetLink($iID, 3, true));
	}

	private function _uploadImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, 1, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		if (!@copy($sFilename, self::GetLink($iID, 1, true))) {
			return false;
		}

		return true;
	}

	private function _uploadSchemeImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, 2, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		if (!@copy($sFilename, self::GetLink($iID, 2, true))) {
			return false;
		}

		return true;
	}

	private function _uploadSizesImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, 3, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		if (!@copy($sFilename, self::GetLink($iID, 3, true))) {
			return false;
		}

		return true;
	}

	private function _updateKitItems($iID, $aKitItems)
	{
		$cKitItems = $this->GetConstructorKitItemsClass($iID);

		$cKitItems->Clear();
		$c = 1;
		if (!empty($aKitItems)) foreach ($aKitItems as $aItem) {
			$aData = array(
				'kit_item_id' => (int)$aItem['id'],
				'ord' => $c++,
				'quant' => (int)$aItem['quant']
			);
			if (!$cKitItems->Add($aData)) {
				CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('kititems' => 'ERROR_WRONG_VALUE'));
				return false;
			}
		}
		return true;
	}

	public function GetList(CSInfo $cSInfo)
	{
		$aWhere = array();
		if ($cSInfo->enabled_only) {
			$aWhere[] = 'enabled = 1';
		}
		if ($cSInfo->type) {
			$aWhere[] = 'type = '.$cSInfo->type;
		}
		if ($cSInfo->subtype) {
			$aWhere[] = 'subtype = '.$cSInfo->subtype;
		}

		if ($aWhere) {
			$sWhere = join(' AND ', $aWhere);
		} else {
			$sWhere = '';
		}

		return parent::GetList($cSInfo, $sWhere);
	}

	public function GetListWithItems(CSInfo $cSInfo)
	{
		$aWhere = array();
		$aHaving = array();
		if ($cSInfo->enabled_only) {
			$aWhere[] = '_t.enabled = 1 AND
				(i1.enabled = 1 OR i1.enabled IS NULL) AND
				(i2.enabled = 1 OR i2.enabled IS NULL) AND
				(i3.enabled = 1 OR i3.enabled IS NULL) AND
				(i4.enabled = 1 OR i4.enabled IS NULL) AND
				(i5.enabled = 1 OR i5.enabled IS NULL) AND
				(i6.enabled = 1 OR i6.enabled IS NULL) AND
				(i7.enabled = 1 OR i7.enabled IS NULL) AND
				(i8.enabled = 1 OR i8.enabled IS NULL) AND
				(i9.enabled = 1 OR i9.enabled IS NULL)';
		}
		if ($cSInfo->with_remains_only) {
			$aHaving[] = '(item_1_remains > 1 OR item_1_remains IS NULL) AND
				(item_2_remains > 1 OR item_2_remains IS NULL) AND
				(item_3_remains > 1 OR item_3_remains IS NULL) AND
				(item_4_remains > 1 OR item_4_remains IS NULL) AND
				(item_5_remains > 1 OR item_5_remains IS NULL) AND
				(item_6_remains > 1 OR item_6_remains IS NULL) AND
				(item_7_remains > 1 OR item_7_remains IS NULL) AND
				(item_8_remains > 1 OR item_8_remains IS NULL) AND
				(item_9_remains > 1 OR item_9_remains IS NULL)';
		}
		if ($cSInfo->type) {
			$aWhere[] = '_t.type = '.$cSInfo->type;
		}
		if ($cSInfo->subtype) {
			$aWhere[] = '_t.subtype = '.$cSInfo->subtype;
		}

		if ($aWhere) {
			$sWhere = ' WHERE '.join(' AND ', $aWhere);
		} else {
			$sWhere = '';
		}
		if ($aHaving) {
			$sHaving = ' HAVING '.join(' AND ', $aHaving);
		} else {
			$sHaving = '';
		}

		$q = 'SELECT _t.*,
				i1.title AS item_1_title, i1.descr AS item_1_descr, i1.articul AS item_1_articul, i1.prices AS item_1_prices,
				SUM(ic1.remains) + SUM(ic1.arsenal_remains) AS item_1_remains,
				i2.title AS item_2_title, i2.descr AS item_2_descr, i2.articul AS item_2_articul, i2.prices AS item_2_prices,
				SUM(ic2.remains) + SUM(ic2.arsenal_remains) AS item_2_remains,
				i3.title AS item_3_title, i3.descr AS item_3_descr, i3.articul AS item_3_articul, i3.prices AS item_3_prices,
				SUM(ic3.remains) + SUM(ic3.arsenal_remains) AS item_3_remains,
				i4.title AS item_4_title, i4.descr AS item_4_descr, i4.articul AS item_4_articul, i4.prices AS item_4_prices,
				SUM(ic4.remains) + SUM(ic4.arsenal_remains) AS item_4_remains,
				i5.title AS item_5_title, i5.descr AS item_5_descr, i5.articul AS item_5_articul, i5.prices AS item_5_prices,
				SUM(ic5.remains) + SUM(ic5.arsenal_remains) AS item_5_remains,
				i6.title AS item_6_title, i6.descr AS item_6_descr, i6.articul AS item_6_articul, i6.prices AS item_6_prices,
				SUM(ic6.remains) + SUM(ic6.arsenal_remains) AS item_6_remains,
				i7.title AS item_7_title, i7.descr AS item_7_descr, i7.articul AS item_7_articul, i7.prices AS item_7_prices,
				SUM(ic7.remains) + SUM(ic7.arsenal_remains) AS item_7_remains,
				i8.title AS item_8_title, i8.descr AS item_8_descr, i8.articul AS item_8_articul, i8.prices AS item_8_prices,
				SUM(ic8.remains) + SUM(ic8.arsenal_remains) AS item_8_remains,
				i9.title AS item_9_title, i9.descr AS item_9_descr, i9.articul AS item_9_articul, i9.prices AS item_9_prices,
				SUM(ic9.remains) + SUM(ic9.arsenal_remains) AS item_9_remains
			FROM '.$this->sTable.' AS _t
			LEFT JOIN items AS i1 ON (_t.item_1_id = i1.id)
			LEFT JOIN item_colors AS ic1 ON (_t.item_1_id = ic1.item_id)
			LEFT JOIN items AS i2 ON (_t.item_2_id = i2.id)
			LEFT JOIN item_colors AS ic2 ON (_t.item_2_id = ic2.item_id)
			LEFT JOIN items AS i3 ON (_t.item_3_id = i3.id)
			LEFT JOIN item_colors AS ic3 ON (_t.item_3_id = ic3.item_id)
			LEFT JOIN items AS i4 ON (_t.item_4_id = i4.id)
			LEFT JOIN item_colors AS ic4 ON (_t.item_4_id = ic4.item_id)
			LEFT JOIN items AS i5 ON (_t.item_5_id = i5.id)
			LEFT JOIN item_colors AS ic5 ON (_t.item_5_id = ic5.item_id)
			LEFT JOIN items AS i6 ON (_t.item_6_id = i6.id)
			LEFT JOIN item_colors AS ic6 ON (_t.item_6_id = ic6.item_id)
			LEFT JOIN items AS i7 ON (_t.item_7_id = i7.id)
			LEFT JOIN item_colors AS ic7 ON (_t.item_7_id = ic7.item_id)
			LEFT JOIN items AS i8 ON (_t.item_8_id = i8.id)
			LEFT JOIN item_colors AS ic8 ON (_t.item_8_id = ic8.item_id)
			LEFT JOIN items AS i9 ON (_t.item_9_id = i9.id)
			LEFT JOIN item_colors AS ic9 ON (_t.item_9_id = ic9.item_id)
			'.$sWhere.' GROUP BY _t.id '.$sHaving.' ORDER BY _t.'.$this->sOrderField.' '.$this->sOrderDir;

		return CDb::getInstance()->query_array($q);
	}

	public function GetItem($iID)
	{
		$mReturn = parent::GetItem($iID);
		if (!$mReturn) return $mReturn;
		$cKitItems = $this->GetConstructorKitItemsClass($iID);
		$mReturn['kititems'] = $cKitItems->GetList();
		return $mReturn;
	}

	public function GetItemEnabled($iID)
	{
		$q = 'SELECT _t.*,
				i1.title AS item_1_title, i1.descr AS item_1_descr, i1.articul AS item_1_articul, i1.prices AS item_1_prices,
				SUM(ic1.remains) + SUM(ic1.arsenal_remains) AS item_1_remains,
				i2.title AS item_2_title, i2.descr AS item_2_descr, i2.articul AS item_2_articul, i2.prices AS item_2_prices,
				SUM(ic2.remains) + SUM(ic2.arsenal_remains) AS item_2_remains,
				i3.title AS item_3_title, i3.descr AS item_3_descr, i3.articul AS item_3_articul, i3.prices AS item_3_prices,
				SUM(ic3.remains) + SUM(ic3.arsenal_remains) AS item_3_remains,
				i4.title AS item_4_title, i4.descr AS item_4_descr, i4.articul AS item_4_articul, i4.prices AS item_4_prices,
				SUM(ic4.remains) + SUM(ic4.arsenal_remains) AS item_4_remains,
				i5.title AS item_5_title, i5.descr AS item_5_descr, i5.articul AS item_5_articul, i5.prices AS item_5_prices,
				SUM(ic5.remains) + SUM(ic5.arsenal_remains) AS item_5_remains,
				i6.title AS item_6_title, i6.descr AS item_6_descr, i6.articul AS item_6_articul, i6.prices AS item_6_prices,
				SUM(ic6.remains) + SUM(ic6.arsenal_remains) AS item_6_remains,
				i7.title AS item_7_title, i7.descr AS item_7_descr, i7.articul AS item_7_articul, i7.prices AS item_7_prices,
				SUM(ic7.remains) + SUM(ic7.arsenal_remains) AS item_7_remains,
				i8.title AS item_8_title, i8.descr AS item_8_descr, i8.articul AS item_8_articul, i8.prices AS item_8_prices,
				SUM(ic8.remains) + SUM(ic8.arsenal_remains) AS item_8_remains,
				i9.title AS item_9_title, i9.descr AS item_9_descr, i9.articul AS item_9_articul, i9.prices AS item_9_prices,
				SUM(ic9.remains) + SUM(ic9.arsenal_remains) AS item_9_remains
			FROM '.$this->sTable.' AS _t
			LEFT JOIN items AS i1 ON (_t.item_1_id = i1.id)
			LEFT JOIN item_colors AS ic1 ON (_t.item_1_id = ic1.item_id)
			LEFT JOIN items AS i2 ON (_t.item_2_id = i2.id)
			LEFT JOIN item_colors AS ic2 ON (_t.item_2_id = ic2.item_id)
			LEFT JOIN items AS i3 ON (_t.item_3_id = i3.id)
			LEFT JOIN item_colors AS ic3 ON (_t.item_3_id = ic3.item_id)
			LEFT JOIN items AS i4 ON (_t.item_4_id = i4.id)
			LEFT JOIN item_colors AS ic4 ON (_t.item_4_id = ic4.item_id)
			LEFT JOIN items AS i5 ON (_t.item_5_id = i5.id)
			LEFT JOIN item_colors AS ic5 ON (_t.item_5_id = ic5.item_id)
			LEFT JOIN items AS i6 ON (_t.item_6_id = i6.id)
			LEFT JOIN item_colors AS ic6 ON (_t.item_6_id = ic6.item_id)
			LEFT JOIN items AS i7 ON (_t.item_7_id = i7.id)
			LEFT JOIN item_colors AS ic7 ON (_t.item_7_id = ic7.item_id)
			LEFT JOIN items AS i8 ON (_t.item_8_id = i8.id)
			LEFT JOIN item_colors AS ic8 ON (_t.item_8_id = ic8.item_id)
			LEFT JOIN items AS i9 ON (_t.item_9_id = i9.id)
			LEFT JOIN item_colors AS ic9 ON (_t.item_9_id = ic9.item_id)
			WHERE _t.id = '.$iID.' AND _t.enabled = 1 AND
				(i1.enabled = 1 OR i1.enabled IS NULL) AND
				(i2.enabled = 1 OR i2.enabled IS NULL) AND
				(i3.enabled = 1 OR i3.enabled IS NULL) AND
				(i4.enabled = 1 OR i4.enabled IS NULL) AND
				(i5.enabled = 1 OR i5.enabled IS NULL) AND
				(i6.enabled = 1 OR i6.enabled IS NULL) AND
				(i7.enabled = 1 OR i7.enabled IS NULL) AND
				(i8.enabled = 1 OR i8.enabled IS NULL) AND
				(i9.enabled = 1 OR i9.enabled IS NULL)
			GROUP BY _t.id';
		return CDb::getInstance()->query_first($q);
	}

	public function GetItemEnabledWithRemains($iID)
	{
		$q = 'SELECT _t.*,
				i1.title AS item_1_title, i1.descr AS item_1_descr, i1.articul AS item_1_articul, i1.prices AS item_1_prices,
				SUM(ic1.remains) + SUM(ic1.arsenal_remains) AS item_1_remains,
				i2.title AS item_2_title, i2.descr AS item_2_descr, i2.articul AS item_2_articul, i2.prices AS item_2_prices,
				SUM(ic2.remains) + SUM(ic2.arsenal_remains) AS item_2_remains,
				i3.title AS item_3_title, i3.descr AS item_3_descr, i3.articul AS item_3_articul, i3.prices AS item_3_prices,
				SUM(ic3.remains) + SUM(ic3.arsenal_remains) AS item_3_remains,
				i4.title AS item_4_title, i4.descr AS item_4_descr, i4.articul AS item_4_articul, i4.prices AS item_4_prices,
				SUM(ic4.remains) + SUM(ic4.arsenal_remains) AS item_4_remains,
				i5.title AS item_5_title, i5.descr AS item_5_descr, i5.articul AS item_5_articul, i5.prices AS item_5_prices,
				SUM(ic5.remains) + SUM(ic5.arsenal_remains) AS item_5_remains,
				i6.title AS item_6_title, i6.descr AS item_6_descr, i6.articul AS item_6_articul, i6.prices AS item_6_prices,
				SUM(ic6.remains) + SUM(ic6.arsenal_remains) AS item_6_remains,
				i7.title AS item_7_title, i7.descr AS item_7_descr, i7.articul AS item_7_articul, i7.prices AS item_7_prices,
				SUM(ic7.remains) + SUM(ic7.arsenal_remains) AS item_7_remains,
				i8.title AS item_8_title, i8.descr AS item_8_descr, i8.articul AS item_8_articul, i8.prices AS item_8_prices,
				SUM(ic8.remains) + SUM(ic8.arsenal_remains) AS item_8_remains,
				i9.title AS item_9_title, i9.descr AS item_9_descr, i9.articul AS item_9_articul, i9.prices AS item_9_prices,
				SUM(ic9.remains) + SUM(ic9.arsenal_remains) AS item_9_remains
			FROM '.$this->sTable.' AS _t
			LEFT JOIN items AS i1 ON (_t.item_1_id = i1.id)
			LEFT JOIN item_colors AS ic1 ON (_t.item_1_id = ic1.item_id)
			LEFT JOIN items AS i2 ON (_t.item_2_id = i2.id)
			LEFT JOIN item_colors AS ic2 ON (_t.item_2_id = ic2.item_id)
			LEFT JOIN items AS i3 ON (_t.item_3_id = i3.id)
			LEFT JOIN item_colors AS ic3 ON (_t.item_3_id = ic3.item_id)
			LEFT JOIN items AS i4 ON (_t.item_4_id = i4.id)
			LEFT JOIN item_colors AS ic4 ON (_t.item_4_id = ic4.item_id)
			LEFT JOIN items AS i5 ON (_t.item_5_id = i5.id)
			LEFT JOIN item_colors AS ic5 ON (_t.item_5_id = ic5.item_id)
			LEFT JOIN items AS i6 ON (_t.item_6_id = i6.id)
			LEFT JOIN item_colors AS ic6 ON (_t.item_6_id = ic6.item_id)
			LEFT JOIN items AS i7 ON (_t.item_7_id = i7.id)
			LEFT JOIN item_colors AS ic7 ON (_t.item_7_id = ic7.item_id)
			LEFT JOIN items AS i8 ON (_t.item_8_id = i8.id)
			LEFT JOIN item_colors AS ic8 ON (_t.item_8_id = ic8.item_id)
			LEFT JOIN items AS i9 ON (_t.item_9_id = i9.id)
			LEFT JOIN item_colors AS ic9 ON (_t.item_9_id = ic9.item_id)
			WHERE _t.id = '.$iID.' AND _t.enabled = 1 AND
				(i1.enabled = 1 OR i1.enabled IS NULL) AND
				(i2.enabled = 1 OR i2.enabled IS NULL) AND
				(i3.enabled = 1 OR i3.enabled IS NULL) AND
				(i4.enabled = 1 OR i4.enabled IS NULL) AND
				(i5.enabled = 1 OR i5.enabled IS NULL) AND
				(i6.enabled = 1 OR i6.enabled IS NULL) AND
				(i7.enabled = 1 OR i7.enabled IS NULL) AND
				(i8.enabled = 1 OR i8.enabled IS NULL) AND
				(i9.enabled = 1 OR i9.enabled IS NULL)
			GROUP BY _t.id
			HAVING (item_1_remains > 1 OR item_1_remains IS NULL) AND
				(item_2_remains > 1 OR item_2_remains IS NULL) AND
				(item_3_remains > 1 OR item_3_remains IS NULL) AND
				(item_4_remains > 1 OR item_4_remains IS NULL) AND
				(item_5_remains > 1 OR item_5_remains IS NULL) AND
				(item_6_remains > 1 OR item_6_remains IS NULL) AND
				(item_7_remains > 1 OR item_7_remains IS NULL) AND
				(item_8_remains > 1 OR item_8_remains IS NULL) AND
				(item_9_remains > 1 OR item_9_remains IS NULL)';
		return CDb::getInstance()->query_first($q);
	}

	function GetKitItems($aIDs)
	{
		return $this->GetConstructorKitItemsClass(null)->GetFullListByIDs($aIDs);
	}

	/**
	* types
	* 1 - image
	* 2 - configurator scheme
	* 3 - sizes image
	*/
	public static function GetLink($iID, $iType, $bFull = false)
	{
		$sUrl = ($bFull ? $_SERVER['DOCUMENT_ROOT'] : '').'/static/constructoritems/'.($iID%100).'/'.$iID;
		if ($iType == 2) {
			$sUrl .= '.svg';
		} elseif ($iType == 3) {
			$sUrl .= '_sizes.jpg';
		} else {
			$sUrl .= '.jpg';
		}
		return $sUrl;
	}

}

?>
