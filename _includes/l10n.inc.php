<?

return array('ru' =>
	array (
		'ERROR_EMPTY_VALUE' => 'Значение не указано',
		'ERROR_SHORT_VALUE' => 'Короткое значение',
		'ERROR_LONG_VALUE' => 'Длинное значение',
		'ERROR_WRONG_VALUE' => 'Неверное значение',
		'ERROR_SMALL_VALUE' => 'Маленькое значение',
		'ERROR_BIG_VALUE' => 'Большое значение',
		'ERROR_NONEXIST_VALUE' => 'Недопустимое значение',
		'ERROR_SOME_ERRORS' => 'Обнаружены ошибки',
		'ERROR_USERNAME_EXISTS' => 'Такой логин уже зарегистрирован',
		'ERROR_EMAIL_EXISTS' => 'Такой E-mail уже зарегистрирован',
		'ERROR_NO_USER'=>'Нет такого пользователя',
		'ERROR_WRONG_PASSWORD'=>'Неверный пароль',
		'ERROR_PASSWORD_MISMATCH'=>'Пароль не совпадаeт',
		'ERROR_EMAIL_MISMATCH'=>'EMail не совпадает',
		'ERROR_FILE_ERROR'=>'Ошибка работы с файлом',
		'ERROR_FILE_UPLOAD_ERROR'=>'Ошибка загрузки файла',
		'ERROR_MAIL_ERROR'=>'Ошибка отправки',
		'ERROR_CAPTCHA' => 'Неверные проверочные цифры',
		'ERROR_CANNOT_MOVE_SELF' => 'Нельзя перенести в себя или в дочерние категории',
		'PAGE_URL_EXISTS' => 'Такой адрес уже существует',
		'NO_SUCH_ADMIN' => 'Такого администратора не существует',
		'NO_RIGHTS' => 'Нет прав',
		'DB_ERROR' => 'Ошибка базы данных',
		'TIME_SEC' => ' сек',
		'TIME_MIN' => ' мин',
		'TIME_HOUR' => ' ч',
		'SIZE_BYTES' => ' б',
		'SIZE_KBYTES' => ' Кб',
		'SIZE_MBYTES' => ' Мб',
		'SIZE_GBYTES' => ' Гб',
		'COUNT_ITEMS_1' => 'товар',
		'COUNT_ITEMS_2-4' => 'товара',
		'COUNT_ITEMS' => 'товаров'
	)
);

?>