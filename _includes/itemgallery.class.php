<?

class CItemGallery extends CContent
{
	protected $sTable = 'item_gallery';
	protected $sOrderField = 'ord';
	protected $sOrderDir = 'ASC';

	protected $aCheckRules = array(
		'item_id' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
		'title' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 250)),
		'enabled' => array('type' => CChecker::TYPE_INT, 'flags' => 6, 'params'=>array('min' => 0, 'max' => 1)),
		'video' => array('type' => CChecker::TYPE_STRING,'flags' => 4, 'params' => array('max' => 32000))
	);

	private static $_instance;

	/**
	 * Item gallery class
	 *
	 * @return CItemGallery
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	public function Add($aData, $sFilename, $sLargeFilename, $sOrigFilename)
	{
		if (!$iID = parent::Add($aData)) return false;

		if ($sFilename && !$this->_processImage($iID, $sFilename)) {
			$this->Delete($iID);
			CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('image' => L10N('ERROR_FILE_ERROR')));
			return false;
		}
		if ($sLargeFilename && !$this->_processLargeImage($iID, $sLargeFilename)) {
			$this->Delete($iID);
			CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('large_image' => L10N('ERROR_FILE_ERROR')));
			return false;
		}
		if ($sOrigFilename && !$this->_processOrigImage($iID, $sOrigFilename)) {
			$this->Delete($iID);
			CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('orig_image' => L10N('ERROR_FILE_ERROR')));
			return false;
		}
		return $iID;
	}

	public function Update($iID, $aData, $sFilename, $sLargeFilename, $sOrigFilename)
	{
		unset($this->aCheckRules['item_id']);
		if (!parent::Update($iID, $aData)) return false;

		if ($sFilename && !$this->_processImage($iID, $sFilename)) {
			CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('image' => L10N('ERROR_FILE_ERROR')));
			return false;
		}
		if ($sLargeFilename && !$this->_processLargeImage($iID, $sLargeFilename)) {
			CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('large_image' => L10N('ERROR_FILE_ERROR')));
			return false;
		}
		if ($sOrigFilename && !$this->_processOrigImage($iID, $sOrigFilename)) {
			CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('orig_image' => L10N('ERROR_FILE_ERROR')));
			return false;
		}
		return true;
	}

	private function _processImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, 2, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		$cImage = new CImage();
		$cImage->SetSource($sFilename);
		$cImage->SetDestionation(self::GetLink($iID, 2, true));
		$aParams = array(
			'to_jpeg' => true,
			'quality' => 100,
			'crop' => true,
			'strict' => true
		);
		if (!$cImage->Resize(98, 98, $aParams)) {
			$this->RemoveFiles($iID);
			return false;
		}
		$cImage->SetDestionation(self::GetLink($iID, 3, true));
		$aParams = array(
			'to_jpeg' => true,
			'quality' => 100,
			'crop' => true,
			'strict' => true
		);
		if (!$cImage->Resize(165, 165, $aParams)) {
			$this->RemoveFiles($iID);
			return false;
		}

		return true;
	}

	private function _processLargeImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, 1, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		if (!@copy($sFilename, self::GetLink($iID, 1, true))) {
			$this->RemoveFiles($iID);
			return false;
		}

		return true;
	}

	private function _processOrigImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, 4, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		if (!@copy($sFilename, self::GetLink($iID, 4, true))) {
			$this->RemoveFiles($iID);
			return false;
		}

		return true;
	}

	function SwitchStatus($iID)
	{
		$q = 'UPDATE '.$this->sTable.' SET enabled = 1-enabled WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	function SwitchMain($iItemID, $iID)
	{
		$q = 'UPDATE '.$this->sTable.' SET main = 0 WHERE item_id = '.$iItemID;
		if (!CDb::getInstance()->query($q)) return false;

		$q = 'UPDATE '.$this->sTable.' SET main = 1 WHERE item_id = '.$iItemID.' AND id='.$iID;
		return CDb::getInstance()->query($q);
	}

	function SetOrder($iID, $iOrd)
	{
		$q = 'UPDATE '.$this->sTable.' SET ord = '.$iOrd.' WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	public function RemoveFiles($iID)
	{
		@unlink(self::GetLink($iID, 1, true));
		@unlink(self::GetLink($iID, 2, true));
		@unlink(self::GetLink($iID, 3, true));
		@unlink(self::GetLink($iID, 4, true));
	}

	public function CopyFiles($iID, $iNewID)
	{
		$sPath = dirname(self::GetLink($iNewID, 1, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}
		@copy(self::GetLink($iID, 1, true), self::GetLink($iNewID, 1, true));
		@copy(self::GetLink($iID, 2, true), self::GetLink($iNewID, 2, true));
		@copy(self::GetLink($iID, 3, true), self::GetLink($iNewID, 3, true));
		@copy(self::GetLink($iID, 4, true), self::GetLink($iNewID, 4, true));
	}

	function Delete($iID)
	{
		if (!parent::Delete($iID)) return false;

		$this->RemoveFiles($iID);

		return true;
	}

	function GetList(CSinfo &$pcSInfo)
	{
		$aWhere = array();
		$sWhere = '';

		if ($pcSInfo->item_id) $aWhere[] = 'item_id = '.$pcSInfo->item_id;
		if ($pcSInfo->enabled) $aWhere[] = 'enabled = 1';
		if ($aWhere) $sWhere = join(' AND ', $aWhere);

		return parent::GetList($pcSInfo,$sWhere);
	}

	function GetMainForItem($iItemID)
	{
		$q = 'SELECT id FROM '.$this->sTable.' WHERE item_id = '.$iItemID.' AND main = 1';
		return CDb::getInstance()->query_field($q);
	}

	/**
	 * type
	 * 1 - large
	 * 2 - thumb
	 * 3 - medium
	 * 4 - original
	 */
	public static function GetLink($iID, $iType = 1, $bFull = false)
	{
		$sUrl = ($bFull?$_SERVER['DOCUMENT_ROOT']:'').'/static/itemgallery/'.($iID%100).'/'.$iID;
		switch ($iType) {
			case 2:
				$sUrl .= '_thumb';
				break;
			case 3:
				$sUrl .= '_medium';
				break;
			case 4:
				$sUrl .= '_orig';
				break;
		}
		$sUrl .= '.jpg';
		return $sUrl;
	}

}

?>
