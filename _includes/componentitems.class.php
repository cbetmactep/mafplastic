<?

class CComponentItems
{
	private $sTable = 'component_items';
	private $iItemID;

	protected $aCheckRules = array(
		'item_id' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
		'component_id' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
		'ord' => array('type' => CChecker::TYPE_INT),
		'quant' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
	);

	function __construct($iItemID = null) {
		$this->iItemID = $iItemID;
	}

	function Add($aData)
	{
		if ($this->iItemID) $aData['item_id'] = $this->iItemID;
		$aData = array_intersect_key($aData,$this->aCheckRules);
		if (!CChecker::CheckArray($aData,$this->aCheckRules)) {
			return false;
		}
		return CDb::getInstance()->insert($this->sTable,$aData);
	}

	function Clear()
	{
		return CDb::getInstance()->delete($this->sTable, array('item_id' => $this->iItemID));
	}

	function GetList()
	{
		$q = 'SELECT component_id as id, quant FROM '.$this->sTable.' WHERE item_id = '.$this->iItemID.' ORDER BY ord ASC';
		$oRes = CDb::getInstance()->query($q);
		if (!$oRes) return false;
		$aReturn = array();
		while ($aRow = CDb::getInstance()->fetch_array($oRes)) {
			$aReturn[$aRow['id']] = $aRow;
		}
		return $aReturn;
	}

	function GetRelList()
	{
		$q = 'SELECT * FROM '.$this->sTable.' WHERE component_id = '.$this->iItemID.' ORDER BY ord ASC';
		$oRes = CDb::getInstance()->query($q);
		if (!$oRes) return false;
		$aReturn = array();
		while ($aRow = CDb::getInstance()->fetch_array($oRes)) {
			$aReturn[] = $aRow['item_id'];
		}
		return $aReturn;
	}

}

?>
