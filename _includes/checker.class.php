<?

class CChecker
{
	private static $sLastError;
	private static $aLastErrorFields;

	const TYPE_STRING = 1;
	const TYPE_INT = 2;
	const TYPE_FLOAT = 3;
	const TYPE_CAPTCHA = 4;
	const TYPE_ARRAY = 5;

	const FLAG_CHECK_EMPTY = 1;
	const FLAG_CHECK_MIN = 2;
	const FLAG_CHECK_MAX = 4;
	const FLAG_CHECK_PREG = 8;
	const FLAG_CHECK_EREG = 16;
	const FLAG_CHECK_EXIST_IN_ARRAY = 32;
	const FLAG_CHECK_DATE = 64;
	const FLAG_CHECK_DATETIME = 128;

	const FLAG_NOHTML = 1024;

	const FLAG_SET_NULL_IF_0 = 8192;
	const FLAG_SET_NULL_IF_EMPTY = 32768;

	const FLAG_RETURN_DATE_INT = 16384;

	const PREG_URL = '~^(?:(?:https?|ftp|telnet)://(?:[a-z0-9_-]{1,32}(?::[a-z0-9_-]{1,32})?@)?)(?:(?:[a-z0-9-]{1,128}\.)+(?:com|net|org|mil|edu|arpa|gov|biz|info|aero|inc|name|mmr|[a-z]{2})|(?!0)(?:(?!0[^.]|255)[0-9]{1,3}\.){3}(?!0|255)[0-9]{1,3})(?:/[a-z0-9.,_@%&?+=\~/-]*)?(?:#[^ \'\"&<>]*)?$~i';
	const PREG_WORD = '/^[\w\._-]*$/is';
	const PREG_NUM_PERC = '/^\d*%?$/is';
	const PREG_PATH = '/^[\w:\._\/()-]*$/is';
	const PREG_USERNAME = '/^[\w_-]*$/is';
	const PREG_MD5_REPLACE = '/[^a-z0-9]/is';
	const PREG_EMAIL = '/^[_\.\+0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,6}$/is';
	const PREG_DATE = '/^\d{1,2}\.\d{1,2}\.\d{2,4}$/is';
	const PREG_TIME = '/^\d{1,2}:\d{1,2}$/is';
	const PREG_COLOR = '/^[0-9a-fA-F]{6}$/is';
	const EREG_USERNAME = '^[a-z0-9а-яА-ЯёЁ_ \.-]+$';
	const EREG_TAGS = '^[a-z0-9а-яА-ЯёЁ_ -]+$';
	const EREG_TAGS_REPLACE = '[^"a-z0-9а-яА-ЯёЁ_ ,-]';

	function CheckString(&$pmValue,$iCheckFlags = 0,$aParams = null)
	{
		$pmValue = trim($pmValue);
		$iLength = mb_strlen($pmValue);

		if ($iCheckFlags & self::FLAG_CHECK_EMPTY && $iLength == 0) return L10N('ERROR_EMPTY_VALUE');
		if ($iCheckFlags & self::FLAG_SET_NULL_IF_0 && $iLength == 0) {
			$pmValue = null;
			return null;
		}
		if ($iCheckFlags & self::FLAG_CHECK_MIN && $iLength < $aParams['min']) return L10N('ERROR_SHORT_VALUE');
		if ($iCheckFlags & self::FLAG_CHECK_MAX && $iLength > $aParams['max']) return L10N('ERROR_LONG_VALUE');
		if ($iCheckFlags & self::FLAG_CHECK_PREG && $iLength && !preg_match($aParams['preg'],$pmValue)) return L10N('ERROR_WRONG_VALUE');
		if ($iCheckFlags & self::FLAG_CHECK_EREG && $iLength && !mb_eregi($aParams['ereg'],$pmValue)) return L10N('ERROR_WRONG_VALUE');
		if ($iCheckFlags & self::FLAG_CHECK_EXIST_IN_ARRAY && !in_array($pmValue,$aParams['values_array'])) return L10N('ERROR_NONEXIST_VALUE');
		if ($iCheckFlags & self::FLAG_CHECK_DATE) {
			if (preg_match('/^\d{1,2}\/\d{1,2}\/\d{2,4}$/',$pmValue)) $pmValue = str_replace('/','.',$pmValue);
			if (!$pmValue = strtotime($pmValue)) return L10N('ERROR_WRONG_VALUE');
			if(!($iCheckFlags & self::FLAG_RETURN_DATE_INT)) $pmValue = date('Y-m-d',$pmValue);
		}
		if ($iCheckFlags & self::FLAG_CHECK_DATETIME) {
			if (preg_match('/^\d{1,2}\/\d{1,2}\/\d{2,4}\s/',$pmValue)) $pmValue = str_replace('/','.',$pmValue);
			if (!$pmValue = strtotime($pmValue)) return L10N('ERROR_WRONG_VALUE');
			if(!($iCheckFlags & self::FLAG_RETURN_DATE_INT)) $pmValue = date('Y-m-d H:i:s',$pmValue);
		}
		if ($iCheckFlags & self::FLAG_NOHTML) $pmValue = strip_tags($pmValue);
		return null;
	}

	function CheckNumeric(&$pmValue,$iCheckFlags = 0,$aParams = null,$bIsFloat = false)
	{
		if ((!isset($pmValue) || $pmValue === '') && $iCheckFlags & self::FLAG_SET_NULL_IF_EMPTY) {
			$pmValue = null;
			return null;
		}
		if ($bIsFloat) $pmValue = (float)$pmValue;
		else $pmValue = (int)$pmValue;

		if ($iCheckFlags & self::FLAG_CHECK_EMPTY && !$pmValue) return L10N('ERROR_EMPTY_VALUE');
		if ($iCheckFlags & self::FLAG_CHECK_MIN && $pmValue < $aParams['min']) return L10N('ERROR_SMALL_VALUE');
		if ($iCheckFlags & self::FLAG_CHECK_MAX && $pmValue > $aParams['max']) return L10N('ERROR_BIG_VALUE');
		if ($iCheckFlags & self::FLAG_CHECK_EXIST_IN_ARRAY && !in_array($pmValue,$aParams['values_array'])) return L10N('ERROR_NONEXIST_VALUE');
		if ($iCheckFlags & self::FLAG_SET_NULL_IF_0 && !$pmValue) $pmValue = null;
		return null;
	}

	function CheckArray(&$paValues, &$paCheckRules)
	{
		foreach ($paCheckRules as $sField=>$aCheckRule) {
			switch ($aCheckRule['type']) {
				case self::TYPE_STRING:
					$mErr = self::CheckString($paValues[$sField],$aCheckRule['flags'],$aCheckRule['params']);
					break;
				case self::TYPE_INT:
				case self::TYPE_FLOAT:
					$mErr = self::CheckNumeric($paValues[$sField],$aCheckRule['flags'],$aCheckRule['params'],($aCheckRule['type'] == self::TYPE_FLOAT?true:false));
					break;
				case self::TYPE_CAPTCHA:
					if (!$_SESSION['CAPTCHAString'] || $_SESSION['CAPTCHAString'] != $paValues[$sField]) $mErr = L10N('ERROR_CAPTCHA');
					unset($paValues[$sField]);
					break;
				case self::TYPE_ARRAY:
					if (empty($paValues[$sField])) {
						if ($aCheckRule['flags'] & self::FLAG_CHECK_EMPTY) $mErr = L10N('ERROR_EMPTY_VALUE');
					} else {
						foreach ($paValues[$sField] as $mKey => &$aValue) {
							switch ($aCheckRule['inttype']) {
								case self::TYPE_STRING:
									$sErr = self::CheckString($aValue,$aCheckRule['flags'],$aCheckRule['params']);
									if (!empty($sErr)) $mErr[$mKey] = $sErr;
									break;
								case self::TYPE_INT:
								case self::TYPE_FLOAT:
									$sErr = self::CheckNumeric($aValue,$aCheckRule['flags'],$aCheckRule['params'],($aCheckRule['inttype'] == self::TYPE_FLOAT?true:false));
									if (!empty($sErr)) $mErr[$mKey] = $sErr;
									break;
							}
						}
						unset($aValue);
					}
					break;
			}
			if ($mErr) $aErr[$sField] = $mErr;
			unset($mErr);
		}
		if ($aErr) {
			self::$sLastError = L10N('ERROR_SOME_ERRORS');
			self::$aLastErrorFields = $aErr;
			return false;
		}
		return true;
	}

	public function SetLastErrors($sError = null, $aErrorFields = null)
	{
		if ($sError) self::$sLastError = L10N($sError);
		if (is_array($aErrorFields)) {
			foreach ($aErrorFields as $sField => $mErrors) {
				if (is_array($mErrors)) {
					foreach ($mErrors as $sKey => $sError) {
						self::$aLastErrorFields[$sField][$sKey] = L10N($sError);
					}
				} else self::$aLastErrorFields[$sField] = L10N($mErrors);
			}
		}
	}

	public function GetLastError()
	{
		return self::$sLastError;
	}

	public function GetLastErrorFields()
	{
		return self::$aLastErrorFields;
	}

/*	function ValidateHTML(&$psValue,$aRemoveTags = array()) {
		require_once($_SERVER['DOCUMENT_ROOT'].'/_includes/_ext/SafeHTML.class.php');
		$cSafeHTML = new SafeHTML();
		$cSafeHTML->deleteTags = array_merge($cSafeHTML->deleteTags,$aRemoveTags);
		$psValue = $cSafeHTML->parse($psValue);
		unset($cSafeHTML);
	}*/

}

?>