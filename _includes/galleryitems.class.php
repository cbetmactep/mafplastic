<?

class CGalleryItems
{
	private $sTable = 'gallery_items';
	private $iGalleryID;

	protected $aCheckRules = array(
		'gallery_id' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
		'item_id' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
		'ord' => array('type' => CChecker::TYPE_INT)
	);

	function __construct($iGalleryID = null) {
		$this->iGalleryID = $iGalleryID;
	}

	function Add($aData)
	{
		if ($this->iGalleryID) $aData['gallery_id'] = $this->iGalleryID;
		$aData = array_intersect_key($aData,$this->aCheckRules);
		if (!CChecker::CheckArray($aData,$this->aCheckRules)) {
			return false;
		}

		$db = CDb::getInstance();
		return CDb::getInstance()->insert($this->sTable,$aData);
	}

	function Clear()
	{
		return CDb::getInstance()->delete($this->sTable, array('gallery_id' => $this->iGalleryID));
	}

	function GetList()
	{
		$q = 'SELECT * FROM '.$this->sTable.' WHERE gallery_id = '.$this->iGalleryID.' ORDER BY ord ASC';
		$oRes = CDb::getInstance()->query($q);
		if (!$oRes) return false;
		$aReturn = array();
		while ($aRow = CDb::getInstance()->fetch_array($oRes)) {
			$aReturn[] = $aRow['item_id'];
		}
		return $aReturn;
	}

	function GetFullList($aItemIds)
	{
		$q = 'SELECT _t.gallery_id, i.* FROM '.$this->sTable.' AS _t
			LEFT JOIN items AS i ON (_t.item_id = i.id)
			WHERE i.enabled = 1 AND _t.gallery_id IN ('.join(',', $aItemIds).')';
		$oRes = CDb::getInstance()->query($q);
		if (!$oRes) return false;
		$aReturn = array();
		while ($aRow = CDb::getInstance()->fetch_array($oRes)) {
			$aReturn[$aRow['gallery_id']][] = $aRow;
		}
		return $aReturn;
	}
	
}

?>
