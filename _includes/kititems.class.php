<?

class CKitItems extends CContent
{
	protected $sTable = 'kit_items';
	protected $sOrderField = 'ord';
	protected $sOrderDir = 'ASC';

	protected $aCheckRules = array(
		'title' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 300)),
		'articul' => array('type' => CChecker::TYPE_STRING, 'flags' => 1028, 'params' => array('max' => 200)),
		'price' => array('type' => CChecker::TYPE_FLOAT),
		'descr' => array('type' => CChecker::TYPE_STRING,'flags' => 5, 'params' => array('max' => 1024000)),
		'enabled' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1))
	);

	/*private $aPriceCheckRules = array(
		'title' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 50)),
		'from' => array('type' => CChecker::TYPE_INT,'flags' => 8192),
		'to' => array('type' => CChecker::TYPE_INT,'flags' => 8192),
		'price' => array('type' => CChecker::TYPE_FLOAT)
	);*/

	private static $_instance;

	/**
	 * Kit items class
	 *
	 * @return CKitItems
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	public function Add($aData, $sFilename)
	{
		$db = CDb::getInstance();
		$db->begin();

		//$this->_processData($aData);

		$iNewID = parent::Add($aData);
		if (empty($iNewID)) {
			$db->rollback();
			return false;
		}

		if ($sFilename && !$this->_uploadImage($iNewID, $sFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image' => 'ERROR_FILE_ERROR'));
			return false;
		}

		$db->commit();
		return $iNewID;
	}

	public function Update($iID, $aData, $sFilename)
	{
		$db = CDb::getInstance();
		$db->begin();

		//$this->_processData($aData);

		if (!parent::Update($iID, $aData)) {
			$db->rollback();
			return false;
		}

		if ($sFilename && !$this->_uploadImage($iID, $sFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image' => 'ERROR_FILE_ERROR'));
			return false;
		}

		$db->commit();
		return true;
	}

	/*private function _processData(&$paData)
	{
		$paData['prices'] = (array)$paData['prices'];
		$aPrices = array();
		foreach ($paData['prices'] as $aPrice) {
			if (CChecker::CheckArray($aPrice, $this->aPriceCheckRules)) {
				$aPrices[] = $aPrice;
			}
		}
		$paData['prices'] = json_encode($aPrices);
	}*/

	function SwitchStatus($iID)
	{
		$q = 'UPDATE '.$this->sTable.' SET enabled = 1 - enabled WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	function SetOrder($iID, $iOrd)
	{
		$q = 'UPDATE '.$this->sTable.' SET ord = '.$iOrd.' WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	public function UpdatePrice($iID, $fPrice)
	{
		$q = 'UPDATE '.$this->sTable.' SET price = '.$fPrice.' WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	public function Delete($iID)
	{
		if (!parent::Delete($iID)) return false;

		$this->RemoveFiles($iID);

		return true;
	}

	public function RemoveFiles($iID)
	{
		@unlink(self::GetLink($iID, 1, true));
		@unlink(self::GetLink($iID, 2, true));
	}

	private function _uploadImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, 1, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		if (!@copy($sFilename, self::GetLink($iID, 1, true))) {
			$this->RemoveFiles($iID);
			return false;
		}

		$cImage = new CImage();
		$cImage->SetSource($sFilename);
		$aParams = array(
			'quality' => 100,
			'crop' => false
		);
		$cImage->SetDestionation(self::GetLink($iID, 2, true));
		if (!$cImage->Resize(394, 1000, $aParams)) {
			$this->RemoveFiles($iID);
			return false;
		}

		return true;
	}

	public function GetList(CSInfo $cSInfo)
	{
		$aWhere = array();
		if ($cSInfo->enabled_only) $aWhere[] = '_t.enabled = 1';
		if ($cSInfo->ids) $aWhere[] = '_t.id IN ('.join(',',$cSInfo->ids).')';

		if ($aWhere) {
			$sWhere = join(' AND ', $aWhere);
		} else {
			$sWhere = '';
		}

		return parent::GetList($cSInfo, $sWhere);
	}

	/*public function GetItem($iID)
	{
		$mReturn = parent::GetItem($iID);
		if (!$mReturn) return $mReturn;
		$mReturn['prices'] = json_decode($mReturn['prices'], true);
		return $mReturn;
	}*/

	public function GetItemByArticul($sArticul)
	{
		if (CChecker::CheckString($sArticul, $this->aCheckRules['articul']['flags'], $this->aCheckRules['articul']['params'])) {
			return false;
		}
		$q = 'SELECT * FROM '.$this->sTable.' WHERE articul = \''.CDb::getInstance()->escape($sArticul).'\'';
		return CDb::getInstance()->query_first($q);
	}

	/**
	* types
	* 1 - original
	* 2 - thumb
	*/
	public static function GetLink($iID, $iType, $bFull = false)
	{
		$sUrl = ($bFull ? $_SERVER['DOCUMENT_ROOT'] : '').'/static/kititems/'.($iID%100).'/'.$iID;
		if ($iType == 2) {
			$sUrl .= '_thumb.jpg';
		} else {
			$sUrl .= '.jpg';
		}
		return $sUrl;
	}

}

?>
