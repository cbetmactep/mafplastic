<?

class CCats extends CContent
{
	protected $sTable = 'cats';
	protected $sOrderField = 'ord';
	protected $sOrderDir = 'ASC';

	protected $aCheckRules = array(
		'parent_id' => array('type' => CChecker::TYPE_INT, 'flags' => 8192),
		'title' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 200)),
		'page_title' => array('type' => CChecker::TYPE_STRING, 'flags' => 1028, 'params' => array('max' => 255)),
		'page_description' => array('type' => CChecker::TYPE_STRING, 'flags' => 1028, 'params' => array('max' => 255)),
		'page_keywords' => array('type' => CChecker::TYPE_STRING, 'flags' => 1028, 'params' => array('max' => 255)),
		'enabled' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1))
	);

	private static $_instance;

	/**
	 * Cats class
	 *
	 * @return CCats
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	public function Add($aData, $sFilename)
	{
		if (!parent::Add($aData)) {
			return false;
		}

		if ($sFilename && !$this->_uploadImage($aData['id'], $sFilename)) {
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image' => 'ERROR_FILE_ERROR'));
			return false;
		}

		return true;
	}

	public function Update($iID, $aData, $sFilename)
	{
		unset($this->aCheckRules['parent_id']);
		if (!parent::Update($iID, $aData)) {
			return false;
		}

		if ($sFilename && !$this->_uploadImage($iID, $sFilename)) {
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image' => 'ERROR_FILE_ERROR'));
			return false;
		}

		return true;
	}

	function SwitchStatus($iID)
	{
		$q = 'UPDATE '.$this->sTable.' SET enabled = 1 - enabled WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	function SetOrder($iID, $iOrd)
	{
		$q = 'UPDATE '.$this->sTable.' SET ord = '.$iOrd.' WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	public function Delete($iID)
	{
		if (!parent::Delete($iID)) return false;

		$this->RemoveImage($iID);

		return true;
	}

	public function RemoveImage($iID)
	{
		@unlink(self::GetLink($iID, true));
	}

	private function _uploadImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		$cImage = new CImage();
		$cImage->SetSource($sFilename);
		$aParams = array(
			'to_jpeg' => true,
			'quality' => 100,
			'crop' => true,
			'strict' => true
		);
		$cImage->SetDestionation(self::GetLink($iID, true));
		if (!$cImage->Resize(165, 165, $aParams)) {
			$this->RemoveImage($iID);
			return false;
		}

		return true;
	}

	public function GetList($cSInfo)
	{
		$aWhere = array();
		$sWhere = '';

		if ($cSInfo->parent_id) $aWhere[] = 'parent_id=' . $cSInfo->parent_id;
		if ($cSInfo->only_top_level) $aWhere[] = 'parent_id IS NULL';
		if ($cSInfo->ids) $aWhere[] = 'id IN (' . join(',', $cSInfo->ids) . ')';
		if ($cSInfo->enabled_only) $aWhere[] = 'enabled = 1';

		if ($aWhere) $sWhere = ' WHERE ' . join(' AND ', $aWhere);

		$q = 'SELECT * FROM '.$this->sTable.$sWhere.' ORDER BY '.$this->sOrderField.' '.$this->sOrderDir;
		if (!$res = CDb::getInstance()->query($q)) return false;
		$aReturn = array();
		while ($aRow = CDb::getInstance()->fetch_array($res)) {
			if ($cSInfo->hierarchy) $aReturn[(int)$aRow['parent_id']][$aRow['id']] = $aRow;
			else $aReturn[$aRow['id']] = $aRow;
		}
		return $aReturn;
	}

	public static function GetLink($iID, $bFull = false)
	{
		return ($bFull?$_SERVER['DOCUMENT_ROOT']:'').'/static/cats/'.($iID%100).'/'.$iID.'.jpg';
	}

}

?>
