<?

class COrders extends CContent
{
	protected $sTable = 'orders';
	protected $sOrderField = 'id';
	protected $sOrderDir = 'DESC';

	protected $aCheckRules = array(
		'user_id' => array('type'=>CChecker::TYPE_INT,'flags'=>8192),
		'date' => array('type'=>CChecker::TYPE_STRING),
		'name' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 250)),
		'phone' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 100)),
		'email' => array('type' => CChecker::TYPE_STRING, 'flags' => 12, 'params' => array('max' => 250, 'preg' => CChecker::PREG_EMAIL)),
		'add_info' => array('type' => CChecker::TYPE_STRING, 'flags' => 1028, 'params' => array('max' => 16000)),
		'params' => array('type' => CChecker::TYPE_STRING),
		'total_price' => array('type'=>CChecker::TYPE_FLOAT),
		'total_count' => array('type'=>CChecker::TYPE_INT)
	);

	private static $_instance;

	/**
	 * Orders class
	 *
	 * @return COrders
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	public function Add($aData)
	{
		$aData['date'] = date('Y-m-d H:i:s');
		$aData['params'] = json_encode($aData['params']);
		if ($aData['send_me']) {
			$this->aCheckRules['email']['flags'] += 1;
		}

		return parent::Add($aData);
	}

	public function Update($iID, $aData)
	{
		return false;
	}

	public function Delete($iID)
	{
		return false;
	}

	public function GetItem($iID)
	{
		$aItem = parent::GetItem($iID);
		if ($aItem) {
			$aItem['params'] = json_decode($aItem['params'], true);
		}
		return $aItem;
	}

	public function GetList(CSInfo $cSInfo)
	{
		$aWhere = array();
		if ($cSInfo->user_id) {
			$aWhere[] = 'user_id = '.$cSInfo->user_id;
		}

		if ($aWhere) {
			$sWhere = join(' AND ', $aWhere);
		} else {
			$sWhere = '';
		}

		$aList = parent::GetList($cSInfo, $sWhere);
		if ($aList) foreach ($aList as &$aItem) {
			$aItem['params'] = json_decode($aItem['params'], true);
		}
		unset($aItem);
		return $aList;
	}
}

?>
