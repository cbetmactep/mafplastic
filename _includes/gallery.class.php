<?

class CGallery extends CContent
{
	protected $sTable = 'gallery';
	protected $sOrderField = 'ord';
	protected $sOrderDir = 'ASC';

	protected $aCheckRules = array(
		'title' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 250)),
		'enabled' => array('type' => CChecker::TYPE_INT, 'flags' => 6, 'params'=>array('min' => 0, 'max' => 1)),
		'video' => array('type' => CChecker::TYPE_STRING,'flags' => 4, 'params' => array('max' => 32000))
	);

	private static $_instance;

	/**
	 * Gallery class
	 *
	 * @return CGallery
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	/**
	 * Items factory
	 *
	 * @param int $iGalleryID
	 * @return CGalleryItems
	 */
	private function GetItemsClass($iGalleryID)
	{
		return new CGalleryItems($iGalleryID);
	}

	public function Add($aData, $sFilename)
	{
		$db = CDb::getInstance();
		$db->begin();

		$aItems = $aData['items'];
		$iImageWidth = $aData['image_width'];
		$iImageHeight = $aData['image_height'];

		if (!$iID = parent::Add($aData)) return false;

		if ($sFilename && !$this->_processImage($iID, $sFilename, $iImageWidth, $iImageHeight)) {
			$db->rollback();
			CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('image' => L10N('ERROR_FILE_ERROR')));
			return false;
		}

		if (!$this->_updateImgInfo($iID)) {
			$db->rollback();
			CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('image' => L10N('ERROR_FILE_ERROR')));
			return false;
		}

		if (!$this->_updateItems($iID, $aItems)) {
			$db->rollback();
			return false;
		}

		$db->commit();
		return true;
	}

	public function Update($iID, $aData, $sFilename)
	{
		$db = CDb::getInstance();
		$db->begin();

		$aItems = $aData['items'];
		$iImageWidth = $aData['image_width'];
		$iImageHeight = $aData['image_height'];

		if (!parent::Update($iID, $aData)) return false;

		if ($sFilename) {
			if (!$this->_processImage($iID, $sFilename, $iImageWidth, $iImageHeight)) {
				$db->rollback();
				CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('image' => L10N('ERROR_FILE_ERROR')));
				return false;
			}
		} else {
			if (!$this->_resizeImage($iID, $iImageWidth, $iImageHeight)) {
				$db->rollback();
				CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('image' => L10N('ERROR_FILE_ERROR')));
				return false;
			}
		}

		if (!$this->_updateImgInfo($iID)) {
			$db->rollback();
			CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('image' => L10N('ERROR_FILE_ERROR')));
			return false;
		}

		if (!$this->_updateItems($iID, $aItems)) {
			$db->rollback();
			return false;
		}

		$db->commit();
		return true;
	}

	private function _processImage($iID, $sFilename, $iImageWidth, $iImageHeight)
	{
		$sPath = dirname(self::GetLink($iID, 1, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		if (!@copy($sFilename, self::GetLink($iID, 1, true))) {
			$this->RemoveFiles($iID);
			return false;
		}

		return $this->_resizeImage($iID, $iImageWidth, $iImageHeight);
	}

	private function _resizeImage($iID, $iImageWidth, $iImageHeight)
	{
		$iImageWidth = (int)$iImageWidth;
		if ($iImageWidth < 1 || $iImageWidth > 10) $iImageWidth = 1;
		$iImageHeight = (int)$iImageHeight;
		if ($iImageHeight < 1 || $iImageHeight > 5) $iImageHeight = 1;

		$iImageWidth = 128 * $iImageWidth - 18;
		$iImageHeight = 128 * $iImageHeight - 18;

		$cImage = new CImage();
		$cImage->SetSource(self::GetLink($iID, 1, true));
		$cImage->SetDestionation(self::GetLink($iID, 2, true));
		$aParams = array(
			'to_jpeg' => true,
			'quality' => 85,
			'crop' => true,
			'strict' => true
		);
		if (!$cImage->Resize($iImageWidth, $iImageHeight, $aParams)) {
			$this->RemoveFiles($iID);
			return false;
		}

		return true;
	}

	private function _updateImgInfo($iID)
	{
		$aImgInfo = @getimagesize(self::GetLink($iID, 2, true));
		$aData = array(
			'width' => $aImgInfo[0],
			'height' => $aImgInfo[1]
		);
		return CDb::getInstance()->update($this->sTable, $aData, array('id' => $iID));
	}

	private function _updateItems($iID, $aItems)
	{
		$cItems = $this->GetItemsClass($iID);

		$cItems->Clear();
		$c = 1;
		if (!empty($aItems)) foreach ($aItems as $iID) {
			$aData = array(
				'item_id' => (int)$iID,
				'ord' => $c++
			);
			if (!$cItems->Add($aData)) {
				CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('items' => array($iID => 'ERROR_WRONG_VALUE')));
				return false;
			}
		}
		return true;
	}

	function SwitchStatus($iID)
	{
		$q = 'UPDATE '.$this->sTable.' SET enabled = 1-enabled WHERE id='.$iID;
		return CDb::getInstance()->query($q);
	}

	function SetOrder($iID, $iOrd)
	{
		$q = 'UPDATE '.$this->sTable.' SET ord = '.$iOrd.' WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	public function RemoveFiles($iID)
	{
		@unlink(self::GetLink($iID, 1, true));
		@unlink(self::GetLink($iID, 2, true));
	}

	function Delete($iID)
	{
		if (!parent::Delete($iID)) return false;

		$this->RemoveFiles($iID);

		return true;
	}

	function GetList(CSinfo &$pcSInfo)
	{
		$aWhere = array();
		$sWhere = '';

		if ($pcSInfo->enabled_only) $aWhere[] = 'enabled=1';
		if ($aWhere) $sWhere = join(' AND ',$aWhere);

		return parent::GetList($pcSInfo,$sWhere);
	}

	public function GetItem($iID)
	{
		$mReturn = parent::GetItem($iID);
		if (!$mReturn) return $mReturn;
		$cItems = $this->GetItemsClass($iID);
		$mReturn['items'] = $cItems->GetList();

		$mReturn['image_width'] = ceil($mReturn['width'] / 128);
		$mReturn['image_height'] = ceil($mReturn['height'] / 128);

		return $mReturn;
	}

	public function GetItems($aIds)
	{
		$cItems = $this->GetItemsClass(null);
		return $cItems->GetFullList($aIds);
	}

	/**
	* types
	* 1 - original
	* 2 - thumb
	*/
	public static function GetLink($iID, $iType, $bFull = false)
	{
		$sUrl = ($bFull ? $_SERVER['DOCUMENT_ROOT'] : '').'/static/gallery/'.($iID%100).'/'.$iID;
		if ($iType == 2) {
			$sUrl .= '_thumb';
		}
		$sUrl .= '.jpg';
		return $sUrl;
	}

}

?>
