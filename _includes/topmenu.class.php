<?

class CTopMenu extends CContent
{
	protected $sTable = 'top_menu';
	protected $sOrderField = 'ord';
	protected $sOrderDir = 'ASC';

	protected $aCheckRules = array(
		'title' => array('type' => CChecker::TYPE_STRING, 'flags'=>1029, 'params' => array('max' => 50)),
		'enabled' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 1)),
		'url' => array('type' => CChecker::TYPE_STRING, 'flags'=>5125, 'params' => array('max' => 500))
	);

	private static $_instance;

	/**
	 * Menu class
	 *
	 * @return CMenu
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	function SwitchStatus($iID)
	{
		$q = 'UPDATE '.$this->sTable.' SET enabled = 1 - enabled WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	function SetOrder($iID, $iOrd)
	{
		$q = 'UPDATE '.$this->sTable.' SET ord = '.$iOrd.' WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	public function GetList($cSInfo)
	{
		$sWhere = '';
		$aWhere = array();

		if ($cSInfo->enabled_only) $aWhere[] = 'enabled = 1';
		if ($aWhere) $sWhere = join(' AND ',$aWhere);

		return parent::GetList($cSInfo, $sWhere);
	}

	function GetItemByPath($sPath)
	{
		if ($sPath) {
			if (CChecker::CheckString($sPath, $this->aCheckRules['url']['flags'], $this->aCheckRules['url']['params'])) return false;
			$aWhere[] = 'url=\''.CDb::getInstance()->escape($sPath).'\'';
		}

		$q = 'SELECT * FROM '.$this->sTable.' WHERE '.join(' AND ',$aWhere);

		return CDb::getInstance()->query_first($q);
	}

}

?>
