<?

class CUsers extends CContent
{
	protected $sTable = 'users';
	protected $sOrderField = 'company_name ASC, last_name';
	protected $sOrderDir = 'ASC';

	private $salt = 'ndRa54jd#o6';

	public $aUserInfo = array();

	const TYPE_PRIVATE = 1;
	const TYPE_COMPANY = 2;

	protected $aCommonCheckRules = array(
		'email' => array('type' => CChecker::TYPE_STRING, 'flags' => 13, 'params' => array('max' => 250, 'preg' => CChecker::PREG_EMAIL)),
		'password' => array('type' => CChecker::TYPE_STRING, 'flags' => 6, 'params' => array('min' => 5, 'max' => 15)),
		'type' => array('type' => CChecker::TYPE_INT,'flags' => 6, 'params' => array('min' => 1, 'max' => 2)),
		'first_name' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 200)),
		'last_name' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 200)),
		'middle_name' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 200)),
		'phone' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 100)),
		'production' => array('type' => CChecker::TYPE_INT,'flags' => 6, 'params' => array('min' => 0, 'max' => 1)),
		'constructor' => array('type' => CChecker::TYPE_INT,'flags' => 6, 'params' => array('min' => 0, 'max' => 1)),
		'all_constructors' => array('type' => CChecker::TYPE_INT,'flags' => 6, 'params' => array('min' => 0, 'max' => 1))
	);

	protected $aCompanyCheckRules = array(
		'company_name' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 250)),
		'company_inn' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 20)),
		'company_kpp' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 20)),
		'company_bik' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 20)),
		'company_rs' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 30)),
		'company_city' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 250)),
		'company_address' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 16000))
	);

	private static $_instance;

	/**
	 * Users class
	 *
	 * @return CUsers
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	function __construct()
	{
		$this->aUserInfo = $this->GetItem($_SESSION['userinfo']['id']);
	}

	function FindByEmail($sEmail)
	{
		$sErr = CChecker::CheckString($sEmail,$this->aCommonCheckRules['email']['flags'],$this->aCommonCheckRules['email']['params']);
		if ($sErr) {
			return false;
		}
		$q = 'SELECT * FROM users WHERE email = \''.$sEmail.'\'';
		return CDb::getInstance()->query_first($q);
	}

	function ChPassword($iID, $sPassword)
	{
		$q = 'UPDATE users SET password = \''.md5($sPassword).'\' WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	function Add($aData)
	{
		if ($aData['type'] == self::TYPE_COMPANY) {
			$aCheckRules = array_merge($this->aCommonCheckRules, $this->aCompanyCheckRules);
		} else {
			$aCheckRules = $this->aCommonCheckRules;
		}

		$aData = array_intersect_key($aData, $aCheckRules);
		if (!CChecker::CheckArray($aData, $aCheckRules)) {
			return false;
		}

		$aData['password'] = md5($aData['password']);

		if (!CDb::getInstance()->insert($this->sTable, $aData)) {
			if (CDb::getInstance()->get_errno() == 1062) {
				CChecker::SetLastErrors(null,array('email' => 'ERROR_EMAIL_EXISTS'));
			}
			return false;
		}

		return true;
	}

	function Update($iUserID, $aData)
	{
		if ($aData['type'] == self::TYPE_COMPANY) {
			$aCheckRules = array_merge($this->aCommonCheckRules, $this->aCompanyCheckRules);
		} else {
			$aCheckRules = $this->aCommonCheckRules;
		}

		if (!$aData['password']) {
			unset($aCheckRules['password']);
		}
		$aData = array_intersect_key($aData, $aCheckRules);
		if (!CChecker::CheckArray($aData, $aCheckRules)) {
			return false;
		}

		if ($aData['type'] === self::TYPE_PRIVATE) {
			foreach ($this->aCompanyCheckRules as $sKey => $aRules) {
				$aData[$sKey] = null;
			}
		}

		if ($aData['password']) {
			$aData['password'] = md5($aData['password']);
		}

		if (!CDb::getInstance()->update($this->sTable, $aData, array('id' => $iUserID))) {
			if (CDb::getInstance()->get_errno() == 1062) {
				CChecker::SetLastErrors('ERROR_SOME_ERRORS', array('email' => 'ERROR_EMAIL_EXISTS'));
			}
			return false;
		}

		if ($this->isLogged()) {
			$this->aUserInfo = $_SESSION['userinfo'] = $this->GetItem($iUserID);
		}

		return true;
	}

	function Login($aData)
	{
		if (CChecker::CheckString($aData['email'], $this->aCommonCheckRules['email']['flags'], $this->aCommonCheckRules['email']['params'])) {
			CChecker::SetLastErrors('ERROR_NO_USER');
			return false;
		}

		$db = CDb::getInstance();

		$q = 'SELECT * FROM users WHERE email = \''.$aData['email'].'\'';
		$aReturn = $db->query_first($q);
		if (empty($aReturn['id'])) {
			CChecker::SetLastErrors('ERROR_NO_USER');
			return false;
		}

		if ($aReturn['password'] !== md5($aData['password'])) {
			CChecker::SetLastErrors('ERROR_WRONG_PASSWORD');
			return false;
		}

		$this->aUserInfo = $_SESSION['userinfo'] = $aReturn;
		if ($aData['remember']) {
			setcookie('user', $aReturn['id'].'_'.md5($aReturn['email'].'#~#'.$aReturn['password'].'#~#'.$this->salt), time() + 86400 * 30, '/');
		}

		return true;
	}

	function CookieLogin()
	{
		if (empty($_COOKIE['user'])) {
			return false;
		}
		$aInfo = explode('_', $_COOKIE['user']);
		if ((int)$aInfo[0]) {
			$aUser = $this->GetItem((int)$aInfo[0]);
			if (!empty($aUser)) {
				if ($aInfo[1] == md5($aUser['email'].'#~#'.$aUser['password'].'#~#'.$this->salt)) {
					$this->aUserInfo = $_SESSION['userinfo'] = $aUser;
					setcookie('user', $aUser['id'].'_'.md5($aUser['email'].'#~#'.$aUser['password'].'#~#'.$this->salt), time() + 86400 * 30, '/');
					return true;
				}
			}
		}
		setcookie('user', '', time() - 86400, '/');
		return false;
	}

	function isLogged()
	{
		return isset($this->aUserInfo['id']);
	}

	function isProduction()
	{
		return (isset($this->aUserInfo['production']) && $this->aUserInfo['production']);
	}

	function Logout()
	{
		$_SESSION['userinfo'] = null;
		setcookie('user', '', time() - 86400, '/');
	}

	function GetList(CSInfo &$pcSInfo)
	{
		$db = CDb::getInstance();

		$aWhere = array();
		if ($pcSInfo->email) {
			$pcSInfo->email = htmlentities($pcSInfo->email, ENT_QUOTES, 'UTF-8');
			$aWhere[] = 'email LIKE \'%'.$pcSInfo->email.'%\'';
		}
		if ($pcSInfo->type) {
			$aWhere[] = 'type = '.$pcSInfo->type;
		}
		if ($pcSInfo->last_name) {
			$pcSInfo->last_name = htmlentities($pcSInfo->last_name, ENT_QUOTES, 'UTF-8');
			$aWhere[] = 'last_name LIKE \'%'.$pcSInfo->last_name.'%\'';
		}
		if ($pcSInfo->company_name) {
			$pcSInfo->company_name = htmlentities($pcSInfo->company_name, ENT_QUOTES, 'UTF-8');
			$aWhere[] = 'company_name LIKE \'%'.$pcSInfo->company_name.'%\'';
		}
		if ($pcSInfo->has_constructors) {
			$aWhere[] = 'id IN (SELECT user_id FROM constructors)';
		}

		if ($aWhere) {
			$sWhere = join(' AND ', $aWhere);
		} else {
			$sWhere = '';
		}

		return parent::GetList($pcSInfo, $sWhere);
	}

	function ID()
	{
		return $this->aUserInfo['id'];
	}

	function ShortName()
	{
		$sName = $this->aUserInfo['last_name'];
		$sName .= ' '.mb_substr($this->aUserInfo['first_name'], 0, 1).'.';
		if ($this->aUserInfo['middle_name']) {
			$sName .= ' '.mb_substr($this->aUserInfo['middle_name'], 0, 1).'.';
		}
		return $sName;
	}

	function GetInfo($sVar = null)
	{
		if ($sVar) {
			return $this->aUserInfo[$sVar];
		} else {
			return $this->aUserInfo;
		}
	}
}

?>
