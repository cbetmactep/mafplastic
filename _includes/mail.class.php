<?

class CMail
{
	private $aTplInfo;
	private $aTplVars;
	private $sSendEmail;
	private $sDeliverEmail;

	private $aTemplateCheckRules = array(
			'subject' => array('type'=>CChecker::TYPE_STRING,'flags'=>6149, 'params' => array('max' => 1024)),
			'body' => array('type'=>CChecker::TYPE_STRING,'flags'=>6145),
	);

	function __construct($iTemplateID = 0)
	{
		if (!$iTemplateID) return false;

		$q = 'SELECT subject,body FROM mail_tpl WHERE id='.$iTemplateID;
		if (!$this->aTplInfo = CDb::getInstance()->query_first($q)) return false;
		return true;
	}

	function AddVar($sVarName,$sVarValue)
	{
		$this->aTplVars[$sVarName] = $sVarValue;
	}

	function SetSender($sEmail)
	{
		$this->sSendEmail = $sEmail;
	}

	function SetDelivery($sEmail)
	{
		$this->sDeliverEmail = $sEmail;
	}

	function Send()
	{
		$sSubject = str_replace(array_keys($this->aTplVars),array_values($this->aTplVars),$this->aTplInfo['subject']);
		$sMessage = nl2br(str_replace(array_keys($this->aTplVars),array_values($this->aTplVars),$this->aTplInfo['body']));

		$sHeaders = 'From: '.$this->sSendEmail."\r\n" .
						'Reply-To: '.$this->sSendEmail."\r\n".
						'Content-type: text/html; charset=utf-8'."\r\n";

		if (!@mail($this->sDeliverEmail,$sSubject,$sMessage,$sHeaders)) {
			CChecker::SetLastErrors('MAILER_ERROR');
			return false;
		}
		return true;
	}

	function GetTemplates()
	{
		$q = 'SELECT id,name FROM mail_tpl';
		return CDb::getInstance()->query_array($q);
	}

	function GetTemplate($iTemplateID)
	{
		$q = 'SELECT * FROM mail_tpl WHERE id='.$iTemplateID;
		return CDb::getInstance()->query_first($q);
	}

	function UpdateTemplate($iTemplateID,&$paData)
	{
		if (!CChecker::CheckArray($paData,$this->aTemplateCheckRules)) {
			return false;
		}

		$q = "UPDATE mail_tpl SET subject = ".$paData['subject'].", body=".$paData['body']." WHERE id=".$iTemplateID;
		if (!CDb::getInstance()->query($q)) return false;

		return true;
	}

}

?>