<?

class CConstructors extends CContent
{
	protected $sTable = 'constructors';
	protected $sOrderField = 'id';
	protected $sOrderDir = 'DESC';

	protected $aCheckRules = array(
		'user_id' => array('type' => CChecker::TYPE_INT, 'flags' => 8192),
		'date' => array('type' => CChecker::TYPE_STRING),
		'params' => array('type' => CChecker::TYPE_STRING),
		'price' => array('type' => CChecker::TYPE_FLOAT),
		'ip' => array('type' => CChecker::TYPE_STRING),
		'city' => array('type' => CChecker::TYPE_STRING)
	);

	private static $_instance;

	/**
	 * Constructor items class
	 *
	 * @return CConstructors
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	public function Add($aData, $sImageFilename)
	{
		$db = CDb::getInstance();
		$db->begin();

		$aData['date'] = date('Y-m-d H:i:s');
		$aData['params'] = json_encode($aData['params']);
		$aData['ip'] = $_SERVER['REMOTE_ADDR'];
		$aCity = CGeo::getInstance()->FindCityByIP($_SERVER['REMOTE_ADDR']);
		if (empty($aCity)) {
			$aCity = array(
				'city' => 'Не определено',
				'region' => 'Не определено',
				'district' => 'Не определено'
			);
		}
		$aData['city'] = json_encode(CGeo::getInstance()->FindCityByIP($_SERVER['REMOTE_ADDR']));

		$iNewID = parent::Add($aData);
		if (empty($iNewID)) {
			$db->rollback();
			return false;
		}
                
		if ($sImageFilename && !$this->_uploadImage($iNewID, $sImageFilename)) {
			$db->rollback();
			CChecker::SetLastErrors('ERROR_SOME_ERRORS',array('image' => 'ERROR_FILE_ERROR'));
			return false;
		}

		$db->commit();
		return $iNewID;
	}

	public function Update($iID, $aData)
	{
		return false;
	}

	public function UpdateUserID($iID, $iUserID)
	{
		return CDb::getInstance()->update($this->sTable, array('user_id' => $iUserID), array('id' => $iID));
	}

	public function Delete($iID)
	{
		return CDb::getInstance()->update($this->sTable, array('user_id' => null), array('id' => $iID));
	}

	private function _uploadImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, 1, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		if (!@copy($sFilename, self::GetLink($iID, 1, true))) {
			return false;
		}

		$cImage = new CImage();
		$cImage->SetSource(self::GetLink($iID, 1, true));
		$cImage->SetDestionation(self::GetLink($iID, 2, true));
		$aParams = array(
			'quality' => 100
		);
		if (!$cImage->Resize(1000, 1400, $aParams)) {
			$this->RemoveFiles($iID);
			return false;
		}

		$cImage = new CImage();
		$cImage->SetSource(self::GetLink($iID, 1, true));
		$cImage->SetDestionation(self::GetLink($iID, 3, true));
		$aParams = array(
			'quality' => 100,
			'strict' => true
		);
		if (!$cImage->Resize(200, 200, $aParams)) {
			$this->RemoveFiles($iID);
			return false;
		}

		return true;
	}

	public function RemoveFiles($iID)
	{
		@unlink(self::GetLink($iID, 1, true));
		@unlink(self::GetLink($iID, 2, true));
	}

	public function GetList(CSInfo $cSInfo)
	{
		$aWhere = array();
		if ($cSInfo->date_from) {
			$aWhere[] = 'date >= \''.$cSInfo->date_from.' 00:00:00\'';
		}
		if ($cSInfo->date_to) {
			$aWhere[] = 'date <= \''.$cSInfo->date_to.' 23:59:59\'';
		}
		if ($cSInfo->id) {
			$aWhere[] = 'id = '.$cSInfo->id;
		}
		if ($cSInfo->user_id) {
			$aWhere[] = 'user_id = '.$cSInfo->user_id;
		}
		if ($cSInfo->no_user) {
			$aWhere[] = 'user_id IS NULL';
		}

		if ($aWhere) {
			$sWhere = join(' AND ', $aWhere);
		} else {
			$sWhere = '';
		}

		$aList = parent::GetList($cSInfo, $sWhere);
		if ($aList) foreach ($aList as &$aItem) {
			$aItem['params'] = json_decode($aItem['params'], true);
			$aItem['city'] = json_decode($aItem['city'], true);
		}
		unset($aItem);
		return $aList;
	}

	public function GetItem($iID)
	{
		$aItem = parent::GetItem($iID);
		if (!empty($aItem)) {
			$aItem['params'] = json_decode($aItem['params'], true);
			$aItem['city'] = json_decode($aItem['city'], true);
		}
		return $aItem;
	}

	/**
	* types
	* 1 - original
	* 2 - print
	* 3 - thumb
	*/
	public static function GetLink($iID, $iType, $bFull = false)
	{
		$sUrl = ($bFull ? $_SERVER['DOCUMENT_ROOT'] : '').'/static/constructors/'.($iID%100).'/'.$iID;
		if ($iType == 2) {
			$sUrl .= '_print';
		} elseif ($iType == 3) {
			$sUrl .= '_thumb';
		}
		$sUrl .= '.png';
		return $sUrl;
	}
}

?>
