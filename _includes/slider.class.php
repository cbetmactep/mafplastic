<?

class CSlider extends CContent
{
	protected $sTable = 'slider';
	protected $sOrderField = 'ord';
	protected $sOrderDir = 'ASC';

	protected $aCheckRules = array(
		'title' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 250)),
		'descr' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 4000)),
		'url' => array('type' => CChecker::TYPE_STRING, 'flags'=>5124, 'params' => array('max' => 500)),
		'enabled' => array('type' => CChecker::TYPE_INT, 'flags' => 6, 'params'=>array('min' => 0, 'max' => 1))
	);

	private static $_instance;

	/**
	 * Slider class
	 *
	 * @return CSlider
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	public function Add($aData, $sFilename)
	{
		if (!$iID = parent::Add($aData)) return false;

		if ($sFilename && !$this->_processImage($iID, $sFilename)) {
			$this->Delete($aData['id']);
			CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('image' => L10N('ERROR_FILE_ERROR')));
			return false;
		}
		return true;
	}

	public function Update($iID, $aData, $sFilename)
	{
		if (!parent::Update($iID, $aData)) return false;

		if ($sFilename && !$this->_processImage($iID, $sFilename)) {
			CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('image' => L10N('ERROR_FILE_ERROR')));
			return false;
		}
		return true;
	}

	private function _processImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		$cImage = new CImage();
		$cImage->SetSource($sFilename);
		$cImage->SetDestionation(self::GetLink($iID, true));
		$aParams = array(
			'quality' => 85,
			'crop' => true
		);
		if (!$cImage->Resize(701, 403, $aParams)) {
			$this->RemoveFiles($iID);
			return false;
		}

		return true;
	}

	function SwitchStatus($iID)
	{
		$q = 'UPDATE '.$this->sTable.' SET enabled = 1-enabled WHERE id='.$iID;
		return CDb::getInstance()->query($q);
	}

	function SetOrder($iID, $iOrd)
	{
		$q = 'UPDATE '.$this->sTable.' SET ord = '.$iOrd.' WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	public function RemoveFiles($iID)
	{
		@unlink(self::GetLink($iID, true));
	}

	function Delete($iID)
	{
		if (!parent::Delete($iID)) return false;

		$this->RemoveFiles($iID);

		return true;
	}

	function GetList(CSinfo &$pcSInfo)
	{
		$aWhere = array();
		$sWhere = '';

		if ($pcSInfo->enabled_only) $aWhere[] = 'enabled=1';
		if ($aWhere) $sWhere = join(' AND ',$aWhere);

		return parent::GetList($pcSInfo,$sWhere);
	}

	public static function GetLink($iID, $bFull = false)
	{
		return ($bFull ? $_SERVER['DOCUMENT_ROOT'] : '').'/static/slider/'.($iID%100).'/'.$iID.'.png';
	}

}

?>
