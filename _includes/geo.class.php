<?

class CGeo
{
	private static $_instance;

	/**
	 * Geo class
	 *
	 * @return CGeo
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	function CreateTmpTables()
	{
		$q = 'DROP TABLE IF EXISTS geo_ips_tmp';
		if (!CDb::getInstance()->query($q)) {
			return false;
		}

		$q = 'CREATE TABLE `geo_ips_tmp` (
				`ip_from` int(10) unsigned NOT NULL,
				`ip_to` int(10) unsigned NOT NULL,
				`city_id` int(10) unsigned NOT NULL,
				UNIQUE KEY `ip_from` (`ip_from`, `ip_to`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
		if (!CDb::getInstance()->query($q)) {
			return false;
		}

		return true;
	}

	function CommitTables()
	{
		$q = 'DROP TABLE IF EXISTS geo_ips';
		if (!CDb::getInstance()->query($q)) {
			return false;
		}

		$q = 'RENAME TABLE geo_ips_tmp TO geo_ips';
		if (!CDb::getInstance()->query($q)) {
			return false;
		}

		return true;
	}

	function AddBlock($uIPBlockFrom, $uIPBlockTo, $iCityID)
	{
		$aData = array(
			'ip_from' => $uIPBlockFrom,
			'ip_to' => $uIPBlockTo,
			'city_id' => $iCityID
		);

		$q = 'INSERT INTO geo_ips_tmp ('.join(',',array_keys($aData)).') VALUES ('.join(',',$aData).')';
		if (!CDb::getInstance()->query($q)) {
			return false;
		}

		return true;
	}

	function FindByIP($sIP)
	{
		$q = 'SELECT * FROM geo_ips WHERE ip_from <= INET_ATON(\''.CDb::getInstance()->escape($sIP).'\')
					AND ip_to >= INET_ATON(\''.CDb::getInstance()->escape($sIP).'\')';
		return CDb::getInstance()->query_first($q);
	}

	function FindCityByIP($sIP)
	{
		$q = 'SELECT * FROM geo_cities WHERE id IN (
					SELECT city_id FROM geo_ips
						WHERE ip_from <= INET_ATON(\''.CDb::getInstance()->escape($sIP).'\')
						AND ip_to >= INET_ATON(\''.CDb::getInstance()->escape($sIP).'\')
				) LIMIT 0,1';
		return CDb::getInstance()->query_first($q);
	}

	function AddCity($iID, $sCity, $sRegion, $sDistrict)
	{
		//проверяем если город уже есть, хотя может быть переименован
		$aCity = $this->GetCity($iID);
		if (!empty($aCity['id'])) {
			if ($aCity['city'] != $sCity) {
				return false;
			} else {
				return true;
			}
		}

		//проверяем нужно ли вообще этот город добавлять
		$q = 'SELECT * FROM geo_ips WHERE city_id = '.$iID;
		$aResult = CDb::getInstance()->query_first($q);
		if (empty($aResult['city_id'])) {
			return true;
		}

		$aData = array(
			'id' => $iID,
			'city' => '\''.CDb::getInstance()->escape($sCity).'\'',
			'region' => '\''.CDb::getInstance()->escape($sRegion).'\'',
			'district' => '\''.CDb::getInstance()->escape($sDistrict).'\'',
		);

		$q = 'INSERT INTO geo_cities ('.join(',',array_keys($aData)).') VALUES ('.join(',',$aData).')';
		if (!CDb::getInstance()->query($q)) {
			return false;
		}

		return true;
	}

	function GetCity($iID)
	{
		$q = 'SELECT * FROM geo_cities WHERE id = '.$iID;
		return CDb::getInstance()->query_first($q);
	}

	function GetCities()
	{
		$q = 'SELECT * FROM geo_cities ORDER BY city ASC';
		return CDb::getInstance()->query_array($q);
	}

}

?>
