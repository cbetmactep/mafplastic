<?

class CItemColors extends CContent
{
	protected $sTable = 'item_colors';
	protected $sOrderField = 'ord';
	protected $sOrderDir = 'ASC';

	static $aAddPrices = array(
		0 => 'Нет',
		1 => '+20%',
		2 => '+30%',
		3 => '+50%',
		4 => '+70%',
		5 => '+100%',
		6 => '+200%'
	);

	static $aAddPricesCoeffs = array(
		0 => 1,
		1 => 1.2,
		2 => 1.3,
		3 => 1.5,
		4 => 1.7,
		5 => 2,
		6 => 3
	);

	public static $aColors = array(
		1 => 'Чёрный',
		2 => 'Белый',
		3 => 'Серый',
		4 => 'Хром',
		5 => 'Латунь',
		6 => 'Металл',
		7 => 'Красный',
		8 => 'Жёлтый',
		9 => 'Зелёный',
		10 => 'Синий',
		11 => 'Бесцветный',
		12 => 'Оранжевый',
		13 => 'Коричневый',
		14 => 'Т.коричневый',
		15 => 'Салатовый',
		16 => 'Бежевый',
		17 => 'Т.бежевый',
		18 => 'Св.бежевый',
		19 => 'Голубой',
		20 => 'Вишня',
		21 => 'Орех',
		22 => 'Махагон',
		23 => 'Дуб',
		24 => 'Сосна',
		25 => 'Кр.дерево',
		26 => 'Бордо',
		27 => 'Охра',
		28 => 'Кр.кирпич',
		29 => 'Разноцветный',
		30 => 'Т.серый',
		31 => 'Св.серый',
		32 => 'Бук',
		33 => 'Розовый',
		34 => 'Сиреневый',
		35 => 'Бирюзовый',
		36 => 'Бордовый',
		37 => 'Голубой/белый',
		38 => 'Св.бирюзовый',
		39 => 'Металлик',
		40 => 'Красный/синий',
		41 => 'Синий/красный',
		42 => 'Чёрный/оранж.',
		43 => 'Чёрный/красн.',
		44 => 'Чёрный/зелён.',
		45 => 'Чёрный/синий',
		46 => 'Чёрный/жёлт.',
		47 => 'Красный/жёлт.',
		48 => 'Жёлтый/синий',
		49 => 'Синий/жёлтый',
		50 => 'Зелёный/желт.',
		51 => 'Жёлтый/жёлтый',
		52 => 'Жёлтый/крас.',
		53 => 'Красный/крас.',
		54 => 'Т.синий',
        55 => 'чёрный (RAL 9005)',
        56 => 'белый (RAL 9003)',
        57 => 'жёлтый (RAL 1023)',
        58 => 'зелёный (RAL 6035)',
        59 => 'коричневый (RAL 8003)',
        60 => 'т.коричневый (RAL 8017)',
        61 => 'т.бежевый (RAL 1019)',
        62 => 'св.бежевый (RAL 1001)',
        63 => 'вишня (RAL 8001)',
        64 => 'орех (RAL 8000)',
        65 => 'махагон (RAL 8012)',
        66 => 'дуб (RAL 8014)',
        67 => 'сосна (RAL 1034)',
        68 => 'кр.дерево (RAL 8015)',
        69 => 'охра (RAL 1005)',
        70 => 'т.серый (RAL 7001)',
        71 => 'св.серый (RAL 7040)',
        72 => 'бук (RAL 1011)',
        73 => 'синий/синий',
        74 => 'красный(N)/жёлтый',
        75 => 'оливковый',
        76 => 'фиолетовый',
        77 => 'бежевый/чёрный',
        78 => 'красный/чёрный',
        79 => 'чёрный/чёрный',
        80 => 'серый/жёлтый',
        81 => 'серый/синий',
        82 => 'св.коричневый',
        83 => 'венге',
        84 => 'чёрный/чёрный/красный',
        85 => 'жёлтый/чёрный',
        86 => 'голубой/белый/жёлтый',
        87 => 'зелёный/синий',
        88 => 'зелёный/чёрный',
        89 => 'синий/чёрный',
        90 => 'красный/зелёный',
        91 => 'жёлтый/зелёный',
        92 => 'зелёный/красный',
        93 => 'зелёный/зелёный',
        94 => 'синий/зелёный',
        95 => 'бежевый/жёлтый',
        96 => 'разноцветный/красный',
        97 => 'оранжевый/чёрный',
        98 => 'серый/красный',
        99 => 'оранжевый/розовый',
        100 => 'белый/чёрный',
        101 => 'св.зелёный',
        102 => 'синий/красный/синий',
        103 => 'серый/розовый',
        104 => 'чёрный/синий/оранжевый',
        105 => 'оранжевый/белый',
        106 => 'фиолетовый/розовый/чёрный',
        107 => 'голубой/белый/синий',
        108 => 'зелёный/синий/жёлтый',
        109 => 'голубой/белый/красный',

        110 => 'белый/чёрный/белый',
        111 => 'жёлтый/красный/жёлтый',
        112 => 'синий/жёлтый/синий',
        113 => 'зелёный/жёлтый/зелёный',
        114 => 'оранжевый/белый/оранжевый',
        115 => 'фиолетовый/чёрный',
        116 => 'цинк',

        117 => 'св.голубой',
        118 => 'серебристый',
        119 => 'белый мрамор',

        120 => 'разноцветный/синий',
        121 => 'разноцветный/жёлтый ',
        122 => 'зелёный/жёлтый/жёлтый',
        123 => 'синий/синий/синий',
        124 => 'зелёный/синий/синий',
        125 => 'белый/синий/синий',
        126 => 'белый/синий/жёлтый',
        127 => 'белый/жёлтый/жёлтый',
        128 => 'синий/синий/жёлтый',
        129 => 'синий/жёлтый/жёлтый',

        130 => 'чёрный (RAL 9017)',
		131 => 'серый (RAL 9006)',
		132 => 'коричневый (RAL 8016)',
		133 => 'бежевый (RAL 1019)',
		134 => 'т.бежевый (RAL 1011)',
		135 => 'серый (RAL 7040)',
		136 => 'бежевый (RAL 1001)',

        137 => 'разноцветный/жёлтый/фиолетовый',

		138 => 'синий/оранжевый',
		139 => 'т.жёлтый',
		140 => 'прозрачный'
	);

	protected $aCheckRules = array(
		'item_id' => array('type' => CChecker::TYPE_INT, 'flags' => 1),
		'color' => array('type' => CChecker::TYPE_INT, 'flags' => 6, 'params'=>array('min' => 1, 'max' => 255)),
		'articul' => array('type' => CChecker::TYPE_STRING, 'flags' => 1029, 'params' => array('max' => 250)),
		'enabled' => array('type' => CChecker::TYPE_INT, 'flags' => 6, 'params'=>array('min' => 0, 'max' => 1)),
		'add_price' => array('type' => CChecker::TYPE_INT,'flags' => 6,'params' => array('min' => 0,'max' => 6)),
		'remains' => array('type' => CChecker::TYPE_INT)
	);

	private static $_instance;

	/**
	 * Item colors class
	 *
	 * @return CItemColors
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	public function Add($aData, $sFilename)
	{
		if (!$iID = parent::Add($aData)) return false;

		if ($sFilename && !$this->_processImage($iID, $sFilename)) {
			$this->Delete($iID);
			CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('image' => L10N('ERROR_FILE_ERROR')));
			return false;
		}
		return $iID;
	}

	public function Update($iID, $aData, $sFilename)
	{
		unset($this->aCheckRules['item_id']);
		if (!parent::Update($iID, $aData)) return false;

		if ($sFilename && !$this->_processImage($iID, $sFilename)) {
			CChecker::SetLastErrors(L10N('ERROR_SOME_ERRORS'), array('image' => L10N('ERROR_FILE_ERROR')));
			return false;
		}
		return true;
	}

	private function _processImage($iID, $sFilename)
	{
		$sPath = dirname(self::GetLink($iID, 1, true));
		if (!file_exists($sPath)) {
			@mkdir($sPath, 0777);
		}

		$cImage = new CImage();
		$cImage->SetSource($sFilename);
		$cImage->SetDestionation(self::GetLink($iID, 1, true));
		$aParams = array(
			'to_jpeg' => true,
			'quality' => 100,
			'crop' => true,
			'strict' => true
		);
		if (!$cImage->Resize(200, 200, $aParams)) {
			$this->RemoveFiles($iID);
			return false;
		}

		if (!@copy($sFilename, self::GetLink($iID, 2, true))) {
			$this->RemoveFiles($iID);
			return false;
		}

		return true;
	}

	function SwitchStatus($iID)
	{
		$q = 'UPDATE '.$this->sTable.' SET enabled = 1-enabled WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	function SwitchMain($iItemID, $iID)
	{
		$q = 'UPDATE '.$this->sTable.' SET main = 0 WHERE item_id = '.$iItemID;
		if (!CDb::getInstance()->query($q)) {
			return false;
		}

		$q = 'UPDATE '.$this->sTable.' SET main = 1 WHERE item_id = '.$iItemID.' AND id='.$iID;
		return CDb::getInstance()->query($q);
	}

	function SetOrder($iID, $iOrd)
	{
		$q = 'UPDATE '.$this->sTable.' SET ord = '.$iOrd.' WHERE id = '.$iID;
		return CDb::getInstance()->query($q);
	}

	public function UpdateRemains($sArticul, $iRemains)
	{
		$q = 'UPDATE '.$this->sTable.' SET remains = '.$iRemains.', updated = 1 WHERE articul = \''.CDb::getInstance()->escape($sArticul).'\'';
		return CDb::getInstance()->query($q);
	}

	public function ClearRemains()
	{
		$q = 'UPDATE '.$this->sTable.' SET remains = 0 WHERE updated = 0';
		if (!CDb::getInstance()->query($q)) {
			return false;
		}

		$q = 'UPDATE '.$this->sTable.' SET updated = 0';
		return CDb::getInstance()->query($q);
	}

	public function UpdateArsenalRemains($sArticul, $iRemains)
	{
		$q = 'UPDATE '.$this->sTable.' SET arsenal_remains = '.$iRemains.', updated = 1 WHERE articul = \''.CDb::getInstance()->escape($sArticul).'\'';
		return CDb::getInstance()->query($q);
	}

	public function ClearArsenalRemains()
	{
		$q = 'UPDATE '.$this->sTable.' SET arsenal_remains = 0 WHERE updated = 0';
		if (!CDb::getInstance()->query($q)) {
			return false;
		}

		$q = 'UPDATE '.$this->sTable.' SET updated = 0';
		return CDb::getInstance()->query($q);
	}

	public function RemoveFiles($iID)
	{
		@unlink(self::GetLink($iID, 1, true));
		@unlink(self::GetLink($iID, 2, true));
	}

	function Delete($iID)
	{
		if (!parent::Delete($iID)) {
			return false;
		}

		$this->RemoveFiles($iID);

		return true;
	}

	function GetList(CSinfo &$pcSInfo)
	{
		$aWhere = array();
		$sWhere = '';

		if ($pcSInfo->item_id) {
			$aWhere[] = 'item_id = '.$pcSInfo->item_id;
		}
		if ($pcSInfo->item_ids) {
			$aWhere[] = 'item_id IN ('.join(',', $pcSInfo->item_ids).')';
		}
		if ($pcSInfo->enabled_only) {
			$aWhere[] = 'enabled = 1';
		}
		if ($pcSInfo->exist_only) {
			$aWhere[] = 'remains > 0';
		}
		if ($pcSInfo->constructor_exist_only) {
			$aWhere[] = 'remains + arsenal_remains > 0';
		}

		if ($aWhere) {
			$sWhere = join(' AND ', $aWhere);
		}

		$q = 'SELECT * FROM '.$this->sTable.($sWhere?' WHERE '.$sWhere:'').' ORDER BY '.$this->sOrderField.' '.$this->sOrderDir;
		if (!$oRes = CDb::getInstance()->query($q)) return false;
		$aReturn = array();
		while ($aRow = CDb::getInstance()->fetch_array($oRes)) {
			if ($pcSInfo->group_by_item) {
				if ($pcSInfo->set_id_keys) {
					$aReturn[$aRow['item_id']][$aRow['id']] = $aRow;
				} elseif ($pcSInfo->set_color_keys) {
					$aReturn[$aRow['item_id']][$aRow['color']] = $aRow;
				} else {
					$aReturn[$aRow['item_id']][] = $aRow;
				}
			} else {
				if ($pcSInfo->set_id_keys) {
					$aReturn[$aRow['id']] = $aRow;
				} elseif ($pcSInfo->set_color_keys) {
					$aReturn[$aRow['color']] = $aRow;
				} else {
					$aReturn[] = $aRow;
				}
			}
		}
		return $aReturn;
	}

	public function GetItemByArticul($sArticul)
	{
		if (CChecker::CheckString($sArticul, $this->aCheckRules['articul']['flags'], $this->aCheckRules['articul']['params'])) {
			return false;
		}
		$q = 'SELECT * FROM '.$this->sTable.' WHERE articul = \''.CDb::getInstance()->escape($sArticul).'\'';
		return CDb::getInstance()->query_first($q);
	}

	/**
	 * type
	 * 1 - thumb
	 * 2 - original
	 */
	public static function GetLink($iID, $iType = 1, $bFull = false)
	{
		$sUrl = ($bFull?$_SERVER['DOCUMENT_ROOT']:'').'/static/itemcolors/'.($iID%100).'/'.$iID;
		switch ($iType) {
			case 2:
				$sUrl .= '_orig';
				break;
		}
		$sUrl .= '.jpg';
		return $sUrl;
	}

}

?>
