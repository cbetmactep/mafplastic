<?

class CCurrencies
{
	const RUR = 0;
	const EUR = 1;
	const USD = 2;

	static $aList = array(
		self::RUR => 'рубли',
		self::EUR => 'евро',
		self::USD => 'доллары'
	);

	private static $_instance;

	/**
	 * Currencies class
	 *
	 * @return CCurrencies
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	public function GetCourses()
	{
		$sTodayURL = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req='.date('d/m/Y');
		$cXML = simplexml_load_file(rawurlencode($sTodayURL));
		$sLastDate = str_replace('/','.',(string)$cXML->attributes()->Date);
		$aCourses = array();
		$aCourses['last_date'] = date('d.m.y', strtotime($sLastDate));
		if ($cXML->Valute) {
			foreach ($cXML->Valute as $cItem) {
				switch ((string)$cItem->CharCode) {
					case 'USD':
						$aCourses['usd'] = (float)str_replace(',','.',(string)$cItem->Value);
						break;
					case 'EUR':
						$aCourses['eur'] = (float)str_replace(',','.',(string)$cItem->Value);
						break;
				}
			}
		}
		return $aCourses;
	}

	public function GetList()
	{
		return $this->aList;
	}

	public function GetTitle($iID)
	{
		if (isset($this->aList[$iID])) {
			return $this->aList[$iID];
		} else {
			return null;
		}
	}
}

?>
