<?

class CPages extends CContent
{
	protected $sTable = 'pages';
	protected $sOrderField = 'path';
	protected $sOrderDir = 'ASC';

	protected $aCheckRules = array(
		'path' => array('type' => CChecker::TYPE_STRING, 'flags' => 13, 'params' => array('max' => 250, 'preg' => CChecker::PREG_PATH)),
		'title' => array('type' => CChecker::TYPE_STRING, 'flags'=> 1029 ,'params' => array('max' => 250)),
		'body' => array('type' => CChecker::TYPE_STRING,'flags' => 4, 'params' => array('max' => 1024000)),
		'meta_title' => array('type' => CChecker::TYPE_STRING, 'flags' => 1028, 'params' => array('max' => 250)),
		'meta_description' => array('type' => CChecker::TYPE_STRING, 'flags' => 1028, 'params' => array('max' => 250)),
		'meta_keywords' => array('type' => CChecker::TYPE_STRING, 'flags' => 1028, 'params' => array('max' => 250)),
		'enabled' => array('type' => CChecker::TYPE_INT, 'flags' => 6, 'params' => array('min' => 0, 'max' => 1)),
	);

	private static $_instance;

	/**
	 * Pages class
	 *
	 * @return CPages
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	function Add($aData)
	{
		$iID = parent::Add($aData);
		if (!$iID) {
			if (CDb::getInstance()->get_errno() == 1062) {
				CChecker::SetLastErrors('ERROR_SOME_ERRORS', array('path' => 'PAGE_URL_EXISTS'));
			}
			return false;
		}
		return true;
	}

	function Update($iID, $aData)
	{
		$bRes = parent::Update($iID,$aData);
		if (!$bRes) {
			if (CDb::getInstance()->get_errno() == 1062) {
				CChecker::SetLastErrors('ERROR_SOME_ERRORS', array('path' => 'PAGE_URL_EXISTS'));
			}
			return false;
		}
		return true;
	}

	function GetList()
	{
		$cSInfo = new CSInfo();
		return parent::GetList($cSInfo);
	}

	function GetItemByPath($sPath)
	{
		if ($sPath) {
			if (CChecker::CheckString($sPath, $this->aCheckRules['path']['flags'], $this->aCheckRules['path']['params'])) return false;
			$aWhere[] = 'path=\''.CDb::getInstance()->escape($sPath).'\'';
		}
		if (!$aWhere) return false;
		$aWhere[] = 'enabled=1';

		$q = 'SELECT * FROM '.$this->sTable.' WHERE '.join(' AND ',$aWhere);

		return  CDb::getInstance()->query_first($q);
	}

}

?>
