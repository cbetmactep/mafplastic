<?

class CDispatcher
{
	private $aParams = array();
	private $sTemplate = '';
	private $sTemplatePath = '';

	private $sController = null;
	private $sAction = null;

	private $sURI = null;
	private $sOrigURI = null;

	private $aRules = null;

	public $bNoRender = false;

	function __set($sVar, $sValue) {
		$this->aParams[$sVar] = $sValue;
	}

	function &__get($sVar)
	{
		if (array_key_exists($sVar,$this->aParams)) return $this->aParams[$sVar];
		return null;
	}

	function __isset($sVar) {return isset($this->aParams[$sVar]);}

	function __unset($sVar) {unset($this->aParams[$sVar]);}

	function __construct($sURI)
	{
		$this->sOrigURI = $this->sURI = preg_replace('/\/+/', '/', strtok($sURI, '?'));

		if(!($this->aRules = @include('./rules.inc.php'))) {
			CDebug::AddLog('ERROR: No rules found');
			throw new Exception();
		}

		if (!$this->applyRules($this->aRules)) {
			$this->SetController('index');
			$this->SetAction('error404');
		}
	}

	function GetURI()
	{
		return $this->sOrigURI;
	}

	/**
	 * Set controller name
	 *
	 * @param string $sController
	 * @return CDispatcher
	 */
	function SetController($sController)
	{
		$this->sController = $sController;
		return $this;
	}

	/**
	 * Set action name
	 *
	 * @param string $sAction
	 * @return CDispatcher
	 */
	function SetAction($sAction)
	{
		$this->bNoRender = false;
		$this->sAction = $sAction.'Action';
		return $this;
	}

	/**
	 * Set request params
	 *
	 * @param string $sAction
	 * @return CDispatcher
	 */
	function SetParams($aParams)
	{
		$this->aParams = $aParams;
		return $this;
	}

	function SetTemplate($sTpl) {$this->sTemplate = $sTpl;}

	function SetTemplatePath($sPath) {$this->sTemplatePath = $sPath;}

	function GetControllerInstance()
	{
		if (!file_exists($_SERVER['DOCUMENT_ROOT'].'/_controllers/'.$this->sController.'.php')) {
			CDebug::AddLog('No controller '.$this->sController.' found');
			throw new Exception();
		}
		require_once($_SERVER['DOCUMENT_ROOT'].'/_controllers/'.$this->sController.'.php');
		if (!class_exists($this->sController)) {
			CDebug::AddLog('Wrong class name in controller '.$this->sController);
			throw new Exception();
		}
		return new $this->sController();
	}

	function Render() {
		$cController = $this->GetControllerInstance();
		$cController->SetDispatcher($this);
		$cController->SetParams($this->aParams);
		if (!method_exists($cController,$this->sAction)) {
			CDebug::AddLog('No method '.$this->sAction.' in controller '.$this->sController);
			throw new Exception();
		}
		$bRet = $cController->_init();
		if ($bRet !== false) {
			$bRet = call_user_func(array($cController,$this->sAction));
		}
		if ($bRet === false) {
			$this->SetHeader404();
			$this->sTemplate = '/404';
		}

		if (!$this->bNoRender) require($this->sTemplatePath.'/'.$this->sTemplate.'.tpl');
	}

	function RenderTemplate()
	{
		require($this->sTemplatePath.'/'.$this->sTemplate.'.tpl');
	}

	function Redirect($sUrl)
	{
		if ($this->isXmlHttpRequest()) {
			die('<script>document.location.href="'.$sUrl.'"</script>');
		} else {
			die(header("Location: {$sUrl}"));
		}
	}

	function isXmlHttpRequest()
	{
		return $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
	}

	function isPost()
	{
		return $_SERVER['REQUEST_METHOD'] === 'POST';
	}

	function isValidReferer()
	{
		if(!$_SERVER['HTTP_REFERER']) return false;
		$aParsedReferer = parse_url($_SERVER['HTTP_REFERER']);
		$sHost = $aParsedReferer['host']  .($aParsedReferer['port'] ? ':' .$aParsedReferer['port'] : '');
		return strtolower($sHost)==strtolower($_SERVER['HTTP_HOST']);
	}
	function getReferer($sDefault='/') { return $this->isValidReferer() ? $_SERVER['HTTP_REFERER'] : $sDefault;}

	protected function applyRules($aRules)
	{
		$this->sController = null;
		$this->sAction = null;
		foreach ($aRules as $sPreg=>$aRule) {
			if($this->sURI && !in_array($sPreg, array('default')) && preg_match('/^' . addcslashes($sPreg, '/') . '/isU', $this->sURI, $aMatches)) {
				//if($aRule['ajax'] && !$this->isXmlHttpRequest()) return false;

				$this->sURI = substr($this->sURI, strlen($aMatches[0]));

				foreach ((array)$aRule['params'] as $sKey=>$iIdent) $this->aParams[$sKey] = $aMatches[$iIdent];

				if($aRule['rules'] && $this->sURI) return $this->applyRules($aRule['rules']);

				if (!$this->sURI) {
					if(isset($aRule['controller'])) $this->SetController($aRule['controller']);
					if(isset($aRule['action'])) $this->SetAction($aRule['action']);
					return true;
				}
			}
		}

		if (!$this->sController && !$this->sAction) {
			if ($this->aRules['default']) {
				if(isset($this->aRules['default']['controller'])) $this->SetController($this->aRules['default']['controller']);
				if(isset($this->aRules['default']['action'])) $this->SetAction($this->aRules['default']['action']);
				return true;
			} else {
				CDebug::AddLog('Warning: No appropriate rule found');
				throw new Exception();
			}
		}

		return false;
	}

	function SetHeader404()
	{
		header('HTTP/1.0 404 Not Found');
	}

	function SetHeader301() {
		header('HTTP/1.0 301 Moved Permanently');
	}

}

?>
