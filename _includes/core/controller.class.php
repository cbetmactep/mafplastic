<?

class CController
{
	private $aParams = array();
	/**
	 * dispatcher class
	 *
	 * @var CDispatcher
	 */
	protected $cDispatcher;

	function __set($sVar, $sValue) {
		$this->aParams[$sVar] = $sValue;
	}

	function &__get($sVar)
	{
		if (array_key_exists($sVar,$this->aParams)) return $this->aParams[$sVar];
		return null;
	}

	function __isset($sVar) {return isset($this->aParams[$sVar]);}

	function __unset($sVar) {unset($this->aParams[$sVar]);}

	public function SetParams(&$paParams)
	{
		$this->aParams = &$paParams;
	}

	public function SetDispatcher(CDispatcher $cDispatcher)
	{
		$this->cDispatcher = $cDispatcher;
	}

	public function _init() {}

}

?>