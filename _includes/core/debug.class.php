<?

class CDebug
{
	private static $aMessages = array();
	private $iQueries = 0;
	private $fStartTime = null;

	function AddLog($sMessage)
	{
		$mTime = microtime(true);
		$mTime = explode(',',$mTime);
		self::$aMessages[] = array(
			'time' => $mTime[0],
			'microtime' => sprintf('%-04s',$mTime[1]),
			'message' => $sMessage
		);
	}
	
	function GetLog()
	{
		return self::$aMessages;
	}
	
	function PrintLog()
	{
		$sHTML = "Debug Log:<br>\r\n";
		foreach (self::$aMessages as $aMessage) {
			$sHTML .= '['.date('d.m.Y H:i:s',$aMessage['time']).' '.$aMessage['microtime'].'] '.$aMessage['message']."<br>\r\n";
		}
		return $sHTML;
	}
	
}

?>