<?php

class CContent
{
	protected $sTable = '';
	protected $sOrderField = 'ord';
	protected $sOrderDir = 'DESC';

	protected $aCheckRules = array();

	private static $_instance;

	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	function Add($aData)
	{
		$aData = array_intersect_key($aData,$this->aCheckRules);
		if (!CChecker::CheckArray($aData,$this->aCheckRules)) {
			return false;
		}
		$db = CDb::getInstance();

		if (!$db->insert($this->sTable,$aData)) {
                    echo "1";
                    exit;
                    return false;
                }

		return $db->insert_id();
	}

	function Update($iID, $aData)
	{
		$aData = array_intersect_key($aData,$this->aCheckRules);
		if (!CChecker::CheckArray($aData,$this->aCheckRules)) {
			return false;
		}

		if (!CDb::getInstance()->update($this->sTable,$aData,array('id'=>$iID))) return false;
		return true;
	}

	function Delete($iID)
	{
		if (!CDB::getInstance()->delete($this->sTable,array('id'=>$iID))) return false;
		return true;
	}

	protected function GetList(CSInfo &$pcSInfo,$sWhere = '')
	{
		$db = CDb::getInstance();

		if ($pcSInfo->perpage) {
			$q = 'SELECT count(id) FROM '.$this->sTable.($sWhere?' WHERE '.$sWhere:'');
			if (!$pcSInfo->total_rows = $db->query_field($q)) return false;
			$pcSInfo->CalculatePages();
			$sLimit = $pcSInfo->GetLimit();
		} elseif ($pcSInfo->count) {
			$sLimit = ' LIMIT 0,'.$pcSInfo->count;
		} else {
			$sLimit = '';
		}

		if ($pcSInfo->random) {
			$sOrder = ' rand()';
		} else {
			$sOrder = $this->sOrderField.' '.$this->sOrderDir;
		}

		$q = 'SELECT * FROM '.$this->sTable.($sWhere?' WHERE '.$sWhere:'').' ORDER BY '.$sOrder.$sLimit;
		if (!$oRes = $db->query($q)) return false;
		$aReturn = array();
		while ($aRow = $db->fetch_array($oRes)) {
			if ($pcSInfo->set_id_keys) {
				$aReturn[$aRow['id']] = $aRow;
			} else {
				$aReturn[] = $aRow;
			}
		}
		if ($pcSInfo->add_first_null) array_unshift($aReturn,array('id'=>0));
		return $aReturn;
	}

	function GetItem($iID)
	{
		$q = 'SELECT * FROM '.$this->sTable.' WHERE id = '.$iID;
		return CDb::getInstance()->query_first($q);
	}
}

?>