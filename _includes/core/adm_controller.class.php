<?

class CAdm_controller extends CController
{
	protected $cContentClass = null;
	protected $sRedirectUrl = null;
	protected $sEditTemplate = null;
	protected $iRoleFlag = null;

	public function addAction()
	{
		if ($_POST) {
			$this->values = $_POST;
			if (!$this->cContentClass->Add($_POST)) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		}
		$this->title = 'Добавление';
		$this->button = 'Добавить';
		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function editAction()
	{
		if ($_POST) {
			$this->values = array_merge($this->values,$_POST);
			if (!$this->cContentClass->Update($this->id,$_POST)) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		}
		$this->title = 'Редактирование';
		$this->button = 'Сохранить';
		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function deleteAction()
	{
		if (!$this->cContentClass->Delete($this->id)) {
			$this->error = CChecker::GetLastError();
			$this->cDispatcher->SetTemplate('error');
		} else {
			$this->cDispatcher->Redirect($this->sRedirectUrl);
		}
	}

	public function _init()
	{
		if (!CAdmin::getInstance()->CheckRole($this->iRoleFlag)) $this->cDispatcher->Redirect('/admin/');

		if (!$this->cContentClass) {
			CDebug::AddLog('No content class initialized');
			return;
		}

		if ($this->id) {
			$this->values = $this->cContentClass->GetItem($this->id);
			if (!$this->values['id']) return false;
		} else {
			$this->values = array();
		}
		return true;
	}

}

?>