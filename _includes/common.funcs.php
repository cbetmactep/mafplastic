<?

function L10N($sErrCode)
{
	static $aL10N;
	if (!$aL10N) $aL10N = @include(dirname(__FILE__).'/l10n.inc.php');
	if (!$sError = $aL10N['ru'][$sErrCode]) $sError = $sErrCode;
	return $sError;
}

function __autoload($sClassName)
{
	$sClassName = strtolower(substr($sClassName,1));
	if (file_exists(dirname(__FILE__).'/'.$sClassName.'.class.php')) require_once(dirname(__FILE__).'/'.$sClassName.'.class.php');
	elseif (file_exists(dirname(__FILE__).'/core/'.$sClassName.'.class.php')) require_once(dirname(__FILE__).'/core/'.$sClassName.'.class.php');
}

function GeneratePassword()
{
	$arr_pass="1234567890QWERTYUIOPASDFGHJKLZXCVBNM1234567890qwertyuiopasdfghjklzxcvbnm1234567890";
	$len=rand(8,12);
	for ($i=0;$i<$len;$i++) {
		$pass.=$arr_pass[rand(0,81)];
	}
	return $pass;
}

function HR_time($iTime)
{
	if ($iTime<60) return $iTime.L10N('TIME_SEC');
	if ($iTime<120) return '1'.L10N('TIME_MIN').' '.($iTime-60).L10N('TIME_SEC');
	if ($iTime<3600) return floor($iTime/60).L10N('TIME_MIN');
	return floor($iTime/3600).L10N('TIME_HOUR').' '.floor(($iTime%3600)/60).L10N('TIME_MIN');
}

function format_time($iTime) {
	return date('H:i:s',$iTime);
}

function format_date($mDate,$mDefault = null, $bWithTime = false) {
	if (!is_numeric($mDate)) $mDate = strtotime($mDate);
	if (!$mDate && $mDefault) {
		if (!is_int($mDefault)) $mDate = strtotime($mDefault);
		else $mDate = $mDefault;
	}
	if (!$mDate) return '';
	return date(CSetts::getInstance()->date_format.($bWithTime?' H:i':''),$mDate);
}

function format_HR_date($mDate,$mDefault = null,$bUpper = false) {
	global $aMonths;
	if (!is_int($mDate)) $mDate = strtotime($mDate);
	if (!$mDate && $mDefault) {
		if (!is_int($mDefault)) $mDate = strtotime($mDefault);
		else $mDate = $mDefault;
	}
	if (!$mDate) return '';

	return date('j',$mDate).' '.($bUpper?mb_strtoupper($aMonths[date('n',$mDate)]):$aMonths[date('n',$mDate)]).' '.date('Y',$mDate).' года';
}

function HR_size($iSize)
{
	if($iSize<1024) return sprintf("%.2f",$iSize).L10N('SIZE_BYTES');
	$iSize = $iSize/1024;
	if($iSize<1024) return sprintf("%.2f",$iSize).L10N('SIZE_KBYTES');
	$iSize = $iSize/1024;
	if($iSize<1024) return sprintf("%.2f",$iSize).L10N('SIZE_MBYTES');
	$iSize = $iSize/1024;
	return sprintf("%.2f",$iSize).L10N('SIZE_GBYTES');
}

function HTMLTimeSelect($sTimeFrom, $sTimeTo, $sTimeSelected)
{
	$sReturn = '';
	for ($c = 0; $c <= 23; $c++) {
		$sTime = sprintf('%02u',$c).':00';
		if ($sTimeFrom <= $sTime && $sTimeTo >= $sTime)
			$sReturn .= '<option value="'.$sTime.'"'.($sTime.':00' == $sTimeSelected?' selected':'').'>'.$sTime.'</option>';
		$sTime = sprintf('%02u',$c).':30';
		if ($sTimeFrom <= $sTime && $sTimeTo >= $sTime)
			$sReturn .= '<option value="'.$sTime.'"'.($sTime.':00' == $sTimeSelected?' selected':'').'>'.$sTime.'</option>';
	}
	return $sReturn;
}

function TimeList($sTimeFrom, $sTimeTo)
{
	$aReturn = array();
	for ($c = 0; $c <= 23; $c++) {
		$sTime = sprintf('%02u',$c).':00';
		if ($sTimeFrom <= $sTime && $sTimeTo >= $sTime) $aReturn[] = $sTime;
		$sTime = sprintf('%02u',$c).':30';
		if ($sTimeFrom <= $sTime && $sTimeTo >= $sTime) $aReturn[] = $sTime;
	}
	return $aReturn;
}

function DownloadFile($sFrom,$sTo)
{
	if (!file_put_contents($sTo,file_get_contents($sFrom))) return false;
	return true;
}

function Translit($cyr_str)
{
	$tr = array(
		"Ґ"=>"G","Ё"=>"YO","Є"=>"E","Ї"=>"YI","І"=>"I",
		"і"=>"i","ґ"=>"g","ё"=>"yo","№"=>"#","є"=>"e",
		"ї"=>"yi","А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
		"Д"=>"D","Е"=>"E","Ж"=>"ZH","З"=>"Z","И"=>"I",
		"Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
		"О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
		"У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
		"Ш"=>"SH","Щ"=>"SCH","Ъ"=>"'","Ы"=>"YI","Ь"=>"",
		"Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
		"в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"zh",
		"з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
		"м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
		"с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
		"ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"'",
		"ый"=>"iy","ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu",
		"я"=>"ya"," "=>"_"
	);
	return strtr($cyr_str,$tr);
}

function RemoveDir($dir) {

	if ($handle = opendir($dir)) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != "..") {
				unlink($dir."/".$file);
			}
		}
		closedir($handle);
	} else return false;
	rmdir($dir);
	return true;
}

function IconvFile($sFilename)
{
	$sContents = file_get_contents($sFilename);
	$sContents = iconv('windows-1251','UTF-8',$sContents);
	file_put_contents($sFilename,$sContents);
}

function html2rgb($color)
{
	if ($color[0] == '#')
	$color = substr($color, 1);

	if (strlen($color) == 6)
		list($r, $g, $b) = array($color[0].$color[1],
									$color[2].$color[3],
									$color[4].$color[5]);
	elseif (strlen($color) == 3)
		list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
	else
		return false;

	$r = hexdec($r);
	$g = hexdec($g);
	$b = hexdec($b);

	return array($r, $g, $b);
}

function cutString($sString, $iLength = 100)
{
	if (mb_strlen($sString) <= $iLength + 5) return $sString;
	$iSpacePos = mb_strpos($sString, ' ', $iLength);
	if ($iSpacePos === false) $iSpacePos = mb_strrpos($sString, ' ');
	if ($iSpacePos === false) $iSpacePos = $iLength;
	$sString = mb_substr($sString, 0, $iSpacePos);
	$sString = rtrim($sString, '.,:?!');
	return $sString.'...';
}

function escape($sValue)
{
	return htmlentities($sValue, ENT_COMPAT, 'UTF-8');
}

function error($sError)
{
	if (!$sError) return '';
	return '<p class="error">'.$sError.'</p>';
}

function prepareForJS($sText)
{
	$sText = htmlentities($sText, ENT_COMPAT, 'UTF-8');
	$sText = nl2br($sText);
	$sText = preg_replace('/\s+/is', ' ', $sText);
	return $sText;
}

function makeSnippet($sKeyword, $sContent, $iPadding = 200)
{
	$sContent = strip_tags($sContent);
	$iPosition = strpos($sContent, $sKeyword);
	$sSnippet = substr($sContent, $iPosition - $iPadding,
				   (strlen($sKeyword) + $iPadding * 2));
	return '...'.$sSnippet.'...';
}

function itemsCount($iCount)
{
	if ($iCount % 100 >= 11 && $iCount % 100 <= 14) return L10N('COUNT_ITEMS');
	elseif ($iCount % 10 == 1) return L10N('COUNT_ITEMS_1');
	elseif ($iCount % 10 >= 2 && $iCount % 10 <= 4) return L10N('COUNT_ITEMS_2-4');
	else return L10N('COUNT_ITEMS');
}

function inBasket($iID)
{
	if (empty($_SESSION['basket'])) return false;
	if (empty($_SESSION['basket'][$iID])) return false;
	return true;
}

?>