<?php

class CDb
{

	private $query = 0;
	private $connection = 0;
	private $last_errno;
	private $last_error;
	private static $_instance;

	/**
	 * Database class
	 *
	 * @return CDb
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	function connect($connect_info)
	{
		$this->connection = @mysqli_connect($connect_info['host'],
									  $connect_info['username'], $connect_info['password'],
									  $connect_info['database']);
		if (!$this->connection) {
			$this->_halt('ERROR [FATAL] : Cannot open mySQL Connection');
		}
	}

	function fetch_array()
	{
		$row = mysqli_fetch_array($this->query);
		return $row;
	}

	function query($sql)
	{
		$this->query = @mysqli_query($this->connection, $sql);
		if (!$this->query) {
			$this->last_error = mysqli_error($this->connection);
			$this->last_errno = mysqli_errno($this->connection);
			CChecker::SetLastErrors('DB_ERROR');
			return false;
		}
		return true;
	}

	function query_first($sql)
	{
		if (!$this->query($sql)) return false;
		$return = $this->fetch_array();
		$this->free_result();
		return (array) $return;
	}

	function affected_rows()
	{
		return mysqli_affected_rows($this->connection);
	}

	function query_array($sql)
	{
		if (!$this->query($sql)) return false;
		$return = array();
		while ($row = $this->fetch_array()) {
			$return[] = $row;
		}
		$this->free_result();
		return $return;
	}

	function query_field($sql)
	{
		if (!$this->query($sql)) return false;
		$row = mysqli_fetch_row($this->query);
		if (isset($row[0])) return $row[0];
		return false;
	}

	function escape($mValue)
	{
		//return mysqli_real_escape_string($this->connection, $mValue);
                return $mValue;
	}

	function insert($sTableName, $aData)
	{
		if (empty($aData)) return false;
		foreach ($aData as &$mValue) {
			if (!is_int($mValue) && !is_float($mValue)) {
				if ($mValue === null) {
					$mValue = 'NULL';
				} else {
					$mValue = "'".$this->escape($mValue)."'";
				}
			}
		}
		unset($mValue);
		$q = "INSERT INTO ".$sTableName." (`".join("`,`", array_keys($aData))."`)
							VALUES (".join(",", $aData).")";

		if (!$this->query($q)) return false;
		return true;
	}

	function insertOrUpdate($sTableName, $aData, $aUpdData)
	{
		if (empty($aData)) return false;
		foreach ($aData as &$mValue) {
			if (!is_int($mValue) && !is_float($mValue)) {
				if ($mValue === null) {
					$mValue = 'NULL';
				} else {
					$mValue = $this->escape($mValue);
				}
			}
		}
		$q = "INSERT INTO ".$sTableName." (`".join("`,`", array_keys($aData))."`)
							VALUES (".join(",", $aData).")
							ON DUPLICATE KEY UPDATE ";
		$qu = array();
		foreach ($aUpdData as $sField) {
			$qu[] = $sField."=".$aData[$sField];
		}
		$q .= join(',', $qu);
		if (!$this->query($q)) return false;
		return true;
	}

	function update($sTableName, $aData, $aConditions = array())
	{
		if (!$aData) return true;
		foreach ($aData as $sField => &$mValue) {
			if (!is_int($mValue) && !is_float($mValue)) {
				if ($mValue === null) {
					$mValue = 'NULL';
				} else {
					$mValue = $this->escape($mValue);
				}
			}
			$q[] = $sField.'='.$mValue;
		}
		unset($mValue);
		$aWhere = array();
		if ($aConditions) foreach ($aConditions as $sField => &$mValue) {
			if (!is_int($mValue) && !is_float($mValue)) {
				$mValue = $this->escape($mValue);
			}
			$aWhere[] = $sField."='".$mValue."'";
		}
		unset($mValue);
		if ($aWhere) {
			$sWhere = ' WHERE '.join(' AND ', $aWhere);
		}
		$q = 'UPDATE '.$sTableName.' SET '.join(', ', $q).$sWhere;
		if (!$this->query($q)) return false;
		return true;
	}

	function delete($sTableName, $aConditions = array())
	{
		$aWhere = array();
		if ($aConditions) foreach ($aConditions as $sField => &$mValue) {
			if (!is_int($mValue) && !is_float($mValue)) {
				$mValue = $this->escape($mValue);
			}
			$aWhere[] = $sField."="."'".$mValue."'";
		}
		unset($mValue);
		if ($aWhere) {
			$sWhere = " WHERE ".join(' AND ', $aWhere);
		}
		$q = "DELETE FROM ".$sTableName.$sWhere;
		if (!$this->query($q)) return false;
		return true;
	}

	function begin()
	{
		if (!$this->query('BEGIN')) return false;
		return true;
	}

	function rollback()
	{
		if (!$this->query('ROLLBACK')) return false;
		return true;
	}

	function commit()
	{
		if (!$this->query('COMMIT')) return false;
		return true;
	}

	function free_result()
	{
		@mysqli_free_result($this->query);
	}

	function num_rows()
	{
		return mysqli_num_rows($this->query);
	}

	function get_num_rows($sql)
	{
		if (!$this->query($sql)) return false;
		return $this->num_rows();
	}

	function insert_id()
	{
		return mysqli_insert_id($this->connection);
	}

	function fetch_row($query)
	{
		return mysqli_fetch_row($query);
	}

	function _halt($the_error)
	{
		echo htmlspecialchars($the_error);
		die();
	}

	function get_error()
	{
		return $this->last_error;
	}

	function get_errno()
	{
		return $this->last_errno;
	}

}

?>