<?

class CSetts
{

	private $aCheckRules = array(
		'site_url' => array('type' => CChecker::TYPE_STRING, 'flags' => 4105, 'params' => array('preg' => CChecker::PREG_URL)),
		'site_address' => array('type' => CChecker::TYPE_STRING, 'flags' => 4105, 'params' => array('preg' => CChecker::PREG_PATH)),
		'site_email' => array('type' => CChecker::TYPE_STRING, 'flags' => 4105, 'params' => array('preg' => CChecker::PREG_EMAIL)),
		'site_title' => array('type' => CChecker::TYPE_STRING, 'flags' => 5125, 'params' => array('max' => 255)),
		'net_price_margin' => array('type' => CChecker::TYPE_FLOAT),
		'net_price_work' => array('type' => CChecker::TYPE_FLOAT),
		'net_price_wearout' => array('type' => CChecker::TYPE_FLOAT),
		'net_price_rent' => array('type' => CChecker::TYPE_FLOAT),
		'net_price_utilities' => array('type' => CChecker::TYPE_FLOAT),
		'currency_usd' => array('type' => CChecker::TYPE_FLOAT),
		'currency_eur' => array('type' => CChecker::TYPE_FLOAT),
		'currency_updated' => array('type' => CChecker::TYPE_STRING, 'flags' => 129)
	);

	private $aSetts = array();

	private static $_instance;

	/**
	 * Settings class
	 *
	 * @return CSetts
	 */
	public static function getInstance()
	{
		if (!self::$_instance) self::$_instance = new self();
		return self::$_instance;
	}

	private function __construct()
	{
		if (!$this->aSetts) $this->aSetts = $this->_getSetts();
	}

	function __set($sVar,$sValue) {
		$this->aSetts[$sVar] = $sValue;
	}

	function &__get($sVar)
	{
		if (array_key_exists($sVar,$this->aSetts)) return $this->aSetts[$sVar];
		return null;
	}

	public function GetSetts()
	{
		return $this->aSetts;
	}

	private function _getSetts()
	{
		global $db;

		$q = "SELECT * FROM setts";
		if (!$oRes = $db->query($q)) return array();
		while ($aRow = $db->fetch_array($oRes)) {
			$aSetts[$aRow['name']] = $aRow['value'];
		}
		return $aSetts;
	}

	public function Save(&$paData)
	{
		$paData = array_intersect_key($paData, $this->aCheckRules);
		$aCheckRules = array_intersect_key($this->aCheckRules, $paData);

		if (!CChecker::CheckArray($paData, $aCheckRules)) {
			CChecker::SetLastErrors('ERROR_SOME_ERRORS');
			return false;
		}

		foreach ($paData as $sKey => $mValue) {
			$aData = array(
				'value' => $mValue
			);
			$aConditions = array(
				'name' => $sKey
			);
			if (!CDb::getInstance()->update('setts', $aData, $aConditions)) return false;
		}

		return true;
	}
}

?>
