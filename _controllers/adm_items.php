<?

class adm_items extends CAdm_controller
{

	function indexAction()
	{
		if ($this->cat_id) {
			if (!empty($_POST['ids']) && empty($_GET['articul']) && empty($aFilter)) {
				$aIds = array_filter(array_unique(array_map('intval', $_POST['ids'])));
				if (!empty($aIds)) foreach ($aIds as $iNum => $iID) {
					$this->cContentClass->SetOrder($iID, $iNum + 1);
				}
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}

			$cSInfo = new CSInfo(array(
				'cat_id' => $this->cat_id,
			));
			if (!empty($_GET['articul'])) {
				$cSInfo->articul = $_GET['articul'];
			}
			$this->items = CItems::getInstance()->GetList($cSInfo);

			$sTpl = 'items';
		} else {
			if (!empty($_GET['articul'])) {
				$cSInfo = new CSInfo(array(
					'articul' => $_GET['articul']
				));
				$this->items = CItems::getInstance()->GetList($cSInfo);

				$sTpl = 'items_search';
			} else {
				$sTpl = 'items_cats';
			}
		}
		$this->cDispatcher->SetTemplate($sTpl);
	}

	public function addAction()
	{
		if ($_POST) {
			$this->values = array_merge($this->values,$_POST);
			$_POST['cat_id'] = $this->catinfo['id'];
			$iNewID = $this->cContentClass->Add($_POST, $_FILES['image']['tmp_name'],
					$_FILES['image_usage']['tmp_name'], $_FILES['image_configurator_scheme']['tmp_name']);
			if (!$iNewID) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				if ($this->id) {
					CItems::getInstance()->CopyFiles($this->id, $iNewID);
					$cSInfo = new CSInfo(array(
						'item_id' => $this->id
					));
					$aImages = CItemGallery::getInstance()->GetList($cSInfo);
					if (!empty($aImages)) {
						$iMainImageID = 0;
						foreach ($aImages as $aImage) {
							$aImage['item_id'] = $iNewID;
							$iNewImageID = CItemGallery::getInstance()->Add($aImage);
							if (!empty($iNewImageID)) {
								CItemGallery::getInstance()->CopyFiles($aImage['id'], $iNewImageID);
								if ($aImage['main']) {
									$iMainImageID = $iNewImageID;
								}
							}
						}
						if ($iMainImageID) {
							CItemGallery::getInstance()->SwitchMain($iNewID, $iMainImageID);
						}
					}
				}
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		} else {
			if (!$this->id) {
				$this->values = array('enabled' => 1, 'cat_id' => $this->catinfo['id']);
			}
			if (!empty($_SERVER['HTTP_REFERER'])) {
				$_SESSION['back_url'] = $_SERVER['HTTP_REFERER'];
			} else {
				$_SESSION['back_url'] = '/admin/items/'.$this->cat_id.'/';
			}
		}
		$this->title = 'Добавление';
		$this->button = 'Добавить';

		$cSInfo = new CSInfo(array(
			'cat_id' => $this->catinfo['id']
		));
		$this->items = CItems::getInstance()->GetList($cSInfo);
		$cSInfo = new CSInfo();
		$this->component_items = CItems::getInstance()->GetList($cSInfo);
		$this->cats = CCats::getInstance()->GetList($cSInfo);
		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function editAction()
	{
		if ($_POST) {
			$this->values = array_merge($this->values, $_POST);
			if (!$this->cContentClass->Update($this->id, $_POST, $_FILES['image']['tmp_name'],
					$_FILES['image_usage']['tmp_name'], $_FILES['image_configurator_scheme']['tmp_name'])) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		} else {
			if (!empty($_SERVER['HTTP_REFERER'])) {
				$_SESSION['back_url'] = $_SERVER['HTTP_REFERER'];
			} else {
				$_SESSION['back_url'] = '/admin/items/'.$this->cat_id.'/';
			}
		}
		$this->title = 'Редактирование';
		$this->button = 'Сохранить';

		$cSInfo = new CSInfo(array(
			'cat_id' => $this->catinfo['id'],
			'except_id' => $this->id
		));
		$this->items = CItems::getInstance()->GetList($cSInfo);
		$cSInfo = new CSInfo(array(
			'except_id' => $this->id
		));
		$this->component_items = CItems::getInstance()->GetList($cSInfo);
		$cSInfo = new CSInfo();
		$this->cats = CCats::getInstance()->GetList($cSInfo);
		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function deleteAction()
	{
		$cSInfo = new CSInfo(array(
			'item_id' => (int)$this->id
		));
		$aItemGallery = CItemGallery::getInstance()->GetList($cSInfo);
		if ($aItemGallery) foreach ($aItemGallery as $aItem) {
			CItemGallery::getInstance()->Delete($aItem['id']);
		}
		$aItemColors = CItemColors::getInstance()->GetList($cSInfo);
		if ($aItemColors) foreach ($aItemColors as $aItem) {
			CItemColors::getInstance()->Delete($aItem['id']);
		}
		parent::deleteAction();
	}

	public function switchstatusAction()
	{
		$this->cContentClass->SwitchStatus($this->id);
		$this->cDispatcher->Redirect($this->sRedirectUrl);
	}

	public function articulautocompleteAction()
	{
		if (CChecker::CheckString($_GET['term'], 1028, array('max' => 50))) {
			$_GET['term'] = '';
		}

		$aJSON = array();
		if ($_GET['term']) {
			$cSInfo = new CSInfo(array(
				'articul' => $_GET['term'],
				'cat_id' => (int)$_GET['cat_id']
			));
			$aItems = CItems::getInstance()->GetList($cSInfo);

			if (!empty($aItems)) foreach ($aItems as $aItem) {
				$aJSON[] = array(
					'label' => $aItem['articul'],
					'id' => $aItem['id'],
					'cat_id' => $aItem['cat_id']
				);
			}
		}

		header('Content-Type: application/json');
		echo json_encode($aJSON);
		$this->cDispatcher->bNoRender = true;
	}

	public function _init()
	{
		if ($this->cat_id) {
			$this->catinfo = CCats::getInstance()->GetItem($this->cat_id);
			if (empty($this->catinfo['id'])) return false;
		}

		$cSInfo = new CSInfo(array(
			'hierarchy' => true
		));
		$this->structCats = CCats::getInstance()->GetList($cSInfo);

		$this->cContentClass = CItems::getInstance();
		if (!empty($_SESSION['back_url'])) {
			$this->sRedirectUrl = $_SESSION['back_url'];
		} else {
			$this->sRedirectUrl = '/admin/items/'.$this->cat_id.'/';
		}
		$this->sEditTemplate = 'items_form';
		$this->iRoleFlag = CAdmin::ROLE_MANAGEITEMS;
		return parent::_init();
	}

}

?>
