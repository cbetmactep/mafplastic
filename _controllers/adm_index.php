<?

class adm_index extends CController
{

	function headerAction()
	{
		$this->cDispatcher->SetTemplate('_header');
	}

	function footerAction()
	{
		$this->cDispatcher->SetTemplate('_footer');
	}

	function indexAction()
	{
		$this->cDispatcher->SetTemplate('index');
	}

	function page404Action()
	{
		$this->cDispatcher->SetHeader404();
		$this->cDispatcher->SetTemplate('404');
	}

	function loginAction()
	{
		if (CAdmin::getInstance()->isLogged()) $this->cDispatcher->Redirect('/admin/');

		if ($_POST) {
			if (!CAdmin::getInstance()->Login($_POST)) {
				$this->error = CChecker::GetLastError();
			} else {
				$this->cDispatcher->Redirect('/admin/');
			}
		}

		$this->cDispatcher->SetTemplate('login');
	}

	function logoutAction()
	{
		CAdmin::getInstance()->Logout();
		$this->cDispatcher->Redirect('/admin/');
	}

}

?>