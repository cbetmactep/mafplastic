<?php

class constructor extends CController
{
	const MIN_CELL_HEIGHT = 90;
	const MIN_CELL_WIDTH = 90;

	function indexAction()
	{
		$this->pageinfo = CPages::getInstance()->GetItemByPath('/constructor/step1/');

		if (empty($_SESSION['constructor'])) {
			$this->create();
			$this->save();
		}

		$this->cDispatcher->SetTemplate('constructor');
	}

	function resetAction()
	{
		$_SESSION['constructor'] = null;
		$this->cDispatcher->Redirect('/constructor/');
	}

	function dataAction()
	{
		$this->returnJson();
	}

	function frameAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		$fWidth = isset($_GET['width'])?(int)$_GET['width']:0;
		$fHeight = isset($_GET['height'])?(int)$_GET['height']:0;
		if ($fWidth && $fHeight) {
			if ($this->width !== $fWidth || $this->height !== $fHeight) {
				$this->width = $fWidth;
				$this->height = $fHeight;
				$this->calculateFrame();
				$this->checkPaddings();
				$this->fillFrame();
				$this->save();
			}
		} else {
			return false;
		}

		$this->returnJson();
	}

	function cellAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		$fCellWidth = isset($_GET['cell_width'])?(int)$_GET['cell_width']:0;
		$fCellHeight = isset($_GET['cell_height'])?(int)$_GET['cell_height']:0;

		if ($fCellWidth && $fCellHeight) {
			/*if ($fCellWidth%5) {
				$fCellWidth = round($fCellWidth / 10) * 10;
			}
			if ($fCellHeight%5) {
				$fCellHeight = round($fCellHeight / 10) * 10;
			}*/
			if ($fCellHeight < self::MIN_CELL_HEIGHT) {
				$fCellHeight = self::MIN_CELL_HEIGHT;
			}
			if ($fCellWidth < self::MIN_CELL_WIDTH) {
				$fCellWidth = self::MIN_CELL_WIDTH;
			}

			if ($this->cellWidth !== $fCellWidth || $this->cellHeight != $fCellHeight) {
				$this->cellWidth = $fCellWidth;
				$this->cellHeight = $fCellHeight;
				$this->calculateFrame();
				$this->checkPaddings();
				if ($this->cols < 1 && $this->rows < 1) {
					return false;
				}
				$this->fillFrame();
				$this->save();
			}
		} else {
			return false;
		}

		$this->returnJson();
	}

	function addVerticalRopeAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		$this->calculateFrame();
		$this->checkPaddings();
		$iPossibleRows = $this->rows;
		$iPossibleCols = $this->cols;
		$this->_init();

		if (!empty($_GET['keep_vertical_cell_size'])) {
			if ($this->cols + 1 <= $iPossibleCols) {
				$this->cols++;
				$this->paddingLeft -= $this->cellWidth / 2;
				$this->fillFrame();
				$this->save();
			}
		} else {
			$fCellWidth = floor($this->width / ($this->cols + 2));
			if ($fCellWidth%5) {
				$fCellWidth = round($fCellWidth / 10) * 10;
			}
			if ($fCellWidth < self::MIN_CELL_WIDTH) {
				$fCellWidth = self::MIN_CELL_WIDTH;
			}
			$fPaddingLeft = round(($this->width - $fCellWidth * $this->cols) / 2);
			if ($fPaddingLeft >= 126) {
				$this->cols++;
				$this->cellWidth = $fCellWidth;
				$this->paddingLeft = $fPaddingLeft;
				$this->fillFrame();
				$this->save();
			}
		}

		$this->returnJson();
	}

	function removeVerticalRopeAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		/*$this->calculateFrame();
		$this->checkPaddings();
		$iPossibleRows = $this->rows;
		$iPossibleCols = $this->cols;
		$this->_init();*/

		if (!empty($_GET['keep_vertical_cell_size'])) {
			if ($this->cols - 1 >= 1) {
				$this->cols--;
				$this->paddingLeft += $this->cellWidth / 2;
				$this->fillFrame();
				$this->save();
			}
		} else {
			$fCellWidth = floor($this->width / $this->cols);
			if ($fCellWidth%5) {
				$fCellWidth = round($fCellWidth / 10) * 10;
			}
			$fPaddingLeft = round(($this->width  - $fCellWidth * ($this->cols - 2)) / 2);
			if ($this->cols - 1 >= 1) {
				$this->cols--;
				$this->cellWidth = $fCellWidth;
				$this->paddingLeft = $fPaddingLeft;
				$this->fillFrame();
				$this->save();
			}
		}

		$this->returnJson();
	}

	function addHorizontalRopeAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		$this->calculateFrame();
		$this->checkPaddings();
		$iPossibleRows = $this->rows;
		$iPossibleCols = $this->cols;
		$this->_init();

		if (!empty($_GET['keep_horizontal_cell_size'])) {
			if ($this->rows + 1 <= $iPossibleRows) {
				$this->rows++;
				$this->paddingTop -= $this->cellHeight / 2;
				$this->fillFrame();
				$this->save();
			}
		} else {
			$fCellHeight = floor($this->height / ($this->rows + 2));
			if ($fCellHeight%5) {
				$fCellHeight = round($fCellHeight / 10) * 10;
			}
			if ($fCellHeight < self::MIN_CELL_HEIGHT) {
				$fCellHeight = self::MIN_CELL_HEIGHT;
			}
			$fPaddingTop = round(($this->height - $fCellHeight * $this->rows) / 2);
			if ($fPaddingTop >= 126) {
				$this->rows++;
				$this->cellHeight = $fCellHeight;
				$this->paddingTop = $fPaddingTop;
				$this->fillFrame();
				$this->save();
			}
		}

		$this->returnJson();
	}

	function removeHorizontalRopeAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		/*$this->calculateFrame();
		$this->checkPaddings();
		$iPossibleRows = $this->rows;
		$iPossibleCols = $this->cols;
		$this->_init();*/

		if (!empty($_GET['keep_horizontal_cell_size'])) {
			if ($this->rows - 1 >= 1) {
				$this->rows--;
				$this->paddingTop += $this->cellHeight / 2;
				$this->fillFrame();
				$this->save();
			}
		} else {
			$fCellHeight = floor($this->height / $this->rows);
			if ($fCellHeight%5) {
				$fCellHeight = round($fCellHeight / 10) * 10;
			}
			$fPaddingTop = round(($this->height - $fCellHeight * ($this->rows - 2)) / 2);
			if ($this->rows - 1 >= 1) {
				$this->rows--;
				$this->cellHeight = $fCellHeight;
				$this->paddingTop = $fPaddingTop;
				$this->fillFrame();
				$this->save();
			}
		}

		$this->returnJson();
	}

	private function calculateFrame()
	{
		$this->cols = floor($this->width / $this->cellWidth);
		$this->rows = floor($this->height / $this->cellHeight);
		$this->paddingLeft = 0;
		$this->paddingTop = 0;

		if ($this->cols < 1 || $this->rows < 1) {
			return;
		}

		//calculating base params
		$fWidthRemains = $this->width - $this->cols * $this->cellWidth;
		if (!$fWidthRemains) {
			$this->paddingLeft = $this->cellWidth;
			$this->cols--;
		} elseif ($fWidthRemains < 126) {
			$this->paddingLeft = ceil($this->cellWidth / 2 + $fWidthRemains / 2);
		} else {
			$this->paddingLeft = ceil($fWidthRemains / 2);
			$this->cols++;
		}

		$fHeightRemains = $this->height - $this->rows * $this->cellHeight;
		if (!$fHeightRemains) {
			$this->paddingTop = $this->cellHeight;
			$this->rows--;
		} elseif ($fHeightRemains < 126) {
			$this->paddingTop = ceil($this->cellHeight / 2 + $fHeightRemains / 2);
		} else {
			$this->paddingTop = ceil($fHeightRemains / 2);
			$this->rows++;
		}
	}

	private function checkPaddings()
	{
		$this->paddingTop = ceil(($this->height - ($this->rows - 1) * $this->cellHeight) / 2);
		//check if padding less than allowed by items
		$maxHorizontalSize = 0;
		for ($c = 0; $c < $this->rows; $c++) {
			$junctionSizeLeft = 0;
			$index = $c;
			if (!empty($this->junctions[$index])) {
				$junctionSizeLeft = $this->junctions[$index]['item_size'];
			}
			$connectionSizeLeft = 0;
			$index = $c;
			if (!empty($this->leftConnections[$index])) {
				$connectionSizeLeft = $this->leftConnections[$index]['item_size'];
			}
			$junctionSizeRight = 0;
			$index = $c + ($this->cols - 1) * $this->rows;
			if (!empty($this->junctions[$index])) {
				$junctionSizeRight = $this->junctions[$index]['item_size'];
			}
			$connectionSizeRight = 0;
			$index = $c;
			if (!empty($this->rightConnections[$index])) {
				$connectionSizeRight = $this->rightConnections[$index]['item_size'];
			}
			$maxHorizontalSize = max($maxHorizontalSize, $junctionSizeLeft + $connectionSizeLeft, $junctionSizeRight + $connectionSizeRight);
		}
		$maxVerticalSize = 0;
		for ($c = 0; $c < $this->cols; $c++) {
			$junctionSizeTop = 0;
			$index = $c * $this->rows;
			if (!empty($this->junctions[$index])) {
				$junctionSizeTop = $this->junctions[$index]['item_size'];
			}
			$connectionSizeTop = 0;
			$index = $c;
			if (!empty($this->topConnections[$index])) {
				$connectionSizeTop = $this->topConnections[$index]['item_size'];
			}
			$junctionSizeBottom = 0;
			$index = ($c + 1) * $this->rows - 1;
			if (!empty($this->junctions[$index])) {
				$junctionSizeBottom = $this->junctions[$index]['item_size'];
			}
			$connectionSizeBottom = 0;
			$index = $c;
			if (!empty($this->bottomConnections[$index])) {
				$connectionSizeBottom = $this->bottomConnections[$index]['item_size'];
			}
			$maxVerticalSize = max($maxVerticalSize, $junctionSizeTop + $connectionSizeTop, $junctionSizeBottom + $connectionSizeBottom);
		}
		//recalc if has t connectors
		if (($this->hasBottomTRope() && !$this->hasTopTRope()) || ($this->hasTopTRope() && !$this->hasBottomTRope())) {
			$this->paddingTop = $this->height - $this->cellHeight * $this->rows;
		}
		while ($this->paddingLeft < $maxHorizontalSize) {
			$this->paddingLeft += $this->cellWidth / 2;
			$this->cols--;
		}
		while ($this->paddingTop < $maxVerticalSize) {
			if (($this->hasBottomTRope() && !$this->hasTopTRope()) || ($this->hasTopTRope() && !$this->hasBottomTRope())) {
				$this->paddingTop += $this->cellHeight;
			} else {
				$this->paddingTop += $this->cellHeight / 2;
			}
			$this->rows--;
		}
		if ($this->hasTopTRope() && !$this->hasBottomTRope()) {
			$this->paddingTop = $this->cellHeight;
		}
	}

	private function results()
	{
		if (empty($this->cellWidth) || empty($this->cellHeight)) {
			return null;
		}
		if (empty($this->rope)) {
			return null;
		}
		$iMaxJunctions = $this->cols * $this->rows;
		if (empty($this->junctions) || count($this->junctions) < $iMaxJunctions) {
			return null;
		}

		$iCols = $this->cols;
		$iRows = $this->rows;
		if ($this->hasTopTRope()) {
			$iRows++;
		}
		if ($this->hasBottomTRope()) {
			$iRows++;
		}
		if (empty($this->topConnections) || count($this->topConnections) < $iCols) {
			return null;
		}
		if (empty($this->bottomConnections) || count($this->bottomConnections) < $iCols) {
			return null;
		}
		if (empty($this->leftConnections) || count($this->leftConnections) < $iRows) {
			return null;
		}
		if (empty($this->rightConnections) || count($this->rightConnections) < $iRows) {
			return null;
		}

		if (empty($_SESSION['constructor']['results'])) {
			$fTime = 0;
			$aItemsQuants = array();
			$aConstructorItemsQuants = array();
			$aHardwareItemsQuants = array();
			$aHardwareStudQuants = array();

			//calculating ropes lengths
			$aRopes = array();
			$iRowAdd = 0;
			if ($this->hasTopTRope()) {
				$fLength = $this->width;
				$fLength += $this->leftConnections[0]['rope_increment'];
				$fLength += $this->rightConnections[0]['rope_increment'];
				$iRowAdd = 1;
				$aRopes[$fLength] = 1;
			}
			if ($this->hasBottomTRope()) {
				$fLength = $this->width;
				$fLength += $this->leftConnections[$this->rows + $iRowAdd]['rope_increment'];
				$fLength += $this->rightConnections[$this->rows + $iRowAdd]['rope_increment'];
				if (!isset($aRopes[$fLength])) {
					$aRopes[$fLength] = 1;
				} else {
					$aRopes[$fLength] ++;
				}
			}
			for ($iRow = 0; $iRow < $this->rows; $iRow ++) {
				$fLength = $this->width;
				$fLength += $this->leftConnections[$iRow + $iRowAdd]['rope_increment'];
				$fLength += $this->rightConnections[$iRow + $iRowAdd]['rope_increment'];
				for ($iCol = 0; $iCol < $this->cols; $iCol ++) {
					$fLength += $this->junctions[$iRow + $iCol * $this->rows]['rope_increment'];
				}
				if (!isset($aRopes[$fLength])) {
					$aRopes[$fLength] = 1;
				} else {
					$aRopes[$fLength] ++;
				}
			}
			for ($iCol = 0; $iCol < $this->cols; $iCol ++) {
				$fLength = $this->height;
				$fLength += $this->topConnections[$iCol]['rope_increment'];
				$fLength += $this->bottomConnections[$iCol]['rope_increment'];
				for ($iRow = 0; $iRow < $this->rows; $iRow ++) {
					$fLength += $this->junctions[$iRow + $iCol * $this->rows]['rope_increment'];
				}
				if (!isset($aRopes[$fLength])) {
					$aRopes[$fLength] = 1;
				} else {
					$aRopes[$fLength] ++;
				}
			}

			$fTotalRopeLength = 0;
			$aRopesDescr = array();
			foreach ($aRopes as $fLength => $iQuant) {
				$fTotalRopeLength += $fLength * $iQuant;
				$aRopesDescr[] = $fLength.' мм. x '.$iQuant.' шт.';
			}
			$aItemsQuants[$this->rope['item']['id']][$this->rope['color']['id']] = round($fTotalRopeLength / 1000, 3);
			$aConstructorItemsQuants[$this->rope['id']] = 1;

			$aJunctions = $this->junctions;
			$aConnections = array_merge($this->topConnections, $this->bottomConnections, $this->leftConnections, $this->rightConnections);

			foreach ($aJunctions as $iKey => $aConstructorItem) {
				if ($iKey >= $iMaxJunctions) {
					continue;
				}
				foreach ($aConstructorItem['levels'] as $aLevel) {
					$iItemID = $aLevel['item']['id'];
					$iSubID = $aLevel['color']['id'].'_'.$aLevel['item']['deletable'];
					if (empty($aItemsQuants[$iItemID][$iSubID])) {
						$aItemsQuants[$iItemID][$iSubID] = 1;
					} else {
						$aItemsQuants[$iItemID][$iSubID]++;
					}
				}
				if (empty($aConstructorItemsQuants[$aConstructorItem['id']])) {
					$aConstructorItemsQuants[$aConstructorItem['id']] = 1;
				} else {
					$aConstructorItemsQuants[$aConstructorItem['id']]++;
				}
				$fTime += $aConstructorItem['time_coeff'];
			}

			foreach ($aConnections as $iKey => $aConstructorItem) {
				foreach ($aConstructorItem['levels'] as $aLevel) {
					$iItemID = $aLevel['item']['id'];
					$sSubID = $aLevel['color']['id'].'_'.$aLevel['item']['deletable'];
					if (empty($aItemsQuants[$iItemID][$sSubID])) {
						$aItemsQuants[$iItemID][$sSubID] = 1;
					} else {
						$aItemsQuants[$iItemID][$sSubID]++;
					}
				}
				if (empty($aConstructorItemsQuants[$aConstructorItem['id']])) {
					$aConstructorItemsQuants[$aConstructorItem['id']] = 1;
				} else {
					$aConstructorItemsQuants[$aConstructorItem['id']]++;
				}
				if (!empty($aConstructorItem['size'])) {
					if ($aConstructorItem['subtype'] === CConstructorItems::SUBTYPE_STUD_SCREW) {
						$aConstructorItem['size']['size'] += $aConstructorItem['crimp_size'];
					}
					if ($aConstructorItem['size']['type'] == CHardwareItems::TYPE_STUD) {
						$aHardwareStudQuants[$aConstructorItem['size']['id']][$aConstructorItem['size']['size']] += $aConstructorItem['size']['size'];
					}
					if (!empty($aHardwareItemsQuants[$aConstructorItem['size']['id']][$aConstructorItem['size']['size']])) {
						$aHardwareItemsQuants[$aConstructorItem['size']['id']][$aConstructorItem['size']['size']]++;
					} else {
						$aHardwareItemsQuants[$aConstructorItem['size']['id']][$aConstructorItem['size']['size']] = 1;
					}
				}
				$fTime += $aConstructorItem['time_coeff'];
			}

			$iTotalRequired = 0;
			$iTotalOptional = 0;

			$aResultItemsRequired = array();
			$aResultItemsOptional = array();

			$cSInfo = new CSInfo(array(
				'ids' => array_keys($aItemsQuants),
				'enabled_only' => true,
				'with_colors' => true,
				'colors_set_color_keys' => true
			));
			$aItems = CItems::getInstance()->GetList($cSInfo);
			foreach ($aItems as $aItem) {
				foreach ($aItemsQuants[$aItem['id']] as $sSubID => $iQuant) {
					list($iColor, $bDeletable) = explode('_', $sSubID);
					$bDeletable = (bool)$bDeletable;
					if ($bDeletable) {
						$iPrice = 0;
						foreach ($aItem['prices'] as $aPrice) {
							if ((!$aPrice['from'] || $iQuant >= $aPrice['from']) && (!$aPrice['to'] || $iQuant <= $aPrice['to'])) {
								$iPrice = $aPrice['price'];
							}
						}
					} else {
						$iPrice = $aItem['net_price'];
						switch ($aItem['net_price_currency']) {
							case CCurrencies::EUR:
								$iPrice = round($iPrice * CSetts::getInstance()->currency_eur, 2);
								break;
							case CCurrencies::USD:
								$iPrice = round($iPrice * CSetts::getInstance()->currency_usd, 2);
								break;
						}
					}
					$iTotal = round($iQuant * $iPrice, 2);

					$iRemains = 0;
					if (isset($aItem['colors'][$iColor])) {
						if (!empty($aItem['colors'][$iColor]['remains'])) {
							$iRemains += $aItem['colors'][$iColor]['remains'];
						}
						if (!empty($aItem['colors'][$iColor]['arsenal_remains'])) {
							$iRemains += $aItem['colors'][$iColor]['arsenal_remains'];
						}
					}

					if ($bDeletable) {
						$aResultItemsOptional[$aItem['id'].'_'.$iColor] = array(
							'id' => $aItem['id'],
							'articul' => $aItem['articul'],
							'title' => $aItem['title'],
							'color' => CItemColors::$aColors[$iColor],
							'quant' => $iQuant,
							'remains' => $iRemains,
							'price' => $iPrice,
							'total' => $iTotal,
							'removed' => false,
							'unit' => $aItem['type'] == CConstructorItems::TYPE_ROPE?'м.':'шт.'
						);
						$iTotalOptional += $iTotal;
					} else {
						$aResultItemsRequired[] = array(
							'id' => $aItem['id'],
							'articul' => $aItem['articul'],
							'title' => $aItem['title'],
							'descr' => $aItem['type'] == CConstructorItems::TYPE_ROPE?join('<br>', $aRopesDescr):'',
							'color' => CItemColors::$aColors[$iColor],
							'quant' => $iQuant,
							'remains' => $iRemains,
							'price' => $iPrice,
							'total' => $iTotal,
							'unit' => $aItem['type'] == CConstructorItems::TYPE_ROPE?'м.':'шт.'
						);
						$iTotalRequired += $iTotal;
					}
				}
			}

			$aResultHardwareItems = array();
			if ($aHardwareItemsQuants) {
				$cSInfo = new CSInfo(array(
					'ids' => array_keys($aHardwareItemsQuants),
					'enabled_only' => true
				));
				$aItems = CHardwareItems::getInstance()->GetList($cSInfo);
				foreach ($aItems as $aItem) {
					foreach ($aHardwareItemsQuants[$aItem['id']] as $iSize => $iQuant) {
						$iPrice = $aItem['price'];
						if ($aItem['type'] == CHardwareItems::TYPE_STUD) {
							$iCalcQuant = ceil($aHardwareStudQuants[$aItem['id']][$iSize] / 20) / 100;
						} else {
							$iCalcQuant = $iQuant;
						}
						$iTotal = round($iCalcQuant * $iPrice, 2);
						//$iSize = $aItem['size'];
						/*if ($aItem['type'] == CHardwareItems::TYPE_SCREW) {
							$iSize += 5;
						}*/
						$sTitle = CHardwareItems::$aTypesSingle[$aItem['type']].' M'.$aItem['thread'].', размер '.$iSize;
						if ($aItem['type'] == CHardwareItems::TYPE_STUD) {
							$sTitle .= ', '.$iQuant.' отрезков';
						}
						$aResultHardwareItems[] = array(
							'id' => $aItem['id'],
							'articul' => $aItem['articul'],
							'title' => $sTitle,
							'quant' => $iCalcQuant,
							'price' => $iPrice,
							'total' => $iTotal
						);
						$iTotalRequired += $iTotal;
					}
				}
			}

			$aResultKitItems = array();
			if ($aConstructorItemsQuants) {
				$aKitItems = CConstructorItems::getInstance()->GetKitItems(array_keys($aConstructorItemsQuants));
				foreach ($aKitItems as $iConstructorItemID => $aItems) {
					foreach ($aItems as $aItem) {
						if (empty($aResultKitItems[$aItem['id']])) {
							$aResultKitItems[$aItem['id']] = array(
								'articul' => $aItem['articul'],
								'title' => $aItem['title'],
								'price' => $aItem['price'],
								'quant' => $aConstructorItemsQuants[$iConstructorItemID] * $aItem['quant']
							);
						} else {
							$aResultKitItems[$aItem['id']]['quant'] += $aConstructorItemsQuants[$iConstructorItemID] * $aItem['quant'];
						}
					}
				}
				foreach ($aResultKitItems as &$aItem) {
					$aItem['total'] = round($aItem['price'] * $aItem['quant'], 2);
					$iTotalRequired += $aItem['total'];
				}
				unset($aItem);
			}

			$fAdditionalPrice = round($fTime * (CSetts::getInstance()->net_price_wearout + CSetts::getInstance()->net_price_rent + CSetts::getInstance()->net_price_utilities), 2);
			$fBuildPrice = round(($iTotalRequired + $fTime * CSetts::getInstance()->net_price_work) * CSetts::getInstance()->net_price_margin + $fAdditionalPrice);
			$fTotalPrice = round($fBuildPrice + $iTotalOptional);

			$sDebugInfo = 'Время на сборку: '.$fTime.'<br>'.
				'Цена сборки: '.
				'('.$iTotalRequired.'+'.$fTime.'*'.CSetts::getInstance()->net_price_work.')*'.CSetts::getInstance()->net_price_margin.'+'.$fAdditionalPrice.'<br>'.
				'Доп цена: '.
				$fTime.'*('.CSetts::getInstance()->net_price_wearout.'+'.CSetts::getInstance()->net_price_rent.'+'.CSetts::getInstance()->net_price_utilities.')<br>';


			$_SESSION['constructor']['results'] = array(
				'items_required' => $aResultItemsRequired,
				'items_optional' => $aResultItemsOptional,
				'hardware_items' => $aResultHardwareItems,
				'kit_items' => $aResultKitItems,
				'total_required' => $iTotalRequired,
				'total_optional' => $iTotalOptional,
				'build_price' => $fBuildPrice,
				'additional_price' => $fAdditionalPrice,
				'total_price' => $fTotalPrice,
				'debug_info' => $sDebugInfo
			);

			$sQueueID = md5(time().'#'.rand(111111, 999999));
			$sFilename = '/static/constructorresults/'.$sQueueID.'.png';

			/*$sPhantomJS = "var page = require('webpage').create();\n".
							"phantom.addCookie({\n".
							"	'name': 'PHPSESSID',\n".
							"	'value': '".$_COOKIE['PHPSESSID']."',\n".
							"	'domain': '".CSetts::getInstance()->site_address."',\n".
							"	'path': '/',\n".
							"	'httponly': false,\n".
							"	'secure': false,\n".
							"	'expires': (new Date()).getTime() + (1000 * 60 * 60)\n".
							"});\n".
							"page.open('".CSetts::getInstance()->site_url."/constructor/createimage/', function() {\n".
							"	window.setTimeout(function () {\n".
							"		page.render('".$_SERVER['DOCUMENT_ROOT'].$sFilename."');\n".
							"		phantom.exit();\n".
							"	}, 100);\n".
							"});";
			$sPhantomJSFilename = $_SERVER['DOCUMENT_ROOT'].'/_bin/'.$sQueueID.'.js';
			file_put_contents($sPhantomJSFilename, $sPhantomJS);*/

			/*putenv($_SERVER['DOCUMENT_ROOT'].'/_bin/');
			$sShellCmd = $_SERVER['DOCUMENT_ROOT'].'/_bin/phantomjs --debug=true '.$_SERVER['DOCUMENT_ROOT'].'/_bin/screenshot.js'.' "'.
								$_COOKIE['PHPSESSID'].'" '.$_SERVER['DOCUMENT_ROOT'].$sFilename.' 2>&1';*/
			/*$sShellCmdFilename = $_SERVER['DOCUMENT_ROOT'].'/_bin/'.$sQueueID.'.sh';
			file_put_contents($sShellCmdFilename, $sShellCmd);
			chmod($sShellCmdFilename, 0744);*/
			/*exec($sShellCmd, $aReturn, $iReturn);
			print_r($aReturn);
			echo $iReturn;*/

			file_put_contents('/home/zagweb/mafplastic/_bin/phantomqueue/'.$sQueueID, $_COOKIE['PHPSESSID']);

			$_SESSION['constructor']['image'] = $sFilename;

		}

		$this->colors = array();
		if (!empty($_SESSION['constructor']['results']['items_optional'])) {
			$aItemIDs = array();
			foreach ($_SESSION['constructor']['results']['items_optional'] as $aItem) {
				if (!in_array($aItem['id'], $aItemIDs)) {
					$aItemIDs[] = $aItem['id'];
				}
			}
			if ($aItemIDs) {
				$cSInfo = new CSInfo(array(
					'item_ids' => $aItemIDs,
					'constructor_exist_only' => true,
					'group_by_item' => true
				));
				$this->colors = CItemColors::getInstance()->GetList($cSInfo);
			}
		}

		$this->items_required = $_SESSION['constructor']['results']['items_required'];
		$this->items_optional = $_SESSION['constructor']['results']['items_optional'];
		$this->hardware_items = $_SESSION['constructor']['results']['hardware_items'];
		$this->kit_items = $_SESSION['constructor']['results']['kit_items'];
		$this->total_required = $_SESSION['constructor']['results']['total_required'];
		$this->total_optional = $_SESSION['constructor']['results']['total_optional'];
		$this->build_price = $_SESSION['constructor']['results']['build_price'];
		$this->additional_price = $_SESSION['constructor']['results']['additional_price'];
		$this->total_price = $_SESSION['constructor']['results']['total_price'];
		$this->debug_info = $_SESSION['constructor']['results']['debug_info'];

		ob_start();
		$this->cDispatcher->SetTemplate('constructor_results');
		$this->cDispatcher->RenderTemplate();
		$sHTML = ob_get_clean();

		return $sHTML;
	}

	function updateAction()
	{
		if (empty($_SESSION['constructor']['results'])) {
			return false;
		}
		if (!empty($_POST['items']) && is_array($_POST['items'])) {
			$aItems = array_map('intval', $_POST['items']);

			if ($_SESSION['constructor']['results']['items_optional']) {
				$iTotalOptional = 0;
				foreach ($_SESSION['constructor']['results']['items_optional'] as $sID => $aItem) {
					if (isset($aItems[$sID])) {
						if ($aItems[$sID]) {
							$_SESSION['constructor']['results']['items_optional'][$sID]['removed'] = false;
							$iTotalOptional += $aItem['total'];
						} else {
							$_SESSION['constructor']['results']['items_optional'][$sID]['removed'] = true;
						}
					}
				}
				unset($_SESSION['constructor']['id']);
			}

			$_SESSION['constructor']['results']['total_optional'] = $iTotalOptional;
			$_SESSION['constructor']['results']['total_price'] = round($_SESSION['constructor']['results']['build_price'] + $_SESSION['constructor']['results']['total_optional']);
		}
		if (!empty($_POST['colors']) && is_array($_POST['colors'])) {
			$aItems = array_map('intval', $_POST['colors']);
			if ($_SESSION['constructor']['results']['items_optional']) {
				foreach ($_SESSION['constructor']['results']['items_optional'] as $sID => $aItem) {
					if (isset($aItems[$sID])) {
						$cSInfo = new CSInfo(array(
							'item_id' => $aItem['id'],
							'constructor_exist_only',
							'set_color_keys' => true
						));
						$aColors = CItemColors::getInstance()->GetList($cSInfo);
						if (!empty($aColors[$aItems[$sID]])) {
							$iOldColor = array_search($aItem['color'], CItemColors::$aColors);
							$iNewColor = $aItems[$sID];
							if ($this->topConnections) {
								foreach ($this->topConnections as $aConnection) {
									if (!empty($aConnection['levels'])) {
										foreach ($aConnection['levels'] as &$aLevel) {
											if ($aLevel['item']['id'] == $aItem['id'] && $aLevel['color']['id'] == $iOldColor) {
												$aLevel['color']['id'] = $iNewColor;
												$aLevel['color']['rgb'] = CConstructorItems::$aColorsRGB[CConstructorItems::TYPE_CONNECTION][$iNewColor];
											}
										}
									}
								}
								unset($aLevel);
							}
							if ($this->bottomConnections) {
								foreach ($this->bottomConnections as $aConnection) {
									if (!empty($aConnection['levels'])) {
										foreach ($aConnection['levels'] as &$aLevel) {
											if ($aLevel['item']['id'] == $aItem['id'] && $aLevel['color']['id'] == $iOldColor) {
												$aLevel['color']['id'] = $iNewColor;
												$aLevel['color']['rgb'] = CConstructorItems::$aColorsRGB[CConstructorItems::TYPE_CONNECTION][$iNewColor];
											}
										}
									}
								}
								unset($aLevel);
							}
							if ($this->leftConnections) {
								foreach ($this->leftConnections as $aConnection) {
									if (!empty($aConnection['levels'])) {
										foreach ($aConnection['levels'] as &$aLevel) {
											if ($aLevel['item']['id'] == $aItem['id'] && $aLevel['color']['id'] == $iOldColor) {
												$aLevel['color']['id'] = $iNewColor;
												$aLevel['color']['rgb'] = CConstructorItems::$aColorsRGB[CConstructorItems::TYPE_CONNECTION][$iNewColor];
											}
										}
									}
								}
								unset($aLevel);
							}
							if ($this->rightConnections) {
								foreach ($this->rightConnections as $aConnection) {
									if (!empty($aConnection['levels'])) {
										foreach ($aConnection['levels'] as &$aLevel) {
											if ($aLevel['item']['id'] == $aItem['id'] && $aLevel['color']['id'] == $iOldColor) {
												$aLevel['color']['id'] = $iNewColor;
												$aLevel['color']['rgb'] = CConstructorItems::$aColorsRGB[CConstructorItems::TYPE_CONNECTION][$iNewColor];
											}
										}
									}
								}
								unset($aLevel);
							}
							$this->save();
							$this->cleanupResults();
						}
					}
				}
				unset($_SESSION['constructor']['id']);
			}
		}
		$this->returnJson();
	}

	function printAction()
	{
		if (empty($_SESSION['constructor']['results'])) {
			return false;
		}

		if (empty($_SESSION['constructor']['id'])) {
			$aData = array(
				'params' => $_SESSION['constructor'],
				'price' => $_SESSION['constructor']['results']['total_price']
			);
			if (CUsers::getInstance()->isLogged()) {
				$aData['user_id'] = CUsers::getInstance()->ID();
			}
			unset($aData['params']['id']);
			unset($aData['params']['image']);
			$sImageFilename = $_SERVER['DOCUMENT_ROOT'].$_SESSION['constructor']['image'];
			$_SESSION['constructor']['id'] = CConstructors::getInstance()->Add($aData, $sImageFilename);
			if (!$_SESSION['constructor']['id']) {
				return false;
			}
		}

		if (!empty($_SESSION['constructor']['results']['items_optional'])) {
			$aItemsOptional = $_SESSION['constructor']['results']['items_optional'];
			foreach ($aItemsOptional as $sID => $aItem) {
				if ($aItem['removed']) {
					unset($aItemsOptional[$sID]);
				}
			}
		}

		$this->items_required = $_SESSION['constructor']['results']['items_required'];
		$this->items_optional = $aItemsOptional;
		$this->hardware_items = $_SESSION['constructor']['results']['hardware_items'];
		$this->kit_items = $_SESSION['constructor']['results']['kit_items'];
		$this->total_required = $_SESSION['constructor']['results']['total_required'];
		$this->total_optional = $_SESSION['constructor']['results']['total_optional'];
		$this->build_price = $_SESSION['constructor']['results']['build_price'];
		$this->total_price = $_SESSION['constructor']['results']['total_price'];
		$this->debug_info = $_SESSION['constructor']['results']['debug_info'];
		$this->id = $_SESSION['constructor']['id'];

		$this->cDispatcher->SetTemplate('constructor_results_print');
	}

	function pdfAction()
	{
		if (empty($_SESSION['constructor']['results'])) {
			return false;
		}

		if (empty($_SESSION['constructor']['id'])) {
			$aData = array(
				'params' => $_SESSION['constructor'],
				'price' => $_SESSION['constructor']['results']['total_price']
			);
			if (CUsers::getInstance()->isLogged()) {
				$aData['user_id'] = CUsers::getInstance()->ID();
			}
			unset($aData['params']['id']);
			unset($aData['params']['image']);
			$sImageFilename = $_SERVER['DOCUMENT_ROOT'].$_SESSION['constructor']['image'];
			$_SESSION['constructor']['id'] = CConstructors::getInstance()->Add($aData, $sImageFilename);
			if (!$_SESSION['constructor']['id']) {
				return false;
			}
		}

		if (!empty($_SESSION['constructor']['results']['items_optional'])) {
			$aItemsOptional = $_SESSION['constructor']['results']['items_optional'];
			foreach ($aItemsOptional as $sID => $aItem) {
				if ($aItem['removed']) {
					unset($aItemsOptional[$sID]);
				}
			}
		}

		$this->items_required = $_SESSION['constructor']['results']['items_required'];
		$this->items_optional = $aItemsOptional;
		$this->hardware_items = $_SESSION['constructor']['results']['hardware_items'];
		$this->kit_items = $_SESSION['constructor']['results']['kit_items'];
		$this->total_required = $_SESSION['constructor']['results']['total_required'];
		$this->total_optional = $_SESSION['constructor']['results']['total_optional'];
		$this->build_price = $_SESSION['constructor']['results']['build_price'];
		$this->total_price = $_SESSION['constructor']['results']['total_price'];
		$this->debug_info = $_SESSION['constructor']['results']['debug_info'];
		$this->id = $_SESSION['constructor']['id'];

		//$this->cDispatcher->SetTemplate('constructor_results_pdf');
		//return;
		ini_set('memory_limit', '1024M');
		$this->cDispatcher->SetTemplate('constructor_results_pdf');
		ob_start();
		$this->cDispatcher->RenderTemplate();
		$sHTML = ob_get_contents();
		ob_clean();

		require_once('_includes/_ext/mypdf.php');
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('mafplastic.ru website');
		$pdf->SetTitle('Канатная конструкция. Проект №'.$this->id);
		$pdf->setPrintHeader(false);
		$pdf->setFooterFont(array('arial', '', 6));
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->SetFont('arial', '', 10, '', true);
		$pdf->AddPage();
		$pdf->writeHTML($sHTML, true, false, true, false, '');
		$pdf->Output('constructor_'.$this->id.'.pdf', 'D');
		die();
	}

	function saveAction()
	{
		if (empty($_SESSION['constructor']['results'])) {
			return false;
		}
		if (CUsers::getInstance()->isLogged()) {
			if (empty($_SESSION['constructor']['id'])) {
				$aData = array(
					'params' => $_SESSION['constructor'],
					'price' => $_SESSION['constructor']['results']['total_price'],
					'user_id' => CUsers::getInstance()->ID()
				);
				unset($aData['params']['id']);
				unset($aData['params']['image']);
				$sImageFilename = $_SERVER['DOCUMENT_ROOT'].$_SESSION['constructor']['image'];
				$_SESSION['constructor']['id'] = CConstructors::getInstance()->Add($aData, $sImageFilename);
                                
				if (!$_SESSION['constructor']['id']) {
					return false;
				}
			} else {
				$aItem = CConstructors::getInstance()->GetItem($_SESSION['constructor']['id']);
				if (!empty($aItem) && empty($aItem['user_id'])) {
					CConstructors::getInstance()->UpdateUserID($_SESSION['constructor']['id'], CUsers::getInstance()->ID());
				}
			}
			$this->contructor_id = $_SESSION['constructor']['id'];
		}

		$this->cDispatcher->SetTemplate('constructor_save');
	}

	function addbasketAction()
	{
		if (empty($_SESSION['constructor']['results'])) {
			return false;
		}
		$iQuant = (int)$_POST['quant'];
		if ($iQuant <= 0 || $iQuant > 1000000) {
			$iQuant = 1;
		}

		if (empty($_SESSION['constructor']['id'])) {
			$aData = array(
				'params' => $_SESSION['constructor'],
				'price' => $_SESSION['constructor']['results']['total_price']
			);
			unset($aData['params']['id']);
			unset($aData['params']['image']);
			$sImageFilename = $_SERVER['DOCUMENT_ROOT'].$_SESSION['constructor']['image'];
			$_SESSION['constructor']['id'] = CConstructors::getInstance()->Add($aData, $sImageFilename);
			if (!$_SESSION['constructor']['id']) {
				return false;
			}
		}

		if ($_POST['add']) {
			if (empty($_SESSION['basket_constructors'][$_SESSION['constructor']['id']])) {
				$_SESSION['basket_constructors'][$_SESSION['constructor']['id']] = array(
					'id' => $_SESSION['constructor']['id'],
					'count' => $iQuant,
					'price' => $_SESSION['constructor']['results']['build_price']
				);
				if (!empty($_SESSION['constructor']['results']['items_optional'])) {
					$aItemsOptional = $_SESSION['constructor']['results']['items_optional'];
					foreach ($aItemsOptional as $sID => $aItem) {
						if ($aItem['removed']) {
							continue;
						}
						if (empty($_SESSION['basket'][$aItem['id']])) {
							$aItemInfo = CItems::getInstance()->GetItem($aItem['id']);
							$iImageID = CItemGallery::getInstance()->GetMainForItem($aItem['id']);
							$_SESSION['basket'][$aItem['id']] = array(
								'count' => $aItem['quant'] * $iQuant,
								'cat_id' => $aItemInfo['cat_id'],
								'title' => $aItemInfo['title'],
								'articul' => $aItemInfo['articul'],
								'prices' => $aItemInfo['prices'],
								'id' => $aItem['id'],
								'image_id' => $iImageID
							);
						} else {
							$_SESSION['basket'][$aItem['id']]['count'] += $aItem['quant'] * $iQuant;
						}
					}
				}
			} else {
				$_SESSION['basket_constructors'][$_SESSION['constructor']['id']]['count'] += $iQuant;
			}

			$this->total_quant = 0;
			$this->total_price = 0;
			if (!empty($_SESSION['basket'])) {
				foreach ($_SESSION['basket'] as $aItem) {
					$this->total_quant ++;
					$aItem['price'] = 0;
					foreach ($aItem['prices'] as $aPrice) {
						if ($aPrice['from'] <= $aItem['count'] && $aPrice['to'] >= $aItem['count']) {
							$aItem['price'] = $aPrice['price'];
						}
					}
					$this->total_price += round($aItem['count'] * $aItem['price'], 2);
				}
			}
			if (!empty($_SESSION['basket_constructors'])) {
				foreach ($_SESSION['basket_constructors'] as $aItem) {
					$this->total_quant ++;
					$this->total_price += round($aItem['count'] * $aItem['price'], 2);
				}
			}
			$this->cDispatcher->SetTemplate('basket_add_success');
			return;
		}

		$aItem = array(
			'id' => $_SESSION['constructor']['id'],
			'count' => $iQuant,
			'price' => $_SESSION['constructor']['results']['build_price']
		);
		$this->quant = $iQuant;
		$this->price = $aItem['price'];
		$this->total = round($iQuant * $aItem['price']);
		$this->item = $aItem;

		$this->items_optional = array();
		if (!empty($_SESSION['constructor']['results']['items_optional'])) {
			foreach ($_SESSION['constructor']['results']['items_optional'] as $sID => $aItem) {
				if ($aItem['removed']) {
					continue;
				}
				$aItemInfo = CItems::getInstance()->GetItem($aItem['id']);
				$iImageID = CItemGallery::getInstance()->GetMainForItem($aItem['id']);
				$aItem['price'] = 0;
				$aItem['quant'] = $aItem['quant'] * $iQuant;
				foreach ($aItemInfo['prices'] as $aPrice) {
					if ($aPrice['from'] <= $aItem['quant'] && $aPrice['to'] >= $aItem['quant']) {
						$aItem['price'] = $aPrice['price'];
					}
				}
				$this->items_optional[] = array(
					'image_id' => $iImageID,
					'id' => $aItem['id'],
					'cat_id' => $aItemInfo['cat_id'],
					'title' => $aItemInfo['title'],
					'articul' => $aItemInfo['articul'],
					'quant' => $aItem['quant'],
					'price' => $aItem['price']
				);
				$this->total += round($aItem['price'] * $aItem['quant'], 2);
			}
		}
		$this->total = round($this->total);

		if (!empty($_POST['quant'])) {
			if ($this->cDispatcher->isXmlHttpRequest()) {
				$aJSON = array(
					'quant' => $this->quant,
					'price' => $this->price,
					'total' => $this->total,
					'items' => array()
				);
				if ($this->items_optional) {
					foreach ($this->items_optional as $aItem) {
						$aJSON['items'][] = array(
							'id' => $aItem['id'],
							'quant' => $aItem['quant'],
							'price' => $aItem['price']
						);
					}
				}
				header('Content-Type: application/json');
				echo json_encode($aJSON);
				$this->cDispatcher->bNoRender = true;
			} else {
				$this->cDispatcher->Redirect('/constructor/addbasket/');
			}
		} else {
			$this->cDispatcher->SetTemplate('constructor_basket_add');
		}
	}

	function deletebasketAction()
	{
		unset($_SESSION['basket_constructors'][(int)$this->item_id]);

		if ($this->cDispatcher->isXmlHttpRequest()) {
			require_once(__DIR__.'/basket.php');
			$controller = new basket();
			return $controller->_returnbasketjson();
		} else {
			$this->cDispatcher->Redirect('/basket/');
		}
	}

	function ropesAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		$this->_itemsList(CConstructorItems::TYPE_ROPE);
	}

	function junctionsAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		$this->_itemsList(CConstructorItems::TYPE_JUNCTION);
	}

	function connectionsAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		$iSubType = isset($_GET['subtype'])?(int)$_GET['subtype']:0;
		if ($iSubType < 1 || $iSubType > 5) {
			return false;
		}
		$sPosition = isset($_GET['position'])?$_GET['position']:'';
		if (!in_array($sPosition, array('top', 'bottom', 'left', 'right'))) {
			return false;
		}
		if (in_array($sPosition, array('left', 'right')) && $iSubType === CConstructorItems::SUBTYPE_T_ROPE) {
			return false;
		}

		$this->_itemsList(CConstructorItems::TYPE_CONNECTION, $iSubType);
	}

	private function _itemsList($iType, $iSubType = 0)
	{
		$cSInfo = new CSInfo(array(
			'type' => $iType,
			'subtype' => $iSubType,
			'enabled_only' => true,
//			'with_remains_only' => true
		));
		$aItems = CConstructorItems::getInstance()->GetListWithItems($cSInfo);

		if ($aItems) {
			foreach ($aItems as $iKey => $aItem) {
				$aPrices = array();
				for ($c = 1; $c <= 9; $c++) {
					if (!empty($aItem['item_'.$c.'_id'])) {
						$aItemPrices = json_decode($aItem['item_'.$c.'_prices'], true);
						if (empty($aItemPrices)) {
							unset($aItems[$iKey]);
						} else {
							$aItemPricesVariants = array();
							foreach ($aItemPrices as $aPrice) {
								$aItemPricesVariants[] = $aPrice['from'].'_'.$aPrice['to'];
							}
							if ($c === 1) {
								$aPrices = $aItemPricesVariants;
							} else {
								if ($aItemPricesVariants !== $aPrices) {
									unset($aItems[$iKey]);
								}
							}
						}
					}
				}
			}
		}

		$this->items = $aItems;
		if ($iType == CConstructorItems::TYPE_CONNECTION) {
			$this->show_back_link = true;
		} else {
			$this->show_back_link = false;
		}

		$this->cDispatcher->SetTemplate('constructor_popup_items');
	}

	function subtypesAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		$iNumber = isset($_GET['number'])?(int)$_GET['number']:0;
		$sPosition = isset($_GET['position'])?$_GET['position']:'';
		if (!in_array($sPosition, array('top', 'bottom', 'left', 'right'))) {
			return false;
		}

		$iCols = $this->cols;
		$iRows = $this->rows;
		if ($this->hasTopTRope()) {
			$iRows++;
		}
		if ($this->hasBottomTRope()) {
			$iRows++;
		}

		if (in_array($sPosition , array('top', 'bottom'))) {
			$iMaxConnections = $iCols;
		} else {
			$iMaxConnections = $iRows;
		}
		if ($iNumber < 0 || $iNumber >= $iMaxConnections) {
			return false;
		}

		if (in_array($sPosition , array('top', 'bottom'))) {
			$this->show_t_rope = true;
		} else {
			$this->show_t_rope = false;
		}
		$this->cDispatcher->SetTemplate('constructor_popup_subtypes');
	}

	function itemAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		$aItem = CConstructorItems::getInstance()->GetItemEnabled($this->id);
		if (empty($aItem)) {
			return false;
		}

		$aReturn = array(
			'id' => $aItem['id'],
			'descr' => $aItem['descr'],
			'prices' => array(),
			'colors' => array()
		);
		if (in_array($aItem['subtype'], array(1, 5))) {
			$aReturn['sizes_image'] = CConstructorItems::GetLink($aItem['id'], 3);
		}

		$aPricesVariants = array();
		$aPrices = array();
		for ($c = 1; $c <= 9; $c++) {
			if (!empty($aItem['item_'.$c.'_id'])) {
				$aItemPrices = json_decode($aItem['item_'.$c.'_prices'], true);
				if (empty($aItemPrices)) {
					return false;
				}
				$aItemPricesVariants = array();
				foreach ($aItemPrices as $aPrice) {
					$sKey = $aPrice['from'].'_'.$aPrice['to'];
					$aItemPricesVariants[] = $sKey;
					if (empty($aPrices[$sKey])) {
						$aPrices[$sKey] = $aPrice;
					} else {
						$aPrices[$sKey]['price'] += $aPrice['price'];
					}
				}
				if ($c === 1) {
					$aPricesVariants = $aItemPricesVariants;
				} else {
					if ($aItemPricesVariants !== $aPricesVariants) {
						return false;
					}
				}
			}
		}


		$cSInfo = new CSInfo(array(
			'item_id' => $aItem['item_1_id'],
			'enabled_only' => true,
//			'constructor_exist_only' => true
		));
		$aColors = CItemColors::getInstance()->GetList($cSInfo);
		if (empty($aColors)) {
			return false;
		}

		$this->item = $aItem;
		$this->prices = array_values($aPrices);
		$this->colors = $aColors;

		if ($this->item['type'] == CConstructorItems::TYPE_ROPE) {
			$sTpl = 'constructor_popup_rope';
		} elseif ($this->item['type'] == CConstructorItems::TYPE_JUNCTION) {
			$sTpl = 'constructor_popup_junction';
		} else {
			if ($this->item['subtype'] == CConstructorItems::SUBTYPE_T_ROPE) {
				$sTpl = 'constructor_popup_connection_t_rope';
			} else {
				$iHardwareType = null;
				switch ($this->item['block_type']) {
					case CConstructorItems::BLOCK_SIZE_STUD:
					case CConstructorItems::BLOCK_SIZE_COLOR_STUD:
						$iHardwareType = CHardwareItems::TYPE_STUD;
						break;
					case CConstructorItems::BLOCK_SIZE_SCREW:
					case CConstructorItems::BLOCK_SIZE_COLOR_SCREW:
						$iHardwareType = CHardwareItems::TYPE_SCREW;
						break;
					case CConstructorItems::BLOCK_SIZE_EYEBOLT:
					case CConstructorItems::BLOCK_SIZE_COLOR_EYEBOLT:
						$iHardwareType = CHardwareItems::TYPE_EYEBOLT;
						break;
				}
				if ($iHardwareType) {
					$cSInfo = new CSInfo(array(
						'type' => $iHardwareType,
						'enabled_only' => true
					));
					$aSizes = CHardwareItems::getInstance()->GetList($cSInfo);
					if (!$aSizes) {
						return false;
					}
					$this->sizes = array();
					foreach ($aSizes as $aSize) {
						$this->sizes[$aSize['thread']][] = $aSize;
					}
				}
				switch ($this->item['block_type']) {
					case CConstructorItems::BLOCK_COLOR:
						$sTpl = 'constructor_popup_connection_color';
						break;
					case CConstructorItems::BLOCK_SIZE_STUD:
						$sTpl = 'constructor_popup_connection_size_stud';
						break;
					case CConstructorItems::BLOCK_SIZE_SCREW:
					case CConstructorItems::BLOCK_SIZE_EYEBOLT:
						$sTpl = 'constructor_popup_connection_size';
						break;
					case CConstructorItems::BLOCK_SIZE_COLOR_STUD:
						$sTpl = 'constructor_popup_connection_color_size_stud';
						break;
					case CConstructorItems::BLOCK_SIZE_COLOR_SCREW:
					case CConstructorItems::BLOCK_SIZE_COLOR_EYEBOLT:
						$sTpl = 'constructor_popup_connection_color_size';
						break;
					case CConstructorItems::BLOCK_EMPTY:
						$sTpl = 'constructor_popup_connection_empty';
						break;
				}
			}
		}

		$this->cDispatcher->SetTemplate($sTpl);
	}

	function ropeAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		$iConstructorID = isset($_GET['id'])?(int)$_GET['id']:0;
		$iColorID = isset($_GET['color'])?(int)$_GET['color']:0;
		if (!$iConstructorID) {
			return false;
		}
		if (!$iColorID) {
			return false;
		}

		$aConstructorItem = CConstructorItems::getInstance()->GetItem($iConstructorID);
		if (!$aConstructorItem || !$aConstructorItem['enabled']) {
			return false;
		}
		if ($aConstructorItem['type'] != CConstructorItems::TYPE_ROPE) {
			return false;
		}

		$aItemIDs = array();
		for ($c = 1; $c <= 9; $c++) {
			$iItemID = $aConstructorItem['item_'.$c.'_id'];
			if ($iItemID) {
				$aItemIDs[] = $iItemID;
			}
		}
		$cSInfo = new CSInfo(array(
			'ids' => $aItemIDs,
			'enabled_only' => true,
			'set_id_keys' => true,
			'with_colors' => true,
			'colors_enabled_only' => true,
			'colors_set_id_keys' => true
		));
		$aItems = CItems::getInstance()->GetList($cSInfo);
		$aPricesVariants = array();
		for ($c = 1; $c <= 9; $c++) {
			$iItemID = $aConstructorItem['item_'.$c.'_id'];
			if ($iItemID) {
				if (empty($aItems[$iItemID])) {
					return false;
				}
				$aItemPricesVariants = array();
				foreach ($aItems[$iItemID]['prices'] as $aPrice) {
					$aItemPricesVariants[] = $aPrice['from'].'_'.$aPrice['to'];
				}
				if ($c === 1) {
					$aPricesVariants = $aItemPricesVariants;
				} else {
					if ($aItemPricesVariants !== $aPricesVariants) {
						return false;
					}
				}
			}
		}

		if (empty($aItems[$aConstructorItem['item_1_id']]['colors'][$iColorID])) {
			return false;
		}
		$iColor = $aItems[$aConstructorItem['item_1_id']]['colors'][$iColorID]['color'];
		$aResultItem = array(
			'id' => $aConstructorItem['id'],
			'image' => CConstructorItems::GetLink($aConstructorItem['id'], 2),
			'item' => array(
				'id' => $aConstructorItem['item_1_id']
			),
			'color' => array(
				'id' => $iColor,
				'rgb' => CConstructorItems::$aColorsRGB[CConstructorItems::TYPE_ROPE][$iColor]
			)
		);

		$this->rope = $aResultItem;
		$this->save();

		$this->returnJson();
	}

	function junctionAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		$iConstructorID = isset($_GET['id'])?(int)$_GET['id']:0;
		$iFillType = isset($_GET['fill_type'])?(int)$_GET['fill_type']:0;
		$iNumber = isset($_GET['number'])?(int)$_GET['number']:0;
		$iColorID = isset($_GET['color'])?(int)$_GET['color']:0;
		if (!$iConstructorID) {
			return false;
		}
		if ($iFillType < 1 || $iFillType > 4) {
			return false;
		}
		if ($iNumber < 0 || $iNumber >= $this->cols * $this->rows) {
			return false;
		}
		if (!$iColorID) {
			return false;
		}

		$aConstructorItem = CConstructorItems::getInstance()->GetItem($iConstructorID);
		if (!$aConstructorItem || !$aConstructorItem['enabled']) {
			return false;
		}
		if ($aConstructorItem['type'] != CConstructorItems::TYPE_JUNCTION) {
			return false;
		}

		$aItemIDs = array();
		for ($c = 1; $c <= 9; $c++) {
			$iItemID = $aConstructorItem['item_'.$c.'_id'];
			if ($iItemID) {
				$aItemIDs[] = $iItemID;
			}
		}
		$cSInfo = new CSInfo(array(
			'ids' => $aItemIDs,
			'enabled_only' => true,
			'set_id_keys' => true,
			'with_colors' => true,
			'colors_enabled_only' => true,
			'colors_set_id_keys' => true
		));
		$aItems = CItems::getInstance()->GetList($cSInfo);
		$aPricesVariants = array();
		for ($c = 1; $c <= 9; $c++) {
			$iItemID = $aConstructorItem['item_'.$c.'_id'];
			if ($iItemID) {
				if (empty($aItems[$iItemID])) {
					return false;
				}
				$aItemPricesVariants = array();
				foreach ($aItems[$iItemID]['prices'] as $aPrice) {
					$aItemPricesVariants[] = $aPrice['from'].'_'.$aPrice['to'];
				}
				if ($c === 1) {
					$aPricesVariants = $aItemPricesVariants;
				} else {
					if ($aItemPricesVariants !== $aPricesVariants) {
						return false;
					}
				}
			}
		}

		if (empty($aItems[$aConstructorItem['item_1_id']]['colors'][$iColorID])) {
			return false;
		}
		if (empty($aItems[$aConstructorItem['item_1_id']]['colors'][$iColorID]['remains']) &&
				empty($aItems[$aConstructorItem['item_1_id']]['colors'][$iColorID]['arsenal_remains'])) {
//			return false;
		}
		$iColor = $aItems[$aConstructorItem['item_1_id']]['colors'][$iColorID]['color'];

		$aResultItem = array(
			'id' => $aConstructorItem['id'],
			'rope_increment' => $aConstructorItem['rope_increment'],
			'time_coeff' => $aConstructorItem['time_coeff'],
			'item_size' => $aConstructorItem['size'],
			'image' => CConstructorItems::GetLink($aConstructorItem['id'], 2),
			'levels' => new ArrayObject()
		);
		for ($c = 1; $c <= 9; $c++) {
			$iItemID = $aConstructorItem['item_'.$c.'_id'];
			if ($iItemID) {
				$aItem = $aItems[$iItemID];
				$aResultItem['levels'][$c] = array(
					'item' => array(
						'id' => $aItem['id'],
						'deletable' => $aConstructorItem['item_'.$c.'_deletable']
					)
				);
				if (count($aItem['colors']) === 1) {
					$aColor = array_shift($aItem['colors']);
					$iCurrentColor = $aColor['color'];
				} else {
					$iCurrentColor = $iColor;
				}
				$aResultItem['levels'][$c]['color'] = array(
					'id' => $iCurrentColor,
					'rgb' => CConstructorItems::$aColorsRGB[CConstructorItems::TYPE_JUNCTION][$iCurrentColor]
				);
			}
		}
		if ($iFillType == 1) {
			$this->junctions[$iNumber] = $aResultItem;
		} else {
			for ($c = 0; $c < $this->cols * $this->rows; $c++) {
				if ($iFillType == 2) {
					if ($iNumber % $this->rows == $c % $this->rows) {
						$this->junctions[$c] = $aResultItem;
					}
				} else if ($iFillType == 3) {
					if ($iNumber - $iNumber % $this->rows == $c - $c % $this->rows) {
						$this->junctions[$c] = $aResultItem;
					}
				} else {
					$this->junctions[$c] = $aResultItem;
				}
			}
		}

		$this->checkPaddings();

		$this->fillFrame();
		$this->save();

		$this->returnJson();
	}

	function connectionAction()
	{
		if (!$this->started) {
			return false;
		}
		$this->cleanupResults();

		$iConstructorID = isset($_GET['id'])?(int)$_GET['id']:0;
		$iFillType = isset($_GET['fill_type'])?(int)$_GET['fill_type']:0;
		$iSubType = isset($_GET['subtype'])?(int)$_GET['subtype']:0;
		$iNumber = isset($_GET['number'])?(int)$_GET['number']:0;
		$iColorID = isset($_GET['color'])?(int)$_GET['color']:0;
		$iThread = isset($_GET['thread'])?(int)$_GET['thread']:0;
		$iSize = isset($_GET['size'])?(int)$_GET['size']:0;
		$sPosition = isset($_GET['position'])?$_GET['position']:'';
		if ($iSubType == CConstructorItems::SUBTYPE_T_ROPE) {
			$iFillType = 2;
		}
		if ($iFillType < 1 || $iFillType > 3) {
			return false;
		}
		if ($iSubType < 1 || $iSubType > 5) {
			return false;
		}
		if (!in_array($sPosition, array('top', 'bottom', 'left', 'right'))) {
			return false;
		}

		$iCols = $this->cols;
		$iRows = $this->rows;
		if ($this->hasTopTRope()) {
			$iRows++;
		}
		if ($this->hasBottomTRope()) {
			$iRows++;
		}

		if (in_array($sPosition , array('top', 'bottom'))) {
			$iMaxConnections = $iCols;
		} else {
			$iMaxConnections = $iRows;
		}
		if ($iNumber < 0 || $iNumber >= $iMaxConnections) {
			return false;
		}

		if (in_array($sPosition , array('top', 'bottom'))) {
			$bTRopeAllow = true;
		} else {
			$bTRopeAllow = false;
		}

		if (!$bTRopeAllow && $iSubType == CConstructorItems::SUBTYPE_T_ROPE) {
			return false;
		}

		$aConstructorItem = CConstructorItems::getInstance()->GetItem($iConstructorID);
		if (!$aConstructorItem || !$aConstructorItem['enabled']) {
			return false;
		}
		if ($aConstructorItem['type'] != CConstructorItems::TYPE_CONNECTION) {
			return false;
		}
		if ($iSubType && $aConstructorItem['subtype'] != $iSubType) {
			return false;
		}

		$aItemIDs = array();
		for ($c = 1; $c <= 9; $c++) {
			$iItemID = $aConstructorItem['item_'.$c.'_id'];
			if ($iItemID) {
				$aItemIDs[] = $iItemID;
			}
		}
		$cSInfo = new CSInfo(array(
			'ids' => $aItemIDs,
			'enabled_only' => true,
			'set_id_keys' => true,
			'with_colors' => true,
			'colors_enabled_only' => true,
			'colors_set_id_keys' => true
		));
		$aItems = CItems::getInstance()->GetList($cSInfo);
		$aPricesVariants = array();
		for ($c = 1; $c <= 9; $c++) {
			$iItemID = $aConstructorItem['item_'.$c.'_id'];
			if ($iItemID) {
				if (empty($aItems[$iItemID])) {
					return false;
				}
				$aItemPricesVariants = array();
				foreach ($aItems[$iItemID]['prices'] as $aPrice) {
					$aItemPricesVariants[] = $aPrice['from'].'_'.$aPrice['to'];
				}
				if ($c === 1) {
					$aPricesVariants = $aItemPricesVariants;
				} else {
					if ($aItemPricesVariants !== $aPricesVariants) {
						return false;
					}
				}
			}
		}

		$bCheckColor = false;
		$bCheckSize = false;
		switch ($aConstructorItem['block_type']) {
			case CConstructorItems::BLOCK_COLOR:
				$bCheckColor = true;
				break;
			case CConstructorItems::BLOCK_SIZE_COLOR_STUD:
				$bCheckColor = true;
			case CConstructorItems::BLOCK_SIZE_STUD:
				$bCheckSize = true;
				$iHardwareType = CHardwareItems::TYPE_STUD;
				break;
			case CConstructorItems::BLOCK_SIZE_COLOR_SCREW:
				$bCheckColor = true;
			case CConstructorItems::BLOCK_SIZE_SCREW:
				$bCheckSize = true;
				$iHardwareType = CHardwareItems::TYPE_SCREW;
				break;
			case CConstructorItems::BLOCK_SIZE_COLOR_EYEBOLT:
				$bCheckColor = true;
			case CConstructorItems::BLOCK_SIZE_EYEBOLT:
				$bCheckSize = true;
				$iHardwareType = CHardwareItems::TYPE_EYEBOLT;
				break;
		}

		if ($bCheckColor) {
			if (!$iColorID || empty($aItems[$aConstructorItem['item_1_id']]['colors'][$iColorID])) {
				return false;
			}
			if (empty($aItems[$aConstructorItem['item_1_id']]['colors'][$iColorID]['remains']) &&
					empty($aItems[$aConstructorItem['item_1_id']]['colors'][$iColorID]['arsenal_remains'])) {
//				return false;
			}
			$iColor = $aItems[$aConstructorItem['item_1_id']]['colors'][$iColorID]['color'];
		} else {
			$iColor = 0;
			//$iColor = CConstructorItems::$aRopeCapColors[$_SESSION['constructor']['rope']['color']['id']];
		}
		if ($bCheckSize) {
			if (!$iThread || !$iSize) {
				return false;
			}
			if (
				$aConstructorItem['block_type'] == CConstructorItems::BLOCK_SIZE_STUD ||
				$aConstructorItem['block_type'] == CConstructorItems::BLOCK_SIZE_COLOR_STUD
			) {
				$cSInfo = new CSInfo(array(
					'type' => $iHardwareType,
					'enabled_only' => true
				));
				$aSizes = CHardwareItems::getInstance()->GetList($cSInfo);
				if (!$aSizes) {
					return false;
				}
				$aThreadedSizes = array();
				foreach ($aSizes as $aSize) {
					$aThreadedSizes[$aSize['thread']][] = $aSize;
				}
				if (empty($aThreadedSizes[$iThread])) {
					return false;
				}
				$aSize = array(
					'id' => $aThreadedSizes[$iThread][0]['id'],
					'size' => $iSize,
					'type' => $aThreadedSizes[$iThread][0]['type']
				);
			} else {
				$aSizeItem = CHardwareItems::getInstance()->GetItem($iSize);
				if (empty($aSizeItem) || !$aSizeItem['enabled'] || $aSizeItem['type'] != $iHardwareType) {
					return false;
				}
				$aSize = array(
					'id' => $aSizeItem['id'],
					'size' => $aSizeItem['size'],
					'type' => $aSizeItem['type']
				);
			}
		} else {
			$aSize = null;
		}

		$aResultItem = array(
			'id' => $aConstructorItem['id'],
			'rope_increment' => $aConstructorItem['rope_increment'],
			'time_coeff' => $aConstructorItem['time_coeff'],
			'item_size' => $aConstructorItem['size'],
			'crimp_size' => $aConstructorItem['crimp_size'],
			'image' => CConstructorItems::GetLink($aConstructorItem['id'], 2),
			'size' => $aSize,
			'subtype' => $iSubType,
			'levels' => new ArrayObject()
		);
		if ($iSubType === CConstructorItems::SUBTYPE_T_ROPE) {
			$aResultItem['t-rope'] = true;
		} else {
			$aResultItem['t-rope'] = false;
			if ($this->isTRope($sPosition, $iNumber) && $iFillType === 1) {
				$iFillType = 2;
			}
		}

		for ($c = 1; $c <= 9; $c++) {
			$iItemID = $aConstructorItem['item_'.$c.'_id'];
			if ($iItemID) {
				$aItem = $aItems[$iItemID];
				$aResultItem['levels'][$c] = array(
					'item' => array(
						'id' => $aItem['id'],
						'deletable' => $aConstructorItem['item_'.$c.'_deletable']
					)
				);
				if (count($aItem['colors']) === 1) {
					$aColor = array_shift($aItem['colors']);
					$iCurrentColor = $aColor['color'];
				} else {
					$iCurrentColor = $iColor;
				}
				$aResultItem['levels'][$c]['color'] = array(
					'id' => $iCurrentColor,
					'rgb' => CConstructorItems::$aColorsRGB[CConstructorItems::TYPE_CONNECTION][$iCurrentColor]
				);
			}
		}
		if ($iFillType == 1) {
			$this->{$sPosition.'Connections'}[$iNumber] = $aResultItem;
		} else {
			if ($iFillType == 2) {
				if (in_array($sPosition , array('top', 'bottom'))) {
					$iLimit = $iCols;
				} else {
					$iLimit = $iRows;
				}
				for ($c = 0; $c < $iLimit; $c++) {
					$this->{$sPosition.'Connections'}[$c] = $aResultItem;
				}
			} else {
				for ($c = 0; $c < $iCols; $c++) {
					$this->topConnections[$c] = $aResultItem;
					$this->bottomConnections[$c] = $aResultItem;
				}
				for ($c = 0; $c < $iRows; $c++) {
					$this->leftConnections[$c] = $aResultItem;
					$this->rightConnections[$c] = $aResultItem;
				}
			}
		}

		$this->checkPaddings();
		$this->fillFrame();
		$this->save();

		$this->returnJson();
	}

	private function isTRope($sPosition, $iNumber)
	{
		if (isset($this->{$sPosition.'Connections'}[$iNumber]) && $this->{$sPosition.'Connections'}[$iNumber]['t-rope'] === true) {
			return true;
		}
		return false;
	}

	private function hasTopTRope()
	{
		return $this->isTRope('top', 0);
	}

	private function hasBottomTRope()
	{
		return $this->isTRope('bottom', 0);
	}

	private function getColsOld()
	{
		return $_SESSION['constructor']['cols'];
	}

	private function getRowsOld()
	{
		return $_SESSION['constructor']['rows'];
	}

	private function hasTopTRopeOld()
	{
		$iNumber = 0;
		if (!empty($_SESSION['constructor']['topConnections'][$iNumber]) && $_SESSION['constructor']['topConnections'][$iNumber]['t-rope'] === true) {
			return true;
		}
		return false;
	}

	private function hasBottomTRopeOld()
	{
		$iNumber = 0;
		if (!empty($_SESSION['constructor']['bottomConnections'][$iNumber]) && $_SESSION['constructor']['bottomConnections'][$iNumber]['t-rope'] === true) {
			return true;
		}
		return false;
	}

	private function insertJunction($iNumber, $aNewElement = null)
	{
		$aNewArray = array();
		if (!empty($this->junctions)) {
			foreach ($this->junctions as $iKey => $aElement) {
				if ($iKey >= $iNumber) {
					$iKey++;
				}
				$aNewArray[$iKey] = $aElement;
			}
		}
		if ($aNewElement) {
			$aNewArray[$iNumber] = $aNewElement;
		}
		ksort($aNewArray);
		$this->junctions = $aNewArray;
	}

	private function removeJunction($iNumber)
	{
		if (empty($this->junctions)) {
			return;
		}
		$aNewArray = array();
		foreach ($this->junctions as $iKey => $aElement) {
			if ($iKey == $iNumber) {
				continue;
			}
			if ($iKey > $iNumber) {
				$iKey--;
			}
			$aNewArray[$iKey] = $aElement;
		}
		ksort($aNewArray);
		$this->junctions = $aNewArray;
	}

	private function recalcColsRows(&$iCols, &$iRows)
	{
		if ($this->hasTopTRope()) {
			$iRows++;
		}
		if ($this->hasBottomTRope()) {
			$iRows++;
		}
	}

	function fillFrame()
	{
		$iOldCols = $this->getColsOld();
		$iOldRows = $this->getRowsOld();
		$iNewCols = $this->cols;
		$iNewRows = $this->rows;

		if ($iNewCols > $iOldCols) {
			for ($c = $iOldCols; $c < $iNewCols; $c++) {
				for ($c1 = 0; $c1 < $iOldRows; $c1++) {
					if (isset($this->junctions[($c - 1) * $iOldRows + $c1])) {
						$this->insertJunction($c * $iOldRows + $c1, $this->junctions[($c - 1) * $iOldRows]);
					}
				}
			}
		} elseif ($iNewCols < $iOldCols) {
			for ($c = $iOldCols - 1; $c >= $iNewCols; $c--) {
				for ($c1 = 0; $c1 < $iOldRows; $c1++) {
					$this->removeJunction($iNewCols * $iOldRows);
				}
			}
		}
		if ($iNewRows > $iOldRows) {
			for ($c = $iOldRows; $c < $iNewRows; $c++) {
				for ($c1 = 0; $c1 < $iNewCols; $c1++) {
					if (isset($this->junctions[($c1 + 1) * $c + $c1 - 1])) {
						$this->insertJunction(($c1 + 1) * $c + $c1, $this->junctions[($c1 + 1) * $c + $c1 - 1]);
					} else {
						$this->insertJunction(($c1 + 1) * $c + $c1);
					}
				}
			}
		} elseif ($iNewRows < $iOldRows) {
			for ($c = $iOldRows - 1; $c >= $iNewRows; $c--) {
				for ($c1 = $iNewCols; $c1 > 0; $c1--) {
					$this->removeJunction($c1 * ($c + 1) - 1);
				}
			}
		}

		if ($this->hasTopTRopeOld()) {
			$iOldRows++;
		}
		if ($this->hasBottomTRopeOld()) {
			$iOldRows++;
		}
		if ($this->hasTopTRope()) {
			$iNewRows++;
		}
		if ($this->hasBottomTRope()) {
			$iNewRows++;
		}

		if ($iNewCols > $iOldCols) {
			for ($c = $iOldCols; $c < $iNewCols; $c++) {
				if (isset($this->topConnections[$c - 1])) {
					$this->topConnections[$c] = $this->topConnections[$c - 1];
				}
				if (isset($this->bottomConnections[$c - 1])) {
					$this->bottomConnections[$c] = $this->bottomConnections[$c - 1];
				}
			}
		} elseif ($iNewCols < $iOldCols) {
			for ($c = $iOldCols - 1; $c >= $iNewCols; $c--) {
				if (isset($this->topConnections[$c])) {
					unset($this->topConnections[$c]);
				}
				if (isset($this->bottomConnections[$c])) {
					unset($this->bottomConnections[$c]);
				}
			}
		}
		if ($iNewRows > $iOldRows) {
			for ($c = $iOldRows; $c < $iNewRows; $c++) {
				if (isset($this->leftConnections[$c - 1])) {
					$this->leftConnections[$c] = $this->leftConnections[$c - 1];
				}
				if (isset($this->rightConnections[$c - 1])) {
					$this->rightConnections[$c] = $this->rightConnections[$c - 1];
				}
			}
		} elseif ($iNewRows < $iOldRows) {
			for ($c = $iOldRows - 1; $c >= $iNewRows; $c--) {
				if (isset($this->leftConnections[$c])) {
					unset($this->leftConnections[$c]);
				}
				if (isset($this->rightConnections[$c])) {
					unset($this->rightConnections[$c]);
				}
			}
		}
	}

	function createimageAction()
	{
		if (!$this->started) {
			return false;
		}
                
		$aJson = $this->returnJson(true);
		unset($aJson['results']);

		$this->data = json_encode($aJson);

		$this->cDispatcher->SetTemplate('constructor_image');
	}

	private function cleanupResults()
	{
		if (!empty($_SESSION['constructor']['results'])) {
			unset($_SESSION['constructor']['results']);
			unset($_SESSION['constructor']['id']);
			if (!empty($_SESSION['constructor']['image'])) {
				@unlink($_SERVER['DOCUMENT_ROOT'].$_SESSION['constructor']['image']);
				unset($_SESSION['constructor']['image']);
			}
		}
	}

	function create()
	{
		$this->width = 2000;
		$this->height = 1000;
		$this->cellWidth = 250;
		$this->cellHeight = 250;
		$this->calculateFrame();
		$this->rope = null;
		$this->junctions = array();
		$this->topConnections = array();
		$this->bottomConnections = array();
		$this->leftConnections = array();
		$this->rightConnections = array();
	}

	function _init()
	{
		if (!CUsers::getInstance()->isLogged() || !CUsers::getInstance()->GetInfo('constructor')) {
			return false;
		}
		if ($_SESSION['constructor']) {
			$this->started = true;
			$this->width = $_SESSION['constructor']['width'];
			$this->height = $_SESSION['constructor']['height'];
			$this->cellWidth = $_SESSION['constructor']['cellWidth'];
			$this->cellHeight = $_SESSION['constructor']['cellHeight'];
			$this->cols = $_SESSION['constructor']['cols'];
			$this->rows = $_SESSION['constructor']['rows'];
			$this->paddingLeft = $_SESSION['constructor']['paddingLeft'];
			$this->paddingTop = $_SESSION['constructor']['paddingTop'];
			$this->rope = $_SESSION['constructor']['rope'];
			$this->junctions = $_SESSION['constructor']['junctions'];
			$this->topConnections = $_SESSION['constructor']['topConnections'];
			$this->bottomConnections = $_SESSION['constructor']['bottomConnections'];
			$this->leftConnections = $_SESSION['constructor']['leftConnections'];
			$this->rightConnections = $_SESSION['constructor']['rightConnections'];
		}
	}

	function save()
	{
		$_SESSION['constructor']['width'] = $this->width;
		$_SESSION['constructor']['height'] = $this->height;
		$_SESSION['constructor']['cellWidth'] = $this->cellWidth;
		$_SESSION['constructor']['cellHeight'] = $this->cellHeight;
		$_SESSION['constructor']['cols'] = $this->cols;
		$_SESSION['constructor']['rows'] = $this->rows;
		$_SESSION['constructor']['paddingLeft'] = $this->paddingLeft;
		$_SESSION['constructor']['paddingTop'] = $this->paddingTop;
		$_SESSION['constructor']['rope'] = $this->rope;
		$_SESSION['constructor']['junctions'] = $this->junctions;
		$_SESSION['constructor']['topConnections'] = $this->topConnections;
		$_SESSION['constructor']['bottomConnections'] = $this->bottomConnections;
		$_SESSION['constructor']['leftConnections'] = $this->leftConnections;
		$_SESSION['constructor']['rightConnections'] = $this->rightConnections;
	}

	function returnJson($return = false)
	{
		$aJson = array(
			'width' => $this->width,
			'height' => $this->height,
			'cellWidth' => $this->cellWidth,
			'cellHeight' => $this->cellHeight,
			'cols' => $this->cols,
			'rows' => $this->rows,
			'paddingLeft' => $this->paddingLeft,
			'paddingTop' => $this->paddingTop,
			'addRopeSizeLeft' => 0,
			'addRopeSizeRight' => 0,
			'addRopeSizeTop' => 0,
			'addRopeSizeBottom' => 0,
			'rope' => $this->rope,
			'junctions' => $this->junctions,
			'topConnections' => $this->topConnections,
			'bottomConnections' => $this->bottomConnections,
			'leftConnections' => $this->leftConnections,
			'rightConnections' => $this->rightConnections,
			'results' => $this->results()
		);

		$iStudSize = 0;
		foreach ($this->topConnections as $iNum => $aConnection) {
			if ($aConnection['size'] && $aConnection['size']['size'] && $aConnection['size']['size']) {
				if (!$iStudSize) {
					$iStudSize = $aConnection['size']['size'];
				} else {
					if ($iStudSize !== $aConnection['size']['size']) {
						$iStudSize = 0;
						break;
					}
				}
			}
			if ($aConnection['subtype'] === CConstructorItems::SUBTYPE_THIMBLE_FRAME) {
				if ($aConnection['crimp_size'] > $aJson['addRopeSizeTop']) {
					$aJson['addRopeSizeTop'] = $aConnection['crimp_size'];
				}
			}
			unset($aJson['topConnections'][$iNum]['subtype']);
			unset($aJson['topConnections'][$iNum]['crimp_size']);
		}
		$aJson['frameHeightTop'] = $iStudSize;

		$iStudSize = 0;
		foreach ($this->bottomConnections as $iNum => $aConnection) {
			if ($aConnection['size'] && $aConnection['size']['size'] && $aConnection['size']['size']) {
				if (!$iStudSize) {
					$iStudSize = $aConnection['size']['size'];
				} else {
					if ($iStudSize !== $aConnection['size']['size']) {
						$iStudSize = 0;
						break;
					}
				}
			}
			if ($aConnection['subtype'] === CConstructorItems::SUBTYPE_THIMBLE_FRAME) {
				if ($aConnection['crimp_size'] > $aJson['addRopeSizeBottom']) {
					$aJson['addRopeSizeBottom'] = $aConnection['crimp_size'];
				}
			}
			unset($aJson['bottomConnections'][$iNum]['subtype']);
			unset($aJson['bottomConnections'][$iNum]['crimp_size']);
		}
		$aJson['frameHeightBottom'] = $iStudSize;

		$iStudSize = 0;
		foreach ($this->leftConnections as $iNum => $aConnection) {
			if ($aConnection['size'] && $aConnection['size']['size'] && $aConnection['size']['size']) {
				if (!$iStudSize) {
					$iStudSize = $aConnection['size']['size'];
				} else {
					if ($iStudSize !== $aConnection['size']['size']) {
						$iStudSize = 0;
						break;
					}
				}
			}
			if ($aConnection['subtype'] === CConstructorItems::SUBTYPE_THIMBLE_FRAME) {
				if ($aConnection['crimp_size'] > $aJson['addRopeSizeLeft']) {
					$aJson['addRopeSizeLeft'] = $aConnection['crimp_size'];
				}
			}
			unset($aJson['leftConnections'][$iNum]['subtype']);
			unset($aJson['leftConnections'][$iNum]['crimp_size']);
		}
		$aJson['frameWidthLeft'] = $iStudSize;

		$iStudSize = 0;
		foreach ($this->rightConnections as $iNum => $aConnection) {
			if ($aConnection['size'] && $aConnection['size']['size'] && $aConnection['size']['size']) {
				if (!$iStudSize) {
					$iStudSize = $aConnection['size']['size'];
				} else {
					if ($iStudSize !== $aConnection['size']['size']) {
						$iStudSize = 0;
						break;
					}
				}
			}
			if ($aConnection['subtype'] === CConstructorItems::SUBTYPE_THIMBLE_FRAME) {
				if ($aConnection['crimp_size'] > $aJson['addRopeSizeRight']) {
					$aJson['addRopeSizeRight'] = $aConnection['crimp_size'];
				}
			}
			unset($aJson['rightConnections'][$iNum]['subtype']);
			unset($aJson['rightConnections'][$iNum]['crimp_size']);
		}
		$aJson['frameWidthRight'] = $iStudSize;

		// показ пока отключен, работники не готовы
		$aJson['addRopeSizeLeft'] = 0;
		$aJson['addRopeSizeRight'] = 0;
		$aJson['addRopeSizeTop'] = 0;
		$aJson['addRopeSizeBottom'] = 0;

		if ($return) {
			return $aJson;
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($aJson);
		$this->cDispatcher->bNoRender = true;
	}
}

?>
