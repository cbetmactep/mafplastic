<?

class adm_constructoritems extends CAdm_controller
{

	function indexAction()
	{
		if (!$this->type) {
			$this->types = CConstructorItems::$aTypes;

			$this->cDispatcher->SetTemplate('constructoritems_types');
		} else {
			if (!empty($_POST['ids'])) {
				$aIds = array_filter(array_unique(array_map('intval', $_POST['ids'])));
				if (!empty($aIds)) foreach ($aIds as $iNum => $iID) {
					$this->cContentClass->SetOrder($iID, $iNum + 1);
				}
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}

			$cSInfo = new CSInfo(array(
				'type' => $this->type
			));
			$aConstructorItems = CConstructorItems::getInstance()->GetListWithItems($cSInfo);

			if ($aConstructorItems) {
				$aItemIDs = array();
				foreach ($aConstructorItems as $aItem) {
					for ($c = 1; $c <= 9; $c++) {
						$iItemID = $aItem['item_'.$c.'_id'];
						if (!empty($iItemID) && !in_array($iItemID, $aItemIDs)) {
							$aItemIDs[] = $iItemID;
						}
					}
				}

				$cSInfo = new CSInfo(array(
					'item_ids' => $aItemIDs,
					'group_by_item' => true
				));
				$aItemsColors = CItemColors::getInstance()->GetList($cSInfo);

				foreach ($aConstructorItems as &$aItem) {
					$aPrices = array();
					$aColors = array();
					for ($c = 1; $c <= 9; $c++) {
						if (!empty($aItem['item_'.$c.'_id'])) {
							$aItem['item_'.$c.'_errors'] = array();
							$aItemPrices = json_decode($aItem['item_'.$c.'_prices'], true);
							if (empty($aItemPrices)) {
								$aItem['item_'.$c.'_errors'][] = 'нет цен';
							} else {
								$aItemPricesVariants = array();
								foreach ($aItemPrices as $aPrice) {
									$aItemPricesVariants[] = $aPrice['from'].'_'.$aPrice['to'];
								}
								if ($c === 1) {
									$aPrices = $aItemPricesVariants;
								} else {
									if ($aItemPricesVariants !== $aPrices) {
										$aItem['item_'.$c.'_errors'][] = 'цены не совпадают';
									}
								}
							}

							/*$aItemColors = $aItemsColors[$aItem['item_'.$c.'_id']];
							if (empty($aItemColors)) {
								$aItem['item_'.$c.'_errors'][] = 'нет цветов';
							} elseif (count($aItemColors) > 1) {
								$aItemColorsVariants = array();
								foreach ($aItemColors as $aColor) {
									$aItemColorsVariants[] = $aColor['color'];
								}
								sort($aItemColorsVariants);
								if ($c === 1) {
									$aColors = $aItemColorsVariants;
								} else {
									if ($aItemColorsVariants !== $aColors) {
										$aItem['item_'.$c.'_errors'][] = 'цвета не совпадают';
									}
								}
							}*/
						}
					}
				}
			}

			$this->constructoritems = $aConstructorItems;

			$this->cDispatcher->SetTemplate('constructoritems');
		}
	}

	public function addAction()
	{
		if ($_POST) {
			$this->values = array_merge($this->values,$_POST);
			$_POST['type'] = $this->type;
			$iNewID = $this->cContentClass->Add($_POST, $_FILES['image']['tmp_name'], $_FILES['image_scheme']['tmp_name'], $_FILES['image_sizes']['tmp_name']);
			if (!$iNewID) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		} else {
			if (!$this->id) {
				$this->values = array('enabled' => 1);
			}
		}
		$this->title = 'Добавление';
		$this->button = 'Добавить';

		$cSInfo = new CSInfo();
		$this->kit_items = CKitItems::getInstance()->GetList($cSInfo);
		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function editAction()
	{
		if ($_POST) {
			$this->values = array_merge($this->values, $_POST);
			if (!$this->cContentClass->Update($this->id, $_POST, $_FILES['image']['tmp_name'], $_FILES['image_scheme']['tmp_name'], $_FILES['image_sizes']['tmp_name'])) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		}
		$this->title = 'Редактирование';
		$this->button = 'Сохранить';

		$cSInfo = new CSInfo();
		$this->kit_items = CKitItems::getInstance()->GetList($cSInfo);
		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function switchstatusAction()
	{
		$this->cContentClass->SwitchStatus($this->id);
		$this->cDispatcher->Redirect($this->sRedirectUrl);
	}

	public function _init()
	{
		if ($this->type) {
			if (empty(CConstructorItems::$aTypes[$this->type])) return false;
			$this->typeinfo = CConstructorItems::$aTypes[$this->type];
		}

		$this->cContentClass = CConstructorItems::getInstance();
		$this->sRedirectUrl = '/admin/constructoritems/'.$this->type.'/';
		$this->sEditTemplate = 'constructoritems_form';
		$this->iRoleFlag = CAdmin::ROLE_MANAGEITEMS;

		$cSInfo = new CSInfo(array(
			'set_id_keys' => true
		));
		$this->items = CItems::getInstance()->GetList($cSInfo);

		return parent::_init();
	}

}

?>
