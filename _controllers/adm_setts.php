<?

class adm_setts extends CController
{

	function indexAction()
	{
		$this->setts = CSetts::getInstance()->GetSetts();
		if ($_POST) {
			if (is_uploaded_file($_FILES['headerimage']['tmp_name'])) {
				move_uploaded_file($_FILES['headerimage']['tmp_name'], CSetts::GetHeaderImgLink(true));
			}
			$this->setts = array_merge($this->setts,$_POST);
			if (CSetts::getInstance()->Save($_POST)) {
				$this->cDispatcher->Redirect('/admin/setts/');
			} else {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			}
		}

		$this->cDispatcher->SetTemplate('setts');
	}

	public function _init() {
		if (!CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGESETTS)) $this->cDispatcher->Redirect('/admin/');
	}

}

?>