<?

class adm_users extends CAdm_controller
{

	function indexAction()
	{
		$cSInfo = new CSInfo(array(
			'email' => $_GET['email'],
			'page' => (int)$_GET['page'],
			'perpage' => 30
		));
		$this->users = $this->cContentClass->GetList($cSInfo);
		$this->pagination = $cSInfo->MakePagination('email='.$_GET['email'].'&');

		$this->cDispatcher->SetTemplate('users');
	}

	function addAction()
	{
		if ($_POST) {
			$this->values = $_POST;
			if (!$this->cContentClass->Add($_POST)) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		} else {
			$this->values = array(
				'type' => CUsers::TYPE_PRIVATE
			);
		}
		$this->title = 'Добавление';
		$this->button = 'Добавить';
		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function _init()
	{
		$this->cContentClass = CUsers::getInstance();
		$this->sRedirectUrl = '/admin/users/';
		$this->sEditTemplate = 'users_form';
		$this->iRoleFlag = CAdmin::ROLE_MANAGEUSERS;
		parent::_init();
	}

}

?>