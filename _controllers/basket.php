<?

class basket extends CController
{

	function blockAction()
	{
		$iCount = 0;
		/*if (!empty($_SESSION['basket'])) foreach ($_SESSION['basket'] as $aItem) {
			$iCount += $aItem['count'];
		}*/
		if (!empty($_SESSION['basket'])) {
			$iCount += count($_SESSION['basket']);
		}
		if (!empty($_SESSION['basket_constructors'])) {
			$iCount += count($_SESSION['basket_constructors']);
		}
		//echo $iCount;
		echo '<div id="basket_block" class="flr '.($iCount > 0?'btn_blue':'btn_white').' cart_mini"><a href="/basket/"><b>&nbsp;</b><span>'.$iCount.'</span> В корзине</a></div>';
		$this->cDispatcher->bNoRender = true;
	}

	function indexAction()
	{
		if ($_SESSION['message_sent']) {
			return $this->_success();
		}

		if (!empty($_POST['send'])) {
			$this->_sendbasket();
		} elseif ($_POST) {
			$this->_updatebasket();
		}

		$this->total_count = 0;
		$this->total_price = 0;
		$this->items = array();
		$this->constructors = array();
		if ($_SESSION['basket']) {
			foreach ($_SESSION['basket'] as $aItem) {
				$aItem['price'] = 0;
				foreach ($aItem['prices'] as $aPrice) {
					if ($aPrice['from'] <= $aItem['count'] && $aPrice['to'] >= $aItem['count']) {
						$aItem['price'] = $aPrice['price'];
					}
				}
				$aItem['total'] = round($aItem['count'] * $aItem['price'], 2);
				$this->items[] = $aItem;
				$this->total_count ++;//= $aItem['count'];
				$this->total_price += $aItem['total'];
			}
		}
		if (!empty($_SESSION['basket_constructors'])) {
			foreach ($_SESSION['basket_constructors'] as $iID => $aItem) {
				$this->total_count ++;
				$this->total_price += round($aItem['count'] * $aItem['price']);
				$this->constructors[] = array(
					'id' => $iID,
					'price' => $aItem['price'],
					'count' => $aItem['count']
				);
			}
		}

		if (CUsers::getInstance()->isLogged() && !$_POST) {
			$this->values = array(
				'name' => trim(CUsers::getInstance()->GetInfo('last_name').' '.CUsers::getInstance()->GetInfo('first_name').' '.CUsers::getInstance()->GetInfo('middle_name')),
				'phone' => CUsers::getInstance()->GetInfo('phone'),
				'email' => CUsers::getInstance()->GetInfo('email')
			);
		}

		$this->cDispatcher->SetTemplate('basket_index');
	}

	function addAction()
	{
		$aItem = CItems::getInstance()->GetItem((int)$this->item_id);
		if (empty($aItem['id']) || empty($aItem['enabled'])) {
			return false;
		}
		$iQuant = (int)$_POST['quant'];
		if ($iQuant <= 0 || $iQuant > 1000000) {
			$iQuant = 1;
		}
		$aItem['image_id'] = CItemGallery::getInstance()->GetMainForItem($aItem['id']);

		if ($_POST['add']) {
			if (empty($_SESSION['basket'][$aItem['id']])) {
				$_SESSION['basket'][$aItem['id']] = array(
					'count' => $iQuant,
					'cat_id' => $aItem['cat_id'],
					'title' => $aItem['title'],
					'articul' => $aItem['articul'],
					'prices' => $aItem['prices'],
					'id' => $aItem['id'],
					'image_id' => $aItem['image_id']
				);
			} else {
				$_SESSION['basket'][$aItem['id']]['count'] += $iQuant;
			}

			$this->total_quant = 0;
			$this->total_price = 0;
			if (!empty($_SESSION['basket'])) {
				foreach ($_SESSION['basket'] as $aItem) {
					$this->total_quant ++;
					$aItem['price'] = 0;
					foreach ($aItem['prices'] as $aPrice) {
						if ($aPrice['from'] <= $aItem['count'] && $aPrice['to'] >= $aItem['count']) {
							$aItem['price'] = $aPrice['price'];
						}
					}
					$this->total_price += round($aItem['count'] * $aItem['price'], 2);
				}
			}
			if (!empty($_SESSION['basket_constructors'])) {
				foreach ($_SESSION['basket_constructors'] as $aItem) {
					$this->total_quant ++;
					$this->total_price += round($aItem['count'] * $aItem['price'], 2);
				}
			}
			$this->cDispatcher->SetTemplate('basket_add_success');
			return;
		}

		$aItem['price'] = 0;
		foreach ($aItem['prices'] as $aPrice) {
			if ($aPrice['from'] <= $iQuant && $aPrice['to'] >= $iQuant) {
				$aItem['price'] = $aPrice['price'];
			}
		}

		$this->quant = $iQuant;
		$this->price = $aItem['price'];
		$this->total = round($iQuant * $aItem['price'], 2);
		$this->item = $aItem;
		if (!empty($_POST['quant'])) {
			if ($this->cDispatcher->isXmlHttpRequest()) {
				$aJSON = array(
					'quant' => $this->quant,
					'price' => $this->price,
					'total' => $this->total
				);
				header('Content-Type: application/json');
				echo json_encode($aJSON);
				$this->cDispatcher->bNoRender = true;
			} else {
				$this->cDispatcher->Redirect('/basket/add/'.$aItem['id'].'/');
			}
		} else {
			$this->cDispatcher->SetTemplate('basket_add');
		}

		/*if (!isset($_POST['quant'])) {
			$this->item = $aItem;
			$this->cDispatcher->SetTemplate('basket_add');
		} else {
			$iQuant = (int)$_POST['quant'];
			if ($iQuant <= 0 || $iQuant > 1000000) $iQuant = 1;

			$_SESSION['basket'][$aItem['id']] = array(
				'count' => (int)$_SESSION['basket'][$aItem['id']]['count'] + $iQuant,
				'cat_id' => $aItem['cat_id'],
				'title' => $aItem['title'],
				'price' => $aItem['price'],
				'id' => $aItem['id'],
				'image_id' => $aItem['image_id']
			);

			if ($this->cDispatcher->isXmlHttpRequest()) {
				return $this->blockAction();
			} else {
				$this->cDispatcher->Redirect('/basket/');
			}
		}*/
	}

	function deleteAction()
	{
		unset($_SESSION['basket'][(int)$this->item_id]);

		if ($this->cDispatcher->isXmlHttpRequest()) {
			$this->_returnbasketjson();
		} else {
			$this->cDispatcher->Redirect('/basket/');
		}
	}

	public function _returnbasketjson()
	{
		$aJSON = array(
			'total_count' => 0,
			'total_price' => 0,
			'items_prices' => array()
		);
		if (!empty($_SESSION['basket'])) {
			foreach ($_SESSION['basket'] as $iID => $aItem) {
				$aItem['price'] = 0;
				foreach ($aItem['prices'] as $aPrice) {
					if ($aPrice['from'] <= $aItem['count'] && $aPrice['to'] >= $aItem['count']) {
						$aItem['price'] = $aPrice['price'];
					}
				}
				$aItem['total'] = round($aItem['count'] * $aItem['price'], 2);
				$aJSON['total_count'] ++;//+= $aItem['count'];
				$aJSON['total_price'] += $aItem['total'];
				$aJSON['items_prices'][$iID] = $aItem['price'];
			}
		}
		if (!empty($_SESSION['basket_constructors'])) {
			foreach ($_SESSION['basket_constructors'] as $iID => $aItem) {
				$aItem['total'] = round($aItem['count'] * $aItem['price']);
				$aJSON['total_count'] ++;
				$aJSON['total_price'] += $aItem['total'];
			}
		}

		header('Content-Type: application/json');
		echo json_encode($aJSON);
		die();
	}

	private function _updatebasket()
	{
		if (!empty($_SESSION['basket'])) {
			foreach ($_SESSION['basket'] as $iID => $aItem) {
				$iCount = (int)$_POST['count'][$iID];
				if ($iCount > 0) {
					$_SESSION['basket'][$iID]['count'] = $iCount;
				}
			}
		}
		if (!empty($_SESSION['basket_constructors'])) {
			foreach ($_SESSION['basket_constructors'] as $iID => $aItem) {
				$iCount = (int)$_POST['count']['c'.$iID];
				if ($iCount > 0) {
					$_SESSION['basket_constructors'][$iID]['count'] = $iCount;
				}
			}
		}

		if ($this->cDispatcher->isXmlHttpRequest()) {
			$this->_returnbasketjson();
		} else {
			$this->cDispatcher->Redirect('/basket/');
		}
	}

	private function _sendbasket()
	{
		if (empty($_SESSION['basket']) && empty($_SESSION['basket_constructors'])) {
			return;
		}

		$iTotalCount = 0;
		$fTotalPrice = 0;
		$aItems = array();
		$aConstructors = array();
		if (!empty($_SESSION['basket'])) {
			foreach ($_SESSION['basket'] as $aItem) {
				$aItem['price'] = 0;
				foreach ($aItem['prices'] as $aPrice) {
					if ($aPrice['from'] <= $aItem['count'] && $aPrice['to'] >= $aItem['count']) {
						$aItem['price'] = $aPrice['price'];
					}
				}
				unset($aItem['prices']);
				$aItem['total'] = round($aItem['count'] * $aItem['price'], 2);
				$aItems[] = $aItem;
				$iTotalCount ++;//= $aItem['count'];
				$fTotalPrice += $aItem['total'];
			}
		}
		if (!empty($_SESSION['basket_constructors'])) {
			foreach ($_SESSION['basket_constructors'] as $iID => $aItem) {
				$iTotalCount ++;
				$fTotalPrice += $aItem['price'];
				$aConstructors[] = array(
					'id' => $iID,
					'price' => $aItem['price']
				);
			}
		}

		$this->values = $_POST;
		$_POST['params'] = array('items' => $aItems, 'constructors' => $aConstructors);
		$_POST['total_count'] = $iTotalCount;
		$_POST['total_price'] = $fTotalPrice;
		if (CUsers::getInstance()->isLogged()) {
			$_POST['user_id'] = CUsers::getInstance()->ID();
		}
		if (!COrders::getInstance()->Add($_POST)) {
			if ($this->cDispatcher->isXmlHttpRequest()) {
				header('Content-Type: application/json');
				$aErrors = array(
					'error' => CChecker::GetLastError(),
					'errors' => CChecker::GetLastErrorFields()
				);
			} else {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			}
		} else {
			$cSetts = CSetts::getInstance();

			$sSubject = '=?UTF-8?B?'.base64_encode('Заказ на сайте MAF PLASTIC').'?=';
			$sMessage = '<html><body>'.
						'<p>Контактное лицо: '.$_POST['name'].'</p>'.
						'<p>Телефон: '.$_POST['phone'].'</p>'.
						'<p>E-mail: '.$_POST['email'].'</p>'.
						'<p>Дополнительная информация:<br>'.nl2br($_POST['add_info']).'</p>';
			$sMessage .= '<table style="width: 100%; border-collapse: collapse; border: 1px solid #000">
					<tr><th style="text-align: center; font-weight: bold; border: 1px solid #000">Наименование</th>
						<th style="text-align: center; font-weight: bold; border: 1px solid #000">Кол-во</th>
						<th style="text-align: center; font-weight: bold; border: 1px solid #000">Цена</th>
						<th style="text-align: center; font-weight: bold; border: 1px solid #000">Сумма</th>';
			foreach ($aItems as $aItem) {
				$sMessage .= '<tr><td style="border: 1px solid #000">'.$aItem['title'];
				if (!empty($aItem['articul'])) {
					$sMessage .= '<br/>[ '.$aItem['articul'].' ]';
				}
				$sMessage .= '</td>
							<td style="border: 1px solid #000; text-align: center">'.$aItem['count'].'</td>
							<td style="border: 1px solid #000; text-align: center" nowrap="nowrap">'.$aItem['price'].' руб. / шт</td>
							<td style="border: 1px solid #000; text-align: center" nowrap="nowrap">'.$aItem['total'].' руб.</td></tr>';
			}
			foreach ($aConstructors as $aItem) {
				$sMessage .= '<tr><td style="border: 1px solid #000">Канатная конструкция. Проект №'.$aItem['id'].'</td>
							<td style="border: 1px solid #000; text-align: center">1</td>
							<td style="border: 1px solid #000; text-align: center" nowrap="nowrap">'.$aItem['price'].' руб. / шт</td>
							<td style="border: 1px solid #000; text-align: center" nowrap="nowrap">'.$aItem['price'].' руб.</td></tr>';
			}

			$sMessage .= '<tr><td colspan="4" style="border: 1px solid #000; font-weight: bold; text-align: right">Итого: '.$fTotalPrice.' руб.</td></tr>'.
						'</table></body></html>';

			$sHeaders = 'From: '.$cSetts->site_email."\r\n" .
					'Reply-To: '.$cSetts->site_email."\r\n".
					'MIME-Version: 1.0'."\r\n".
					'Content-type: text/html; charset=utf-8'."\r\n";
			if (!mail($cSetts->site_email, $sSubject, $sMessage, $sHeaders)) {
				if ($this->cDispatcher->isXmlHttpRequest()) {
					$aErrors = array(
						'error' => L10N('ERROR_MAIL_ERROR'),
						'errors' => ''
					);
				} else {
					$this->error = L10N('ERROR_MAIL_ERROR');
				}
			} else {
				if ($_POST['send_me']) {
					 mail($_POST['email'], $sSubject, $sMessage, $sHeaders);
				}
				unset($_SESSION['basket']);
				unset($_SESSION['basket_constructors']);
				if ($this->cDispatcher->isXmlHttpRequest()) {
					$aErrors = array(
						'error' => '',
						'errors' => ''
					);
				} else {
					$_SESSION['message_sent'] = 1;
					$this->cDispatcher->Redirect('/basket/');
				}
			}
		}
		if ($this->cDispatcher->isXmlHttpRequest()) {
			header('Content-Type: application/json');
			echo json_encode($aErrors);
			$this->cDispatcher->bNoRender = true;
		}
	}

	private function _success()
	{
		unset($_SESSION['message_sent']);
		$this->cDispatcher->SetTemplate('basket_success');
	}

}

?>
