<?

class adm_hardwareitems extends CAdm_controller
{

	function indexAction()
	{
		if (!$this->type) {
			$this->types = CHardwareItems::$aTypes;

			$this->cDispatcher->SetTemplate('hardwareitems_types');
		} else {
			$cSInfo = new CSInfo(array(
				'type' => $this->type
			));
			$this->hardwareitems = CHardwareItems::getInstance()->GetList($cSInfo);

			$this->cDispatcher->SetTemplate('hardwareitems');
		}
	}

	public function addAction()
	{
		if ($_POST) {
			$_POST['type'] = $this->type;
		}
		return parent::addAction();
	}

	public function editAction()
	{
		if ($_POST) {
			$_POST['type'] = $this->type;
		}
		return parent::editAction();
	}

	public function switchstatusAction()
	{
		$this->cContentClass->SwitchStatus($this->id);
		$this->cDispatcher->Redirect($this->sRedirectUrl);
	}

	public function _init()
	{
		if ($this->type) {
			if (empty(CHardwareItems::$aTypes[$this->type])) return false;
			$this->typeinfo = CHardwareItems::$aTypes[$this->type];
		}

		$this->cContentClass = CHardwareItems::getInstance();
		$this->sRedirectUrl = '/admin/hardwareitems/'.$this->type.'/';
		$this->sEditTemplate = 'hardwareitems_form';
		$this->iRoleFlag = CAdmin::ROLE_MANAGEITEMS;
		if (!parent::_init()) {
			return false;
		}
		if ($this->values && $this->values['type'] != $this->type) {
			return false;
		}
	}

}

?>
