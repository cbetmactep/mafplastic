<?

class adm_pages extends CAdm_controller
{

	function indexAction()
	{
		$this->pages = $this->cContentClass->GetList();

		$this->cDispatcher->SetTemplate('pages');
	}

	public function _init()
	{
		$this->cContentClass = CPages::getInstance();
		$this->sRedirectUrl = '/admin/pages/';
		$this->sEditTemplate = 'pages_form';
		$this->iRoleFlag = CAdmin::ROLE_MANAGEPAGES;

		return parent::_init();
	}

}

?>