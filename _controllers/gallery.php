<?

class gallery extends CController
{

	function indexAction()
	{
		$this->pageinfo = CPages::getInstance()->GetItemByPath('/gallery/');
		
		$cSInfo = new CSInfo(array(
			'enabled_only' => true,
			'set_id_keys' => true
		));
		$aGallery = CGallery::getInstance()->GetList($cSInfo);
		
		if ($aGallery) {
			$aIds = array_keys($aGallery);
			$aItems = CGallery::getInstance()->GetItems($aIds);
			foreach ($aGallery as &$aItem) {
				if (!empty($aItems[$aItem['id']])) {
					$aItem['items'] = $aItems[$aItem['id']];
				} else {
					$aItem['items'] = array();
				}
			}
			unset($aItem);
		}
		
		$this->gallery = $aGallery;
		
		$this->cDispatcher->SetTemplate('gallery');
	}

}

?>
