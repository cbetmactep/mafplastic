<?

class adm_admins extends CAdm_controller
{

	function indexAction()
	{
		if (!CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGEADMINS)) $this->cDispatcher->Redirect('/admin/admins/edit/'.CAdmin::getInstance()->ID().'/');
		$this->admins = $this->cContentClass->GetList();

		$this->cDispatcher->SetTemplate('admins');
	}

	function addAction()
	{
		if (!CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGEADMINS)) $this->cDispatcher->Redirect('/admin/');
		parent::addAction();
	}

	function editAction()
	{
		if (!CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGEADMINS) && $this->id != CAdmin::getInstance()->ID())
				$this->cDispatcher->Redirect('/admin/');

		if (!CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGEADMINS) || CAdmin::getInstance()->ID() == $this->id) $this->no_rights = true;
		parent::editAction();
	}

	public function _init()
	{
		$this->cContentClass = CAdmin::getInstance();
		$this->sRedirectUrl = '/admin/admins/';
		$this->sEditTemplate = 'admins_form';

		if ($this->id) {
			$this->values = $this->cContentClass->GetItem($this->id);
			if (!$this->values['id']) return false;
		}

	}

}

?>