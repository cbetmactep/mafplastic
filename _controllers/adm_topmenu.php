<?

class adm_topmenu extends CAdm_controller
{

	function indexAction()
	{
		if (!empty($_POST['ids'])) {
			$aIds = array_filter(array_unique(array_map('intval', $_POST['ids'])));
			if (!empty($aIds)) foreach ($aIds as $iNum => $iID) {
				$this->cContentClass->SetOrder($iID, $iNum + 1);
			}
			$this->cDispatcher->Redirect($this->sRedirectUrl);
		}

		$cSInfo = new CSInfo();
		$this->menu = $this->cContentClass->GetList($cSInfo);
		$this->cDispatcher->SetTemplate('top_menu');
	}

	public function switchstatusAction()
	{
		$this->cContentClass->SwitchStatus($this->id);
		$this->cDispatcher->Redirect($this->sRedirectUrl);
	}

	public function _init()
	{
		$this->cContentClass = CTopMenu::getInstance();
		$this->sRedirectUrl = '/admin/topmenu/';
		$this->sEditTemplate = 'top_menu_form';
		$this->iRoleFlag = CAdmin::ROLE_MANAGETOPMENU;
		return parent::_init();
	}

}

?>
