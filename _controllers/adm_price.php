<?

class adm_price extends CAdm_controller
{

	function indexAction()
	{
		if ($this->cat_id) {
			if (!empty($_POST['ids'])) {
				$aIds = array_filter(array_unique(array_map('intval', $_POST['ids'])));
				if (!empty($aIds)) foreach ($aIds as $iNum => $iID) {
					$this->cContentClass->SetOrder($iID, $iNum + 1);
				}
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
			$cSInfo = new CSInfo(array(
				'cat_id' => $this->cat_id
			));
			$this->price = $this->cContentClass->GetList($cSInfo);

			$sTpl = 'price';
		} else {
			$cSInfo = new CSInfo(array(
				'only_top_level' => true
			));
			$this->cats = CCats::getInstance()->GetList($cSInfo);

			$sTpl = 'price_cats';
		}
		$this->cDispatcher->SetTemplate($sTpl);
	}

	public function addAction()
	{
		if ($_POST) {
			$this->values = $_POST;
			$_POST['cat_id'] = $this->catinfo['id'];
			if (!$this->cContentClass->Add($_POST, $_FILES['file']['tmp_name'])) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		} else {
			$this->values = array(
				'enabled' => 1
			);
		}
		$this->title = 'Добавление';
		$this->button = 'Добавить';

		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function editAction()
	{
		if ($_POST) {
			if ($_POST['remove_file']) $this->cContentClass->RemoveFiles($this->id);
			$this->values = array_merge($this->values, $_POST);
			if (!$this->cContentClass->Update($this->id,$_POST,$_FILES['file']['tmp_name'])) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		}
		$this->title = 'Редактирование';
		$this->button = 'Сохранить';

		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function switchstatusAction()
	{
		$this->cContentClass->SwitchStatus($this->id);
		$this->cDispatcher->Redirect($this->sRedirectUrl);
	}

	public function _init()
	{
		if ($this->cat_id) {
			$this->catinfo = CCats::getInstance()->GetItem($this->cat_id);
			if (empty($this->catinfo['id']) || !empty($this->catinfo['parent_id'])) return false;
		}

		$this->subdomains = CSubdomains::getInstance()->GetList();

		$this->cContentClass = CPrice::getInstance();
		$this->sRedirectUrl = '/admin/price/'.$this->catinfo['id'].'/';
		$this->sEditTemplate = 'price_form';
		$this->iRoleFlag = CAdmin::ROLE_MANAGEPRICE;
		return parent::_init();
	}

}

?>