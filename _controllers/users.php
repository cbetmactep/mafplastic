<?

class users extends CController
{

	function indexAction()
	{
		if (!CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/login/');
		}

		$this->name = CUsers::getInstance()->GetInfo('first_name').' '.CUsers::getInstance()->GetInfo('middle_name');

		$this->cDispatcher->SetTemplate('user_index');
	}

	function loginAction()
	{
		if (CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/user/');
		}

		if ($_POST) {
			$this->values = $_POST;
			if (!CUsers::getInstance()->Login($_POST)) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($_SERVER['HTTP_REFERER']);
			}
		}

		$this->cDispatcher->SetTemplate('user_login');
	}

	function logoutAction()
	{
		CUsers::getInstance()->Logout();
		$this->cDispatcher->Redirect('/');
	}

	function registrationAction()
	{
		if (CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/user/');
		}

		if ($_POST) {
			$this->values = $_POST;
			$_POST['password'] = GeneratePassword();
			if (!CUsers::getInstance()->Add($_POST)) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$cSetts = CSetts::getInstance();
				$cMail = new CMail(1);
				$cMail->SetSender($cSetts->noreply_email);
				$cMail->SetDelivery($_POST['email']);
				$cMail->AddVar('{PASSWORD}', $_POST['password']);
				$cMail->AddVar('{FIRST_NAME}', $_POST['first_name']);
				$cMail->AddVar('{LAST_NAME}', $_POST['last_name']);
				$cMail->AddVar('{MIDDLE_NAME}', $_POST['middle_name']);
				$cMail->Send();

				$_SESSION['registration_data'] = array(
					'email' => $_POST['email'],
					'password' => $_POST['password']
				);
				if ($this->cDispatcher->isXmlHttpRequest()) {
					return $this->registrationsuccessAction();
				} else {
					$this->cDispatcher->Redirect('/user/registration/success/');
				}
			}
		} else {
			$this->values = array(
				'type' => CUsers::TYPE_PRIVATE
			);
		}

		$this->cDispatcher->SetTemplate('user_registration');
	}

	function registrationsuccessAction()
	{
		if (CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/user/');
		}
		if (!$_SESSION['registration_data']) {
			$this->cDispatcher->Redirect('/user/registration/');
		}
		$this->values = $_SESSION['registration_data'];
		unset($_SESSION['registration_data']);
		$this->cDispatcher->SetTemplate('user_registration_success');
	}

	function profileAction()
	{
		if (!CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/login/');
		}

		if ($_SESSION['profile_success']) {
			$this->error = 'Изменения сохранены';
			unset($_SESSION['profile_success']);
		}

		$this->values = CUsers::getInstance()->GetInfo();

		if ($_POST) {
			$this->values = array_merge($this->values,$_POST);
			$_POST['production'] = CUsers::getInstance()->GetInfo('production');
			$_POST['constructor'] = CUsers::getInstance()->GetInfo('constructor');
			$_POST['all_constructors'] = CUsers::getInstance()->GetInfo('all_constructors');
			if (!CUsers::getInstance()->Update(CUsers::getInstance()->ID(), $_POST)) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				if ($this->cDispatcher->isXmlHttpRequest()) {
					$this->error = 'Изменения сохранены';
				} else {
					$_SESSION['profile_success'] = 1;
					$this->cDispatcher->Redirect('/user/profile/');
				}
			}
		}

		$this->cDispatcher->SetTemplate('user_profile');
	}

	function recoveryAction()
	{
		if (CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/profile/');
		}

		if ($_SESSION['recovery_success']) {
			$this->error = 'Новый пароль выслан на Ваш e-mail';
			unset($_SESSION['recovery_success']);
		} elseif ($_POST) {
			$this->values = $_POST;
			$aItem = CUsers::getInstance()->FindByEmail($_POST['email']);
			if (!$aItem) {
				$this->error = L10N('ERROR_NO_USER');
			} else {
				$sPassword = GeneratePassword();
				if (!CUsers::getInstance()->ChPassword($aItem['id'], $sPassword)) {
					$this->error = CChecker::GetLastError();
				} else {
					$cSetts = CSetts::getInstance();
					$cMail = new CMail(2);
					$cMail->SetSender($cSetts->site_email);
					$cMail->SetDelivery($aItem['email']);
					$cMail->AddVar('{PASSWORD}', $sPassword);
					$cMail->AddVar('{FIRST_NAME}', $aItem['first_name']);
					$cMail->AddVar('{LAST_NAME}', $aItem['last_name']);
					$cMail->AddVar('{MIDDLE_NAME}', $aItem['middle_name']);
					if (!$cMail->Send()) {
						$this->error = CChecker::GetLastError();
					} else {
						$_SESSION['recovery_success'] = 1;
						$this->cDispatcher->Redirect('/user/recovery/');
					}
				}
			}
		}

		$this->cDispatcher->SetTemplate('user_recovery');
	}

	function ordersAction()
	{
		if (!CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/login/');
		}

		$cSInfo = new CSInfo(array(
			'user_id' => CUsers::getInstance()->ID(),
			'perpage' => 12,
			'page' => (int)$_GET['page']
		));
		/*if (!CChecker::CheckString($_GET['date_from'], 65)) $cSInfo->date_from = $_GET['date_from'];
		else $cSInfo->date_from = date('Y-m-d', strtotime('-1 YEAR'));
		if (!CChecker::CheckString($_GET['date_to'], 65)) $cSInfo->date_to = $_GET['date_to'];
		else $cSInfo->date_to = date('Y-m-d');*/

		$this->orders = COrders::getInstance()->GetList($cSInfo);

		/*$this->date_from = $cSInfo->date_from;
		$this->date_to = $cSInfo->date_to;*/
		$this->pagination = $cSInfo->MakePagination();

		$this->name = CUsers::getInstance()->GetInfo('first_name').' '.CUsers::getInstance()->GetInfo('middle_name');

		$this->cDispatcher->SetTemplate('user_orders');
	}

	/*function ordersitemAction()
	{
		if (!CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/login/');
		}

		$this->item = COrders::getInstance()->GetItem($this->id);
		if (!$this->item || $this->item['user_id'] != CUsers::getInstance()->ID()) {
			return false;
		}

		$this->cDispatcher->SetTemplate('user_orders_item');
	}*/

	function constructorsAction()
	{
		if (!CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/login/');
		}

		$cSInfo = new CSInfo(array(
			'user_id' => CUsers::getInstance()->ID(),
			'perpage' => 12,
			'page' => (int)$_GET['page']
		));

		$this->constructors = CConstructors::getInstance()->GetList($cSInfo);

		$this->pagination = $cSInfo->MakePagination();

		$this->name = CUsers::getInstance()->GetInfo('first_name').' '.CUsers::getInstance()->GetInfo('middle_name');

		$this->cDispatcher->SetTemplate('user_constructors');
	}

	function constructorsitemAction()
	{
		if (!CUsers::getInstance()->isLogged()) {
			return false;
		}
		$this->item = CConstructors::getInstance()->GetItem($this->id);
		if (empty($this->item)) {
			return false;
		}
		if (!CUsers::getInstance()->GetInfo('all_constructors') && (empty($this->item['user_id']) || $this->item['user_id'] != CUsers::getInstance()->ID())) {
			return false;
		}

		if ($this->item['params']['results']['items_optional']) {
			foreach ($this->item['params']['results']['items_optional'] as $sID => $aItem) {
				if ($aItem['removed']) {
					unset($this->item['params']['results']['items_optional'][$sID]);
				}
			}
		}

		$this->cDispatcher->SetTemplate('user_constructor');
	}

	function constructorsitemprintAction()
	{
		if (!CUsers::getInstance()->isLogged()) {
			return false;
		}
		$aConstructorItem = CConstructors::getInstance()->GetItem($this->id);
		if (empty($aConstructorItem)) {
			return false;
		}
		if (!CUsers::getInstance()->GetInfo('all_constructors') && (empty($aConstructorItem['user_id']) || $aConstructorItem['user_id'] != CUsers::getInstance()->ID())) {
			return false;
		}

		if (!empty($aConstructorItem['params']['results']['items_optional'])) {
			$aItemsOptional = $aConstructorItem['params']['results']['items_optional'];
			foreach ($aItemsOptional as $sID => $aItem) {
				if ($aItem['removed']) {
					unset($aItemsOptional[$sID]);
				}
			}
		}

		$this->items_required = $aConstructorItem['params']['results']['items_required'];
		$this->items_optional = $aItemsOptional;
		$this->hardware_items = $aConstructorItem['params']['results']['hardware_items'];
		$this->kit_items = $aConstructorItem['params']['results']['kit_items'];
		$this->total_required = $aConstructorItem['params']['results']['total_required'];
		$this->total_optional = $aConstructorItem['params']['results']['total_optional'];
		$this->build_price = $aConstructorItem['params']['results']['build_price'];
		$this->total_price = $aConstructorItem['params']['results']['total_price'];
		$this->debug_info = $aConstructorItem['params']['results']['debug_info'];
		$this->id = $aConstructorItem['id'];

		$this->cDispatcher->SetTemplate('constructor_results_print');
	}

	function constructorsitempdfAction()
	{
		if (!CUsers::getInstance()->isLogged()) {
			return false;
		}
		$aConstructorItem = CConstructors::getInstance()->GetItem($this->id);
		if (empty($aConstructorItem)) {
			return false;
		}
		if (!CUsers::getInstance()->GetInfo('all_constructors') && (empty($aConstructorItem['user_id']) || $aConstructorItem['user_id'] != CUsers::getInstance()->ID())) {
			return false;
		}

		if (!empty($aConstructorItem['params']['results']['items_optional'])) {
			$aItemsOptional = $aConstructorItem['params']['results']['items_optional'];
			foreach ($aItemsOptional as $sID => $aItem) {
				if ($aItem['removed']) {
					unset($aItemsOptional[$sID]);
				}
			}
		}

		$this->items_required = $aConstructorItem['params']['results']['items_required'];
		$this->items_optional = $aItemsOptional;
		$this->hardware_items = $aConstructorItem['params']['results']['hardware_items'];
		$this->kit_items = $aConstructorItem['params']['results']['kit_items'];
		$this->total_required = $aConstructorItem['params']['results']['total_required'];
		$this->total_optional = $aConstructorItem['params']['results']['total_optional'];
		$this->build_price = $aConstructorItem['params']['results']['build_price'];
		$this->total_price = $aConstructorItem['params']['results']['total_price'];
		$this->debug_info = $aConstructorItem['params']['results']['debug_info'];
		$this->id = $aConstructorItem['id'];

		ini_set('memory_limit', '1024M');
		$this->cDispatcher->SetTemplate('constructor_results_pdf');
		ob_start();
		$this->cDispatcher->RenderTemplate();
		$sHTML = ob_get_contents();
		ob_clean();

		require_once('_includes/_ext/mypdf.php');
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('mafplastic.ru website');
		$pdf->SetTitle('Канатная конструкция. Проект №'.$this->id);
		$pdf->setPrintHeader(false);
		$pdf->setFooterFont(array('arial', '', 6));
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->SetFont('arial', '', 10, '', true);
		$pdf->AddPage();
		$pdf->writeHTML($sHTML, true, false, true, false, '');
		$pdf->Output('constructor_'.$this->id.'.pdf', 'D');
		die();
	}

	function constructorsitemdeleteAction()
	{
		if (!CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/login/');
		}

		$aItem = CConstructors::getInstance()->GetItem($this->id);
		if (empty($aItem) || $aItem['user_id'] != CUsers::getInstance()->ID()) {
			return false;
		}

		CConstructors::getInstance()->Delete($this->id);

		$this->cDispatcher->Redirect('/user/constructors/');
	}

	function constructorsitemcopyAction()
	{
		if (!CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/login/');
		}

		$aItem = CConstructors::getInstance()->GetItem($this->id);
		if (empty($aItem) || $aItem['user_id'] != CUsers::getInstance()->ID()) {
			return false;
		}

		$_SESSION['constructor'] = $aItem['params'];
		$_SESSION['constructor']['id'] = $aItem['id'];

		$this->cDispatcher->Redirect('/constructor/');
	}

	function constructorsallAction()
	{
		if (!CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/login/');
		}
		if (!CUsers::getInstance()->GetInfo('all_constructors')) {
			$this->cDispatcher->Redirect('/user/');
		}

		$cSInfo = new CSInfo(array(
			'has_constructors' => true
		));
		$this->users = CUsers::getInstance()->GetList($cSInfo);

		$this->cDispatcher->SetTemplate('user_constructors_all');
	}

	function constructorsallsearchAction()
	{
		if (!CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/login/');
		}

		$cSInfo = new CSInfo(array(
			'perpage' => 12,
			'page' => (int)$_GET['page']
		));
		if (!CChecker::CheckString($_GET['date_from'], 65)) {
			$cSInfo->date_from = $_GET['date_from'];
		} else {
			$cSInfo->date_from = '';
		}
		if (!CChecker::CheckString($_GET['date_to'], 65)) {
			$cSInfo->date_to = $_GET['date_to'];
		} else {
			$cSInfo->date_to = '';
		}
		$cSInfo->id = (int)$_GET['number'];

		if ($cSInfo->date_from || $cSInfo->date_to || $cSInfo->id) {
			$this->constructors = CConstructors::getInstance()->GetList($cSInfo);
			$sAddParams = http_build_query(array(
				'date_from' => $_GET['date_from'],
				'date_to' => $_GET['date_to'],
				'number' => $_GET['number']
			));
			$this->pagination = $cSInfo->MakePagination($sAddParams.'&');
			$cSInfo = new CSInfo(array(
				'has_constructors' => true,
				'set_id_keys' => true
			));
			$this->users = CUsers::getInstance()->GetList($cSInfo);
		}

		/*$this->date_from = $cSInfo->date_from?date('d.m.Y', strtotime($cSInfo->date_from)):'';
		$this->date_to = $cSInfo->date_to?date('d.m.Y', strtotime($cSInfo->date_to)):'';
		$this->number = $cSInfo->id?$cSInfo->id:'';*/

		$this->cDispatcher->SetTemplate('user_constructors_all_search');
	}

	function constructorsalluserAction()
	{
		if (!CUsers::getInstance()->isLogged()) {
			$this->cDispatcher->Redirect('/login/');
		}
		if (!CUsers::getInstance()->GetInfo('all_constructors')) {
			$this->cDispatcher->Redirect('/user/');
		}

		$cSInfo = new CSInfo(array(
			'perpage' => 12,
			'page' => (int)$_GET['page']
		));
		if (!$this->id) {
			$cSInfo->no_user = true;
			$this->name = 'Незарегистрированные пользователи';
		} else {
			$aUser = CUsers::getInstance()->GetItem($this->id);
			if (empty($aUser)) {
				return false;
			}
			$cSInfo->user_id = $aUser['id'];
			$this->name = $aUser['last_name'].' '.$aUser['first_name'].' '.$aUser['middle_name'];
			if ($aUser['type'] == CUsers::TYPE_COMPANY) {
				$this->name = $aUser['company_name'].' ('.$this->name.')';
			}
		}

		$this->constructors = CConstructors::getInstance()->GetList($cSInfo);
		$this->pagination = $cSInfo->MakePagination();

		$this->cDispatcher->SetTemplate('user_constructors_all_user');
	}
}

?>
