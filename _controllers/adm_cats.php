<?

class adm_cats extends CAdm_controller
{

	function indexAction()
	{
		if (!empty($_POST['ids'])) {
			foreach ($_POST['ids'] as $aIDs) {
				$aIDs = array_filter(array_unique(array_map('intval', $aIDs)));
				if (!empty($aIDs)) foreach ($aIDs as $iNum => $iID) {
					$this->cContentClass->SetOrder($iID, $iNum + 1);
				}
			}
			$this->cDispatcher->Redirect($this->sRedirectUrl);
		}

		$cSInfo = new CSInfo(array(
			'hierarchy' => true
		));
		$this->cats = $this->cContentClass->GetList($cSInfo);

		$this->cDispatcher->SetTemplate('cats');
	}

	public function addAction()
	{
		if ($_POST) {
			$this->values = $_POST;
			$_POST['parent_id'] = 2;
			if (!$this->cContentClass->Add($_POST,$_FILES['image']['tmp_name'])) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		} else {
			$this->values = array(
				'enabled' => 1
			);
		}
		$this->title = 'Добавление';
		$this->button = 'Добавить';
		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function editAction()
	{
		if ($_POST) {
			$this->values = array_merge($this->values,$_POST);
			if (!$this->cContentClass->Update($this->id,$_POST,$_FILES['image']['tmp_name'])) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		}
		$this->title = 'Редактирование';
		$this->button = 'Сохранить';
		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function deleteAction()
	{
		if ($this->id == 1 || $this->id == 2) return false;
		
		$cSInfo = new CSInfo(array(
			'cat_id' => (int)$this->id
		));
		$aItems = CItems::getInstance()->GetList($cSInfo);
		if ($aItems) foreach ($aItems as $aItem) {
			$cSInfo = new CSInfo(array(
				'item_id' => $aItem['id']
			));
			$aItemGallery = CItemGallery::getInstance()->GetList($cSInfo);
			if ($aItemGallery) foreach ($aItemGallery as $aItemGallery) {
				CItemGallery::getInstance()->Delete($aItemGallery['id']);
			}
			CItems::getInstance()->Delete($aItem['id']);
		}
		$cSInfo = new CSInfo(array(
			'parent_id' => (int)$this->id
		));
		$aChildCats = CCats::getInstance()->GetList($cSInfo);
		if ($aChildCats) foreach ($aChildCats as $aItem) {
			CCats::getInstance()->Delete($aItem['id']);
		}
		parent::deleteAction();
	}

	public function switchstatusAction()
	{
		if ($this->id == 1 || $this->id == 2) return false;
		
		$this->cContentClass->SwitchStatus($this->id);
		$this->cDispatcher->Redirect($this->sRedirectUrl);
	}

	public function _init()
	{
		$this->cContentClass = CCats::getInstance();
		$this->sRedirectUrl = '/admin/cats/';
		$this->sEditTemplate = 'cats_form';
		$this->iRoleFlag = CAdmin::ROLE_MANAGECATS;

		/*$cSInfo = new CSInfo(array('only_top_level' => true));
		$this->topcats = CCats::getInstance()->GetList($cSInfo);*/

		return parent::_init();
	}

}

?>
