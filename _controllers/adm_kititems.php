<?

class adm_kititems extends CAdm_controller
{

	function indexAction()
	{
		if (!empty($_POST['ids'])) {
			$aIds = array_filter(array_unique(array_map('intval', $_POST['ids'])));
			if (!empty($aIds)) foreach ($aIds as $iNum => $iID) {
				$this->cContentClass->SetOrder($iID, $iNum + 1);
			}
			$this->cDispatcher->Redirect($this->sRedirectUrl);
		}

		$cSInfo = new CSInfo();
		$this->kititems = CKitItems::getInstance()->GetList($cSInfo);

		$this->cDispatcher->SetTemplate('kititems');
	}

	public function addAction()
	{
		if ($_POST) {
			$this->values = array_merge($this->values,$_POST);
			$_POST['cat_id'] = $this->catinfo['id'];
			$iNewID = $this->cContentClass->Add($_POST, $_FILES['image']['tmp_name']);
			if (!$iNewID) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		} else {
			if (!$this->id) {
				$this->values = array('enabled' => 1);
			}
		}
		$this->title = 'Добавление';
		$this->button = 'Добавить';

		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function editAction()
	{
		if ($_POST) {
			$this->values = array_merge($this->values, $_POST);
			if (!$this->cContentClass->Update($this->id, $_POST, $_FILES['image']['tmp_name'])) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		}
		$this->title = 'Редактирование';
		$this->button = 'Сохранить';

		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function switchstatusAction()
	{
		$this->cContentClass->SwitchStatus($this->id);
		$this->cDispatcher->Redirect($this->sRedirectUrl);
	}

	public function _init()
	{
		$this->cContentClass = CKitItems::getInstance();
		$this->sRedirectUrl = '/admin/kititems/';
		$this->sEditTemplate = 'kititems_form';
		$this->iRoleFlag = CAdmin::ROLE_MANAGEITEMS;
		return parent::_init();
	}

}

?>
