<?

class adm_itemcolors extends CAdm_controller
{

	function indexAction()
	{
		if (!empty($_POST['ids'])) {
			$aIds = array_filter(array_unique(array_map('intval', $_POST['ids'])));
			if (!empty($aIds)) foreach ($aIds as $iNum => $iID) {
				$this->cContentClass->SetOrder($iID, $iNum + 1);
			}
			$this->cDispatcher->Redirect($this->sRedirectUrl);
		}

		$cSInfo = new CSInfo(array(
			'item_id' => $this->item_id
		));
		$this->list = $this->cContentClass->GetList($cSInfo);

		$this->cDispatcher->SetTemplate('itemcolors');
	}

	public function addAction()
	{
		if ($_POST) {
			$this->values = $_POST;
			$_POST['item_id'] = $this->item_id;
			if (!$this->cContentClass->Add($_POST, $_FILES['image']['tmp_name'])) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		} else {
			$this->values = array(
				'enabled' => 1
			);
		}
		$this->title = 'Добавление';
		$this->button = 'Добавить';

		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function editAction()
	{
		if ($_POST) {
			$this->values = array_merge($this->values, $_POST);
			if (!$this->cContentClass->Update($this->id,$_POST,$_FILES['image']['tmp_name'])) {
				$this->error = CChecker::GetLastError();
				$this->errors = CChecker::GetLastErrorFields();
			} else {
				$this->cDispatcher->Redirect($this->sRedirectUrl);
			}
		}
		$this->title = 'Редактирование';
		$this->button = 'Сохранить';

		$this->cDispatcher->SetTemplate($this->sEditTemplate);
	}

	public function switchstatusAction()
	{
		$this->cContentClass->SwitchStatus($this->id);
		$this->cDispatcher->Redirect($this->sRedirectUrl);
	}

	public function switchmainAction()
	{
		$this->cContentClass->SwitchMain($this->item_id, $this->id);
		$this->cDispatcher->Redirect($this->sRedirectUrl);
	}

	public function _init()
	{
		if (!$this->item_id) {
			return false;
		}
		$this->iteminfo = CItems::getInstance()->GetItem($this->item_id);
		if (empty($this->iteminfo['id'])) {
			return false;
		}

		$this->cContentClass = CItemColors::getInstance();
		$this->sRedirectUrl = '/admin/itemcolors/'.$this->item_id.'/';
		$this->sEditTemplate = 'itemcolors_form';
		$this->iRoleFlag = CAdmin::ROLE_MANAGEITEMS;
		return parent::_init();
	}

}

?>
