<?

class index extends CController
{

	function indexAction()
	{
		$this->pageinfo = CPages::getInstance()->GetItemByPath('/');

		$cSInfo = new CSInfo(array(
			'enabled_only' => true
		));
		$this->slider = CSlider::getInstance()->GetList($cSInfo);

		$cSInfo = new CSInfo(array(
			'enabled_only' => true,
			'parent_id' => 2
		));
		$this->cats = CCats::getInstance()->GetList($cSInfo);

		$cSInfo = new CSInfo(array(
			'cat_id' => 1,
			'enabled_only' => true,
			'on_main_only' => true
		));
		$this->items = CItems::getInstance()->GetList($cSInfo);

		$this->cDispatcher->SetTemplate('index');
	}

	function pageAction()
	{
		$sURI = $this->cDispatcher->GetURI();

		$this->pageinfo = CPages::getInstance()->GetItemByPath($sURI);
		if (empty($this->pageinfo['enabled'])) {
			return false;
		}

		$this->cDispatcher->SetTemplate('page');
	}

	function headerAction()
	{
		if ($this->cDispatcher->isXmlHttpRequest()) {
			$this->cDispatcher->bNoRender = true;
			return;
		}

		$this->setts = CSetts::getInstance();

		$sMeta = '<title>';
		if ($this->pageinfo['meta_title']) {
			$sMeta .= $this->pageinfo['meta_title'];
		} else {
			$sMeta .= $this->setts->site_title;
			if ($this->pageinfo['title']) $sMeta .= ' - '.$this->pageinfo['title'];
		}
		$sMeta .= '</title>'."\r\n".'<meta name="keywords" content="';
		if ($this->pageinfo['meta_keywords']) $sMeta .= escape($this->pageinfo['meta_keywords']);
		$sMeta .= '" />'."\r\n".'<meta name="keywords" content="';
		if ($this->pageinfo['meta_description']) $sMeta .= escape($this->pageinfo['meta_description']);
		$sMeta .= '" />'."\r\n";

		$this->meta = $sMeta;

		$cSInfo = new CSInfo(array(
			'enabled_only' => true,
			'parent_id' => 2
		));
		$this->cats = CCats::getInstance()->GetList($cSInfo);

		$cSInfo = new CSInfo(array(
			'enabled_only' => true
		));
		$this->topmenu = CTopMenu::getInstance()->GetList($cSInfo);

		$this->cDispatcher->SetTemplate('_header');
	}

	function footerAction()
	{
		if ($this->cDispatcher->isXmlHttpRequest()) {
			$this->cDispatcher->bNoRender = true;
			return;
		}

		$this->cDispatcher->SetTemplate('_footer');
	}

	function ping()
	{
		echo 'pong';
		$this->cDispatcher->bNoRender = true;
	}
}

?>
