<?

class catalog extends CController
{

	function categoryAction()
	{
		$this->catinfo = CCats::getInstance()->GetItem($this->cat_id);
		if (!$this->catinfo['id'] || !$this->catinfo['enabled']) return false;

		$this->pageinfo = array(
			'title' => $this->catinfo['page_title']?$this->catinfo['page_title']:$this->catinfo['title'],
			'description' => $this->catinfo['page_description'],
			'keywords' => $this->catinfo['page_keywords']
		);

		$cSInfo = new CSInfo(array(
			'parent_id' => $this->catinfo['id']
		));
		$this->cats = CCats::getInstance()->GetList($cSInfo);
		if ($this->cats) {
			$this->cDispatcher->SetTemplate('top_category');
		} else {
			if ($this->catinfo['parent_id']) {
				$this->parentcatinfo = CCats::getInstance()->GetItem($this->catinfo['parent_id']);
			}

			$cSInfo = new CSInfo(array(
				'cat_id' => $this->catinfo['id'],
				'enabled_only' => true
			));
			$this->items = CItems::getInstance()->GetList($cSInfo);

			if ($this->items) foreach ($this->items as &$aItem) {
				$aItem['price'] = 0;
				if ($aItem['prices']) foreach ($aItem['prices'] as $aPrice) {
					if (!$aItem['price'] || $aItem['price'] < $aPrice['price']) {
						$aItem['price'] = $aPrice['price'];
					}
				}
			}
			unset($aItem);

			$this->basket = $_SESSION['basket'];

			$this->cDispatcher->SetTemplate('category');
		}
	}

	function itemAction()
	{
		$this->item = CItems::getInstance()->GetItem($this->item_id);
		if (!$this->item['id'] || !$this->item['enabled'] || $this->item['cat_id'] != $this->cat_id) {
			return false;
		}

		$this->catinfo = CCats::getInstance()->GetItem($this->item['cat_id']);
		if (!$this->catinfo['id'] || !$this->catinfo['enabled']) {
			return false;
		}

		$this->item['price'] = 0;
		if ($this->item['prices']) foreach ($this->item['prices'] as $aPrice) {
			if (!$this->item['price'] || $this->item['price'] < $aPrice['price']) {
				$this->item['price'] = $aPrice['price'];
			}
		}

		$this->pageinfo = array(
			'title' => $this->item['page_title']?$this->item['page_title']:$this->item['title'],
			'description' => $this->item['page_description'],
			'keywords' => $this->item['page_keywords']
		);

		if ($this->catinfo['parent_id']) {
			$this->parentcatinfo = CCats::getInstance()->GetItem($this->catinfo['parent_id']);
		}

		$cSInfo = new CSInfo(array(
			'item_id' => $this->item['id'],
			'enabled_only' => true
		));
		$this->gallery = CItemGallery::getInstance()->GetList($cSInfo);

		$cSInfo = new CSInfo(array(
			'cat_id' => $this->item['cat_id'],
			'except_ids' => array($this->item['id']),
			'enabled_only' => true
		));
		if ($this->item['sameitems']) {
			$cSInfo->except_ids = array_merge($cSInfo->except_ids, $this->item['sameitems']);
		}
		$this->sameitems = CItems::getInstance()->GetList($cSInfo);

		/*if ($this->item['componentitems']) {
			$cSInfo = new CSInfo(array(
				'ids' => array_keys($this->item['componentitems']),
				'except_id' => $this->item['id'],
				'enabled_only' => true
			));
			$this->componentitems = CItems::getInstance()->GetList($cSInfo);
			if ($this->componentitems) foreach ($this->componentitems as &$aItem) {
				$aItem['quant'] = $this->item['componentitems'][$aItem['id']]['quant'];
			}
			unset($aItem);
		}*/

		$aInComponents = CItems::getInstance()->GetInComponents($this->item_id);
		if ($aInComponents) {
			$cSInfo = new CSInfo(array(
				'ids' => $aInComponents,
				'except_id' => $this->item['id'],
				'enabled_only' => true
			));
			$this->incomponentitems = CItems::getInstance()->GetList($cSInfo);
		}

		$this->cDispatcher->SetTemplate('item');
	}

}

?>
