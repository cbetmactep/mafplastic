<?
return array (
	'default' => array('controller' => 'index', 'action' => 'page'),
	'/catalog/([0-9]+)/' => array('controller' => 'catalog', 'action' => 'category', 'params' => array('cat_id' => 1), 'rules' => array(
		'([0-9]+)/' => array('controller' => 'catalog', 'action' => 'item', 'params' => array('item_id' => 1))
	)),
	'/basket/' => array('controller' => 'basket', 'action' => 'index', 'rules' => array(
		'add/([0-9]+)/' => array('controller' => 'basket', 'action' => 'add', 'params' => array('item_id' => 1)),
		'delete/([0-9]+)/' => array('controller' => 'basket', 'action' => 'delete', 'params' => array('item_id' => 1)),
		'order/' => array('controller' => 'basket', 'action' => 'order'),
	)),
	'/gallery/' => array('controller' => 'gallery', 'action' => 'index'),
	'/constructor/' => array('controller' => 'constructor', 'action' => 'index', 'rules' => array(
		'reset/' => array('controller' => 'constructor', 'action' => 'reset'),
		'data/' => array('controller' => 'constructor', 'action' => 'data'),
		'item/([0-9]+)/' => array('controller' => 'constructor', 'action' => 'item', 'params' => array('id' => 1)),
		'frame/' => array('controller' => 'constructor', 'action' => 'frame'),
		'cell/' => array('controller' => 'constructor', 'action' => 'cell'),
		'add_vertical_rope/' => array('controller' => 'constructor', 'action' => 'addVerticalRope'),
		'remove_vertical_rope/' => array('controller' => 'constructor', 'action' => 'removeVerticalRope'),
		'add_horizontal_rope/' => array('controller' => 'constructor', 'action' => 'addHorizontalRope'),
		'remove_horizontal_rope/' => array('controller' => 'constructor', 'action' => 'removeHorizontalRope'),
		'ropes/' => array('controller' => 'constructor', 'action' => 'ropes'),
		'rope/' => array('controller' => 'constructor', 'action' => 'rope'),
		'junctions/' => array('controller' => 'constructor', 'action' => 'junctions'),
		'junction/' => array('controller' => 'constructor', 'action' => 'junction'),
		'connections/' => array('controller' => 'constructor', 'action' => 'connections'),
		'connection/' => array('controller' => 'constructor', 'action' => 'connection'),
		'subtypes/' => array('controller' => 'constructor', 'action' => 'subtypes'),
		'print/' => array('controller' => 'constructor', 'action' => 'print'),
		'pdf/' => array('controller' => 'constructor', 'action' => 'pdf'),
		'update/' => array('controller' => 'constructor', 'action' => 'update'),
		'createimage/' => array('controller' => 'constructor', 'action' => 'createimage'),
		'save/' => array('controller' => 'constructor', 'action' => 'save'),
		'addbasket/' => array('controller' => 'constructor', 'action' => 'addbasket'),
		'deletebasket/([0-9]+)/' => array('controller' => 'constructor', 'action' => 'deletebasket', 'params' => array('item_id' => 1))
	)),
	'/user/' => array('controller' => 'users', 'action' => 'index', 'rules' => array(
		'registration/' => array('controller' => 'users', 'action' => 'registration', 'rules' => array(
			'success/' => array('controller' => 'users', 'action' => 'registrationsuccess')
		)),
		'login/' => array('controller' => 'users', 'action' => 'login'),
		'logout/' => array('controller' => 'users', 'action' => 'logout'),
		'recovery/' => array('controller' => 'users', 'action' => 'recovery'),
		'index/' => array('controller' => 'users', 'action' => 'index'),
		'profile/' => array('controller' => 'users', 'action' => 'profile'),
		'orders/' => array('controller' => 'users', 'action' => 'orders', 'rules' => array(
//			'([0-9]+)/' => array('controller' => 'users', 'action' => 'ordersitem', 'params' => array('id' => 1))
		)),
		'constructors/' => array('controller' => 'users', 'action' => 'constructors', 'rules' => array(
			'([0-9]+)/' => array('controller' => 'users', 'action' => 'constructorsitem', 'params' => array('id' => 1), 'rules' => array(
				'print/' => array('controller' => 'users', 'action' => 'constructorsitemprint'),
				'pdf/' => array('controller' => 'users', 'action' => 'constructorsitempdf'),
				'delete/' => array('controller' => 'users', 'action' => 'constructorsitemdelete'),
				'copy/' => array('controller' => 'users', 'action' => 'constructorsitemcopy')
			)),
			'all/' => array('controller' => 'users', 'action' => 'constructorsall', 'rules' => array(
				'search/' => array('controller' => 'users', 'action' => 'constructorsallsearch'),
				'([0-9]+)/' => array('controller' => 'users', 'action' => 'constructorsalluser', 'params' => array('id' => 1))
			))
		))
	)),
	'/ping/' => array('controller' => 'index', 'action' => 'ping'),
	'/' => array('controller' => 'index', 'action' => 'index')
)
?>
