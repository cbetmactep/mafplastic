<?=$this->SetController('index')->SetAction('header')->Render();?>

<?if ($this->slider):?>
		<section class="inner">
			<div class="slider_place">
				<div class="slider">
					<ul>
<?foreach ($this->slider as $aItem): ?>
						<li>
							<div class="photo"><img src="<?=CSlider::GetLink($aItem['id'])?>" alt="" /></div>
							<div class="text_place">
								<div class="name"><?=$aItem['title']?></div>
								<div class="text"><?=nl2br($aItem['descr'])?></div>
<?if($aItem['url']):?>
								<div class="btn_white inline more"><a href="<?=$aItem['url']?>">Узнать больше ...</a></div>
<?endif?>
							</div>
						</li>
<?endforeach?>
					</ul>
				</div>
				<div class="navig">
<?foreach ($this->slider as $iNum => $aItem):?>
					<a data-slide-index="<?=$iNum?>" href="#"<?if (!$iNum):?> class="active"<?endif?>>&nbsp;</a>
<?endforeach?>
				</div>
				<a href="#" class="prev">Назад</a>
				<a href="#" class="next">Вперёд</a>
			</div>
		</section>
<?php endif; ?>

		<section class="bg_white bord_top_blue">
			<div class="inner">
				<div class="title">Комплектующие</div>
				<div class="items_place">
<?if ($this->cats):?>
<?foreach ($this->cats as $aItem):?>
					<div class="item">
						<div class="photo"><a href="/catalog/<?=$aItem['id']?>/"><img src="<?=CCats::GetLink($aItem['id'])?>" alt="" /></a></div>
						<div class="name_bold"><a href="/catalog/<?=$aItem['id']?>/"><?=$aItem['title']?></a></div>
					</div>
<?endforeach?>
<?for ($c = 0; $c < (4 - count($this->count)%4); $c++):?>
					<div class="item"></div>
<?endfor?>
<?endif?>
					<div class="bot"></div>
				</div>
				<div class="title">Готовые решения</div>
				<div class="items_place">
<?if ($this->items):?>
<?foreach ($this->items as $aItem):?>
					<div class="item">
						<div class="photo"><a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><img src="<?=CItemGallery::GetLink($aItem['image_id'], 3)?>" alt="" /></a></div>
						<div class="name"><a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><?=$aItem['title']?></a></div>
						<div class="btn_darkblue in_cart"><a class="add_basket" href="/basket/add/<?=$aItem['id']?>/"><?if (inBasket($aItem['id'])):?>В корзине<?else:?>Купить<?endif?></a></div>
						<div class="price"><?=$aItem['price']?> <span>руб.</span></div>
					</div>
<?endforeach?>
<?for ($c = 0; $c < (4 - count($this->items)%4); $c++):?>
					<div class="item"></div>
<?endfor?>
<?endif?>
					<div class="bot"></div><!-- нужен для растягивания блоков по всей ширине -->
				</div>
			</div>
		</section>

<?=$this->SetController('index')->SetAction('footer')->Render();?>
