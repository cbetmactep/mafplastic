<?=$this->SetController('index')->SetAction('header')->Render()?>

<div class="popup_place">
	<div class="bg"></div>
	<div class="popup">
		<div class="close"><a href="/"></a></div>
		<h1>Заказ №<?=$this->item['id']?></h1>
		<p class="large">Дата заказа: <?=format_date($this->item['date'])?></p>
		<div class="table_place">
			<table>
				<thead>
					<tr>
						<th>Фото</th>
						<th class="name">Название</th>
						<th>Количество</th>
						<th>Цена</th>
					</tr>
				</thead>
				<tbody>
<?if ($this->item['params']['items']) foreach ($this->item['params']['items'] as $aItem):?>
					<tr>
						<td class="img">
							<a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><img src="<?=CItemGallery::GetLink($aItem['image_id'], 2)?>" alt="" /></a>
						</td>
						<td class="name">
							<a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><?=$aItem['title']?></a>
							<?if (!empty($aItem['articul'])):?>
							<br/><br/>[ Артикул <?=$aItem['articul']?> ]
							<?endif?>
						</td>
						<td class="amount">
							<?=$aItem['count']?>
						</td>
						<td class="price"><?=$aItem['price']?> (руб. / шт)</td>
					</tr>
<?endforeach?>
<?if ($this->item['params']['constructors']) foreach ($this->item['params']['constructors'] as $aItem):?>
					<tr>
						<td class="img">
							<img width="98" src="<?=CConstructors::GetLink($aItem['id'], 3)?>" alt="" />
						</td>
						<td class="name">
							Канатная конструкция. Проект №<?=$aItem['id']?>
						</td>
						<td class="amount">
							1
						</td>
						<td class="price"><?=$aItem['price']?> (руб. / шт)</td>
					</tr>
<?endforeach?>

				</tbody>
				<tfoot>
					<tr>
						<td colspan="4" class="price">
							<span class="amount">Итого:</span>
							<span id="basket_form_total_price"><?=$this->item['total_price']?></span> руб.</td>
						<td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>

<?=$this->SetController('index')->SetAction('footer')->Render()?>