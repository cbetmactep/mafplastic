<?=$this->SetController('index')->SetAction('header')->Render();?>

		<section class="inner">
			<div class="breadcrumbs">
				<a href="/">На главную</a>
				<span>&raquo;</span><?=$this->pageinfo['title']?>
			</div>
			<article>
				<div class="constructor_panel">
					<h1><?=$this->pageinfo['title']?></h1>
					<?=$this->pageinfo['body']?>
				</div>

				<div class="constructor_params">
					Габариты сетки (в мм)<br>
					<span class="size">ширина<br><input class="input" type="text" name="width" value="<?=$this->width?>"></span>
					<span class="size bold"><br>x</span>
					<span class="size">высота<br><input class="input" type="text" name="height" value="<?=$this->height?>"></span>
					<br><br>
					Габариты ячейки (в мм)<br>
					<span class="size">ширина<br><input class="input" type="text" name="cell_width" value="<?=$this->cellWidth?>"></span>
					<span class="size bold"><br>x</span>
					<span class="size">высота<br><input class="input" type="text" name="cell_height" value="<?=$this->cellHeight?>"></span>
					<br><br>
					<div class="btn_darkblue">
					<a href="/constructor/add_vertical_rope/" id="add_vertical_rope">Добавить верт. стропу</a>
					</div>
					<div class="btn_darkblue">
					<a href="/constructor/remove_vertical_rope/" id="remove_vertical_rope">Убрать верт. стропу</a>
					</div>
					<input type="checkbox" id="keep_vertical_cell_size" checked> сохранить размеры ячеек
					<br><br>
					<div class="btn_darkblue">
					<a href="/constructor/add_horizontal_rope/" id="add_horizontal_rope">Добавить горизонт. стропу</a>
					</div>
					<div class="btn_darkblue">
					<a href="/constructor/remove_horizontal_rope/" id="remove_horizontal_rope">Убрать горизонт. стропу</a>
					</div>
					<input type="checkbox" id="keep_horizontal_cell_size" checked> сохранить размеры ячеек
					<br><br>
					<div class="btn_darkblue">
					<a href="/constructor/reset/">Сбросить сетку</a>
					</div>
				</div>
				<div class="constructor_container">
					<div class="zoom_control">
						<a href="#" class="zoom_out">-</a>
						<a href="#" class="zoom_in">+</a>
					</div>
					<div id="constructor"></div>
				</div>

			</article>
		</section>

<script>
var constructor = new Constructor();
constructor.init();
</script>

<?=$this->SetController('index')->SetAction('footer')->Render();?>