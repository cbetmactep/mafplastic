<?=$this->SetController('index')->SetAction('header')->Render();?>

<div class="popup_place">
	<div class="bg"></div>
	<div class="popup add_basket">
		<div class="close"><a href="/"></a></div>
		<h1>Добавление в корзину</h1>
		<form method="post" action="/basket/add/<?=$this->item['id']?>/" id="add_basket_form">
		<div class="table_place">
			<table>
				<thead>
					<tr>
						<th>Фото</th>
						<th class="name">Название</th>
						<th>Количество</th>
						<th>Цена</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="img">
							<a href="/catalog/<?=$this->item['cat_id']?>/<?=$this->item['id']?>/"><img src="<?=CItemGallery::GetLink($this->item['image_id'], 2)?>" alt="" /></a>
						</td>
						<td class="name">
							<a href="/catalog/<?=$this->item['cat_id']?>/<?=$this->item['id']?>/"><?=$this->item['title']?></a>
							<?if (!empty($this->item['articul'])):?>
							<br/><br/>[ Артикул <?=$this->item['articul']?> ]
							<?endif?>
						</td>
						<td class="amount">
							<input type="text" name="quant" value="<?=$this->quant?>" title="Количество"/>
						</td>
						<td class="price"><span id="add_basket_form_price"><?=$this->price?></span> (руб. / шт)</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4" class="price">
							<span class="amount">Итого:</span>
							<span id="add_basket_form_total_price"><?=$this->total?></span> руб.
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="form_place">
			<div class="btn t_center"><button type="submit">Добавить</button></div>
		</div>
		</form>
	</div>
</div>

<?=$this->SetController('index')->SetAction('footer')->Render();?>
