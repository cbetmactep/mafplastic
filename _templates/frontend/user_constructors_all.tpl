<?=$this->SetController('index')->SetAction('header')->Render()?>

		<section class="inner">
			<div class="breadcrumbs">
				<a href="/">На главную</a>
				<span>&raquo;</span>Все проекты
			</div>
			<h1>Все проекты</h1>
			<div class="user_orders">
				<form method="get" action="/user/constructors/all/search/" class="items_filter">
					Дата с
					<input type="text" name="date_from" value="<?=$this->date_from?>">
					по
					<input type="text" name="date_to" value="<?=$this->date_to?>">
					номер
					<input type="text" name="number" value="<?=$this->number?>">
					<input type="submit" value="Искать">
				</form>
					<div class="row">
						<a href="/user/constructors/all/0/">
							Незарегистрированные пользователи
						</a>
					</div>
<?if ($this->users):?>
<?foreach ($this->users as $aUser):?>
					<div class="row">
						<a href="/user/constructors/all/<?=$aUser['id']?>/">
<?if ($aUser['type'] == CUsers::TYPE_COMPANY):?>
							<?=$aUser['company_name']?> (<?=$aUser['last_name']?> <?=$aUser['first_name']?> <?=$aUser['middle_name']?>)
<?else:?>
							<?=$aUser['last_name']?> <?=$aUser['first_name']?> <?=$aUser['middle_name']?>
<?endif?>
						</a>
					</div>
<?endforeach?>
<?endif?>
			</div>
<?=$this->pagination?>
		</section>

<script>
$(document).ready(function(){
	$('input[name="date_from"]').mask('99.99.9999', {placeholder: '__.__.____'});
	$('input[name="date_to"]').mask('99.99.9999', {placeholder: '__.__.____'});
});
</script>

<?=$this->SetController('index')->SetAction('footer')->Render()?>