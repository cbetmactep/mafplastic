<div class="popup_place">
	<div class="bg"></div>
	<div class="popup constructor_items">
		<div class="close"><a href="#"></a></div>

		<div class="subtypes">
			<h3>Выберите тип креплений</h3>
			<a class="subtype_selector" href="#" data-id="1">
				<div></div>
				<span>При выборе данного типа крепления возможно изменение количества ячеек и строп, т.к. габариты крепления могут превышать длину свободных концов каната.</span>
				<img src="/static/images/constructor/subtype_1.jpg" alt="Болты и шпильки">
			</a>
			<a class="subtype_selector" href="#" data-id="3">
				<div></div>
				<span>При выборе данного типа крепления возможно изменение количества ячеек и строп, т.к. габариты крепления могут превышать длину свободных концов каната.</span>
				<img src="/static/images/constructor/subtype_3.jpg" alt="Коуши">
			</a>
			<a class="subtype_selector" href="#" data-id="5">
				<div></div>
				<span>При выборе данного типа крепления возможно изменение количества ячеек и строп, т.к. габариты крепления могут превышать длину свободных концов каната.</span>
				<img src="/static/images/constructor/subtype_5.jpg" alt="Коуши к раме">
			</a>
			<a class="subtype_selector" href="#" data-id="2">
				<div></div>
				<span>При выборе данного типа крепления возможно изменение количества ячеек и строп, т.к. габариты крепления могут превышать длину свободных концов каната.</span>
				<img src="/static/images/constructor/subtype_2.jpg" alt="Тэшки для рамы">
			</a>
<?if ($this->show_t_rope): ?>
			<a class="subtype_selector" href="#" data-id="4">
				<div></div>
				<span>При выборе данного типа крепления возможно изменение количества ячеек и строп, т.к. габариты крепления могут превышать длину свободных концов каната.</span>
				<img src="/static/images/constructor/subtype_4.jpg" alt="Тэшки для каната">
			</a>
<?endif?>
		</div>

	</div>
</div>
