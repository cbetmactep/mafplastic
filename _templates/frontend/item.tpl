<?=$this->SetController('index')->SetAction('header')->Render();?>


		<section class="inner">
			<div class="breadcrumbs">
				<a href="/">На главную</a>
<?if ($this->parentcatinfo):?>
				<span>&raquo;</span><a href="/catalog/<?=$this->parentcatinfo['id']?>/"><?=$this->parentcatinfo['title']?></a>
<?endif?>
				<span>&raquo;</span><a href="/catalog/<?=$this->catinfo['id']?>/"><?=$this->catinfo['title']?></a>
				<span>&raquo;</span><?=$this->item['title']?>
			</div>
			<div class="product_place">
				<div class="top_block">
					<div class="fll gallery_place">
<?if ($this->gallery || file_exists(CItems::GetLink($this->item['id'], 2, true))):?>
						<div class="gallery">
<?if ($this->gallery):?>
							<div class="photo_big">
<?foreach ($this->gallery as $aItem):?>
<?if ($aItem['video']):?>
								<a href="<?=$aItem['video']?>" data-group="gallery" class="jackbox"><img<?if ($aItem['main']):?> class="active"<?endif?> src="<?=CItemGallery::GetLink($aItem['id'], 1)?>" alt="<?=prepareForJS($aItem['title'])?>" /></a>
<?else:?>
								<a href="<?=CItemGallery::GetLink($aItem['id'], 4)?>" data-group="gallery" class="jackbox"><img<?if ($aItem['main']):?> class="active"<?endif?> src="<?=CItemGallery::GetLink($aItem['id'], 1)?>" alt="<?=prepareForJS($aItem['title'])?>" /></a>
<?endif?>
<?endforeach?>
							</div>
<?endif?>
<?if (file_exists(CItems::GetLink($this->item['id'], 2, true))):?>
							<div class="img bord_blue_3"><a href="<?=CItems::GetLink($this->item['id'], 1)?>" data-group="gallery" class="jackbox"><img src="<?=CItems::GetLink($this->item['id'], 2)?>" alt="Схема" /></a></div>
<?endif?>
<?if ($this->gallery):?>
							<ul class="thumbs">
<?foreach ($this->gallery as $aItem):?>
								<li<?if ($aItem['main']):?> class="active"<?endif?>><a href="#"><img src="<?=CItemGallery::GetLink($aItem['id'], 2)?>" alt="" /></a></li>
<?endforeach?>
							</ul>
<?endif?>
						</div>
<?endif?>
					</div>
					<div class="text_place">
						<h1><?=$this->item['title']?></h1>
<?if ($this->item['in_stock']):?>
								<span class="in_stock"><img src="/static/images/ico_tick.png" alt="" />В наличии</span>
<?endif?>
						<div class="cols_2 mid">
							<div class="col article">
<?if ($this->item['articul']):?>
							[ Артикул <?=$this->item['articul']?> ]
<?endif?>
							</div>
							<div class="col t_red">
<?if ($this->componentitems):?>
								*Цена данного продукта формируется из цены комплектующих перечисленных в таблице, а также работ.
<?endif?>
							</div>
						</div>
						<div class="text">
							<?=$this->item['descr']?>
						</div>
						<div class="price_block">
							<div class="price"><strong>от <?=$this->item['price']?> руб.</strong></div>
							<div class="btn_darkblue inline in_cart"><a class="add_basket" href="/basket/add/<?=$this->item['id']?>/"><?if (inBasket($this->item['id'])):?>В корзине<?else:?>Купить<?endif?></a></div>
						</div>
<?if ($this->componentitems):?>
						<h2> Изделие состоит из комплектующих</h2>
						<div class="table_place_simple" id="components_table">
							<table>
								<thead>
									<tr>
										<th class="nr">№</th>
										<th class="name">Наименование</th>
										<th class="amount">Штук</th>
									</tr>
								</thead>
								<tbody>
<?foreach ($this->componentitems as $iNum => $aItem):?>
									<tr>
										<td class="nr"><?=$iNum + 1?></td>
										<td class="name"><a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><?=$aItem['title']?><img src="<?=CItemGallery::GetLink($aItem['image_id'], 2)?>" alt="" /></a></td>
										<td class="amount"><?=$aItem['quant']?></td>
									</tr>
<?endforeach?>
								</tbody>
							</table>
						</div>
<?endif?>
					</div>
				</div>
<?if ($this->sameitems || $this->incomponentitems):?>
				<div class="bord_top_blue mid_block">
<?if ($this->incomponentitems):?>
					<div class="title mid">Изделие используется</div>
					<div class="items_place">
<?foreach ($this->incomponentitems as $aItem):?>
						<div class="item">
							<div class="photo"><a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><img src="<?=CItemGallery::GetLink($aItem['image_id'], 3)?>" alt="" /></a></div>
							<div class="name"><a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><?=$aItem['title']?></a></div>
							<div class="btn_darkblue in_cart"><a class="add_basket" href="/basket/add/<?=$aItem['id']?>/"><?if (inBasket($aItem['id'])):?>В корзине<?else:?>Купить<?endif?></a></div>
							<div class="price"><?=$aItem['price']?> <span>руб.</span></div>
						</div>
<?endforeach?>
<?for ($c = 0; $c < (4 - count($this->incomponentitems)%4); $c++):?>
						<div class="item"></div>
<?endfor?>
						<div class="bot"></div><!-- нужен для растягивания блоков по всей ширине -->
					</div>
<?endif?>
<?if ($this->sameitems):?>
					<div class="title mid">Похожие изделия</div>
					<div class="items_place">
<?foreach ($this->sameitems as $aItem):?>
						<div class="item">
							<div class="photo"><a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><img src="<?=CItemGallery::GetLink($aItem['image_id'], 3)?>" alt="" /></a></div>
							<div class="name"><a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><?=$aItem['title']?></a></div>
							<div class="btn_darkblue in_cart"><a class="add_basket" href="/basket/add/<?=$aItem['id']?>/"><?if (inBasket($aItem['id'])):?>В корзине<?else:?>Купить<?endif?></a></div>
							<div class="price"><?=$aItem['price']?> <span>руб.</span></div>
						</div>
<?endforeach?>
<?for ($c = 0; $c < (4 - count($this->sameitems)%4); $c++):?>
						<div class="item"></div>
<?endfor?>
						<div class="bot"></div><!-- нужен для растягивания блоков по всей ширине -->
					</div>
<?endif?>
				</div>
<?endif?>
			</div>
		</section>

<?=$this->SetController('index')->SetAction('footer')->Render();?>
