<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="/static/css/style.css?v=201609081330" />
<script type="text/javascript" src="/static/js/jquery.js"></script>
<script type="text/javascript" src="/static/js/jackbox.min.js"></script>
<script type="text/javascript" src="/static/js/scripts.js?v=201609081330"></script>
<style>
body {
	background: #fff;
}
</style>
</head>
<body>

<div style="padding: 80px">
<div id="constructor"></div>
</div>

<script>
var constructor = new Constructor();
constructor.init(<?=$this->data?>);
</script>

</body>
</html>
