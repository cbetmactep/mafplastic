<?=$this->SetController('index')->SetAction('header')->Render()?>

<div class="popup_place">
	<div class="bg"></div>
	<div class="popup narrow">
		<div class="close"><a href="/"></a></div>
		<h1>Вход</h1>
		<div class="form_place">
			<?if ($this->error):?>
			<p class="error"><?=$this->error?></p>
			<?endif?>
			<form method="post" action="/user/login/">
				<p>
					<label>e-mail:</label>
					<input type="text" name="email" value="<?=$this->values['email']?>" class="txt" />
				</p>
				<p>
					<label>пароль:</label>
					<input type="password" name="password" value="" class="txt" />
				</p>
				<div class="check">
					<input type="checkbox" name="remember" value="1">
					<label>запомнить вход</label>
				</div>
				<div class="btn t_center">
					<button type="submit">Вход</button>
				</div>
			</form>
			<br>
			<div class="fll"><a href="/user/registration/" class="openpopup">Регистрация</a></div>
			<div class="flr"><a href="/user/recovery/" class="openpopup">Забыли пароль?</a></div>
		</div>
	</div>
</div>

<?=$this->SetController('index')->SetAction('footer')->Render()?>