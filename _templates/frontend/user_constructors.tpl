<?=$this->SetController('index')->SetAction('header')->Render()?>

		<section class="inner">
			<div class="breadcrumbs">
				<a href="/">На главную</a>
				<span>&raquo;</span><a href="/user/"><?=$this->name?></a>
				<span>&raquo;</span>Проекты
			</div>
			<h1>Проекты</h1>
			<div class="items_place">
<?if ($this->constructors):?>
<?foreach ($this->constructors as $aItem):?>
					<div class="item constructor">
						<div class="photo">
							<strong>Проект №<?=$aItem['id']?> от <?=format_date($aItem['date'])?></strong>
							<div class="sizes">Габариты сетки:<br> <?=$aItem['params']['width']?>x<?=$aItem['params']['height']?>мм</div>
							<strong>Стоимость: <span class="flr"><?=$aItem['price']?> руб.</span></strong>
						</div>
						<div class="btn_darkblue in_cart">
							<a href="/user/constructors/<?=$aItem['id']?>/copy/">Копировать</a>
						</div>
						<div class="btn_darkblue in_cart">
							<a href="/user/constructors/<?=$aItem['id']?>/">Посмотреть</a>
						</div>
						<div class="btn_darkblue in_cart">
							<a href="/user/constructors/<?=$aItem['id']?>/delete/">Удалить</a>
						</div>
					</div>
<?endforeach?>
<?for ($c = 0; $c < (4 - count($this->constructors)%4); $c++):?>
					<div class="item"></div>
<?endfor?>
<?endif?>
				<div class="bot"></div><!-- нужен для растягивания блоков по всей ширине -->
			</div>
<?=$this->pagination?>
		</section>

<?=$this->SetController('index')->SetAction('footer')->Render()?>