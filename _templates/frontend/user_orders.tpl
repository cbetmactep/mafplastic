<?=$this->SetController('index')->SetAction('header')->Render()?>

		<section class="inner">
			<div class="breadcrumbs">
				<a href="/">На главную</a>
				<span>&raquo;</span><a href="/user/"><?=$this->name?></a>
				<span>&raquo;</span>Заказы
			</div>
			<h1>Заказы</h1>
			<div class="user_orders">
<?if ($this->orders):?>
<?foreach ($this->orders as $aOrder):?>
					<div class="row">
						<a href="#" class="item">
							Заказ №<?=$aOrder['id']?> от <?=format_HR_date($aOrder['date'])?> на сумму <?=number_format($aOrder['total_price'], 2, ',', ' ')?> руб.
						</a>
						<div class="table_place">
							<table>
								<thead>
									<tr>
										<th>Фото</th>
										<th class="name">Название</th>
										<th>Количество</th>
										<th>Цена</th>
									</tr>
								</thead>
								<tbody>
<?if ($aOrder['params']['items']) foreach ($aOrder['params']['items'] as $aItem):?>
									<tr>
										<td class="img">
											<a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><img src="<?=CItemGallery::GetLink($aItem['image_id'], 2)?>" alt="" /></a>
										</td>
										<td class="name">
											<a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><?=$aItem['title']?></a>
											<?if (!empty($aItem['articul'])):?>
											<br/><br/>[ Артикул <?=$aItem['articul']?> ]
											<?endif?>
										</td>
										<td class="amount">
											<?=$aItem['count']?>
										</td>
										<td class="price"><?=$aItem['price']?> (руб. / шт)</td>
									</tr>
<?endforeach?>
<?if ($aOrder['params']['constructors']) foreach ($aOrder['params']['constructors'] as $aItem):?>
									<tr>
										<td class="img">
											<img width="98" src="<?=CConstructors::GetLink($aItem['id'], 3)?>" alt="" />
										</td>
										<td class="name">
											Канатная конструкция. Проект №<?=$aItem['id']?>
										</td>
										<td class="amount">
											1
										</td>
										<td class="price"><?=$aItem['price']?> (руб. / шт)</td>
									</tr>
<?endforeach?>

								</tbody>
							</table>
						</div>
					</div>
<?endforeach?>
<?endif?>
			</div>
<?=$this->pagination?>
		</section>

<?=$this->SetController('index')->SetAction('footer')->Render()?>