		<div class="apply">
			<h4>Применить:</h4>
			<input id="fill_type_1" type="radio" name="fill_type" value="1" />
			<label for="fill_type_1">Только этот элемент</label><br/>
			<input id="fill_type_2" type="radio" name="fill_type" value="2" />
			<label for="fill_type_2">Все элементы по горизонтали</label><br/>
			<input id="fill_type_3" type="radio" name="fill_type" value="3" />
			<label for="fill_type_3">Все элементы по вертикали</label><br/>
			<input id="fill_type_4" type="radio" name="fill_type" value="4" checked />
			<label for="fill_type_4">Все элементы</label>
		</div>
		<div class="information">
			<h4>Описание:</h4>
			<div class="description">
				<?=$this->item['descr']?>
			</div>
			<div class="images">
<?if ($this->colors) foreach ($this->colors as $aColor):?>
				<img src="<?=CItemColors::GetLink($aColor['id'], 1)?>" alt="" style="display: none" id="popup_constructor_items_color_<?=$aColor['id']?>" />
<?endforeach?>
			</div>
			<div class="prices">
				<h4>Цена за шт.:</h4>
<?if ($this->prices) foreach ($this->prices as $iKey => $aPrice):?>
<?if ($iKey > 2) continue;?>
<?if ($aPrice['from'] && $aPrice['to']):?>
	От <?=$aPrice['from']?> до <?=$aPrice['to']?>
<?elseif ($aPrice['from']):?>
	От <?=$aPrice['from']?>
<?elseif ($aPrice['to']):?>
	До <?=$aPrice['to']?>
<?endif?>
	&mdash;
	<?=$aPrice['price']?> руб.<br/>
<?endforeach?>
			</div>
			<div class="selector">
				<h4>Цвет:</h4>
				<select name="color">
<?if ($this->colors) foreach ($this->colors as $aColor):?>
					<option value="<?=$aColor['id']?>"><?=CItemColors::$aColors[$aColor['color']]?></option>
<?endforeach?>
				</select>
			</div>
		</div>
		<div class="clr"></div>
		<input type="hidden" name="id" value="<?=$this->item['id']?>"/>
