<?=$this->SetController('index')->SetAction('header')->Render();?>

		<section class="inner">
			<div class="breadcrumbs">
				<a href="/">На главную</a>
				<span>&raquo;</span><?=$this->pageinfo['title']?>

			</div>
			<h1><?=$this->pageinfo['title']?></h1>
			<article id="gallery">
<?if ($this->gallery) foreach ($this->gallery as $aItem):?>
<?if ($aItem['video']):?>
				<a href="<?=$aItem['video']?>" data-group="gallery"<?if ($aItem['items']):?> data-description="#gallery_<?=$aItem['id']?>"<?endif?> class="jackbox"><img class="gallery_item" src="<?=CGallery::GetLink($aItem['id'], 2)?>" width="<?=$aItem['width']?>" height="<?=$aItem['height']?>" alt="" /></a>
<?else:?>
				<a href="<?=CGallery::GetLink($aItem['id'], 1)?>" data-group="gallery"<?if ($aItem['items']):?> data-description="#gallery_<?=$aItem['id']?>"<?endif?> class="jackbox"><img class="gallery_item" src="<?=CGallery::GetLink($aItem['id'], 2)?>" width="<?=$aItem['width']?>" height="<?=$aItem['height']?>" alt="" /></a>
<?endif?>
<?endforeach?>
			</article>
		</section>

<?if ($this->gallery) foreach ($this->gallery as $aItem):?>
<?if ($aItem['items']):?>
		<div id="gallery_<?=$aItem['id']?>" class="jackbox_description">
			Состоит из:
<?foreach ($aItem['items'] as $iNum => $aItem):?>
			<?if ($iNum):?> | <?endif?><a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><?=$aItem['title']?></a>
<?endforeach?>
		</div>
<?endif?>
<?endforeach?>

<?=$this->SetController('index')->SetAction('footer')->Render();?>