<?=$this->SetController('index')->SetAction('header')->Render();?>

		<section class="inner">
			<div class="breadcrumbs">
				<a href="/">На главную</a>
<?if ($this->parentcatinfo):?>
				<span>&raquo;</span><a href="/catalog/<?=$this->parentcatinfo['id']?>/"><?=$this->parentcatinfo['title']?></a>
<?endif?>
				<span>&raquo;</span><?=$this->catinfo['title']?>
			</div>
			<h1><?=$this->catinfo['title']?></h1>
			<div class="items_place">
<?if ($this->items):?>
<?foreach ($this->items as $aItem):?>
					<div class="item">
						<div class="photo"><a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><img src="<?=CItemGallery::GetLink($aItem['image_id'], 3)?>" alt="" /></a></div>
						<div class="name"><a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><?=$aItem['title']?></a></div>
						<div class="btn_darkblue in_cart"><a class="add_basket" href="/basket/add/<?=$aItem['id']?>/"><?if (inBasket($aItem['id'])):?>В корзине<?else:?>Купить<?endif?></a></div>
						<div class="price"><span>от</span> <?=$aItem['price']?> <span>руб.</span></div>
					</div>
<?endforeach?>
<?for ($c = 0; $c < (4 - count($this->items)%4); $c++):?>
					<div class="item"></div>
<?endfor?>
<?endif?>
				<div class="bot"></div><!-- нужен для растягивания блоков по всей ширине -->
			</div>
		</section>

<?=$this->SetController('index')->SetAction('footer')->Render();?>
