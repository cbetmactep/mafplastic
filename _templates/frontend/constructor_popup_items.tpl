<div class="popup_place">
	<div class="bg"></div>
	<div class="popup constructor_items">
		<div class="close">
			<a href="/"></a>
<?if ($this->show_back_link):?>
			<a class="back" href="/"></a>
<?endif?>
		</div>

		<form id="popup_constructor_item"></form>

		<div class="constructor_items_container">
<?foreach ($this->items as $aItem):?>
			<a href="#" data-item-id="<?=$aItem['id']?>"><img src="<?=CConstructorItems::GetLink($aItem['id'], 1)?>"></a>
<?endforeach?>
		</div>
		<div class="clr"></div>
		<div class="btn_darkblue">
			<a href="#" id="popup_constructor_ok_button">Применить</a>
		</div>
	</div>
</div>
