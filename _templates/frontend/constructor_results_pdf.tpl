<style>
* {
        font-size: 25px;
        font-family: arial;
}

h1 {
	margin: 5px 0;
	font-size: 40px;
	color: #000;
	padding: 0px;
	font-weight: bold;
}

p {
	font-weight: bold;
	text-align: right;
	padding: 0px;
	margin: 0px;
}

table {
	border: none;
	border-collapse: collapse;
	padding: 0px;
	margin: 0px;
}

table th {
	font-weight: bold;
	background-color: #aaaaaa;
	text-align: center;
	border: 1px solid #000;
}

table td {
	border: 1px solid #000;
}

table .row1 {
	width: 100px;
}

table .row2 {
	width: 200px;
}

table .row3 {
	width: 60px;
}

table .row4 {
	width: 80px;
}

table .row5 {
	width: 80px;
}

table .row6 {
	width: 100px;
}

table td.row3 {
	text-align: center;
}

table td.row4 {
	text-align: center;
}

table td.row5 {
	text-align: center;
}

table td.row6 {
	text-align: center;
}

</style>

	<h1>Канатная конструкция. Проект №<?=$this->id?></h1>
<?
$filesize = getimagesize(CConstructors::GetLink($this->id, 2, true));
if ($filesize[1] > 850) {
	$style = 'style="height: 850px"';
} else {
	$style = '';
}
?>
	<img src="<?=CConstructors::GetLink($this->id, 2, true)?>" alt="" <?=$style?>>

<?if (CUsers::getInstance()->isLogged() && CUsers::getInstance()->isProduction()):?>
<div>
	<?=$this->debug_info?>
</div>
					<table nobr="true">
							<tr>
								<th class="row1">Артикул</th>
								<th class="row2">Описание</th>
								<th class="row3">Кол-во</th>
								<th class="row4">Цвет</th>
								<th class="row5">Цена за 1 шт.</th>
								<th class="row6">Общая стоимость</th>
							</tr>
<?if ($this->items_required) foreach ($this->items_required as $aItem):?>
							<tr>
								<td class="row1"><?=$aItem['articul']?></td>
								<td class="row2">
									<?=$aItem['title']?>
<?if ($aItem['descr']):?>
									<br><?=$aItem['descr']?>
<?endif?>
								</td>
								<td class="row3">
									<?=$aItem['quant']?> <?=$aItem['unit']?>
<?if ($aItem['remains'] < $aItem['quant']): ?>
									<br><strong>(<?=$aItem['remains']?> <?=$aItem['unit']?>)</strong>
<?endif?>
								</td>
								<td class="row4"><?=$aItem['color']?></td>
								<td class="row5" nowrap><?=$aItem['price']?> руб.</td>
								<td class="row6" nowrap><?=$aItem['total']?> руб.</td>
							</tr>
<?endforeach?>
<?if ($this->kit_items) foreach ($this->kit_items as $aItem):?>
							<tr>
								<td class="row1"><?=$aItem['articul']?></td>
								<td class="row2"><?=$aItem['title']?></td>
								<td class="row3" nowrap><?=$aItem['quant']?> шт.</td>
								<td class="row4"></td>
								<td class="row5" nowrap><?=$aItem['price']?> руб.</td>
								<td class="row6" nowrap><?=$aItem['total']?> руб.</td>
							</tr>
<?endforeach?>
<?if ($this->hardware_items) foreach ($this->hardware_items as $aItem):?>
							<tr>
								<td class="row1"><?=$aItem['articul']?></td>
								<td class="row2"><?=$aItem['title']?></td>
								<td class="row3" nowrap><?=$aItem['quant']?> шт.</td>
								<td class="row4"></td>
								<td class="row5" nowrap><?=$aItem['price']?> руб.</td>
								<td class="row6" nowrap><?=$aItem['total']?> руб.</td>
							</tr>
<?endforeach?>
					</table>
					<p>Стоимость изделий: <?=$this->total_required?> руб.</p>
<?endif?>
					<p>Стоимость сетки: <?=$this->build_price?> руб.</p>

<?if ($this->items_optional):?>
					<h1>Дополнительные изделия</h1>
					<table nobr="true">
							<tr>
								<th class="row1">Артикул</th>
								<th class="row2">Описание</th>
								<th class="row3">Кол-во</th>
								<th class="row4">Цвет</th>
								<th class="row5">Цена за 1 шт.</th>
								<th class="row6">Общая стоимость</th>
							</tr>
<?foreach ($this->items_optional as $sID => $aItem):?>
							<tr>
								<td class="row1"><?=$aItem['articul']?></td>
								<td class="row2"><?=$aItem['title']?></td>
								<td class="row3">
									<?=$aItem['quant']?> <?=$aItem['unit']?>
<?if ($aItem['remains'] < $aItem['quant']): ?>
									<br><strong>(<?=$aItem['remains']?> <?=$aItem['unit']?>)</strong>
<?endif?>
								</td>
								<td class="row4"><?=$aItem['color']?></td>
								<td class="row5" nowrap><?=$aItem['price']?> руб.</td>
								<td class="row6" nowrap><?=$aItem['total']?> руб.</td>
							</tr>
<?endforeach?>
					</table>
					<p>Стоимость дополнительных изделий: <?=$this->total_optional?> руб.</p>
<?endif?>
					<p>Итого: <?=$this->total_price?> руб.</p>
