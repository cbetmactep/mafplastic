<?=$this->SetController('index')->SetAction('header')->Render();?>

<div class="popup_place">
	<div class="bg"></div>
	<div class="popup">
		<div class="close"><a href="/"></a></div>
		<h1>Корзина</h1>
<?if ($this->items || $this->constructors):?>
		<p class="large">В Вашей корзине <span id="basket_form_total_quant"><?=$this->total_count?></span> <?=itemsCount($this->total_count)?> на сумму <span id="basket_form_total_sum"><?=$this->total_price?></span> руб.</p>
		<form method="post" action="/basket/" id="basket_form">
		<div class="table_place">
			<table>
				<thead>
					<tr>
						<th>Фото</th>
						<th class="name">Название</th>
						<th>Количество</th>
						<th>Цена</th>
						<th>Удалить</th>
					</tr>
				</thead>
				<tbody>
<?if ($this->items) foreach ($this->items as $aItem):?>
					<tr>
						<td class="img">
							<a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><img src="<?=CItemGallery::GetLink($aItem['image_id'], 2)?>" alt="" /></a>
						</td>
						<td class="name">
							<a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><?=$aItem['title']?></a>
							<?if (!empty($aItem['articul'])):?>
							<br/><br/>[ Артикул <?=$aItem['articul']?> ]
							<?endif?>
						</td>
						<td class="amount">
							<input type="text" name="count[<?=$aItem['id']?>]" value="<?=$aItem['count']?>" title="Количество"/>
						</td>
						<td class="price"><span id="basket_form_price_<?=$aItem['id']?>"><?=$aItem['price']?></span> (руб. / шт)</td>
						<td class="btn delete"><a href="/basket/delete/<?=$aItem['id']?>/">Удалить</a></td>
					</tr>
<?endforeach?>
<?if ($this->constructors) foreach ($this->constructors as $aItem):?>
					<tr>
						<td class="img">
							<img width="98" src="<?=CConstructors::GetLink($aItem['id'], 3)?>" alt="" />
						</td>
						<td class="name">
							Канатная конструкция. Проект №<?=$aItem['id']?>
						</td>
						<td class="amount">
							<input type="text" name="count[c<?=$aItem['id']?>]" value="<?=$aItem['count']?>" title="Количество"/>
						</td>
						<td class="price"><?=$aItem['price']?> (руб. / шт)</td>
						<td class="btn delete"><a href="/constructor/deletebasket/<?=$aItem['id']?>/">Удалить</a></td>
					</tr>
<?endforeach?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5" class="price">
							<span class="amount">Итого:</span>
							<span id="basket_form_total_price"><?=$this->total_price?></span> руб.</td>
						<td>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="table_place">
			<div class="btn t_center"><button type="submit" id="basket_form_update">Сохранить</button></div>
		</div>
		</form>
		<form method="post" action="/basket/" id="order_form">
		<div class="form_place">
			<div class="cols_2">
				<div class="col">
					<p>
						<label for="name">Контактное лицо:</label>
						<input type="text" name="name" value="<?=$this->values['name']?>" id="name" />
					</p>
					<?=error($this->errors['name'])?>
					<p>
						<label for="phone">Ваш мобильный телефон (*):</label>
						<input type="text" name="phone" value="<?=$this->values['phone']?>" id="phone" />
					</p>
					<?=error($this->errors['phone'])?>
					<div class="check">
						<input type="checkbox" name="send_me" id="send_me"<?if ($this->values['send_me']):?> checked<?endif?> />
						<label for="send_me">Послать заявку мне на почту</label>
					</div>
					<p>
						<input type="text" name="email" value="<?=$this->values['email']?>" id="email" />
					</p>
					<?=error($this->errors['email'])?>
				</div>
				<div class="col">
					<p>
						<label for="l5">Примечание:</label>
						<textarea name="add_info" id="l5"><?=$this->values['add_info']?></textarea>
						<?=$this->errors['add_info']?>
					</p>
					<div class="note">* Наш менеджер свяжется с Вами по указанному телефону</div>
				</div>
			</div>
			<div class="btn t_center"><button type="submit">Заказать</button></div>
		</div>
		<input type="hidden" name="send" value="1">
		</form>
<?else:?>
		<p class="large t_center">В корзину пока ничего не добавлено.</p>
<?endif?>
	</div>
</div>

<?=$this->SetController('index')->SetAction('footer')->Render();?>
