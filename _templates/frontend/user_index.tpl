<?=$this->SetController('index')->SetAction('header')->Render()?>

		<section class="inner">
			<div class="breadcrumbs">
				<a href="/">На главную</a>
				<span>&raquo;</span><?=$this->name?>
			</div>
			<p>Текст</p>
			<div class="items_place">
				<div class="item">
					<div class="photo">
						<a href="/user/profile/" class="openpopup">Контактные данные</a>
					</div>
				</div>
				<div class="item">
					<div class="photo">
						<a href="/user/orders/">Заказы</a>
					</div>
				</div>
				<div class="item">
<?if (CUsers::getInstance()->isLogged() && CUsers::getInstance()->GetInfo('constructor')):?>
					<div class="photo">
						<a href="/user/constructors/">Проекты</a>
					</div>
<?endif?>
				</div>
				<div class="item">
<?if (CUsers::getInstance()->isLogged() && CUsers::getInstance()->GetInfo('all_constructors')):?>
					<div class="photo">
						<a href="/user/constructors/all/">Все проекты</a>
					</div>
<?endif?>
				</div>
				<div class="bot"></div>
			</div>
		</section>

<?=$this->SetController('index')->SetAction('footer')->Render()?>