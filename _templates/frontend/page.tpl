<?=$this->SetController('index')->SetAction('header')->Render();?>

		<section class="inner">
			<div class="breadcrumbs">
				<a href="/">На главную</a>
				<span>&raquo;</span><?=$this->pageinfo['title']?>
			</div>
			<h1><?=$this->pageinfo['title']?></h1>
			<article>
				<?=$this->pageinfo['body']?>
			</article>
		</section>

<?=$this->SetController('index')->SetAction('footer')->Render();?>