<?=$this->SetController('index')->SetAction('header')->Render()?>

<div class="popup_place">
	<div class="bg"></div>
	<div class="popup">
		<div class="close"><a href="/"></a></div>
		<h1>Регистрация</h1>
		<div class="form_place" id="login_block">
			<?if ($this->error):?>
			<p class="error"><?=$this->error?></p>
			<?endif?>
			<form method="post" action="/user/registration/">
				<div class="user_type">
					<input type="radio" name="type" value="<?=CUsers::TYPE_COMPANY?>" <?if ($this->values['type'] == CUsers::TYPE_COMPANY):?> checked<?endif?>/>
					<label for="company">Юридическое лицо</label>
					<br>
					<input type="radio" name="type" value="<?=CUsers::TYPE_PRIVATE?>" <?if ($this->values['type'] == CUsers::TYPE_PRIVATE):?> checked<?endif?>/>
					<label for="private">Физическое лицо</label>
				</div>
				<div id="private_block">
					<div class="first_column">
						<p>
							<label>Фамилия:</label>
							<input type="text" name="last_name" value="<?=$this->values['last_name']?>" />
							<?if ($this->errors['last_name']):?>
							<span class="error"><?=$this->errors['last_name']?></span>
							<?endif?>
						</p>
						<p>
							<label>Имя:</label>
							<input type="text" name="first_name" value="<?=$this->values['first_name']?>" />
							<?if ($this->errors['first_name']):?>
							<span class="error"><?=$this->errors['first_name']?></span>
							<?endif?>
						</p>
						<p>
							<label>Отчество:</label>
							<input type="text" name="middle_name" value="<?=$this->values['middle_name']?>" />
							<?if ($this->errors['middle_name']):?>
							<span class="error"><?=$this->errors['middle_name']?></span>
							<?endif?>
						</p>
					</div>
					<div class="second_column">
						<p>
							<label>Ваш мобильный телефон:</label>
							<input type="text" name="phone" value="<?=$this->values['phone']?>" />
							<?if ($this->errors['phone']):?>
							<span class="error"><?=$this->errors['phone']?></span>
							<?endif?>
						<p>
						<p>
							<label>Ваша почта:</label>
							<input type="text" name="email" value="<?=$this->values['email']?>" />
							<?if ($this->errors['email']):?>
							<span class="error"><?=$this->errors['email']?></span>
							<?endif?>
						</p>
					</div>
				</div>
				<div id="company_block">
					<div class="first_column">
						<p>
							<label>Фамилия:</label>
							<input type="text" name="last_name" value="<?=$this->values['last_name']?>" />
							<?if ($this->errors['last_name']):?>
							<span class="error"><?=$this->errors['last_name']?></span>
							<?endif?>
						</p>
						<p>
							<label>Имя:</label>
							<input type="text" name="first_name" value="<?=$this->values['first_name']?>" />
							<?if ($this->errors['first_name']):?>
							<span class="error"><?=$this->errors['first_name']?></span>
							<?endif?>
						</p>
						<p>
							<label>Отчество:</label>
							<input type="text" name="middle_name" value="<?=$this->values['middle_name']?>" />
							<?if ($this->errors['middle_name']):?>
							<span class="error"><?=$this->errors['middle_name']?></span>
							<?endif?>
						</p>
						<p>
							<label>Телефон:</label>
							<input type="text" name="phone" value="<?=$this->values['phone']?>" />
							<?if ($this->errors['phone']):?>
							<span class="error"><?=$this->errors['phone']?></span>
							<?endif?>
						<p>
						<p>
							<label>Почта:</label>
							<input type="text" name="email" value="<?=$this->values['email']?>" />
							<?if ($this->errors['email']):?>
							<span class="error"><?=$this->errors['email']?></span>
							<?endif?>
						</p>
					</div>
					<div class="second_column">
						<p>
							<label>Полное наименование организации:</label>
							<input type="text" name="company_name" value="<?=$this->values['company_name']?>" />
							<?if ($this->errors['company_name']):?>
							<span class="error"><?=$this->errors['company_name']?></span>
							<?endif?>
						</p>
						<div class="first_column">
							<p>
								<label>ИНН:</label>
								<input type="text" name="company_inn" value="<?=$this->values['company_inn']?>" />
								<?if ($this->errors['company_inn']):?>
								<span class="error"><?=$this->errors['company_inn']?></span>
								<?endif?>
							</p>
						</div>
						<div class="second_column">
							<p>
								<label>КПП:</label>
								<input type="text" name="company_kpp" value="<?=$this->values['company_kpp']?>" />
								<?if ($this->errors['company_kpp']):?>
								<span class="error"><?=$this->errors['company_kpp']?></span>
								<?endif?>
							</p>
						</div>
						<p>
							<label>БИК:</label>
							<input type="text" name="company_bik" value="<?=$this->values['company_bik']?>" />
							<?if ($this->errors['company_bik']):?>
							<span class="error"><?=$this->errors['company_bik']?></span>
							<?endif?>
						</p>
						<p>
							<label>Расчётный счёт:</label>
							<input type="text" name="company_rs" value="<?=$this->values['company_rs']?>" />
							<?if ($this->errors['company_rs']):?>
							<span class="error"><?=$this->errors['company_rs']?></span>
							<?endif?>
						</p>
						<p>
							<label>Город:</label>
							<input type="text" name="company_city" value="<?=$this->values['company_city']?>" />
							<?if ($this->errors['company_city']):?>
							<span class="error"><?=$this->errors['company_city']?></span>
							<?endif?>
						</p>
						<p>
							<label >Юридический адрес:</label>
							<textarea cols="1" rows="1" class="company" name="company_address" value="<?=$this->values['company_address']?>"></textarea>
							<?if ($this->errors['company_address']):?>
							<span class="error"><?=$this->errors['company_address']?></span>
							<?endif?>
						</p>
					</div>
				</div>
				<div class="btn t_center clr">
					<button type="submit">Зарегистрироваться</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
$(document).ready(function () {
	$('.user_type input[type="radio"]:checked').trigger('change');
});
</script>
<?=$this->SetController('index')->SetAction('footer')->Render()?>