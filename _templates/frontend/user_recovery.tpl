<?=$this->SetController('index')->SetAction('header')->Render()?>

<div class="popup_place">
	<div class="bg"></div>
	<div class="popup narrow">
		<div class="close"><a href="/"></a></div>
		<h1>Восстановление пароля</h1>
		<div class="form_place">
			<?if ($this->error):?>
			<p class="error"><?=$this->error?></p>
			<?endif?>
			<form method="post" action="/user/recovery/">
				<p>
					<label>e-mail:</label>
					<input type="text" name="email" value="<?=$this->values['email']?>" class="txt" />
				</p>
				<div class="btn t_center">
					<button type="submit">Восстановить</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?=$this->SetController('index')->SetAction('footer')->Render()?>