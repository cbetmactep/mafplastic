<?=$this->SetController('index')->SetAction('header')->Render();?>

<div class="popup_place">
	<div class="bg"></div>
	<div class="popup add_basket">
		<div class="close"><a href="/"></a></div>
		<h1>Товар добавлен в корзину</h1>
		<p class="large">В Вашей корзине <span id="add_basket_form_total_quant"><?=$this->total_quant?></span> <?=itemsCount($this->total_quant)?> на сумму <span id="add_basket_form_total_sum"><?=$this->total_price?></span> руб.</p>
		<div class="finish_buttons">
			<a class="active" href="/basket/">Оформить заказ</a>
			<a href="/constructor/">Продолжить покупки</a>
		</div>
	</div>
</div>

<?=$this->SetController('index')->SetAction('footer')->Render();?>
