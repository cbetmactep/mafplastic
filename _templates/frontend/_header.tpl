<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8" />
<?=$this->meta?>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montserrat" />
<link rel="stylesheet" type="text/css" href="/static/css/style.css?v=201609081330" />
<link rel="stylesheet" type="text/css" href="/static/css/jackbox.min.css" />
<!--[if lt IE 9]><script type="text/javascript" src="/static/js/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
<script type="text/javascript" src="/static/js/jquery.js"></script>
<script type="text/javascript" src="/static/js/jquery-ui.js"></script>
<script type="text/javascript" src="/static/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/static/js/masonry.min.js"></script>
<script type="text/javascript" src="/static/js/jackbox.min.js"></script>
<script type="text/javascript" src="/static/js/jcanvas.min.js"></script>
<script type="text/javascript" src="/static/js/jquery.mask.min.js"></script>
<script type="text/javascript" src="/static/js/scripts.js?v=20180831152234"></script>
</head>
<body>
<div class="wrapper">
	<header>
		<div class="inner top">
			<nav>
				<ul>
<?if ($this->topmenu) foreach ($this->topmenu as $aItem):?>
					<li><a href="<?=$aItem['url']?>"><?=$aItem['title']?></a></li>
<?endforeach?>
				</ul>
			</nav>
			<?=$this->SetController('basket')->SetAction('block')->Render();?>
			<div class="email">
				sales@mafplastic.ru
			</div>
			<div class="row vcard">
				<div class="tel">8 800 707 50 33</div>
				<p>звонок по России бесплатный</p>
				<p>8 (812) 407 30 33 &mdash; Санкт-Петербург</p>
				<p>8 (499) 322 30 33 &mdash; Москва</p>
			</div>
			<div>
<?if (CUsers::getInstance()->isLogged()):?>
				<a href="/user/index/"><?=CUsers::getInstance()->GetInfo('first_name')?> <?=CUsers::getInstance()->GetInfo('middle_name')?></a>
				<a href="/user/logout/">Выход</a>
<?else:?>
				<a href="/user/login/" class="openpopup">Вход</a>
				<a href="/user/registration/" class="openpopup">Регистрация</a>
<?endif?>
			</div>
		</div>
		<div class="inner bg_white round_18 bord_bot_blue vcard mid">
			<div class="fn org"><a href="/">Mafplastic</a></div>
			<nav>
				<ul>
					<li class="parent"><a href="javascript:void(0)">КОМПЛЕКТУЮЩИЕ<b>&nbsp;</b></a>
						<div class="child">
							<img class="parent_img" src="<?=CCats::GetLink(2)?>" alt="" />
							<ul>
<?if ($this->cats) foreach ($this->cats as $aItem):?>
								<li>
									<a href="/catalog/<?=$aItem['id']?>/">
										<span class="img"><img src="<?=CCats::GetLink($aItem['id'])?>" alt="" /></span>
										<span class="name"><?=$aItem['title']?></span>
									</a>
								</li>
<?endforeach?>
							</ul>
						</div>
					</li>
					<li><a href="/catalog/1/">ГОТОВЫЕ РЕШЕНИЯ</a></li>
<?if (CUsers::getInstance()->isLogged() && CUsers::getInstance()->GetInfo('constructor')):?>
					<li><a href="/constructor/">КОНФИГУРАТОР</a></li>
<?endif?>
					<li><a href="/gallery/">ФОТО / ВИДЕО</a></li>
				</ul>
			</nav>
		</div>
	</header>
	<div class="container">
