<?=$this->SetController('index')->SetAction('header')->Render();?>

		<section class="bg_white bord_top_blue">
			<div class="inner">
				<div class="title"><?=$this->catinfo['title']?></div>
				<div class="items_place">
<?if ($this->cats):?>
<?foreach ($this->cats as $aItem):?>
					<div class="item">
						<div class="photo"><a href="/catalog/<?=$aItem['id']?>/"><img src="<?=CCats::GetLink($aItem['id'])?>" alt="" /></a></div>
						<div class="name_bold"><a href="/catalog/<?=$aItem['id']?>/"><?=$aItem['title']?></a></div>
					</div>
<?endforeach?>
<?for ($c = 0; $c < (4 - count($this->count)%4); $c++):?>
					<div class="item"></div>
<?endfor?>
<?endif?>
					<div class="bot"></div>
				</div>
			</div>
		</section>

<?=$this->SetController('index')->SetAction('footer')->Render();?>
