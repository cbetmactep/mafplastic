		<div class="apply">
			<h4>Применить:</h4>
			<input id="fill_type_1" type="radio" name="fill_type" value="1" />
			<label for="fill_type_1">Только этот элемент</label><br/>
			<input id="fill_type_2" type="radio" name="fill_type" value="2" />
			<label for="fill_type_2">Все элементы на этой стороне</label><br/>
			<input id="fill_type_3" type="radio" name="fill_type" value="3" checked />
			<label for="fill_type_3">Все элементы</label>
		</div>
		<div class="information">
			<h4>Описание:</h4>
			<div class="description">
				<?=$this->item['descr']?>
			</div>
			<div class="images">
				<img src="<?=CConstructorItems::GetLink($this->item['id'], 3)?>" alt="" />
			</div>
			<div class="prices">
				<h4>Цена за шт.:</h4>
<?if ($this->prices) foreach ($this->prices as $iKey => $aPrice):?>
<?if ($iKey > 2) continue;?>
<?if ($aPrice['from'] && $aPrice['to']):?>
	От <?=$aPrice['from']?> до <?=$aPrice['to']?>
<?elseif ($aPrice['from']):?>
	От <?=$aPrice['from']?>
<?elseif ($aPrice['to']):?>
	До <?=$aPrice['to']?>
<?endif?>
	&mdash;
	<span id="popup_constructor_price_<?=$aPrice['from']?>_<?=$aPrice['to']?>" data-price="<?=$aPrice['price']?>"><?=$aPrice['price']?></span>	руб.<br/>
<?endforeach?>
			</div>
			<div class="selector">
				<h4>Резьба:</h4>
<?if ($this->sizes):?>
				<select name="thread">
<?foreach($this->sizes as $iThread => $aItems):?>
					<option value="<?=$iThread?>">M<?=$iThread?></option>
<?endforeach?>
				</select>
<?endif?>
				<h4>Размер:</h4>
<?if ($this->sizes):?>
				<select name="size">
<?foreach($this->sizes as $iThread => $aItems):?>
<?foreach($aItems as $aItem):?>
					<option data-thread="<?=$iThread?>" value="<?=$aItem['id']?>" data-price="<?=$aItem['price']?>"><?=$aItem['size']?></option>
<?endforeach?>
<?endforeach?>
				</select>
<?endif?>
			</div>
		</div>
		<div class="clr"></div>
		<input type="hidden" name="id" value="<?=$this->item['id']?>"/>