				<div class="constructor_container constructor_results">
<?if (CUsers::getInstance()->isLogged() && CUsers::getInstance()->isProduction()):?>
<?=$this->debug_info?>
					<table border="1" width="100%">
						<thead>
							<tr>
								<th>Артикул</th>
								<th>Описание</th>
								<th>Кол-во</th>
								<th>Цвет</th>
								<th>Цена за 1 шт.</th>
								<th>Общая стоимость</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
<?if ($this->items_required) foreach ($this->items_required as $aItem):?>
							<tr>
								<td><?=$aItem['articul']?></td>
								<td>
									<?=$aItem['title']?>
<?if ($aItem['descr']):?>
									<br><?=$aItem['descr']?>
<?endif?>
								</td>
								<td nowrap>
									<?=$aItem['quant']?> <?=$aItem['unit']?>
<?if ($aItem['remains'] < $aItem['quant']): ?>
									<br><strong>(<?=$aItem['remains']?> <?=$aItem['unit']?>)</strong>
<?endif?>
								</td>
								<td><?=$aItem['color']?></td>
								<td nowrap><?=$aItem['price']?> руб.</td>
								<td nowrap><?=$aItem['total']?> руб.</td>
								<td></td>
							</tr>
<?endforeach?>
<?if ($this->kit_items) foreach ($this->kit_items as $aItem):?>
							<tr>
								<td><?=$aItem['articul']?></td>
								<td><?=$aItem['title']?></td>
								<td nowrap><?=$aItem['quant']?> шт.</td>
								<td></td>
								<td nowrap><?=$aItem['price']?> руб.</td>
								<td nowrap><?=$aItem['total']?> руб.</td>
								<td></td>
							</tr>
<?endforeach?>
<?if ($this->hardware_items) foreach ($this->hardware_items as $aItem):?>
							<tr>
								<td><?=$aItem['articul']?></td>
								<td><?=$aItem['title']?></td>
								<td nowrap><?=$aItem['quant']?> шт.</td>
								<td></td>
								<td nowrap><?=$aItem['price']?> руб.</td>
								<td nowrap><?=$aItem['total']?> руб.</td>
								<td></td>
							</tr>
<?endforeach?>
						</tbody>
					</table>
					<p>Стомость изделий: <?=$this->total_required?> руб.</p>
<?endif?>
					<p>Стоимость сетки: <?=$this->build_price?> руб.</p>

<?if ($this->items_optional):?>
					<h1>Дополнительные изделия</h1>
					<h2>При необходимости данные изделия можно удалить</h2>
					<table border="1" width="100%">
						<thead>
							<tr>
								<th>Артикул</th>
								<th>Описание</th>
								<th>Кол-во</th>
								<th>Цвет</th>
								<th>Цена за 1 шт.</th>
								<th>Общая стоимость</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
<?foreach ($this->items_optional as $sID => $aItem):?>
							<tr>
								<td><?=$aItem['articul']?></td>
								<td><?=$aItem['title']?></td>
								<td nowrap>
									<?=$aItem['quant']?> <?=$aItem['unit']?>
<?if ($aItem['remains'] < $aItem['quant']): ?>
									<br><strong>(<?=$aItem['remains']?> <?=$aItem['unit']?>)</strong>
<?endif?>
								</td>
								<td>
<?if ($this->colors[$aItem['id']] && count($this->colors[$aItem['id']]) > 1):?>
									<select data-id="<?=$sID?>">
<?if (!$aItem['color']):?>
										<option value="">Не выбран</option>
<?endif?>
<?foreach($this->colors[$aItem['id']] as $aColor):?>
										<option value="<?=$aColor['color']?>"<?if ($aItem['color'] == CItemColors::$aColors[$aColor['color']]):?> selected<?endif?>>
											<?=CItemColors::$aColors[$aColor['color']]?>
										</option>
<?endforeach?>
									</select>
<?else:?>
									<?=$aItem['color']?>
<?endif?>
								</td>
								<td nowrap><?=$aItem['price']?> руб.</td>
								<td nowrap><?=$aItem['total']?> руб.</td>
								<td><input type="checkbox" data-id="<?=$sID?>"<?if (!$aItem['removed']):?> checked<?endif?>></td>
							</tr>
<?endforeach?>
						</tbody>
					</table>
					<p>Стоимость дополнительных изделий: <span id="constructor_optional_items_total"><?=$this->total_optional?></span> руб.</p>
<?endif?>
					<p>Итого: <span id="constructor_total_price"><?=$this->total_price?></span> руб.</p>

					<div class="btn_darkblue">
						<a id="constructor_add_basket_button" href="/constructor/addbasket/">В корзину</a>
					</div>
					<!--div class="btn_darkblue">
						<a target="_blank" href="/constructor/print/">Распечатать</a>
					</div-->
					<div class="btn_darkblue">
						<a href="/constructor/pdf/">Сохранить в PDF</a>
					</div>
					<div class="btn_darkblue">
						<a id="constructor_save_button" href="/constructor/save/">Сохранить проект</a>
					</div>
					<div class="clr"></div>

				</div>
