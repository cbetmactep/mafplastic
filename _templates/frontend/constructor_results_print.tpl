<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="/static/css/style_print.css?v=201609121338" />
<script type="text/javascript" src="/static/js/jquery.js"></script>
<script type="text/javascript" src="/static/js/jackbox.min.js"></script>
<script type="text/javascript" src="/static/js/scripts.js?v=201609281453"></script>
</head>
<body>

<div class="nopagebreak">
	<h1>Канатная конструкция. Проект №<?=$this->id?></h1>
	<img class="constructor_image" src="<?=CConstructors::GetLink($this->id, 2)?>" alt="">
</div>

<?if (CUsers::getInstance()->isLogged() && CUsers::getInstance()->isProduction()):?>
<div class="nopagebreak">
	<?=$this->debug_info?>
</div>
<div class="nopagebreak">
					<table border="1" width="100%">
						<thead>
							<tr>
								<th>Артикул</th>
								<th>Описание</th>
								<th>Кол-во</th>
								<th>Цвет</th>
								<th>Цена за 1 шт.</th>
								<th>Общая стоимость</th>
							</tr>
						</thead>
						<tbody>
<?if ($this->items_required) foreach ($this->items_required as $aItem):?>
							<tr>
								<td><?=$aItem['articul']?></td>
								<td>
									<?=$aItem['title']?>
<?if ($aItem['descr']):?>
									<br><?=$aItem['descr']?>
<?endif?>
								</td>
								<td nowrap>
									<?=$aItem['quant']?> <?=$aItem['unit']?>
<?if ($aItem['remains'] < $aItem['quant']): ?>
									<br><strong>(<?=$aItem['remains']?> <?=$aItem['unit']?>)</strong>
<?endif?>
								</td>
								<td><?=$aItem['color']?></td>
								<td nowrap><?=$aItem['price']?> руб.</td>
								<td nowrap><?=$aItem['total']?> руб.</td>
							</tr>
<?endforeach?>
<?if ($this->kit_items) foreach ($this->kit_items as $aItem):?>
							<tr>
								<td><?=$aItem['articul']?></td>
								<td><?=$aItem['title']?></td>
								<td nowrap><?=$aItem['quant']?> шт.</td>
								<td></td>
								<td nowrap><?=$aItem['price']?> руб.</td>
								<td nowrap><?=$aItem['total']?> руб.</td>
							</tr>
<?endforeach?>
<?if ($this->hardware_items) foreach ($this->hardware_items as $aItem):?>
							<tr>
								<td><?=$aItem['articul']?></td>
								<td><?=$aItem['title']?></td>
								<td nowrap><?=$aItem['quant']?> шт.</td>
								<td></td>
								<td nowrap><?=$aItem['price']?> руб.</td>
								<td nowrap><?=$aItem['total']?> руб.</td>
							</tr>
<?endforeach?>
						</tbody>
					</table>
					<p>Стомость изделий: <?=$this->total_required?> руб.</p>
</div>
<?endif?>
					<p>Стоимость сетки: <?=$this->build_price?> руб.</p>

<?if ($this->items_optional):?>
<div class="nopagebreak">
					<h1>Дополнительные изделия</h1>
					<table border="1" width="100%">
						<thead>
							<tr>
								<th>Артикул</th>
								<th>Описание</th>
								<th>Кол-во</th>
								<th>Цвет</th>
								<th>Цена за 1 шт.</th>
								<th>Общая стоимость</th>
							</tr>
						</thead>
						<tbody>
<?foreach ($this->items_optional as $sID => $aItem):?>
							<tr>
								<td><?=$aItem['articul']?></td>
								<td><?=$aItem['title']?></td>
								<td nowrap>
									<?=$aItem['quant']?> <?=$aItem['unit']?>
<?if ($aItem['remains'] < $aItem['quant']): ?>
									<br><strong>(<?=$aItem['remains']?> <?=$aItem['unit']?>)</strong>
<?endif?>
								</td>
								<td><?=$aItem['color']?></td>
								<td nowrap><?=$aItem['price']?> руб.</td>
								<td nowrap><?=$aItem['total']?> руб.</td>
							</tr>
<?endforeach?>
						</tbody>
					</table>
					<p>Стоимость дополнительных изделий: <?=$this->total_optional?> руб.</p>
</div>
<?endif?>
					<p>Итого: <?=$this->total_price?> руб.</p>

<script>
window.setTimeout(function () {
	window.print();
}, 300);
</script>
</body>
</html>
