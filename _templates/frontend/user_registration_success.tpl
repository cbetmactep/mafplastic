<?=$this->SetController('index')->SetAction('header')->Render()?>

<div class="popup_place">
	<div class="bg"></div>
	<div class="popup">
		<div class="close"><a href="/"></a></div>
		<h1>Регистрация</h1>
		<p class="large">Спасибо за регистрацию. Для входа на сайт используйте следующие данные:</p>
		<p class="large">
			E-mail: <strong><?=$this->values['email']?></strong><br>
			Пароль: <strong><?=$this->values['password']?></strong>
		</p>
	</div>
</div>


<?=$this->SetController('index')->SetAction('footer')->Render()?>