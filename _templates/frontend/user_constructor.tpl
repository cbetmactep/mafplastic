<?=$this->SetController('index')->SetAction('header')->Render()?>

		<section class="inner">
			<div class="breadcrumbs">
				<a href="/">На главную</a>
				<span>&raquo;</span><a href="/user/constructors/">Проекты</a>
				<span>&raquo;</span>Канатная конструкция. Проект №<?=$this->item['id']?>
			</div>
			<h1>Канатная конструкция. Проект №<?=$this->item['id']?></h1>

			<img src="<?=CConstructors::GetLink($this->item['id'], 1)?>" alt="">

<div class="constructor_container constructor_results">
<?if (CUsers::getInstance()->GetInfo('all_constructors')):?>
			<?=$this->item['params']['results']['debug_info']?>
<?endif?>
<?if (CUsers::getInstance()->isLogged() && CUsers::getInstance()->isProduction()):?>
					<table border="1" width="100%">
						<thead>
							<tr>
								<th>Артикул</th>
								<th>Описание</th>
								<th>Кол-во</th>
								<th>Цвет</th>
								<th>Цена за 1 шт.</th>
								<th>Общая стоимость</th>
							</tr>
						</thead>
						<tbody>
<?if ($this->item['params']['results']['items_required']) foreach ($this->item['params']['results']['items_required'] as $aItem):?>
							<tr>
								<td><?=$aItem['articul']?></td>
								<td>
									<?=$aItem['title']?>
<?if ($aItem['descr']):?>
									<br><?=$aItem['descr']?>
<?endif?>
								</td>
								<td nowrap><?=$aItem['quant']?> <?=$aItem['unit']?></td>
								<td><?=$aItem['color']?></td>
								<td nowrap><?=$aItem['price']?> руб.</td>
								<td nowrap><?=$aItem['total']?> руб.</td>
							</tr>
<?endforeach?>
<?if ($this->item['params']['results']['kit_items']) foreach ($this->item['params']['results']['kit_items'] as $aItem):?>
							<tr>
								<td><?=$aItem['articul']?></td>
								<td><?=$aItem['title']?></td>
								<td nowrap><?=$aItem['quant']?> шт.</td>
								<td></td>
								<td nowrap><?=$aItem['price']?> руб.</td>
								<td nowrap><?=$aItem['total']?> руб.</td>
							</tr>
<?endforeach?>
<?if ($this->item['params']['results']['hardware_items']) foreach ($this->item['params']['results']['hardware_items'] as $aItem):?>
							<tr>
								<td></td>
								<td><?=$aItem['title']?></td>
								<td nowrap><?=$aItem['quant']?> шт.</td>
								<td></td>
								<td nowrap><?=$aItem['price']?> руб.</td>
								<td nowrap><?=$aItem['total']?> руб.</td>
							</tr>
<?endforeach?>
						</tbody>
					</table>
					<p>Стомость изделий: <?=$this->item['params']['results']['total_required']?> руб.</p>
<?endif?>
					<p>Стоимость сетки: <?=$this->item['params']['results']['build_price']?> руб.</p>

<?if ($this->item['params']['results']['items_optional']):?>
					<h1>Дополнительные изделия</h1>
					<table border="1" width="100%">
						<thead>
							<tr>
								<th>Артикул</th>
								<th>Описание</th>
								<th>Кол-во</th>
								<th>Цвет</th>
								<th>Цена за 1 шт.</th>
								<th>Общая стоимость</th>
							</tr>
						</thead>
						<tbody>
<?foreach ($this->item['params']['results']['items_optional'] as $sID => $aItem):?>
							<tr>
								<td><?=$aItem['articul']?></td>
								<td><?=$aItem['title']?></td>
								<td nowrap><?=$aItem['quant']?> <?=$aItem['unit']?></td>
								<td><?=$aItem['color']?></td>
								<td nowrap><?=$aItem['price']?> руб.</td>
								<td nowrap><?=$aItem['total']?> руб.</td>
							</tr>
<?endforeach?>
						</tbody>
					</table>
					<p>Стоимость дополнительных изделий: <?=$this->item['params']['results']['total_optional']?> руб.</p>
<?endif?>
					<p>Итого: <?=$this->item['params']['results']['total_price']?> руб.</p>

					<!--div class="btn_darkblue">
						<a target="_blank" href="/user/constructors/<?=$this->item['id']?>/print/">Распечатать</a>
					</div-->
					<div class="btn_darkblue">
						<a href="/user/constructors/<?=$this->item['id']?>/pdf/">Сохранить в PDF</a>
					</div>
</div>

<?=$this->SetController('index')->SetAction('footer')->Render()?>