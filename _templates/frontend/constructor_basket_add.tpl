<?=$this->SetController('index')->SetAction('header')->Render();?>

<div class="popup_place">
	<div class="bg"></div>
	<div class="popup add_basket">
		<div class="close"><a href="/"></a></div>
		<h1>Добавление в корзину</h1>
		<form method="post" action="/constructor/addbasket/" id="add_basket_form">
		<div class="table_place">
			<table>
				<thead>
					<tr>
						<th>Фото</th>
						<th class="name">Название</th>
						<th>Количество</th>
						<th>Цена</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="img">
							<img width="98" src="<?=CConstructors::GetLink($this->item['id'], 3)?>" alt="" />
						</td>
						<td class="name">
							Канатная конструкция. Проект №<?=$this->item['id']?>
						</td>
						<td class="amount">
							<input type="text" name="quant" value="<?=$this->quant?>" title="Количество"/>
						</td>
						<td class="price"><?=$this->item['price']?> (руб. / шт)</td>
					</tr>
<?if ($this->items_optional):?>
<?foreach ($this->items_optional as $aItem):?>
					<tr data-item-id="<?=$aItem['id']?>">
						<td class="img">
							<a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><img src="<?=CItemGallery::GetLink($aItem['image_id'], 2)?>" alt="" /></a>
						</td>
						<td class="name">
							<a href="/catalog/<?=$aItem['cat_id']?>/<?=$aItem['id']?>/"><?=$aItem['title']?></a>
							<?if (!empty($aItem['articul'])):?>
							<br/><br/>[ Артикул <?=$aItem['articul']?> ]
							<?endif?>
						</td>
						<td class="amount">
							<input type="text" name="quant" value="<?=$aItem['quant']?>" title="Количество" disabled/>
						</td>
						<td class="price"><span><?=$aItem['price']?></span> (руб. / шт)</td>
					</tr>
<?endforeach?>
<?endif?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4" class="price">
							<span class="amount">Итого:</span>
							<span id="add_basket_form_total_price"><?=$this->total?></span> руб.
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="form_place">
			<div class="btn t_center"><button type="submit" name="add" value="1">Добавить</button></div>
		</div>
		</form>
	</div>
</div>

<?=$this->SetController('index')->SetAction('footer')->Render();?>
