<?=$this->SetController('index')->SetAction('header')->Render();?>

<div class="popup_place">
	<div class="bg"></div>
	<div class="popup add_basket">
		<div class="close"><a href="/"></a></div>
		<h1>Сохранение проекта</h1>
<?if (CUsers::getInstance()->isLogged()):?>
		<p class="large">Ваш проект №<?=$this->contructor_id?> добавлен в раздел «Проекты» в личном кабинете.</p>

		<div class="finish_buttons">
			<a href="/user/constructors/">Проекты</a>
			<a href="/constructor/">Закрыть</a>
		</div>
<?else:?>
		<p>Проект можно сохранить для повторного заказа. Для этого необходимо зарегистрироваться.</p>
<?endif?>
	</div>
</div>

<?=$this->SetController('index')->SetAction('footer')->Render();?>
