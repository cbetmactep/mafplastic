<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST" enctype="multipart/form-data">
<table cellspacing="1" class="content_table" width="800">
	<tr>
		<th colspan="2">
			<?=$this->title?> пункта верхнего меню
			<a class="flr" href="/admin/topmenu/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td>Название</td>
		<td width="100%">
			<input type="text" name="title" value="<?=escape($this->values['title'])?>">
			<?=error($this->errors['title'])?>
		</td>
	</tr>
	<tr>
		<td>Включено</td>
		<td>
			<input type="checkbox" class="checkbox_width" name="enabled" value="1"<?if ($this->values['enabled']):?> checked<?endif;?>>
			<?=error($this->errors['enabled'])?>
		</td>
	</tr>
	<tr>
		<td>Адрес</td>
		<td>
			<input type="text" name="url" value="<?=escape($this->values['url'])?>">
			<?=error($this->errors['url'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="button" value="<?=$this->button?>"></td>
	</tr>
</table>
</form>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
