<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST" enctype="multipart/form-data">
<table cellspacing="1" class="content_table" width="800">
	<tr>
		<th colspan="2">
			<?=$this->title?> категории
			<a class="flr" href="/admin/cats/">Вернуться</a>
		</th>
	</tr>
<? /*
	<tr>
		<td nowrap>Родительская категория</td>
		<td width="100%">
			<select name="parent_id" id="parent_id"<?if ($this->values['id']):?> disabled<?endif?>>
				<option value="0">Корневая</option>
				<?if ($this->topcats) foreach ($this->topcats as $aItem):?>
				<option value="<?=$aItem['id']?>"<?if ($this->values['parent_id'] == $aItem['id']):?> selected<?endif?>><?=$aItem['title']?> <?=$aItem['title2']?></option>
				<?endforeach?>
			</select>
			<?=error($this->errors['parent_id'])?>
		</td>
	</tr>
*/ ?>
	<tr>
		<td>Название</td>
		<td width="100%">
			<input type="text" name="title" value="<?=escape($this->values['title'])?>">
			<?=error($this->errors['title'])?>
		</td>
	</tr>
	<tr>
		<td nowrap>Включено</td>
		<td>
			<input type="checkbox" class="checkbox_width" name="enabled" value="1"<?if ($this->values['enabled']):?> checked<?endif;?>>
			<?=error($this->errors['enabled'])?>
		</td>
	</tr>
	<tr>
		<td>Картинка</td>
		<td>
			<?if ($this->values['id'] && file_exists(CCats::GetLink($this->values['id'], true))):?>
			<img src="<?=CCats::GetLink($this->values['id'], false)?>" alt=""><br>
			<?endif;?>
			<input type="file" name="image">
			<?=error($this->errors['image'])?>
		</td>
	</tr>
	<tr>
		<td>Тэг title (<span id="page_title_counter"></span>)</td>
		<td>
			<input type="text" name="page_title" id="page_title" value="<?=escape($this->values['page_title'])?>">
			<?=error($this->errors['page_title'])?>
		</td>
	</tr>
	<tr>
		<td nowrap>Тэг description (<span id="page_description_counter"></span>)</td>
		<td>
			<input type="text" name="page_description" id="page_description" value="<?=escape($this->values['page_description'])?>">
			<?=error($this->errors['page_description'])?>
		</td>
	</tr>
	<tr>
		<td>Тэг keywords (<span id="page_keywords_counter"></span>)</td>
		<td>
			<input type="text" name="page_keywords" id="page_keywords" value="<?=escape($this->values['page_keywords'])?>">
			<?=error($this->errors['page_keywords'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="button" value="<?=$this->button?>"></td>
	</tr>
</table>
</form>

<script type="text/javascript">
$(document).ready(function () {
	init_count_chars('page_title');
	init_count_chars('page_description');
	init_count_chars('page_keywords');
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
