<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST" enctype="multipart/form-data">
<table cellspacing="1" class="content_table" width="800">
	<tr>
		<th colspan="2">
			<?=$this->title?> новости
			<a class="flr" href="/admin/slider/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td nowrap>Заголовок</td>
		<td width="100%">
			<input type="text" name="title" value="<?=escape($this->values['title'])?>">
			<?=error($this->errors['title'])?>
		</td>
	</tr>
	<tr>
		<td>Включено</td>
		<td>
			<input class="checkbox_width" type="checkbox" name="enabled" value="1"<?if ($this->values['enabled']):?> checked<?endif;?>>
			<?=error($this->errors['enabled'])?>
		</td>
	</tr>
	<tr>
		<td>Ссылка</td>
		<td>
			<input type="text" name="url" value="<?=escape($this->values['url'])?>">
			<?=error($this->errors['url'])?>
		</td>
	</tr>
	<tr>
		<td>Изображение</td>
		<td>
			<? if ($this->values['id'] && file_exists(CSlider::GetLink($this->values['id'], true))): ?>
			<a href="<?=CSlider::GetLink($this->values['id'], false) ?>" class="fancybox"><img src="<?=CSlider::GetLink($this->values['id'], false) ?>" alt="" width="600"></a><br>
			<?endif?>
			<input type="file" name="image">
			<?=error($this->errors['image'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2">Описание</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea id="descr" name="descr" rows="5"><?=$this->values['descr']?></textarea>
			<?=error($this->errors['descr'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="button" value="<?=$this->button?>"></td>
	</tr>
</table>
</form>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>