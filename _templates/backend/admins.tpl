<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<table cellspacing="1" width="800" class="content_table">
	<tr>
		<th colspan="3">Администраторы</th>
	</tr>
	<tr>
		<td colspan="3"><a href="/admin/admins/add/">Добавить</a></td>
	</tr>
<?foreach ($this->admins as $aAdmin):?>
	<tr>
		<td width="100%"><?=$aAdmin['username']?></td>
		<td><a href="/admin/admins/edit/<?=$aAdmin['id']?>/"><img src="/static/images/admin/edit.gif" alt="Редактировать" title="Редактировать" /></a></td>
		<td><a href="javascript:void(0)" onclick="withrequest('Удалить?','/admin/admins/delete/<?=$aAdmin['id']?>/')"><img src="/static/images/admin/delete.gif" alt="Удалить" title="Удалить" /></a></td>
	</tr>
<?endforeach?>
	<tr>
		<td colspan="3"><a href="/admin/admins/add/">Добавить</a></td>
	</tr>
</table>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>