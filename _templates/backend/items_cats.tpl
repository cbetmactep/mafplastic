<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<table cellspacing="1" width="800" class="content_table">
	<tr>
		<th>Товары</th>
	</tr>
	<tr>
		<td colspan="8">
			<input class="checkbox_width" type="text" name="articul" value="<?=escape($_GET['articul'])?>" />
			<input class="button" type="button" value="Искать" />
		</td>
	</tr>
	<tr>
		<th>Выберите категорию</th>
	</tr>
<?if (!empty($this->structCats[0])) foreach ($this->structCats[0] as $aItem):?>
	<tr>
		<td>
<?if (!empty($this->structCats[$aItem['id']])): ?>
			<?=$aItem['title']?>
<?else:?>
			<a href="/admin/items/<?=$aItem['id']?>/"><?=$aItem['title']?></a>
<?endif?>
		</td>
	</tr>
<?if (!empty($this->structCats[$aItem['id']])) foreach ($this->structCats[$aItem['id']] as $aSubItem):?>
	<tr>
		<td>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/admin/items/<?=$aSubItem['id']?>/"><?=$aSubItem['title']?></a>
		</td>
	</tr>
<?endforeach;?>
<?endforeach;?>
</table>

<script>
var cache = {};
$(document).ready(function () {
	$('input[type="button"]').on('click', function () {
		var articul = $('input[name="articul"]').val();
		if (articul) {
			document.location.href = '?articul=' + articul;
		}
		return false;
	});
	$('input[name="articul"]').autocomplete({
		source: function(request, response) {
			var term = request.term;
			if (term in cache) {
				response(cache[term]);
				return;
			}
			var lastXhr = $.getJSON('/admin/items/articulautocomplete/', request, function(data, status, xhr) {
				cache[term] = data;
				if (xhr === lastXhr) {
					response(data);
				}
			});
		},
		select: function(event, ui) {
			if (ui.item) {
				document.location.href = '/admin/items/' + ui.item.cat_id + '/edit/' + ui.item.id + '/';
			}
		}
	});
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
