<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST" enctype="multipart/form-data">
<table cellspacing="1" class="content_table" width="800">
	<tr>
		<th colspan="2">
			<?=$this->title?> дополнительного товара
			<a class="flr" href="/admin/kititems/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td>Название</td>
		<td width="100%">
			<input type="text" name="title" value="<?=escape($this->values['title'])?>">
			<?=error($this->errors['title'])?>
		</td>
	</tr>
	<tr>
		<td>Включено</td>
		<td>
			<input type="checkbox" class="checkbox_width" name="enabled" value="1"<?if ($this->values['enabled']):?> checked<?endif;?>>
			<?=error($this->errors['enabled'])?>
		</td>
	</tr>
	<tr>
		<td>Артикул</td>
		<td width="100%">
			<input type="text" name="articul" value="<?=escape($this->values['articul'])?>">
			<?=error($this->errors['articul'])?>
		</td>
	</tr>
	<tr>
		<td>Цена</td>
		<td>
			<input type="text" name="price" value="<?=escape($this->values['price'])?>">
			<?=error($this->errors['price'])?>
		</td>
	</tr>
	<tr>
		<td>Схема</td>
		<td>
			<?if ($this->values['id'] && file_exists(CKitItems::GetLink($this->values['id'], 2, true))):?>
			<a href="<?=CKitItems::GetLink($this->values['id'], 1, false)?>" class="fancybox"><img src="<?=CKitItems::GetLink($this->values['id'], 2, false)?>" alt=""></a><br>
			<?endif;?>
			<input type="file" name="image"/>
			<?=error($this->errors['image'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2">Описание</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea id="descr" name="descr" rows="20"><?=$this->values['descr']?></textarea>
			<?=error($this->errors['descr'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="button" value="<?=$this->button?>"></td>
	</tr>
</table>
</form>

<script type="text/javascript">
init_tinymce('descr');
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
