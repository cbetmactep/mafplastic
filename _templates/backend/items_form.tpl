<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST" enctype="multipart/form-data">
<table cellspacing="1" class="content_table" width="800">
	<tr>
		<th colspan="2">
			<?=$this->title?> товара в категории &quot;<?=$this->catinfo['title']?>&quot;
			<a class="flr" href="<?=$_SESSION['back_url']?>">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td>Категория</td>
		<td width="100%">
			<select name="cat_id">
<?if (!empty($this->structCats[0])) foreach ($this->structCats[0] as $aItem):?>
				<option value="<?=$aItem['id']; ?>"<?if ($this->values['cat_id'] == $aItem['id']):?> selected<?endif?><?if (!empty($this->structCats[$aItem['id']])): ?> disabled<?endif?>><?=$aItem['title']?></option>
<?if (!empty($this->structCats[$aItem['id']])) foreach ($this->structCats[$aItem['id']] as $aSubItem):?>
				<option value="<?=$aSubItem['id']; ?>"<?if ($this->values['cat_id'] == $aSubItem['id']):?> selected<?endif?>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$aSubItem['title']?></option>
<?endforeach?>
<?endforeach?>
			</select>
		</td>
	</tr>
	<tr>
		<td>Краткое описание</td>
		<td width="100%">
			<input type="text" name="title" value="<?=escape($this->values['title'])?>">
			<?=error($this->errors['title'])?>
		</td>
	</tr>
	<tr>
		<td>Артикул</td>
		<td width="100%">
			<input type="text" name="articul" value="<?=escape($this->values['articul'])?>">
			<?=error($this->errors['articul'])?>
		</td>
	</tr>
	<tr>
		<td>Тип</td>
		<td width="100%">
			<select name="type">
				<option value="0"></option>
<?foreach (CItems::$aTypes as $iID => $sTitle):?>
				<option value="<?=$iID?>"<?if ($this->values['type'] == $iID):?> selected<?endif?>><?=$sTitle?></option>
<?endforeach?>
			</select>
			<?=error($this->errors['articul'])?>
		</td>
	</tr>
	<tr>
		<td>Включено</td>
		<td>
			<input type="checkbox" class="checkbox_width" name="enabled" value="1"<?if ($this->values['enabled']):?> checked<?endif;?>>
			<?=error($this->errors['enabled'])?>
		</td>
	</tr>
	<tr>
		<td>В наличии</td>
		<td>
			<input type="checkbox" class="checkbox_width" name="in_stock" value="1"<?if ($this->values['in_stock']):?> checked<?endif;?>>
			<?=error($this->errors['in_stock'])?>
		</td>
	</tr>
	<tr<?if ($this->values['cat_id'] != 1):?> style="display:none"<?endif?>>
		<td nowrap>Показывать на главной</td>
		<td>
			<input type="checkbox" class="checkbox_width" name="on_main" value="1"<?if ($this->values['on_main']):?> checked<?endif;?>>
			<?=error($this->errors['on_main'])?>
		</td>
	</tr>
	<tr>
		<td>Цены</td>
		<td>
			<div style="display: none" id="prices_selector">
				<input type="text" class="mediumsmall_width"/>
				от <input type="text" class="small_width"/>
				до <input type="text" class="small_width"/>
				цена <input type="text" class="small_width"/>
				<a href="javascript:void(0)" onclick="$(this).parent().remove()">X</a>
			</div>
			<a href="javascript:void(0)" onclick="add_prices_selector('', '', '', '')">Добавить</a>
			<?=error($this->errors['prices'])?>
			<div>
			<br>
			Для "от" допустимо пустое значение, оно будет считаться нулем.<br>
			Для "до" допустимо пустое значение, оно будет считаться бесконечностью.<br>
			Все количества используются "включительно", т.е. в промежуток от 100 до 500 войдут 100 и 500, поэтому следует указывать промежутки со следующего числа (101-500, 501-1000)
			</div>
		</td>
	</tr>
	<tr>
		<td>Цена для конструктора</td>
		<td>
			<input class="mediumsmall_width" type="text" name="net_price" value="<?=escape($this->values['net_price'])?>">
			<select class="mediumsmall_width" name="net_price_currency">
<?foreach (CCurrencies::$aList as $iID => $sTitle):?>
				<option value="<?=$iID?>"<?if ($this->values['net_price_currency'] == $iID):?> selected<?endif?>><?=$sTitle?></option>
<?endforeach?>
			</select>
			<?=error($this->errors['net_price'])?>
			<?=error($this->errors['net_price_currency'])?>
		</td>
	</tr>
	<tr>
		<td>Схема</td>
		<td>
			<?if ($this->values['id'] && file_exists(CItems::GetLink($this->values['id'], 2, true))):?>
			<a href="<?=CItems::GetLink($this->values['id'], 1, false)?>" class="fancybox"><img src="<?=CItems::GetLink($this->values['id'], 2, false)?>" alt=""></a><br>
			<?endif;?>
			<input type="file" name="image"/>
			<?=error($this->errors['image'])?>
		</td>
	</tr>
	<tr>
		<td>Применение</td>
		<td>
			<?if ($this->values['id'] && file_exists(CItems::GetLink($this->values['id'], 3, true))):?>
			<img src="<?=CItems::GetLink($this->values['id'], 3, false)?>" alt=""><br>
			<?endif;?>
			<input type="file" name="image_usage"/>
			<?=error($this->errors['image_usage'])?>
		</td>
	</tr>
	<tr>
		<td>Чертеж для конфигуратора</td>
		<td>
			<?if ($this->values['id'] && file_exists(CItems::GetLink($this->values['id'], 4, true))):?>
			<img src="<?=CItems::GetLink($this->values['id'], 4, false)?>" width="100" height="100" alt=""><br>
			<?endif;?>
			<input type="file" name="image_configurator_scheme"/>
			<?=error($this->errors['image_configurator_scheme'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2">Описание</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea id="descr" name="descr" rows="20"><?=$this->values['descr']?></textarea>
			<?=error($this->errors['descr'])?>
		</td>
	</tr>
	<tr>
		<td>Исключить из похожих товаров</td>
		<td>
			<div class="sortable">
			<div style="display: none" id="sameitems_selector">
				<img src="/static/images/admin/hand.gif" alt="" />
				<select class="medium_width">
					<?if ($this->items) foreach ($this->items as $aItem):?>
					<option value="<?=$aItem['id'] ?>"><?=$aItem['title'] ?></option>
					<? endforeach; ?>
				</select>
				<a href="javascript:void(0)" onclick="$(this).parent().remove()">X</a>
			</div>
			</div>
			<a href="javascript:void(0)" onclick="add_sameitems_selector(0)">Добавить</a>
			<?=error($this->errors['sameitems'])?>
		</td>
	</tr>
	<tr>
		<td>Комплектующие</td>
		<td>
			<div class="sortable">
			<div style="display: none" id="components_selector">
				<img src="/static/images/admin/hand.gif" alt="" />
				<select class="medium_width">
					<?if ($this->component_items) foreach ($this->component_items as $aItem):?>
					<option value="<?=$aItem['id'] ?>"><?=$this->cats[$aItem['cat_id']]['title']?> / <?=$aItem['title'] ?></option>
					<? endforeach; ?>
				</select>
				<input type="text" class="small_width" />
				<a href="javascript:void(0)" onclick="$(this).parent().remove()">X</a>
			</div>
			</div>
			<a href="javascript:void(0)" onclick="add_components_selector(0, 1)">Добавить</a>
			<?=error($this->errors['components'])?>
		</td>
	</tr>
	<tr>
		<td>Тэг title (<span id="page_title_counter"></span>)</td>
		<td>
			<input type="text" name="page_title" id="page_title" value="<?=escape($this->values['page_title'])?>">
			<?=error($this->errors['page_title'])?>
		</td>
	</tr>
	<tr>
		<td nowrap>Тэг description (<span id="page_description_counter"></span>)</td>
		<td>
			<input type="text" name="page_description" id="page_description" value="<?=escape($this->values['page_description'])?>">
			<?=error($this->errors['page_description'])?>
		</td>
	</tr>
	<tr>
		<td>Тэг keywords (<span id="page_keywords_counter"></span>)</td>
		<td>
			<input type="text" name="page_keywords" id="page_keywords" value="<?=escape($this->values['page_keywords'])?>">
			<?=error($this->errors['page_keywords'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="button" value="<?=$this->button?>"></td>
	</tr>
</table>
</form>

<script type="text/javascript">
init_tinymce('descr');
$(document).ready(function () {
	$('.sortable').sortable();
	init_count_chars('page_title');
	init_count_chars('page_description');
	init_count_chars('page_keywords');
<?if ($this->values['prices']) foreach ($this->values['prices'] as $aItem):?>
	add_prices_selector('<?=$aItem['title'] ?>', '<?=$aItem['from'] ?>', '<?=$aItem['to'] ?>', '<?=$aItem['price'] ?>');
<?endforeach;?>
<?if ($this->values['sameitems']) foreach ($this->values['sameitems'] as $iItem):?>
	add_sameitems_selector(<?=$iItem ?>);
<?endforeach;?>
<?if ($this->values['componentitems']) foreach ($this->values['componentitems'] as $aItem):?>
	add_components_selector(<?=$aItem['id']?>, <?=$aItem['quant']?>);
<?endforeach;?>
	$('select[name="cat_id"]').on('change', function () {
		if ($(this).val() == 1) {
			$('input[name="on_main"]').parents('tr').eq(0).show();
		} else {
			$('input[name="on_main"]').parents('tr').eq(0).hide();
		}
	});
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
