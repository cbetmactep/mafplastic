<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="post">
<table cellspacing="1" width="800" class="content_table">
	<thead>
	<tr>
		<th colspan="5">Галерея</th>
	</tr>
	<tr>
		<td colspan="5"><a href="/admin/gallery/add/">Добавить</a></td>
	</tr>
	</thead>
<?if ($this->gallery):?>
	<tbody class="sortable">
<?foreach ($this->gallery as $aItem):?>
	<tr>
		<td>
			<input type="hidden" name="ids[]" value="<?=$aItem['id']?>">
			<img src="/static/images/admin/hand.gif" alt="">
		</td>
		<td width="100%"><?=$aItem['title']?></td>
		<td><a href="/admin/gallery/edit/<?=$aItem['id']?>/"><img src="/static/images/admin/edit.gif" alt="Редактировать" title="Редактировать" /></a></td>
		<td><a href="javascript:void(0)" onclick="withrequest('Удалить?','/admin/gallery/delete/<?=$aItem['id']?>/')"><img src="/static/images/admin/delete.gif" alt="Удалить" title="Удалить" /></a></td>
		<td><a href="/admin/gallery/switch/<?=$aItem['id']?>/"><img src="/static/images/admin/<?=$aItem['enabled']?'show':'hide'?>.gif" alt="<?=$aItem['enabled']?'Вкл':'Выкл'?>" title="<?=$aItem['enabled']?'Вкл':'Выкл'?>" /></a></td>
	</tr>
<?endforeach;?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="6"><input class="button" type="submit" value="Сохранить порядок"></td>
	</tr>
	</tfoot>
<?endif;?>
</table>
</form>

<script>
$(document).ready(function () {
	$('.sortable').sortable();
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>