<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST">
<table width="800" cellspacing="1" class="content_table">
	<tr>
		<td>e-mail*</td>
		<td><input type="text" name="email" value="<?=$this->values['email']?>"><?=$this->errors['email']?></td>
	</tr>
	<tr>
		<td>Пароль</td>
		<td><input type="password" name="password"><?=$this->errors['password']?></td>
	</tr>
	<tr>
		<td nowrap>Фамилия*</td>
		<td width="100%"><input type="text" name="last_name" value="<?=escape($this->values['last_name'])?>"><?=$this->errors['last_name']?></td>
	</tr>
	<tr>
		<td nowrap>Имя*</td>
		<td width="100%"><input type="text" name="first_name" value="<?=$this->values['first_name']?>"><?=$this->errors['first_name']?></td>
	</tr>
	<tr>
		<td nowrap>Отчество</td>
		<td width="100%"><input type="text" name="middle_name" value="<?=$this->values['middle_name']?>"><?=$this->errors['middle_name']?></td>
	</tr>
	<tr>
		<td>Телефон</td>
		<td><input type="text" name="phone" value="<?=$this->values['phone']?>"><?=$this->errors['phone']?></td>
	</tr>
	<tr>
		<td>Тип</td>
		<td>
			<input class="checkbox_width" type="radio" name="type" value="<?=CUsers::TYPE_COMPANY?>" id="company"<?if ($this->values['type'] == CUsers::TYPE_COMPANY):?> checked<?endif?> onchange="check_user_form_admin()" /> <label for="company">юридическое лицо</label>
			<input class="checkbox_width" type="radio" name="type" value="<?=CUsers::TYPE_PRIVATE?>" id="private"<?if ($this->values['type'] == CUsers::TYPE_PRIVATE):?> checked<?endif?> onchange="check_user_form_admin()" /> <label for="private">физическое лицо</label>
		</td>
	</tr>
	<tr>
		<td>Показывать конфигуратор</td>
		<td>
			<input class="checkbox_width" type="checkbox" name="constructor" value="1"<?if ($this->values['constructor']):?> checked<?endif;?>>
			<?=error($this->errors['constructor'])?>
		</td>
	</tr>
	<tr>
		<td>Показывать все проекты</td>
		<td>
			<input class="checkbox_width" type="checkbox" name="all_constructors" value="1"<?if ($this->values['all_constructors']):?> checked<?endif;?>>
			<?=error($this->errors['all_constructors'])?>
		</td>
	</tr>
	<tr>
		<td>Производство</td>
		<td>
			<input class="checkbox_width" type="checkbox" name="production" value="1"<?if ($this->values['production']):?> checked<?endif;?>>
			<?=error($this->errors['production'])?>
		</td>
	</tr>
	<tr class="company">
		<td nowrap>Название организации*</td>
		<td><input type="text" name="company_name" value="<?=$this->values['company_name']?>"><?=$this->errors['company_name']?></td>
	</tr>
	<tr class="company">
		<td>ИНН*</td>
		<td><input type="text" name="company_inn" value="<?=$this->values['company_inn']?>"><?=$this->errors['company_inn']?></td>
	</tr>
	<tr class="company">
		<td>КПП</td>
		<td><input type="text" name="company_kpp" value="<?=$this->values['company_kpp']?>"><?=$this->errors['company_kpp']?></td>
	</tr>
	<tr class="company">
		<td>БИК*</td>
		<td><input type="text" name="company_bik" value="<?=$this->values['company_bik']?>"><?=$this->errors['company_bik']?></td>
	</tr>
	<tr class="company">
		<td nowrap>Расчетный счет*</td>
		<td><input type="text" name="company_rs" value="<?=$this->values['company_rs']?>"><?=$this->errors['company_rs']?></td>
	</tr>
	<tr class="company">
		<td>Город*</td>
		<td><input type="text" name="company_city" value="<?=$this->values['company_city']?>"><?=$this->errors['company_city']?></td>
	</tr>
	<tr class="company">
		<td nowrap>Юридический адрес*</td>
		<td><textarea rows="3" name="company_address"><?=$this->values['company_address']?></textarea><?=$this->errors['company_address']?></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="button" name="update" value="Сохранить"></td>
	</tr>
</table>
</form>

<script>
$(document).ready(function () {
	check_user_form_admin();
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
