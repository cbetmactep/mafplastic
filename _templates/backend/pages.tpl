<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<table cellspacing="1" width="800" class="content_table">
	<tr>
		<th colspan="3">Статичные страницы</th>
	</tr>
	<tr>
		<td colspan="3"><a href="/admin/pages/add/">Добавить</a></td>
	</tr>
<?if ($this->pages):?>
<?foreach ($this->pages as $aItem):?>
	<tr>
		<td width="100%"><?=$aItem['title']?> (<?=$aItem['path']?>)</td>
		<td><a href="/admin/pages/edit/<?=$aItem['id']?>/"><img src="/static/images/admin/edit.gif" alt="Редактировать" title="Редактировать" /></a></td>
		<td><a href="javascript:void(0)" onclick="withrequest('Удалить?','/admin/pages/delete/<?=$aItem['id']?>/')"><img src="/static/images/admin/delete.gif" alt="Удалить" title="Удалить" /></a></td>
	</tr>
<?endforeach;?>
	<tr>
		<td colspan="3"><a href="/admin/pages/add/">Добавить</a></td>
	</tr>
<?endif;?>
</table>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
