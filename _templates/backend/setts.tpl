<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST" enctype="multipart/form-data">
<table border="0" class="content_table" cellspacing="1" width="800">
	<tr>
		<th colspan="2">Настройки</th>
	</tr>
	<tr>
		<td nowrap>URL сайта (без / в конце)</td>
		<td width="100%">
			<input type="text" name="site_url" value="<?=$this->setts['site_url']?>">
			<?=error($this->errors['site_url'])?>
		</td>
	</tr>
	<tr>
		<td nowrap>Адрес сайта (без / в конце)</td>
		<td width="100%">
			<input type="text" name="site_address" value="<?=$this->setts['site_address']?>">
			<?=error($this->errors['site_address'])?>
		</td>
	</tr>
	<tr>
		<td>Заголовок сайта</td>
		<td>
			<input type="text" name="site_title" value="<?=$this->setts['site_title']?>">
			<?=error($this->errors['site_title'])?>
		</td>
	</tr>
	<tr>
		<td>E-mail</td>
		<td>
			<input type="text" name="site_email" value="<?=$this->setts['site_email']?>">
			<?=error($this->errors['site_email'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" value="Сохранить" class="button"></td>
	</tr>
	<tr>
		<th colspan="2">Конструктор</th>
	</tr>
	<tr>
		<td>Наценка</td>
		<td>
			<?=$this->setts['net_price_margin']?>
		</td>
	</tr>
	<tr>
		<td>Цена ч/ч</td>
		<td>
			<?=$this->setts['net_price_work']?>
		</td>
	</tr>
	<tr>
		<td>Амортизация</td>
		<td>
			<?=$this->setts['net_price_wearout']?>
		</td>
	</tr>
	<tr>
		<td>Аренда</td>
		<td>
			<?=$this->setts['net_price_rent']?>
		</td>
	</tr>
	<tr>
		<td>Коммунальные услуги</td>
		<td>
			<?=$this->setts['net_price_utilities']?>
		</td>
	</tr>
	<tr>
		<th colspan="2">Валюта</th>
	</tr>
	<tr>
		<td>Последнее обновление</td>
		<td>
			<?=format_date($this->setts['currency_updated'], null, true)?>
		</td>
	</tr>
	<tr>
		<td>Евро</td>
		<td>
			<?=$this->setts['currency_eur']?>
		</td>
	</tr>
	<tr>
		<td>Доллар</td>
		<td>
			<?=$this->setts['currency_usd']?>
		</td>
	</tr>
</table>
</form>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>