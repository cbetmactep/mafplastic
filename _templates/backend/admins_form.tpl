<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST">
<table cellspacing="1" class="content_table" width="800">
	<tr>
		<th colspan="2">
			<?=$this->title?> администратора
			<a class="flr" href="/admin/admins/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td>Логин</td>
		<td>
		<?if ($this->values['id']) {?>
			<?=$this->values['username']?>
		<?} else {?>
			<input type="text" name="username" value="<?=escape($this->values['username'])?>"><?=$this->errors['username']?>
		<?}?>
		</td>
	</tr>
	<tr>
		<td>Пароль</td>
		<td><input type="password" name="password"><?=$this->errors['password']?></td>
	</tr>
	<? if (!$this->no_rights) {?>
	<tr>
		<td>Права</td>
		<td>
			<?foreach (CAdmin::$aRoles as $iRole => $sTitle):?>
			<input class="checkbox_width" type="checkbox" name="role[<?=$iRole?>]"<?=(CAdmin::getInstance()->CheckRole($iRole,$this->values) || $this->values['role'][$iRole]?' checked':'')?>> <?=$sTitle?><br>
			<?endforeach;?>
		</td>
	</tr>
	<? } ?>
	<tr>
		<td colspan="2"><input type="submit" class="button" value="Сохранить"></td>
	</tr>
</table>
</form>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
