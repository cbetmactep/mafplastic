<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<table cellspacing="1" width="800" class="content_table">
	<tr>
		<th>Товары для конструктора</th>
	</tr>
	<tr>
		<th>Выберите тип</th>
	</tr>
<?foreach ($this->types as $iType => $sTitle):?>
	<tr>
		<td>
			<a href="/admin/constructoritems/<?=$iType?>/"><?=$sTitle?></a>
		</td>
	</tr>
<?endforeach;?>
</table>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
