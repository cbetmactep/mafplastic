<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="post">
<table cellspacing="1" width="800" class="content_table">
	<thead>
	<tr>
		<th colspan="8">
			Дополнительные товары
		</th>
	</tr>
	<tr>
		<td colspan="8"><a href="/admin/kititems/<?=$this->catinfo['id']?>/add/">Добавить</a></td>
	</tr>
	</thead>
<?if ($this->kititems):?>
	<tbody class="sortable">
<?foreach ($this->kititems as $aItem):?>
	<tr>
		<td>
			<input type="hidden" name="ids[]" value="<?=$aItem['id']?>">
			<img src="/static/images/admin/hand.gif" alt="">
		</td>
		<td><?=$aItem['articul']?></td>
		<td width="100%"><?=$aItem['title']?></td>
		<td><a href="/admin/kititems/edit/<?=$aItem['id']?>/"><img src="/static/images/admin/edit.gif" alt="Редактировать" title="Редактировать" /></a></td>
		<td><a href="javascript:void(0)" onclick="withrequest('Удалить?','/admin/kititems/delete/<?=$aItem['id']?>/')"><img src="/static/images/admin/delete.gif" alt="Удалить" title="Удалить" /></a></td>
		<td><a href="/admin/kititems/switch/<?=$aItem['id']?>/"><img src="/static/images/admin/<?=$aItem['enabled']?'show':'hide'?>.gif" alt="<?=$aItem['enabled']?'Вкл':'Выкл'?>" title="<?=$aItem['enabled']?'Вкл':'Выкл'?>" /></a></td>
	</tr>
<?endforeach;?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="8"><input class="button" type="submit" value="Сохранить порядок"></td>
	</tr>
	</tfoot>
<?endif;?>
</table>
</form>

<script>
$(document).ready(function () {
	$('.sortable').sortable();
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
