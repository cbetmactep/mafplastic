<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST" enctype="multipart/form-data">
<table cellspacing="1" class="content_table" width="800">
	<tr>
		<th colspan="2">
			<?=$this->title?> картинки в галерее товара &quot;<?=$this->iteminfo['title']?>&quot;
			<a class="flr" href="/admin/itemgallery/<?=$this->iteminfo['id']?>/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td nowrap>Заголовок</td>
		<td>
			<input type="text" name="title" value="<?=escape($this->values['title'])?>">
			<?=error($this->errors['title'])?>
		</td>
	</tr>
	<tr>
		<td>Включено</td>
		<td>
			<input class="checkbox_width" type="checkbox" name="enabled" value="1"<?if ($this->values['enabled']):?> checked<?endif;?>>
			<?=error($this->errors['enabled'])?>
		</td>
	</tr>
	<tr>
		<td>Изображение маленькое для товара (98х98) и для списков (165х165), авторесайз</td>
		<td width="100%">
			<?if ($this->values['id'] && file_exists(CItemGallery::GetLink($this->values['id'], 2, true))):?>
			<a href="<?=CItemGallery::GetLink($this->values['id'], 3, false) ?>" class="fancybox"><img src="<?=CItemGallery::GetLink($this->values['id'], 2, false) ?>" alt=""></a><br>
			<?endif?>
			<input type="file" name="image">
			<?=error($this->errors['image'])?>
		</td>
	</tr>
	<tr>
		<td>Изображение крупное для товара (370х370)</td>
		<td width="100%">
			<?if ($this->values['id'] && file_exists(CItemGallery::GetLink($this->values['id'], 1, true))):?>
			<a href="<?=CItemGallery::GetLink($this->values['id'], 1, false) ?>" class="fancybox"><img width="100" src="<?=CItemGallery::GetLink($this->values['id'], 1, false) ?>" alt=""></a><br>
			<?endif?>
			<input type="file" name="large_image">
			<?=error($this->errors['large_image'])?>
		</td>
	</tr>
	<tr>
		<td>Изображение крупное для товара в попап (размер произвольный)</td>
		<td width="100%">
			<?if ($this->values['id'] && file_exists(CItemGallery::GetLink($this->values['id'], 4, true))):?>
			<a href="<?=CItemGallery::GetLink($this->values['id'], 4, false) ?>" class="fancybox"><img width="100" src="<?=CItemGallery::GetLink($this->values['id'], 4, false) ?>" alt=""></a><br>
			<?endif?>
			<input type="file" name="orig_image">
			<?=error($this->errors['orig_image'])?>
		</td>
	</tr>
	<tr>
		<td nowrap>Адрес видео</td>
		<td width="100%">
			<input type="text" name="video" value="<?=escape($this->values['video'])?>">
			<?=error($this->errors['video'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="button" value="<?=$this->button?>"></td>
	</tr>
</table>
</form>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
