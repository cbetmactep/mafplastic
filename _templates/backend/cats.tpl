<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="post">
<table cellspacing="1" width="800" class="content_table">
	<thead>
	<tr>
		<th colspan="7">
			Категории
		</th>
	</tr>
	<tr>
		<td colspan="7"><a href="/admin/cats/add/">Добавить</a></td>
	</tr>
	</thead>
<?if (!empty($this->cats[0])):?>
	<tbody class="catsmultisort">
<?foreach ($this->cats[0] as $aItem):?>
	<tr class="topcat">
		<td width="100%">
			<input type="hidden" name="ids[0][]" value="<?=$aItem['id']?>">
			<?=$aItem['title']?>
		</td>
		<td></td>
		<td></td>
		<td><a href="/admin/cats/edit/<?=$aItem['id']?>/"><img src="/static/images/admin/edit.gif" alt="Редактировать" title="Редактировать" /></a></td>
		<td></td>
		<td></td>
	</tr>
<?if (!empty($this->cats[$aItem['id']])) foreach ($this->cats[$aItem['id']] as $aSubItem):?>
	<tr class="subcat">
		<td width="100%">
			<input type="hidden" name="ids[<?=$aItem['id']?>][]" value="<?=$aSubItem['id']?>">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$aSubItem['title']?>
		</td>
		<td><a href="#" class="up_sub">Выше</a></td>
		<td><a href="#" class="down_sub">Ниже</a></td>
		<td><a href="/admin/cats/edit/<?=$aSubItem['id']?>/"><img src="/static/images/admin/edit.gif" alt="Редактировать" title="Редактировать" /></a></td>
		<td><a href="javascript:void(0)" onclick="withrequest('Удалить?','/admin/cats/delete/<?=$aSubItem['id']?>/')"><img src="/static/images/admin/delete.gif" alt="Удалить" title="Удалить" /></a></td>
		<td><a href="/admin/cats/switch/<?=$aSubItem['id']?>/"><img src="/static/images/admin/<?=$aSubItem['enabled']?'show':'hide'?>.gif" alt="<?=$aSubItem['enabled']?'Вкл':'Выкл'?>" title="<?=$aSubItem['enabled']?'Вкл':'Выкл'?>" /></a></td>
	</tr>
<?endforeach;?>
<?endforeach;?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="7"><input class="button" type="submit" value="Сохранить порядок"></td>
	</tr>
	</tfoot>
<?endif;?>
</table>
</form>

<script>
$(document).ready(function () {
	SortMultiCats.init();
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
