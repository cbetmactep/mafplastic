<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST" enctype="multipart/form-data">
<table cellspacing="1" class="content_table" width="800">
	<tr>
		<th colspan="2">
			<?=$this->title?> метиза типа &quot;<?=$this->typeinfo?>&quot;
			<a class="flr" href="/admin/hardwareitems/<?=$this->type?>/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td>Размер</td>
		<td width="100%">
			<input type="text" name="size" value="<?=escape($this->values['size'])?>">
			<?=error($this->errors['size'])?>
		</td>
	</tr>
	<tr>
		<td>Резьба</td>
		<td width="100%">
			<input type="text" name="thread" value="<?=escape($this->values['thread'])?>">
			<?=error($this->errors['thread'])?>
		</td>
	</tr>
	<tr>
		<td nowrap>Включено</td>
		<td width="100%">
			<input type="checkbox" class="checkbox_width" name="enabled" value="1"<?if ($this->values['enabled']):?> checked<?endif;?>>
			<?=error($this->errors['enabled'])?>
		</td>
	</tr>
	<tr>
		<td>Артикул</td>
		<td width="100%">
			<input type="text" name="articul" value="<?=escape($this->values['articul'])?>">
			<?=error($this->errors['articul'])?>
		</td>
	</tr>
	<tr>
		<td nowrap>
			Цена
<?if ($this->type == CHardwareItems::TYPE_STUD): ?>
			(за 2 м.)
<?endif?>
		</td>
		<td>
			<input type="text" name="price" value="<?=escape($this->values['price'])?>">
			<?=error($this->errors['price'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="button" value="<?=$this->button?>"></td>
	</tr>
</table>
</form>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
