<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="post">
<table cellspacing="1" width="800" class="content_table">
	<thead>
	<tr>
		<th colspan="8">
			Товары
			<a class="flr" href="/admin/items/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td colspan="8">
			<input class="checkbox_width" type="text" name="articul" value="<?=escape($_GET['articul'])?>" />
			<input class="button" type="button" value="Искать" />
		</td>
	</tr>
	<tr>
		<td colspan="8"><a href="/admin/items/<?=$aItem['cat_id']?>/add/">Добавить</a></td>
	</tr>
	</thead>
<?if ($this->items):?>
	<tbody>
<?foreach ($this->items as $aItem):?>
	<tr>
		<td width="100%"><?=$aItem['title']?> (<?=$aItem['articul']?>)</td>
		<td><a href="/admin/itemgallery/<?=$aItem['id']?>/">Галерея</a></td>
		<td><a href="/admin/itemcolors/<?=$aItem['id']?>/">Цвета</a></td>
		<td><a href="/admin/items/<?=$aItem['cat_id']?>/copy/<?=$aItem['id']?>/"><img src="/static/images/admin/copy.gif" alt="Копировать" title="Копировать" /></a></td>
		<td><a href="/admin/items/<?=$aItem['cat_id']?>/edit/<?=$aItem['id']?>/"><img src="/static/images/admin/edit.gif" alt="Редактировать" title="Редактировать" /></a></td>
		<td><a href="javascript:void(0)" onclick="withrequest('Удалить?','/admin/items/<?=$aItem['cat_id']?>/delete/<?=$aItem['id']?>/')"><img src="/static/images/admin/delete.gif" alt="Удалить" title="Удалить" /></a></td>
		<td><a href="/admin/items/<?=$aItem['cat_id']?>/switch/<?=$aItem['id']?>/"><img src="/static/images/admin/<?=$aItem['enabled']?'show':'hide'?>.gif" alt="<?=$aItem['enabled']?'Вкл':'Выкл'?>" title="<?=$aItem['enabled']?'Вкл':'Выкл'?>" /></a></td>
	</tr>
<?endforeach;?>
	</tbody>
<?endif;?>
</table>
</form>

<script>
$(document).ready(function () {
	$('input[type="button"]').on('click', function () {
		var articul = $('input[name="articul"]').val();
		if (articul) {
			document.location.href = '?articul=' + articul;
		}
		return false;
	});
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
