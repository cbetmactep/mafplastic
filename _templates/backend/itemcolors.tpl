<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="post">
<table cellspacing="1" width="800" class="content_table">
	<thead>
	<tr>
		<th colspan="6">
			Цвета товара &quot;<?=$this->iteminfo['title']?>&quot;
			<a class="flr" href="/admin/items/<?=$this->iteminfo['cat_id']?>/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td colspan="6"><a href="/admin/itemcolors/<?=$this->iteminfo['id']?>/add/">Добавить</a></td>
	</tr>
	</thead>
<?if ($this->list):?>
	<tbody class="sortable">
<?foreach ($this->list as $aItem):?>
	<tr>
		<td>
			<input type="hidden" name="ids[]" value="<?=$aItem['id']?>">
			<img src="/static/images/admin/hand.gif" alt="">
		</td>
		<td width="100%"><?=CItemColors::$aColors[$aItem['color']]?> (<?=$aItem['articul']?>)</td>
		<td><a href="/admin/itemcolors/<?=$this->iteminfo['id']?>/main/<?=$aItem['id']?>/"><img src="/static/images/admin/<?=$aItem['main']?'active':'inactive'?>.gif" alt="<?=$aItem['enabled']?'Основная':'Неосновная'?>" title="<?=$aItem['enabled']?'Основная':'Неосновная'?>" /></a></td>
		<td><a href="/admin/itemcolors/<?=$this->iteminfo['id']?>/edit/<?=$aItem['id']?>/"><img src="/static/images/admin/edit.gif" alt="Редактировать" title="Редактировать" /></a></td>
		<td><a href="javascript:void(0)" onclick="withrequest('Удалить?','/admin/itemcolors/<?=$this->iteminfo['id']?>/delete/<?=$aItem['id']?>/')"><img src="/static/images/admin/delete.gif" alt="Удалить" title="Удалить" /></a></td>
		<td><a href="/admin/itemcolors/<?=$this->iteminfo['id']?>/switch/<?=$aItem['id']?>/"><img src="/static/images/admin/<?=$aItem['enabled']?'show':'hide'?>.gif" alt="<?=$aItem['enabled']?'Вкл':'Выкл'?>" title="<?=$aItem['enabled']?'Вкл':'Выкл'?>" /></a></td>
	</tr>
<?endforeach;?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="6"><input class="button" type="submit" value="Сохранить порядок"></td>
	</tr>
	</tfoot>
<?endif;?>
</table>
</form>

<script>
$(document).ready(function () {
	$('.sortable').sortable();
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
