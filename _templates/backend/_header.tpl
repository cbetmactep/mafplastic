<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<TITLE>MAF PLASTIC</TITLE>
<link rel="stylesheet" href="/static/css/admin.css">
<link rel="stylesheet" href="/static/css/jquery.fancybox.css">
<link rel="stylesheet" href="/static/css/jquery-ui.css">
<script language="javascript" type="text/javascript" src="/static/js/jquery.js"></script>
<script language="javascript" type="text/javascript" src="/static/js/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="/_tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript" src="/_tiny_mce/tinybrowser/tb_tinymce.js.php"></script>
<script language="javascript" type="text/javascript" src="/static/js/jquery.fancybox.pack.js"></script>
<script language="javascript" type="text/javascript" src="/static/js/admin.js"></script>
<script>
$('a.fancybox').fancybox();
</script>
</head>

<body>

<table cellpadding="0" cellspacing="0" border="0" class="main_table">
	<tr>
		<td colspan="2" class="header">
			MAF PLASTIC
		</td>
	</tr>
	<tr>
		<td class="menu_td">
			<ul class="main_menu">
				<li><a href="/admin/admins/">Администраторы</a></li>
				<?if (CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGEUSERS)):?>
				<li><a href="/admin/users/">Пользователи</a></li>
				<?endif?>
				<?if (CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGESETTS)):?>
				<li><a href="/admin/setts/">Настройки</a></li>
				<?endif?>
				<?if (CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGEPAGES)):?>
				<li><a href="/admin/pages/">Статичные страницы</a></li>
				<?endif?>
				<?if (CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGECATS)):?>
				<li><a href="/admin/cats/">Категории</a></li>
				<?endif?>
				<?if (CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGEITEMS)):?>
				<li><a href="/admin/items/">Товары</a></li>
				<?endif?>
				<?if (CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGEGALLERY)):?>
				<li><a href="/admin/gallery/">Галерея</a></li>
				<?endif?>
				<?if (CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGESLIDER)):?>
				<li><a href="/admin/slider/">Слайдер</a></li>
				<?endif?>
				<?if (CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGETOPMENU)):?>
				<li><a href="/admin/topmenu/">Верхнее меню</a></li>
				<?endif?>
				<?if (CAdmin::getInstance()->CheckRole(CAdmin::ROLE_MANAGEITEMS)):?>
				<li><a href="/admin/constructoritems/">Товары для конструктора</a></li>
				<li><a href="/admin/kititems/">Дополнительные товары</a></li>
				<li><a href="/admin/hardwareitems/">Метизы</a></li>
				<?endif?>
				<li><a href="/admin/logout/">Выход</a></li>
			</ul>
		</td>
		<td class="content">

<?if ($this->error): ?>
<div class="error"><?=$this->error?></div>
<?endif?>
