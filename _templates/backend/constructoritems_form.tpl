<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST" enctype="multipart/form-data">
<table cellspacing="1" class="content_table" width="800">
	<tr>
		<th colspan="2">
			<?=$this->title?> товара типа &quot;<?=$this->typeinfo?>&quot;
			<a class="flr" href="/admin/constructoritems/<?=$this->type?>/">Вернуться</a>
		</th>
	</tr>
<?if ($this->type == CConstructorItems::TYPE_CONNECTION): ?>
	<tr>
		<td>Группа</td>
		<td>
			<select name="subtype">
<?foreach (CConstructorItems::$aSubTypes as $iID => $sTitle):?>
				<option value="<?=$iID?>"<?if ($this->values['subtype'] == $iID):?> selected<?endif?>><?=$sTitle?></option>
<?endforeach?>
			</select>
		</td>
	</tr>
	<tr>
		<td>Тип блока</td>
		<td>
			<select name="block_type">
<?foreach (CConstructorItems::$aBlockTypes as $iID => $sTitle):?>
				<option value="<?=$iID?>"<?if ($this->values['block_type'] == $iID):?> selected<?endif?>><?=$sTitle?></option>
<?endforeach?>
			</select>
		</td>
	</tr>
<?endif?>
	<tr>
		<td nowrap>Включено</td>
		<td width="100%">
			<input type="checkbox" class="checkbox_width" name="enabled" value="1"<?if ($this->values['enabled']):?> checked<?endif;?>>
			<?=error($this->errors['enabled'])?>
		</td>
	</tr>
<?if ($this->type == CConstructorItems::TYPE_CONNECTION || $this->type == CConstructorItems::TYPE_JUNCTION): ?>
	<tr>
		<td nowrap>Размер, мм</td>
		<td>
			<input type="text" name="size" value="<?=$this->values['size']?>">
			<?=error($this->errors['size'])?>
		</td>
	</tr>
	<tr>
		<td nowrap>Удлинение каната, мм</td>
		<td>
			<input type="text" name="rope_increment" value="<?=$this->values['rope_increment']?>">
			<?=error($this->errors['rope_increment'])?>
		</td>
	</tr>
<?if ($this->type == CConstructorItems::TYPE_CONNECTION): ?>
	<tr class="hidden subtype_hidden subtype_show_1">
		<td nowrap>Длина шпильки<br/> внутри гильзы, мм</td>
		<td>
			<input type="text" name="crimp_size" value="<?=$this->values['crimp_size']?>">
			<?=error($this->errors['crimp_size'])?>
		</td>
	</tr>
	<tr class="hidden subtype_hidden subtype_show_5">
		<td nowrap>Длина коуша + канат, мм</td>
		<td>
			<input type="text" name="crimp_size" value="<?=$this->values['crimp_size']?>">
			<?=error($this->errors['crimp_size'])?>
		</td>
	</tr>
<?endif?>
	<tr>
		<td nowrap>Коэффициент для сеток</td>
		<td>
			<input type="text" name="time_coeff" value="<?=$this->values['time_coeff']?>">
			<?=error($this->errors['time_coeff'])?>
		</td>
	</tr>
<?endif?>
	<tr>
		<td>Изображение</td>
		<td>
			<?if ($this->values['id'] && file_exists(CConstructorItems::GetLink($this->values['id'], 1, true))):?>
			<img src="<?=CConstructorItems::GetLink($this->values['id'], 1, false)?>" alt=""><br>
			<?endif;?>
			<input type="file" name="image"/>
			<?=error($this->errors['image'])?>
		</td>
	</tr>
	<tr>
		<td>Чертеж для<br> конфигуратора</td>
		<td>
			<?if ($this->values['id'] && file_exists(CConstructorItems::GetLink($this->values['id'], 2, true))):?>
			<img src="<?=CConstructorItems::GetLink($this->values['id'], 2, false)?>" width="100" height="100" alt=""><br>
			<?endif;?>
			<input type="file" name="image_scheme"/>
			<?=error($this->errors['image_scheme'])?>
		</td>
	</tr>
	<tr>
		<td>Схема размеров</td>
		<td>
			<?if ($this->values['id'] && file_exists(CConstructorItems::GetLink($this->values['id'], 3, true))):?>
			<img src="<?=CConstructorItems::GetLink($this->values['id'], 3, false)?>" alt=""><br>
			<?endif;?>
			<input type="file" name="image_sizes"/>
			<?=error($this->errors['image_sizes'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2">Описание</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea id="descr" name="descr" rows="20"><?=$this->values['descr']?></textarea>
			<?=error($this->errors['descr'])?>
		</td>
	</tr>
	<tr>
		<td nowrap>Товар 1</td>
		<td>
			<select name="item_1_id" class="searchable medium_width">
				<?if ($this->items) foreach ($this->items as $aItem):?>
				<option value="<?=$aItem['id'] ?>"<?if ($this->values['item_1_id'] == $aItem['id']):?> selected<?endif?>>
					(<?=$aItem['articul'] ?>) <?=$aItem['title'] ?>
				</option>
				<? endforeach; ?>
			</select>
			<input type="checkbox" name="item_1_deletable" class="checkbox_width" value="1"<?if ($this->values['item_1_deletable']):?> checked<?endif;?>>
			<?=error($this->errors['item_1_id'])?>
		</td>
	</tr>
	<?for ($c = 2; $c <= 9; $c++):?>
	<tr>
		<td nowrap>Товар <?=$c?></td>
		<td>
			<select name="item_<?=$c?>_id" class="searchable medium_width">
				<option value="0"></option>
				<?if ($this->items) foreach ($this->items as $aItem):?>
				<option value="<?=$aItem['id'] ?>"<?if ($this->values['item_'.$c.'_id'] == $aItem['id']):?> selected<?endif?>>
					(<?=$aItem['articul'] ?>) <?=$aItem['title'] ?>
				</option>
				<? endforeach; ?>
			</select>
			<input type="checkbox" name="item_<?=$c?>_deletable" class="checkbox_width" value="1"<?if ($this->values['item_'.$c.'_deletable']):?> checked<?endif;?>>
			<?=error($this->errors['item_'.$c.'_id'])?>
		</td>
	</tr>
	<?endfor?>
	<tr>
		<td nowrap>Дополнительные товары</td>
		<td>
			<div class="sortable">
			<div style="display: none" id="kititems_selector">
				<img src="/static/images/admin/hand.gif" alt="" />
				<select class="medium_width">
					<?if ($this->kit_items) foreach ($this->kit_items as $aItem):?>
					<option value="<?=$aItem['id'] ?>"><?=$aItem['title'] ?> (<?=$aItem['articul'] ?>)</option>
					<? endforeach; ?>
				</select>
				<input type="text" class="small_width" />
				<a href="javascript:void(0)" onclick="$(this).parent().remove()">X</a>
			</div>
			</div>
			<a href="javascript:void(0)" onclick="add_kititems_selector(0, 1)">Добавить</a>
			<?=error($this->errors['kititems'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="button" value="<?=$this->button?>"></td>
	</tr>
</table>
</form>

<link rel="stylesheet" href="/static/css/select2.min.css">
<script language="javascript" type="text/javascript" src="/static/js/select2.min.js"></script>
<script>
init_tinymce('descr');
$(document).ready(function () {
	$('.sortable').sortable();
	$('.searchable').select2();
<?if ($this->values['kititems']) foreach ($this->values['kititems'] as $aItem):?>
	add_kititems_selector(<?=$aItem['id']?>, <?=$aItem['quant']?>);
<?endforeach;?>
	$('select[name="subtype"]').on('change', function () {
		var subtype = $(this).val();
		$('.subtype_hidden').hide().find('input').prop('disabled', true);
		$('.subtype_show_' + subtype).show().find('input').prop('disabled', false);
	});
	$('select[name="subtype"]').trigger('change');
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
