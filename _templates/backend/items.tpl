<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="post">
<table cellspacing="1" width="800" class="content_table">
	<thead>
	<tr>
		<th colspan="8">
			Товары из категории &quot;<?=$this->catinfo['title']?>&quot;
			<a class="flr" href="/admin/items/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td colspan="8">
			<input class="checkbox_width" type="text" name="articul" value="<?=escape($_GET['articul'])?>" />
			<input class="button" type="button" value="Искать" />
		</td>
	</tr>
	<tr>
		<td colspan="8"><a href="/admin/items/<?=$this->catinfo['id']?>/add/">Добавить</a></td>
	</tr>
	</thead>
<?if ($this->items):?>
	<tbody class="sortable">
<?foreach ($this->items as $aItem):?>
	<tr>
		<td>
			<input type="hidden" name="ids[]" value="<?=$aItem['id']?>">
			<img src="/static/images/admin/hand.gif" alt="">
		</td>
		<td width="100%"><?=$aItem['title']?> (<?=$aItem['articul']?>)</td>
		<td><a href="/admin/itemgallery/<?=$aItem['id']?>/">Галерея</a></td>
		<td><a href="/admin/itemcolors/<?=$aItem['id']?>/">Цвета</a></td>
		<td><a href="/admin/items/<?=$this->catinfo['id']?>/copy/<?=$aItem['id']?>/"><img src="/static/images/admin/copy.gif" alt="Копировать" title="Копировать" /></a></td>
		<td><a href="/admin/items/<?=$this->catinfo['id']?>/edit/<?=$aItem['id']?>/"><img src="/static/images/admin/edit.gif" alt="Редактировать" title="Редактировать" /></a></td>
		<td><a href="javascript:void(0)" onclick="withrequest('Удалить?','/admin/items/<?=$this->catinfo['id']?>/delete/<?=$aItem['id']?>/')"><img src="/static/images/admin/delete.gif" alt="Удалить" title="Удалить" /></a></td>
		<td><a href="/admin/items/<?=$this->catinfo['id']?>/switch/<?=$aItem['id']?>/"><img src="/static/images/admin/<?=$aItem['enabled']?'show':'hide'?>.gif" alt="<?=$aItem['enabled']?'Вкл':'Выкл'?>" title="<?=$aItem['enabled']?'Вкл':'Выкл'?>" /></a></td>
	</tr>
<?endforeach;?>
	</tbody>
<?if (empty($_GET['articul'])):?>
	<tfoot>
	<tr>
		<td colspan="8"><input class="button" type="submit" value="Сохранить порядок"></td>
	</tr>
	</tfoot>
<?endif;?>
<?endif;?>
</table>
</form>

<script>
var cache = {};
$(document).ready(function () {
<?if (empty($_GET['articul'])):?>
	$('.sortable').sortable();
<?endif;?>
	$('input[type="button"]').on('click', function () {
		var articul = $('input[name="articul"]').val();
		if (articul) {
			document.location.href = '?articul=' + articul;
		}
		return false;
	});
	$('input[name="articul"]').autocomplete({
		source: function(request, response) {
			var term = request.term;
			if (term in cache) {
				response(cache[term]);
				return;
			}
			request.cat_id = <?=$this->catinfo['id']?>;
			var lastXhr = $.getJSON('/admin/items/articulautocomplete/', request, function(data, status, xhr) {
				cache[term] = data;
				if (xhr === lastXhr) {
					response(data);
				}
			});
		},
		select: function(event, ui) {
			if (ui.item) {
				document.location.href = '/admin/items/' + ui.item.cat_id + '/edit/' + ui.item.id + '/';
			}
		}
	});
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
