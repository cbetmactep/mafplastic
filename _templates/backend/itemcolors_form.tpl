<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST" enctype="multipart/form-data">
<table cellspacing="1" class="content_table" width="800">
	<tr>
		<th colspan="2">
			<?=$this->title?> цвета товара &quot;<?=$this->iteminfo['title']?>&quot;
			<a class="flr" href="/admin/itemcolors/<?=$this->iteminfo['id']?>/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td nowrap>Цвет</td>
		<td>
			<select name="color">
				<?foreach (CItemColors::$aColors as $iID => $sTitle):?>
				<option value="<?=$iID?>"<?if ($this->values['color'] == $iID):?> selected<?endif?>><?=$sTitle?></option>
				<?endforeach?>
			</select>
			<?=error($this->errors['color'])?>
		</td>
	</tr>
	<tr>
		<td nowrap>Артикул</td>
		<td>
			<input type="text" name="articul" value="<?=escape($this->values['articul'])?>">
			<?=error($this->errors['articul'])?>
		</td>
	</tr>
	<tr>
		<td nowrap>Остатки</td>
		<td>
			<input type="text" name="remains" value="<?=escape($this->values['remains'])?>">
			<?=error($this->errors['remains'])?>
		</td>
	</tr>
	<tr>
		<td>Включено</td>
		<td>
			<input class="checkbox_width" type="checkbox" name="enabled" value="1"<?if ($this->values['enabled']):?> checked<?endif;?>>
			<?=error($this->errors['enabled'])?>
		</td>
	</tr>
	<tr>
		<td nowrap>Наценка за цвет</td>
		<td>
			<select name="add_price">
<?foreach (CItemColors::$aAddPrices as $iID => $sTitle):?>
				<option value="<?=$iID?>"<?if ($this->values['add_price'] == $iID):?> selected<?endif;?>><?=$sTitle?></option>
<?endforeach?>
			</select>
			<?=error($this->errors['add_price'])?>
		</td>
	</tr>
	<tr>
		<td>Изображение (200x200), авторесайз</td>
		<td width="100%">
			<?if ($this->values['id'] && file_exists(CItemColors::GetLink($this->values['id'], 1, true))):?>
			<img src="<?=CItemColors::GetLink($this->values['id'], 1, false) ?>" alt=""><br>
			<?endif?>
			<input type="file" name="image">
			<?=error($this->errors['image'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="button" value="<?=$this->button?>"></td>
	</tr>
</table>
</form>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
