<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST" enctype="multipart/form-data">
<table cellspacing="1" class="content_table" width="800">
	<tr>
		<th colspan="2">
			<?=$this->title?> статичной страницы
			<a class="flr" href="/admin/pages/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td>URL</td>
		<td width="100%">
			<input type="text" name="path" value="<?=escape($this->values['path'])?>">
			<?=error($this->errors['path'])?>
		</td>
	</tr>
	<tr>
		<td>Заголовок</td>
		<td>
			<input type="text" name="title" value="<?=escape($this->values['title'])?>">
			<?=error($this->errors['title'])?>
		</td>
	</tr>
	<tr>
		<td>Включена</td>
		<td>
			<input class="checkbox_width" type="checkbox" name="enabled" value="1"<?if ($this->values['enabled']):?> checked<?endif;?>>
			<?=error($this->errors['enabled'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2">Текст</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea name="body" rows="20"><?=$this->values['body']?></textarea>
			<?=error($this->errors['body'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2">Заголовок (тэг Title) (<span id="title_counter"></span>)</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea name="meta_title" id="title" rows="3"><?=$this->values['meta_title']?></textarea>
			<?=error($this->errors['meta_title'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2">Описание (тэг Description) (<span id="description_counter"></span>)</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea name="meta_description" id="description" rows="3"><?=$this->values['meta_description']?></textarea>
			<?=error($this->errors['meta_description'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2">Ключевые слова (тэг Keywords) (<span id="keywords_counter"></span>)</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea name="meta_keywords" id="keywords" rows="3"><?=$this->values['meta_keywords']?></textarea>
			<?=error($this->errors['meta_keywords'])?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="button" value="<?=$this->button?>"></td>
	</tr>
</table>
</form>

<script type="text/javascript">
init_tinymce('body');
$(document).ready(function () {
	init_count_chars('title');
	init_count_chars('description');
	init_count_chars('keywords');
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
