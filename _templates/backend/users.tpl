<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<table cellspacing="1" width="800" class="content_table">
	<form method="GET">
	<tr>
		<td colspan="5">
			EMail: <input type="text" class="checkbox_width" name="email" value="<?=$_GET['email']?>">
			<input type="submit" class="button" value="Искать">
		</td>
	</tr>
	</form>
	<tr>
		<td colspan="5"><a href="/admin/users/add/">Добавить</a></td>
	</tr>
<?if ($this->pagination):?>
	<tr>
		<td colspan="5"><?=$this->pagination?></td>
	</tr>
<?endif;?>
<?if ($this->users):?>
<?foreach ($this->users as $aUser):?>
	<tr>
		<td width="100%">
			<?if ($aUser['type'] == CUsers::TYPE_PRIVATE):?>
			<?=$aUser['last_name']?> <?=$aUser['first_name']?> <?=$aUser['middle_name']?>
			<?else:?>
			<?=$aUser['company_name']?>
			<?endif?>
		</td>
		<td><?=$aUser['email']?> <?if ($aUser['name']):?>(<?=$aUser['name']?>)<?endif;?></td>
		<td><a href="/admin/users/edit/<?=$aUser['id']?>/"><img src="/static/images/admin/edit.gif" alt="Редактировать" title="Редактировать"/></a></td>
		<td><a href="javascript:void(0)" onclick="withrequest('Удалить?','/admin/users/delete/<?=$aUser['id']?>/')"><img src="/static/images/admin/delete.gif" alt="Удалить" title="Удалить"/></a></td>
	</tr>
<? endforeach;?>
<?if ($this->pagination):?>
	<tr>
		<td colspan="5"><?=$this->pagination?></td>
	</tr>
<?endif;?>
	<tr>
		<td colspan="5"><a href="/admin/users/add/">Добавить</a></td>
	</tr>
<?endif;?>
</table>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
