<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="post">
<table cellspacing="1" width="800" class="content_table">
	<thead>
	<tr>
		<th colspan="8">
			Товары типа &quot;<?=$this->typeinfo?>&quot;
			<a class="flr" href="/admin/constructoritems/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td colspan="8"><a href="/admin/constructoritems/<?=$this->type?>/add/">Добавить</a></td>
	</tr>
	</thead>
<?if ($this->constructoritems):?>
	<tbody class="sortable">
<?foreach ($this->constructoritems as $aItem):?>
	<tr>
		<td>
			<input type="hidden" name="ids[]" value="<?=$aItem['id']?>">
			<img src="/static/images/admin/hand.gif" alt="">
		</td>
		<td width="100%">
<?if ($this->type == CConstructorItems::TYPE_CONNECTION): ?>
			<strong><?=CConstructorItems::$aSubTypes[$aItem['subtype']]?></strong><br>
<?endif?>
<?for ($c = 1; $c <= 9; $c++):?>
<?if ($aItem['item_'.$c.'_id']):?>
			<?=$this->items[$aItem['item_'.$c.'_id']]['title']?>
<?if ($aItem['item_'.$c.'_errors']):?>
			<br/><span class="error"><?=join(', ', $aItem['item_'.$c.'_errors'])?></span>
<?endif?>
			<br/>
<?endif?>
<?endfor?>
		</td>
		<td>
			<?if (file_exists(CConstructorItems::GetLink($aItem['id'], 2, true))):?>
			<img src="<?=CConstructorItems::GetLink($aItem['id'], 2, false)?>" width="30" alt=""><br>
			<?endif;?>
		</td>
		<td><a href="/admin/constructoritems/<?=$this->type?>/edit/<?=$aItem['id']?>/"><img src="/static/images/admin/edit.gif" alt="Редактировать" title="Редактировать" /></a></td>
		<td><a href="javascript:void(0)" onclick="withrequest('Удалить?','/admin/constructoritems/<?=$this->type?>/delete/<?=$aItem['id']?>/')"><img src="/static/images/admin/delete.gif" alt="Удалить" title="Удалить" /></a></td>
		<td><a href="/admin/constructoritems/<?=$this->type?>/switch/<?=$aItem['id']?>/"><img src="/static/images/admin/<?=$aItem['enabled']?'show':'hide'?>.gif" alt="<?=$aItem['enabled']?'Вкл':'Выкл'?>" title="<?=$aItem['enabled']?'Вкл':'Выкл'?>" /></a></td>
	</tr>
<?endforeach;?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="8"><input class="button" type="submit" value="Сохранить порядок"></td>
	</tr>
	</tfoot>
<?endif;?>
</table>
</form>

<script>
$(document).ready(function () {
	$('.sortable').sortable();
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
