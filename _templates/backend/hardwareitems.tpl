<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="post">
<table cellspacing="1" width="800" class="content_table">
	<thead>
	<tr>
		<th colspan="8">
			Метизы типа &quot;<?=$this->typeinfo?>&quot;
			<a class="flr" href="/admin/hardwareitems/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td colspan="8"><a href="/admin/hardwareitems/<?=$this->type?>/add/">Добавить</a></td>
	</tr>
	</thead>
<?if ($this->hardwareitems):?>
	<tbody>
<?foreach ($this->hardwareitems as $aItem):?>
	<tr>
		<td nowrap><?=$aItem['articul']?></td>
		<td>M<?=$aItem['thread']?></td>
		<td width="100%"><?=$aItem['size']?></td>
		<td><a href="/admin/hardwareitems/<?=$this->type?>/edit/<?=$aItem['id']?>/"><img src="/static/images/admin/edit.gif" alt="Редактировать" title="Редактировать" /></a></td>
		<td><a href="javascript:void(0)" onclick="withrequest('Удалить?','/admin/hardwareitems/<?=$this->type?>/delete/<?=$aItem['id']?>/')"><img src="/static/images/admin/delete.gif" alt="Удалить" title="Удалить" /></a></td>
		<td><a href="/admin/hardwareitems/<?=$this->type?>/switch/<?=$aItem['id']?>/"><img src="/static/images/admin/<?=$aItem['enabled']?'show':'hide'?>.gif" alt="<?=$aItem['enabled']?'Вкл':'Выкл'?>" title="<?=$aItem['enabled']?'Вкл':'Выкл'?>" /></a></td>
	</tr>
<?endforeach;?>
	</tbody>
<?endif;?>
</table>
</form>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
