<?=$this->SetController('adm_index')->SetAction('header')->Render();?>

<form method="POST" enctype="multipart/form-data">
<table cellspacing="1" class="content_table" width="800">
	<tr>
		<th colspan="2">
			<?=$this->title?> галереи
			<a class="flr" href="/admin/gallery/">Вернуться</a>
		</th>
	</tr>
	<tr>
		<td nowrap>Заголовок</td>
		<td width="100%">
			<input type="text" name="title" value="<?=escape($this->values['title'])?>">
			<?=error($this->errors['title'])?>
		</td>
	</tr>
	<tr>
		<td>Включено</td>
		<td>
			<input class="checkbox_width" type="checkbox" name="enabled" value="1"<?if ($this->values['enabled']):?> checked<?endif;?>>
			<?=error($this->errors['enabled'])?>
		</td>
	</tr>
	<tr>
		<td>Изображение</td>
		<td>
			<? if ($this->values['id'] && file_exists(CGallery::GetLink($this->values['id'], 2, true))): ?>
			<a href="<?=CGallery::GetLink($this->values['id'], 1, false) ?>" class="fancybox"><img src="<?=CGallery::GetLink($this->values['id'], 2, false) ?>" alt=""></a><br>
			<? endif; ?>
			<input type="file" name="image">
			<p>Размер (1 модуль - 110 пикселей, всего 10 модулей по ширине):</p>
			<p>
				Ширина:
				<select name="image_width" class="small_width">
					<?for ($c = 1; $c <= 10; $c++):?>
					<option value="<?=$c?>"<?if ($this->values['image_width'] == $c):?> selected<?endif?>><?=$c?></option>
					<?endfor?>
				</select>
				Высота:
				<select name="image_height" class="small_width">
					<?for ($c = 1; $c <= 5; $c++):?>
					<option value="<?=$c?>"<?if ($this->values['image_height'] == $c):?> selected<?endif?>><?=$c?></option>
					<?endfor?>
				</select>
			</p>
			<?=error($this->errors['image'])?>
		</td>
	</tr>
	<tr>
		<td nowrap>Адрес видео</td>
		<td width="100%">
			<input type="text" name="video" value="<?=escape($this->values['video'])?>">
			<?=error($this->errors['video'])?>
		</td>
	</tr>
	<tr>
		<td>Товары</td>
		<td>
			<div class="sortable">
			<div style="display: none" id="items_selector">
				<img src="/static/images/admin/hand.gif" alt="" />
				<select class="medium_width">
					<?if ($this->items) foreach ($this->items as $aItem):?>
					<option value="<?=$aItem['id'] ?>"><?=$aItem['title'] ?></option>
					<? endforeach; ?>
				</select>
				<a href="javascript:void(0)" onclick="$(this).parent().remove()">X</a>
			</div>
			</div>
			<a href="javascript:void(0)" onclick="add_items_selector(0)">Добавить</a>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="button" value="<?=$this->button?>"></td>
	</tr>
</table>
</form>

<script type="text/javascript">
$(document).ready(function () {
	$('.sortable').sortable();
<?if ($this->values['items']) foreach ($this->values['items'] as $iItem):?>
	add_items_selector(<?=$iItem ?>);
<?endforeach;?>
});
</script>

<?=$this->SetController('adm_index')->SetAction('footer')->Render();?>
