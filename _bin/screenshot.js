var system = require('system');
var webpage = require('webpage');

window.setTimeout(function(){
	phantom.exit();
}, 10000);

if (system.args.length === 3) {
	var page = webpage.create();
	page.addCookie({
		name: 'PHPSESSID',
		value: encodeURIComponent(system.args[1]),
		domain: 'mafplastic.ru',
		path: '/',
		httponly: false,
		secure: false,
		expires: (new Date()).getTime() + (5000 * 60 * 60),
		implicitDomain: true
	});

	page.open('http://mafplastic.ru/constructor/createimage/', function(status) {
		console.log('Loaded url');
		if (status === 'success') {
			window.setTimeout(function () {
				console.log('Saved image');
				page.render(system.args[2]);
				phantom.exit();
			}, 200);
		} else {
			phantom.exit();
		}
	});
} else {
	phantom.exit();
}
